package in.wringg.dao;

import in.wringg.entity.AddressBook;
import in.wringg.entity.AgentCallingDetail;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentDetailTime;
import in.wringg.entity.AgentLunchDetail;
import in.wringg.entity.CallSchedulerBase;
import in.wringg.entity.SmeCustomerRemarks;
import in.wringg.pojo.AgentDetailBean;
import in.wringg.pojo.CallBaseBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface AgentProfileDao {
  boolean canCreateAgent(Long paramLong);
  
  ServerResponse createAgent(AgentDetailBean paramAgentDetailBean) throws IOException;
  
  ServerResponse updateAgentDeatil(AgentDetailBean paramAgentDetailBean, Long paramLong) throws IOException;
  
  void deleteSingleAgent(Long paramLong);
  
  List<AgentDetail> fetchAgentDetailList(Long paramLong);
  
  AgentDetail fetchSingleAgent(Long paramLong);
  
  AgentDetailTime fetchSingleAgentT(Long paramLong);
  
  AgentDetail fetchSingleAgentByEmail(String paramString);
  
  List<CallSchedulerBase> getBaseForAgent(Long paramLong);
  
  List<AgentCallingDetail> getAgentCallingDetail(StartEndDateRequest paramStartEndDateRequest);
  
  void lunchInEntry(Long paramLong1, Long paramLong2, Date paramDate, String paramString);
  
  AgentLunchDetail getLastInEntry(Long paramLong);
  
  List<AgentLunchDetail> getSameDayBrkEntry(Long paramLong);
  
  AgentDetailTime getTimeForToday(Long paramLong);
  
  void saveAddressBook(AddressBook paramAddressBook);
  
  AddressBook getAddressBookBySmeIdAndMobile(Long paramLong, String paramString);
  
  AddressBook getAddressBookById(Long paramLong);
  
  List<AddressBook> getAddressBookBySmeId(Long paramLong);
  
  List<AddressBook> getAddressBookByAgentId(Long paramLong);
  
  void saveSmeCustomerRemarks(SmeCustomerRemarks paramSmeCustomerRemarks);
  
  SmeCustomerRemarks getSmeCustomerRemarks(Long paramLong);
  
  List<SmeCustomerRemarks> getRemaksByAgentId(Long paramLong);
  
  List<SmeCustomerRemarks> getRemaksBySmeId(Long paramLong);
  
  List<SmeCustomerRemarks> getRemaksByAddressBookId(Long paramLong);
  
  void saveCallSheduleBase(CallBaseBean paramCallBaseBean, Long paramLong);
  
  Long callSchBaseCounterOnCallType(Long paramLong, Integer paramInteger);
  
  CallSchedulerBase findById(Long paramLong);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\AgentProfileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */