package in.wringg.dao;

import in.wringg.entity.EdvaantageDemoRequest;
import in.wringg.entity.Longcode;
import in.wringg.entity.Users;
import in.wringg.pojo.AdminDashboardRequest;
import in.wringg.pojo.AdminDashboardRevenue;
import in.wringg.pojo.AdminDashboardSystemDetail;
import in.wringg.pojo.AdminMaxAgents;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.UserRoleDetailBean;
import in.wringg.pojo.ZoneBean;
import java.text.ParseException;
import java.util.List;

public interface AdminDao {
  Users getUserInfo(String paramString);
  
  Users getUserInfoForLogin(String paramString);
  
  UserRoleDetailBean roleDeatail(String paramString);
  
  List<AdminDashboardRevenue> dashBoardRevenue(AdminDashboardRequest paramAdminDashboardRequest);
  
  AdminDashboardSystemDetail dashSystemSnapshot(AdminDashboardRequest paramAdminDashboardRequest) throws ParseException;
  
  List<ZoneBean> getZone();
  
  void saveLongCode(Longcode paramLongcode);
  
  Longcode fetchSingeLongCode(Long paramLong);
  
  List<Longcode> getLongCode(Byte paramByte);
  
  AdminMaxAgents getMaxAgent();
  
  boolean login(LoginBean paramLoginBean);
  
  void saveDemoRequest(EdvaantageDemoRequest paramEdvaantageDemoRequest);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\AdminDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */