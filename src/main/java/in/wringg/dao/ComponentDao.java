package in.wringg.dao;

import in.wringg.pojo.CallCdrBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;

public interface ComponentDao {
  void addCdrDetails(CallCdrBean paramCallCdrBean);
  
  void changeSchuduleStatus(Long paramLong);
  
  ServerResponse chargingDetails(StartEndDateRequest paramStartEndDateRequest, Long paramLong);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\ComponentDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */