package in.wringg.dao;

import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentReportDetail;
import in.wringg.entity.AlertNotification;
import in.wringg.entity.CallBaseHistory;
import in.wringg.entity.IncomingIvrCallCdr;
import in.wringg.entity.MpbxCallRecording;
import in.wringg.entity.OutbondIvrCampaign;
import in.wringg.entity.OutbondIvrCdr;
import in.wringg.entity.SmeComplaint;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.SmePrompts;
import in.wringg.entity.Users;
import in.wringg.entity.VoicemailCallCdr;
import in.wringg.pojo.AddBulkSmsBean;
import in.wringg.pojo.AddOutBoundIvrBean;
import in.wringg.pojo.AgentGroupDetailBean;
import in.wringg.pojo.CallCdrResponseBean;
import in.wringg.pojo.IvrCallFlowBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.MissCallMarketingBean;
import in.wringg.pojo.PassswordTokenBean;
import in.wringg.pojo.PromptDetailsBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SmeAgentDetailBean;
import in.wringg.pojo.SmeComplaintBean;
import in.wringg.pojo.SmeDashboardSystemDetail;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UploadedBaseDesc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;

public interface SmeDao {
	ServerResponse resetPassword(LoginBean paramLoginBean);

	ServerResponse genForgotToken(String paramString) throws ParseException, IOException;

	ServerResponse forgetPassword(String paramString1, String paramString2) throws ParseException;

	ServerResponse newPassword(String paramString, PassswordTokenBean paramPassswordTokenBean) throws ParseException;

	List<AgentGroupDetailBean> getAgentGroupDetail(Long paramLong);

	SmeDashboardSystemDetail smeDashboardDetail(StartEndDateRequest paramStartEndDateRequest, Long paramLong);

	SmeAgentDetailBean smeDashboadForAgent(Long paramLong);

	void addSmsCampaign(AddBulkSmsBean paramAddBulkSmsBean, Long paramLong) throws IOException;

	void updateSmsCampaign(AddBulkSmsBean paramAddBulkSmsBean, Long paramLong1, Long paramLong2);

	void addIvrManager(List<IvrCallFlowBean> paramList, String paramString) throws IOException;

	List<IvrCallFlowBean> getIvrManger(String paramString);

	boolean isIvrManger(String paramString);

	List<AddBulkSmsBean> getBulkSmslist(String paramString);

	void deleteBulkSmsList(String paramString, Long paramLong);

	AddBulkSmsBean fetchBulkSmsById(String paramString, Long paramLong);

	List<MissCallMarketingBean> getMissCallList(StartEndDateRequest paramStartEndDateRequest, long paramLong);

	void addSmeComplaint(SmeComplaintBean paramSmeComplaintBean, Long paramLong);

	ServerResponse updateSmeComplaint(SmeComplaintBean paramSmeComplaintBean, Long paramLong);

	ServerResponse deleteSmeComplaint(Long paramLong1, Long paramLong2);

	List<SmeComplaint> getSmeComplaints(Long paramLong);

	SmeComplaint fetchSmeComplaintById(Long paramLong);

	Boolean addOutboundIvrCampaign(AddOutBoundIvrBean paramAddOutBoundIvrBean);

	Boolean updateOutboundIvrCampaign(AddOutBoundIvrBean paramAddOutBoundIvrBean, BigInteger paramBigInteger);

	Boolean deleteOutboundIvrCampaign(BigInteger paramBigInteger);

	OutbondIvrCampaign findOutboundCampaignByID(BigInteger paramBigInteger);

	List<AddOutBoundIvrBean> getOutboundCampaignlit();

	List<CallCdrResponseBean> getCDRListForService(ManageClientPageRequest paramManageClientPageRequest, Long paramLong,
			Byte paramByte) throws FileNotFoundException, IOException;

	void deleteIvrManger(String paramString, Session paramSession);

	List<AgentReportDetail> fetchAgentReportDetail(Long paramLong,
			ManageClientPageRequest paramManageClientPageRequest);

	Long getAgetntReportCount(Long paramLong, ManageClientPageRequest paramManageClientPageRequest);

	Long getCdrListCount(ManageClientPageRequest paramManageClientPageRequest, Long paramLong, Byte paramByte);

	String genricFilterAppend(ManageClientPageRequest paramManageClientPageRequest, String paramString);

	List<AgentDetail> agentDashboardSummary(Long paramLong, Byte paramByte);

	List<AgentDetail> agentDashboardSummary(Long paramLong, List<Byte> paramList);

	List<AlertNotification> alertNotificationList(String paramString, int paramInt);

	void updateAlertNotification(AlertNotification paramAlertNotification);

	AlertNotification getAlertNotification(int paramInt);

	void deleteAlertNotification(AlertNotification paramAlertNotification);

	MpbxCallRecording recordingFileList(String paramString);

	CallCdrResponseBean fetchOutLeg(Long paramLong, String paramString, byte paramByte)
			throws FileNotFoundException, IOException;

	Users fetchUser(String paramString);

	SmeProfile getSmeProfile(long paramLong);

	void saveBase(List<UploadedBaseDesc> paramList, long paramLong);

	void saveCallBaseHistory(CallBaseHistory paramCallBaseHistory);

	List<CallBaseHistory> getBaseHistory(long paramLong);

	List<IncomingIvrCallCdr> getCDRLInForAgent(long paramLong);

	List<OutbondIvrCdr> getCDRLOutForAgent(long paramLong);

	List<VoicemailCallCdr> getCDRVoiceForAgent(long paramLong);

	List<CallCdrResponseBean> getCDRLForAgent(long paramLong, byte paramByte) throws FileNotFoundException, IOException;

	List<CallCdrResponseBean> getNotAnsweredMissCall(Set<String> paramSet) throws FileNotFoundException, IOException;

	List<AgentReportDetail> fetchAgentReportDetail(long paramLong);

	void saveSmePrompt(PromptDetailsBean promptDetails);

	List<SmePrompts> getSmePrompt(Long id, Long promptId,int pageNum,int pageSize);
}

/*
 * Location: D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\SmeDao.class
 * Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */