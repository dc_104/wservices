package in.wringg.dao;

import in.wringg.entity.SmeProfile;
import in.wringg.pojo.AdminSmeProfileBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import java.text.ParseException;
import java.util.List;

public interface SmeProfileDao {
  ServerResponse addSmeProfile(AdminSmeProfileBean paramAdminSmeProfileBean) throws Exception;
  
  ServerResponse deleteSmeProfile(Long paramLong);
  
  List<SmeProfile> findAllSMEs(StartEndDateRequest paramStartEndDateRequest);
  
  ServerResponse updateSmeProfile(AdminSmeProfileBean paramAdminSmeProfileBean) throws ParseException;
  
  SmeProfile findByID(Long paramLong);
  
  List<SmeProfile> findAllSMEs(String paramString, Integer paramInteger1, Integer paramInteger2);
  
  Long getRowCount(String paramString);
  
  String createQueryForSME(ManageClientPageRequest paramManageClientPageRequest);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\SmeProfileDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */