package in.wringg.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;






public abstract class AbstractDao<PK extends Serializable, T>
{
/* 17 */   private final Class<T> persistentClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[1];
  
  @Autowired
  public SessionFactory sessionFactory;

  
  protected Session getSession() {
/* 24 */     return this.sessionFactory.getCurrentSession();
  }
  
  public T getByKey(PK key) {
/* 28 */     return (T)getSession().get(this.persistentClass, (Serializable)key);
  }
  
  public void persist(T entity) {
/* 32 */     getSession().persist(entity);
  }
  public void delete(T entity) {
/* 35 */     getSession().delete(entity);
  }

  
  protected Criteria createEntityCriteria() {
/* 40 */     return getSession().createCriteria(this.persistentClass);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\dao\AbstractDao.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */