package in.wringg.daoImpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.wringg.dao.AbstractDao;
import in.wringg.dao.AdminDao;
import in.wringg.dao.AgentProfileDao;
import in.wringg.dao.SmeProfileDao;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentGroupDetail;
import in.wringg.entity.EdvaantageDemoRequest;
import in.wringg.entity.Longcode;
import in.wringg.entity.MpbxChargingSucc;
import in.wringg.entity.RevenueDetail;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.SystemSnapshot;
import in.wringg.entity.UserRole;
import in.wringg.entity.Users;
import in.wringg.entity.Zone;
import in.wringg.pojo.AdminAgentDetailBean;
import in.wringg.pojo.AdminDashboardRequest;
import in.wringg.pojo.AdminDashboardRevenue;
import in.wringg.pojo.AdminDashboardSystemDetail;
import in.wringg.pojo.AdminMaxAgents;
import in.wringg.pojo.AdminSmeDetailBean;
import in.wringg.pojo.DashboardCallDeatialBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.UserRoleDetailBean;
import in.wringg.pojo.ZoneBean;
import in.wringg.utility.Constants;
import in.wringg.utility.Utility;

@Transactional
@Repository("HibernateRepo")
public class AdminDaoImpl<T> extends AbstractDao<Serializable, Void> implements AdminDao {
	@Autowired
	private AgentProfileDao agentProfileDao;
	@Autowired
	SmeProfileDao profileDao;
	private Utility.getServiceName serviceName;

	public UserRoleDetailBean roleDeatail(String id) {
		/* 59 */ UserRoleDetailBean userDetailBean = new UserRoleDetailBean();

		/* 61 */ Criteria crit = getSession().createCriteria(Users.class);
		/* 62 */ crit.add((Criterion) Restrictions.eq("username", id));

		/* 64 */ Users user = (Users) crit.uniqueResult();
		/* 65 */ Set<UserRole> roles = user.getUserRole();
		/* 66 */ userDetailBean.setUserName(id);
		/* 67 */ userDetailBean.setRoles(((UserRole) roles.iterator().next()).getRole().toString());
		/* 68 */ if ("Client".equalsIgnoreCase(userDetailBean.getRoles())) {
			/* 69 */ SmeProfile profile = this.profileDao
					.findByID(Long.valueOf(Long.parseLong(userDetailBean.getUserName())));
			/* 70 */ if (profile != null) {
				userDetailBean.getProfileDetail().setAllowedAgents(profile.getAllowedAgents());
				/* 72 */ userDetailBean.getProfileDetail().setEmailId(profile.getEmailId());
				/* 73 */ userDetailBean.getProfileDetail().setAlternateNumber(profile.getAlternateNumber());
				/* 74 */ userDetailBean.getProfileDetail().setId(profile.getId().longValue());
				/* 75 */ userDetailBean.getProfileDetail().setSmeName(profile.getName());
				/* 76 */ userDetailBean.getProfileDetail().setZoneId(profile.getZoneId().getId().longValue());
				/* 77 */ userDetailBean.getProfileDetail().setLongCodeId(profile.getLongcodeId().getId().longValue());
				/* 78 */ userDetailBean.getProfileDetail().setStatus(profile.getStatus());
				/* 79 */ userDetailBean.getProfileDetail().setServiceFlag(profile.getServiceFlag());
				/* 80 */ userDetailBean.getProfileDetail().setSmeMobile(profile.getSmeMobile());
				/* 81 */ userDetailBean.getProfileDetail().setStatus(profile.getStatus());
				/* 82 */ userDetailBean.getProfileDetail().setBalance(profile.getBalance());
				/* 83 */ userDetailBean.getProfileDetail().setRecording(profile.getRecording());
				/* 84 */ userDetailBean.getProfileDetail().setMasking(profile.getMasking());
				/* 85 */ userDetailBean.getProfileDetail().setSelectionAlgo(profile.getSelectionAlgo());
				/* 86 */ userDetailBean.getProfileDetail().setBizAddress(profile.getBizAddress());
				/* 87 */ userDetailBean.getProfileDetail().setLongCode(profile.getLongcodeId().getLongcode());
				/* 88 */ userDetailBean.getProfileDetail().setInShortCode(profile.getLongcodeId().getInShortCode());
				/* 89 */ userDetailBean.getProfileDetail().setOutShortCode(profile.getLongcodeId().getOutShortCode());
				/* 90 */ userDetailBean.getProfileDetail().setRecValidity(profile.getRecValidity());
				/* 91 */ userDetailBean.getProfileDetail().setUserToken(user.getUserToken());
				/* 92 */ userDetailBean.getProfileDetail().setAccountSid(profile.getAccountSid());
				/* 93 */ userDetailBean.getProfileDetail().setQueueLimit(profile.getQueueLimit());
				userDetailBean.getProfileDetail().setCallBackUrl(profile.getCallBackUrl());
				userDetailBean.getProfileDetail().setStickyAlgo(profile.getStickyAlgo());
				userDetailBean.getProfileDetail().setInPermissionFlag(profile.getInPermissionFlag());
				userDetailBean.getProfileDetail().setOutPermissionFlag(profile.getOutPermissionFlag());
				userDetailBean.setInPermissionFlag(profile.getInPermissionFlag());
				userDetailBean.setOutPermissionFlag(profile.getOutPermissionFlag());
			}

		}
		/* 99 */ else if ("Agent".equalsIgnoreCase(userDetailBean.getRoles())) {
			/* 100 */ userDetailBean.getProfileDetail().setId(0L);
			/* 101 */ userDetailBean.getProfileDetail().setSmeName(id);
			/* 102 */ AgentDetail obj = this.agentProfileDao.fetchSingleAgentByEmail(id);
			/* 103 */ if (obj != null) {
				/* 104 */ List<Long> groups = new ArrayList<>();
				/* 105 */ for (AgentGroupDetail group : obj.getGroups()) {
					/* 106 */ groups.add(group.getGroupId());
				}
				/* 108 */ userDetailBean.getProfileDetail().setAgentMasking(obj.getAgentMasking());
				/* 109 */ userDetailBean.getProfileDetail().setStickyAgent(obj.getStickyAgent());
				/* 110 */ userDetailBean.getProfileDetail().setStickyDays(obj.getStickyDays());
				/* 111 */ userDetailBean.getProfileDetail().setAgentScore(obj.getAgentScore());
				/* 112 */ userDetailBean.getProfileDetail().setAgentGroups(groups);
				/* 113 */ userDetailBean.getProfileDetail().setAgentEmail(obj.getAgentEmail());
				/* 114 */ userDetailBean.getProfileDetail().setAgentExtention(obj.getAgentExtention());
				/* 115 */ userDetailBean.getProfileDetail().setAgentId(obj.getAgentId());
				/* 116 */ userDetailBean.getProfileDetail().setAgentMobile(obj.getAgentMobile());
				/* 117 */ userDetailBean.getProfileDetail().setAgentName(obj.getAgentName());
				/* 118 */ userDetailBean.getProfileDetail().setInsertTime(obj.getInsertTime());
				/* 119 */ userDetailBean.getProfileDetail().setInTime(obj.getInTime());
				/* 120 */ userDetailBean.getProfileDetail().setOutTime(obj.getOutTime());
				/* 121 */ userDetailBean.getProfileDetail().setSmeId(obj.getSmeId());
				/* 122 */ userDetailBean.getProfileDetail().setStatus(obj.getStatus());
				/* 123 */ userDetailBean.getProfileDetail().setDaysFlag(obj.getDaysFlag());
				/* 124 */ userDetailBean.getProfileDetail().setInDate(/* 125 */ Utility
						.getFormattedDateTime(Utility.timeToDate(obj.getInTime()), "yyyy-MM-dd HH:mm:ss"));
				/* 126 */ userDetailBean.getProfileDetail().setOutDate(/* 127 */ Utility
						.getFormattedDateTime(Utility.timeToDate(obj.getOutTime()), "yyyy-MM-dd HH:mm:ss"));
				/* 128 */ userDetailBean.getProfileDetail().setStatus(obj.getStatus());
				/* 129 */ userDetailBean.getProfileDetail().setAssignVoicemailCalls(obj.getAssignVoicemailCalls());
				/* 130 */ userDetailBean.getProfileDetail().setAssignFailedCalls(obj.getAssignFailedCalls());
				/* 131 */ SmeProfile profile = this.profileDao.findByID(userDetailBean.getProfileDetail().getSmeId());
				/* 132 */ userDetailBean.getProfileDetail().setAccountSid(profile.getAccountSid());
				/* 133 */ userDetailBean.getProfileDetail().setLongCodeId(profile.getLongcodeId().getId().longValue());
				/* 134 */ userDetailBean.getProfileDetail().setLongCode(profile.getLongcodeId().getLongcode());
				/* 135 */ userDetailBean.getProfileDetail().setInShortCode(profile.getLongcodeId().getInShortCode());
				/* 136 */ userDetailBean.getProfileDetail().setOutShortCode(profile.getLongcodeId().getOutShortCode());
				userDetailBean.getProfileDetail().setInPermissionFlag(profile.getInPermissionFlag());
				userDetailBean.getProfileDetail().setOutPermissionFlag(profile.getOutPermissionFlag());
				userDetailBean.setInPermissionFlag(obj.getInPermissionFlag());
				userDetailBean.setOutPermissionFlag(obj.getOutPermissionFlag());
				
			}
		} else {

			/* 140 */ userDetailBean.getProfileDetail().setId(0L);
			/* 141 */ userDetailBean.getProfileDetail().setSmeName(id);
		}
		/* 143 */ return userDetailBean;
	}

	public Users getUserInfo(String user) {
		/* 148 */ Criteria crit = getSession().createCriteria(Users.class);
		/* 149 */ crit.add((Criterion) Restrictions.eq("username", user));
		/* 150 */ crit.setMaxResults(1);
		/* 151 */ return (Users) crit.uniqueResult();
	}

	public Users getUserInfoForLogin(String user) {
		/* 155 */ Criteria crit = getSession().createCriteria(Users.class);
		/* 156 */ crit.add((Criterion) Restrictions.eq("username", user));
		/* 157 */ crit.add((Criterion) Restrictions.eq("enabled", Constants.BYTE_1));
		/* 158 */ crit.setMaxResults(1);
		/* 159 */ return (Users) crit.uniqueResult();
	}

	public List<AdminDashboardRevenue> dashBoardRevenue(AdminDashboardRequest revenueRequest) {
		/* 163 */ Map<Integer, Double> revenueMap = new HashMap<>();
		/* 164 */ for (Utility.getServiceName serviceCode : Utility.getServiceName.values()) {
			/* 165 */ revenueMap.put(Integer.valueOf(serviceCode.ordinal() + 1), Double.valueOf(0.0D));
		}
		/* 167 */ Criteria criteria = getSession().createCriteria(RevenueDetail.class);
		/* 168 */ criteria.setProjection(
				(Projection) Projections.projectionList().add(Projections.groupProperty("serviceId").as("serviceId"))
						/* 169 */ .add((Projection) Projections.sum("revenue")));

		/* 171 */ if (revenueRequest != null && revenueRequest.getStartDate() != null
				&& revenueRequest.getEndDate() != null) {
			/* 172 */ criteria
					.add(Restrictions.between("dateTime", revenueRequest.getStartDate(), revenueRequest.getEndDate()));
		}
		/* 174 */ List<Object[]> list = criteria.list();
		/* 175 */ List<AdminDashboardRevenue> revenueList = new ArrayList<>();
		/* 176 */ for (Object[] o : list) {
			/* 177 */ int x = ((Integer) o[0]).intValue();

			/* 179 */ x = (x == 11) ? 1 : x;

			/* 181 */ if (revenueMap.containsKey(Integer.valueOf(x))) {
				/* 182 */ Double rev = revenueMap.get(Integer.valueOf(x));
				/* 183 */ revenueMap.put(Integer.valueOf(x),
						Double.valueOf(rev.doubleValue() + ((Double) o[1]).doubleValue()));
			}
		}
		/* 186 */ for (Integer key : revenueMap.keySet()) {
			/* 187 */ AdminDashboardRevenue adminDashboardRevenue = new AdminDashboardRevenue();
			/* 188 */ this.serviceName = Utility.getServiceName.values()[key.intValue() - 1];
			/* 189 */ adminDashboardRevenue.setName(this.serviceName.toString());
			/* 190 */ adminDashboardRevenue.setValue((int) ((Double) revenueMap.get(key)).doubleValue());
			/* 191 */ revenueList.add(adminDashboardRevenue);
		}
		/* 193 */ Criteria subs = getSession().createCriteria(MpbxChargingSucc.class);
		/* 194 */ subs.setProjection(
				(Projection) Projections.projectionList().add((Projection) Projections.sum("charegedAmt")));
		/* 195 */ if (revenueRequest != null && revenueRequest.getStartDate() != null
				&& revenueRequest.getEndDate() != null) {
			/* 196 */ subs.add(
					Restrictions.between("billingDate", revenueRequest.getStartDate(), revenueRequest.getEndDate()));
		}
		/* 198 */ Double subscription = (Double) subs.uniqueResult();
		/* 199 */ int sub = (int) ((subscription == null) ? 0.0D : subscription.doubleValue());
		/* 200 */ AdminDashboardRevenue obj = new AdminDashboardRevenue();
		/* 201 */ obj.setName("Subscription");

		/* 203 */ obj.setValue(sub / 100);
		/* 204 */ revenueList.add(obj);
		/* 205 */ return revenueList;
	}

	public AdminDashboardSystemDetail dashSystemSnapshot(AdminDashboardRequest revenueRequest) {
		/* 210 */ AdminDashboardSystemDetail res = new AdminDashboardSystemDetail();
		/* 211 */ Criteria criteria = getSession().createCriteria(SystemSnapshot.class);

		/* 215 */ criteria.addOrder(Order.desc("insertTime"));
		/* 216 */ criteria.setMaxResults(1);
		/* 217 */ SystemSnapshot system = (SystemSnapshot) criteria.uniqueResult();
		/* 218 */ if (system != null) {

			/* 220 */ AdminAgentDetailBean agentCount = new AdminAgentDetailBean();
			/* 221 */ agentCount.setCurrentAgents(system.getCurrentAgents());
			/* 222 */ agentCount.setTotalAgents(system.getMaxAgents());
			/* 223 */ agentCount.setFreeAgents(system.getMaxAgents() - system.getCurrentAgents());
			/* 224 */ res.setAgentDetail(agentCount);
			/* 225 */ AdminSmeDetailBean smeCount = new AdminSmeDetailBean();
			/* 226 */ smeCount.setCurrentSME(system.getCurrentSme());
			/* 227 */ smeCount.setTotalSME(system.getMaxSme());
			/* 228 */ smeCount.setFreeSME(system.getMaxSme() - system.getCurrentSme());
			/* 229 */ res.setSmeDetail(smeCount);
		}
		/* 231 */ Criteria crit = getSession().createCriteria(RevenueDetail.class);
		/* 232 */ crit.setProjection(
				(Projection) Projections.projectionList().add(Projections.groupProperty("serviceId").as("serviceId"))
						/* 233 */ .add((Projection) Projections.sum("totalCalls")));

		/* 235 */ if (revenueRequest.getStartDate() != null) {
			/* 236 */ crit.add(Restrictions.between("dateTime", revenueRequest.getStartDate(),
					/* 237 */ Utility.addDays(revenueRequest.getStartDate(), 1)));
		}
		/* 239 */ Map<Integer, Long> callDetailMap = new HashMap<>();
		/* 240 */ for (Utility.getServiceName serviceCode : Utility.getServiceName.values()) {
			/* 241 */ callDetailMap.put(Integer.valueOf(serviceCode.ordinal() + 1), Long.valueOf(0L));
		}
		/* 243 */ callDetailMap.put(Integer.valueOf(11),
				Long.valueOf((callDetailMap.get(Integer.valueOf(11)) == null) ? 0L
						: ((Long) callDetailMap.get(Integer.valueOf(11))).longValue()));
		/* 244 */ List<Object[]> detail = crit.list();
		/* 245 */ List<DashboardCallDeatialBean> calldetail = new ArrayList<>();
		/* 246 */ if (detail != null) {
			/* 247 */ for (Object[] o : detail) {
				/* 248 */ int x = ((Integer) o[0]).intValue();
				/* 249 */ if (x == 11) {
					/* 250 */ Long calls = callDetailMap.get(Integer.valueOf(x));
					/* 251 */ callDetailMap.put(Integer.valueOf(x),
							Long.valueOf(calls.longValue() + ((Long) o[1]).longValue()));
				}

				/* 254 */ x = (x == 11) ? 1 : x;
				/* 255 */ if (callDetailMap.containsKey(Integer.valueOf(x))) {
					/* 256 */ Long calls = callDetailMap.get(Integer.valueOf(x));
					/* 257 */ callDetailMap.put(Integer.valueOf(x),
							Long.valueOf(calls.longValue() + ((Long) o[1]).longValue()));
				}
			}

			/* 261 */ for (Integer key : callDetailMap.keySet()) {
				/* 262 */ DashboardCallDeatialBean obj = new DashboardCallDeatialBean();
				/* 263 */ if (key.intValue() == 11) {
					/* 264 */ obj.setName("Voicemail");
					/* 265 */ obj.setValue(Integer.valueOf(((Long) callDetailMap.get(key)).intValue()));
				} else {
					/* 267 */ this.serviceName = Utility.getServiceName.values()[key.intValue() - 1];
					/* 268 */ obj.setName(this.serviceName.toString());
					/* 269 */ obj.setValue(Integer.valueOf(((Long) callDetailMap.get(key)).intValue()));
				}
				/* 271 */ calldetail.add(obj);
			}
			/* 273 */ res.setCallDetail(calldetail);
		}
		/* 275 */ return res;
	}

	public List<ZoneBean> getZone() {
		/* 279 */ Criteria criteria = getSession().createCriteria(Zone.class);
		/* 280 */ criteria.add((Criterion) Restrictions.eq("status", Constants.BYTE_1));
		/* 281 */ List<Zone> list = criteria.list();
		/* 282 */ List<ZoneBean> zoneList = new ArrayList<>();
		/* 283 */ for (Zone zone : list) {
			/* 284 */ ZoneBean obj = new ZoneBean();
			/* 285 */ obj.setId(zone.getId().longValue());
			/* 286 */ obj.setZoneName(zone.getZoneName());
			/* 287 */ zoneList.add(obj);
		}
		/* 289 */ return zoneList;
	}

	public List<Longcode> getLongCode(Byte flag) {
		/* 294 */ Criteria crit = getSession().createCriteria(Longcode.class);

		/* 296 */ if (flag == Constants.LONGCODE_ACTIVE) {
			/* 297 */ crit.add((Criterion) Restrictions.eq("status", Constants.LONGCODE_ACTIVE));
		} else {
			/* 299 */ crit.add((Criterion) Restrictions.disjunction()
					.add((Criterion) Restrictions.eq("status", Constants.LONGCODE_ACTIVE))
					/* 300 */ .add((Criterion) Restrictions.eq("status", Constants.LONGCODE_IN_ACTIVE)));
			/* 301 */ crit.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		}

		/* 304 */ return crit.list();
	}

	public AdminMaxAgents getMaxAgent() {
		/* 310 */ Criteria crit = getSession().createCriteria(SystemSnapshot.class);
		/* 311 */ crit.addOrder(Order.desc("insertTime"));
		/* 312 */ crit.setMaxResults(1);
		/* 313 */ SystemSnapshot obj = (SystemSnapshot) crit.uniqueResult();
		/* 314 */ AdminMaxAgents adminMaxAgents = new AdminMaxAgents();
		/* 315 */ adminMaxAgents.setMaxAllowed(Integer.valueOf(obj.getMaxAgents() - obj.getCurrentAgents()));
		/* 316 */ return adminMaxAgents;
	}

	public boolean login(LoginBean bean) {
		/* 321 */ UserRoleDetailBean role = roleDeatail(bean.getUserName());
		/* 322 */ if (!role.getRoles().equals("ADMIN")) {
			/* 323 */ Criteria critProfile = getSession().createCriteria(SmeProfile.class);
			/* 324 */ critProfile.setProjection((Projection) Projections.property("status"));
			/* 325 */ critProfile
					.add((Criterion) Restrictions.eq("id", Long.valueOf(Long.parseLong(bean.getUserName()))));
			/* 326 */ Byte status = (Byte) critProfile.uniqueResult();
			/* 327 */ if (status.byteValue() != 1) {
				/* 328 */ return false;
			}
		}
		/* 331 */ Criteria crit = getSession().createCriteria(Users.class);
		/* 332 */ crit.add((Criterion) Restrictions.eq("username", bean.getUserName()));
		/* 333 */ crit.add((Criterion) Restrictions.eq("password", bean.getPassword()));
		/* 334 */ if (crit.list().size() > 0) {
			/* 335 */ return true;
		}
		/* 337 */ return false;
	}

	public Longcode fetchSingeLongCode(Long id) {
		/* 342 */ return (Longcode) getSession().get(Longcode.class, id);
	}

	public void saveLongCode(Longcode longCode) {
		/* 347 */ getSession().persist(longCode);
	}

	public void saveDemoRequest(EdvaantageDemoRequest demoRequest) {
		/* 352 */ getSession().save(demoRequest);
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\daoImpl\AdminDaoImpl.class
 * Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */