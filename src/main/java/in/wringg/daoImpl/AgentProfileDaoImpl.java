package in.wringg.daoImpl;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import flexjson.JSONSerializer;
import in.wringg.dao.AbstractDao;
import in.wringg.dao.AgentProfileDao;
import in.wringg.email.MailSender;
import in.wringg.entity.AddressBook;
import in.wringg.entity.AgentCallingDetail;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentDetailTime;
import in.wringg.entity.AgentGroupDetail;
import in.wringg.entity.AgentLunchDetail;
import in.wringg.entity.CallSchedulerBase;
import in.wringg.entity.SmeCustomerRemarks;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.UserRole;
import in.wringg.entity.Users;
import in.wringg.pojo.AgentDetailBean;
import in.wringg.pojo.CallBaseBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.utility.Constants;
import in.wringg.utility.DaysofWeek;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import in.wringg.utility.Utility;

@Repository("agentDetailRepo")
public class AgentProfileDaoImpl extends AbstractDao<Serializable, Void> implements AgentProfileDao {
	public boolean canCreateAgent(Long smeId) {
		/* 52 */ Criteria decrement = getSession().createCriteria(AgentDetail.class);
		/* 53 */ decrement.setProjection(Projections.rowCount());
		/* 54 */ decrement.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 55 */ decrement.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 56 */ Integer createdAgents = Integer.valueOf(/* 57 */ Integer
				.parseInt((decrement.uniqueResult().toString() == null) ? "0" : decrement.uniqueResult().toString()));
		/* 58 */ SmeProfile smeProfile = (SmeProfile) getSession().get(SmeProfile.class, smeId);
		/* 59 */ if (smeProfile.getAllowedAgents() - createdAgents.intValue() > 0) {
			/* 60 */ return true;
		}

		/* 63 */ return false;
	}

	private ServerResponse createUserForAgent(AgentDetailBean agentDetail) throws IOException {
		/* 67 */ ServerResponse response = new ServerResponse();
		/* 68 */ Users user = null;
		/* 69 */ Users userIfExist = (Users) getSession().get(Users.class, agentDetail.getAgentEmail());
		/* 70 */ if (userIfExist != null && userIfExist.getEnabled() != -9) {
			/* 71 */ response.setSuccess(false);
			/* 72 */ ErrorDetails error = new ErrorDetails();
			/* 73 */ error.setErrorCode(ErrorConstants.ERROR_FAILED_AGENT_EMAIL_ALREADY_USED.getErrorCode());
			/* 74 */ error.setErrorString(
					"createAgent::" + ErrorConstants.ERROR_FAILED_AGENT_EMAIL_ALREADY_USED/* 75 */ .getErrorMessage());
			/* 76 */ error.setDisplayMessage(ErrorConstants.ERROR_FAILED_AGENT_EMAIL_ALREADY_USED.getDisplayMessage());
			/* 77 */ JSONSerializer serializer = new JSONSerializer();
			/* 78 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 79 */ response.setResult(res);
			/* 80 */ return response;
		}
		/* 82 */ if (userIfExist != null) {
			/* 83 */ user = userIfExist;
			/* 84 */ user.setPassword(Utility.genrateDigit(10));
			/* 85 */ user.setEnabled(Constants.BYTE_1.byteValue());
		} else {
			/* 87 */ user = new Users();
			/* 88 */ user.setUsername(agentDetail.getAgentEmail());
			/* 89 */ user.setPassword(Utility.genrateToken(10));
			/* 90 */ user.setEnabled(Constants.BYTE_1.byteValue());
			/* 91 */ UserRole userRole = new UserRole();
			/* 92 */ userRole.setRole("Agent");
			/* 93 */ userRole.setUser(user);
			/* 94 */ getSession().save(userRole);

			/* 96 */ getSession().flush();
		}

		/* 99 */ new MailSender(agentDetail.getAgentEmail(), "Wringg Account Detail",
				"Dear User, \n \n \t This is to inform you that your account has been created successfully by Wringg, \n Please use the following credentials to login. \n \n ",
				Constants.NEW_PROFILE_END_MSG, "Login ID: " + user

						/* 101 */ .getUsername() + " \n Login Password: " + user.getPassword());
		/* 102 */ response.setSuccess(true);
		/* 103 */ return response;
	}

	private AgentDetail addExtention(AgentDetail agentDetail, Long smeId) {
		/* 109 */ Criteria criteriaAgent = getSession().createCriteria(AgentDetail.class);
		/* 110 */ criteriaAgent.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 111 */ criteriaAgent.addOrder(Order.desc("agentId"));
		/* 112 */ criteriaAgent.setMaxResults(1);
		/* 113 */ AgentDetail agentDetailEntity = (AgentDetail) criteriaAgent.uniqueResult();
		/* 114 */ agentDetail.setAgentExtention(
				Integer.valueOf((agentDetailEntity != null && agentDetailEntity.getAgentExtention() != null
						&& agentDetailEntity/* 115 */ .getAgentExtention().intValue() > 0)
								? (agentDetailEntity/* 116 */ .getAgentExtention().intValue() + 1)
								: 1001));

		/* 118 */ return agentDetail;
	}

	public ServerResponse createAgent(AgentDetailBean bean) throws IOException {
		ServerResponse response = new ServerResponse();
		SmeProfile profile = (SmeProfile) getSession().get(SmeProfile.class, bean.getSmeId());
		if (profile == null || profile.getBillingStatus().byteValue() == 0) {
			/* 126 */ ErrorDetails error = new ErrorDetails();
			/* 127 */ error.setErrorCode(ErrorConstants.ERROR_BILLING_PENDING_AENET.getErrorCode());
			/* 128 */ error
					.setErrorString("createAgent::" + ErrorConstants.ERROR_BILLING_PENDING_AENET.getErrorMessage());
			/* 129 */ error.setDisplayMessage(ErrorConstants.ERROR_BILLING_PENDING_AENET.getDisplayMessage());
			/* 130 */ JSONSerializer serializer = new JSONSerializer();
			/* 131 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 132 */ response.setResult(res);
		}
		/* 134 */ else if (canCreateAgent(bean.getSmeId())) {
			/* 135 */ Criteria criteriaAgent = getSession().createCriteria(AgentDetail.class);
			/* 136 */ criteriaAgent.add((Criterion) Restrictions.eq("agentMobile", bean.getAgentMobile()));
			/* 137 */ criteriaAgent.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
			/* 138 */ criteriaAgent.setMaxResults(1);
			/* 139 */ if (criteriaAgent.uniqueResult() != null) {
				/* 140 */ ErrorDetails error = new ErrorDetails();
				/* 141 */ error.setErrorCode(ErrorConstants.ERROR_FAILED_AGENT_MOBILE_ALREADY_USED.getErrorCode());
				/* 142 */ error.setErrorString("createAgent::"
						+ ErrorConstants.ERROR_FAILED_AGENT_MOBILE_ALREADY_USED/* 143 */ .getErrorMessage());
				/* 144 */ error
						.setDisplayMessage(ErrorConstants.ERROR_FAILED_AGENT_MOBILE_ALREADY_USED.getDisplayMessage());
				/* 145 */ JSONSerializer serializer = new JSONSerializer();
				/* 146 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
				/* 147 */ response.setResult(res);
				/* 148 */ return response;
			}

			/* 152 */ Criteria agentMax = getSession().createCriteria(AgentDetail.class);
			/* 153 */ agentMax.setProjection(
					(Projection) Projections.projectionList().add((Projection) Projections.max("agentPosition")));
			/* 154 */ Object orderMax = agentMax.uniqueResult();
			/* 155 */ Integer order = Integer.valueOf((orderMax != null) ? Integer.parseInt(orderMax.toString()) : 0);
			/* 156 */ AgentDetail agentDetail = new AgentDetail();
			/* 157 */ agentDetail.setAgentPosition(Integer.valueOf(order.intValue() + 1));
			/* 158 */ agentDetail.setAgentMobile(bean.getAgentMobile());
			/* 159 */ agentDetail.setAgentName(bean.getAgentName());
			/* 160 */ agentDetail.setInTime(bean.getInTime());
			agentDetail.setOutTime(bean.getOutTime());
			agentDetail.setSmeId(bean.getSmeId());
			agentDetail.setStatus(bean.getStatus());
			agentDetail.setDaysFlag(bean.getDaysFlag());
			agentDetail.setInPermissionFlag(bean.getInPermissionFlag());
			agentDetail.setOutPermissionFlag(bean.getOutPermissionFlag());
			/* 165 */ Set<AgentGroupDetail> groups = new HashSet<>();
			for (Long groupId : bean.getAgentGroups()) {
				/* 167 */ AgentGroupDetail group = (AgentGroupDetail) getSession().get(AgentGroupDetail.class, groupId);
				/* 168 */ groups.add(group);
			}

			agentDetail.setGroups(groups);
			Set<AgentDetailTime> agent_time = new HashSet<>();

			for (String days : Utility.getWeekdays(bean.getDaysFlag().intValue())) {
				int double_days = Utility.getDateTimeDiff(bean.getInTime().toString(), bean.getOutTime().toString());
				if (double_days == -1) {
					SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
					for (int i = 0; i < 2; i++) {
						Time start = new Time((new Date()).getTime()), end = new Time((new Date()).getTime());
						if (i == 0) {
							start = bean.getInTime();

							try {
								Date d1 = format.parse("23:59:00");
								Time ppstime = new Time(d1.getTime());
								end = ppstime;
							} catch (ParseException e) {
								e.printStackTrace();
							}
						} else {
							/* 191 */ end = bean.getOutTime();

							try {
								/* 194 */ Date d1 = format.parse("00:00:00");
								/* 195 */ Time ppstime = new Time(d1.getTime());
								/* 196 */ start = ppstime;
								/* 197 */ days = Utility.getNextDay(days);
								/* 198 */ } catch (ParseException e) {
								/* 199 */ e.printStackTrace();
							}
						}
						/* 202 */ AgentDetailTime agentDetailTime = new AgentDetailTime();
						/* 203 */ agentDetailTime.setAgentPosition(Integer.valueOf(order.intValue() + 1));
						/* 204 */ agentDetailTime.setAgentMobile(bean.getAgentMobile());
						/* 205 */ agentDetailTime.setAgentName(bean.getAgentName());
						/* 206 */ agentDetailTime.setInTime(start);
						/* 207 */ agentDetailTime.setOutTime(end);
						/* 208 */ agentDetailTime.setSmeId(bean.getSmeId());
						/* 209 */ agentDetailTime.setStatus(bean.getStatus());
						/* 210 */ agentDetailTime.setDaysFlag(days);
						/* 211 */ agentDetailTime.setAgent_details(agentDetail);
						/* 212 */ agent_time.add(agentDetailTime);
					}
					continue;
				}
				AgentDetailTime agent = new AgentDetailTime();
				agent.setAgentPosition(Integer.valueOf(order.intValue() + 1));
				agent.setAgentMobile(bean.getAgentMobile());
				agent.setAgentName(bean.getAgentName());
				agent.setInTime(bean.getInTime());
				agent.setOutTime(bean.getOutTime());
				agent.setSmeId(bean.getSmeId());
				agent.setStatus(bean.getStatus());
				agent.setDaysFlag(days);
				agent.setAgent_details(agentDetail);
				agent_time.add(agent);
			}

			/* 228 */ agentDetail.setAgent_details_timing(agent_time);
			/* 229 */ agentDetail.setStickyAgent(bean.getStickyAgent());
			/* 230 */ agentDetail.setAgentMasking(bean.getAgentMasking());
			/* 231 */ agentDetail = addExtention(agentDetail, bean.getSmeId());
			/* 232 */ if (bean.getAgentEmail() != null && !"".equals(bean.getAgentEmail())) {
				/* 233 */ agentDetail.setAgentEmail(bean.getAgentEmail());
				/* 234 */ ServerResponse res = createUserForAgent(bean);
				/* 235 */ if (!res.isSuccess()) {
					/* 236 */ return res;
				}
			}
			/* 239 */ agentDetail.setStickyDays(bean.getStickyDays());
			/* 240 */ agentDetail.setIsUpdated(Constants.ACTIVE_STATUS);
			/* 241 */ agentDetail.setAssignVoicemailCalls(bean.getAssignVoicemailCalls());
			/* 242 */ agentDetail.setAssignFailedCalls(bean.getAssignFailedCalls());
			/* 243 */ getSession().persist(agentDetail);
			/* 244 */ response.setSuccess(true);
			/* 245 */ response.setResult("Agent Created");
		} else {

			/* 248 */ ErrorDetails error = new ErrorDetails();
			/* 249 */ error.setErrorCode(ErrorConstants.ERROR_AGENT_CAPACITY.getErrorCode());
			/* 250 */ error.setErrorString("createAgent::" + ErrorConstants.ERROR_AGENT_CAPACITY.getErrorMessage());
			/* 251 */ error.setDisplayMessage(ErrorConstants.ERROR_AGENT_CAPACITY.getDisplayMessage());
			/* 252 */ JSONSerializer serializer = new JSONSerializer();
			/* 253 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 254 */ response.setResult(res);
		}
		/* 256 */ return response;
	}

	public ServerResponse updateAgentDeatil(AgentDetailBean bean, Long id) throws IOException {
		/* 261 */ ServerResponse res = new ServerResponse();
		/* 262 */ AgentDetail agentDetail = fetchSingleAgent(id);
		/* 263 */ if (agentDetail != null && agentDetail.getStatus().byteValue() != -9) {
			/* 264 */ agentDetail.setStickyAgent(bean.getStickyAgent());
			/* 265 */ agentDetail.setAgentMobile(bean.getAgentMobile());
			/* 266 */ agentDetail.setAgentName(bean.getAgentName());
			/* 267 */ agentDetail.setInsertTime(bean.getInsertTime());
			/* 268 */ agentDetail.setInTime(bean.getInTime());
			/* 269 */ agentDetail.setOutTime(bean.getOutTime());
			/* 270 */ agentDetail.setSmeId(bean.getSmeId());
			/* 271 */ agentDetail.setStatus(bean.getStatus());
			/* 272 */ agentDetail.setDaysFlag(bean.getDaysFlag());
			agentDetail.setInPermissionFlag(bean.getInPermissionFlag());
			agentDetail.setOutPermissionFlag(bean.getOutPermissionFlag());
			/* 273 */ Set<AgentGroupDetail> groups = new HashSet<>();
			/* 274 */ for (Long groupId : bean.getAgentGroups()) {
				/* 275 */ AgentGroupDetail group = (AgentGroupDetail) getSession().get(AgentGroupDetail.class, groupId);
				/* 276 */ groups.add(group);
			}
			/* 278 */ agentDetail.setGroups(groups);
			/* 279 */ Iterator<AgentDetailTime> it = agentDetail.getAgent_details_timing().iterator();
			/* 280 */ while (it.hasNext()) {
				/* 281 */ AgentDetailTime a = it.next();
				/* 282 */ it.remove();
				/* 283 */ getSession().delete(a);
			}

			Set<AgentDetailTime> agent_time = new HashSet<>();

			for (String days : Utility.getWeekdays(bean.getDaysFlag().intValue())) {
				int double_days = Utility.getDateTimeDiff(bean.getInTime().toString(), bean.getOutTime().toString());
				if (double_days == -1) {
					SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
					for (int i = 0; i < 2; i++) {
						Time start = new Time((new Date()).getTime()), end = new Time((new Date()).getTime());
						if (i == 0) {
							start = bean.getInTime();

							try {
								/* 298 */ Date d1 = format.parse("23:59:00");
								/* 299 */ Time ppstime = new Time(d1.getTime());
								/* 300 */ end = ppstime;
							} catch (ParseException e) {
								e.printStackTrace();
							}
						} else {
							end = bean.getOutTime();

							try {
								/* 308 */ Date d1 = format.parse("00:00:00");
								/* 309 */ Time ppstime = new Time(d1.getTime());
								/* 310 */ start = ppstime;
								/* 311 */ days = Utility.getNextDay(days);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						/* 316 */ AgentDetailTime agentDetailTime = new AgentDetailTime();
						/* 317 */ agentDetailTime.setAgentPosition(agentDetail.getAgentPosition());
						/* 318 */ agentDetailTime.setAgentMobile(bean.getAgentMobile());
						/* 319 */ agentDetailTime.setAgentName(bean.getAgentName());
						/* 320 */ agentDetailTime.setInTime(start);
						/* 321 */ agentDetailTime.setOutTime(end);
						/* 322 */ agentDetailTime.setSmeId(bean.getSmeId());
						/* 323 */ agentDetailTime.setStatus(bean.getStatus());
						/* 324 */ agentDetailTime.setDaysFlag(days);
						/* 325 */ agentDetailTime.setAgent_details(agentDetail);
						/* 326 */ agent_time.add(agentDetailTime);
					}
					continue;
				}
				/* 329 */ AgentDetailTime agent = new AgentDetailTime();
				/* 330 */ agent.setAgentPosition(agentDetail.getAgentPosition());
				/* 331 */ agent.setAgentMobile(bean.getAgentMobile());
				/* 332 */ agent.setAgentName(bean.getAgentName());
				/* 333 */ agent.setInTime(bean.getInTime());
				/* 334 */ agent.setOutTime(bean.getOutTime());
				/* 335 */ agent.setSmeId(bean.getSmeId());
				/* 336 */ agent.setStatus(bean.getStatus());
				/* 337 */ agent.setDaysFlag(days);
				/* 338 */ agent.setAgent_details(agentDetail);
				/* 339 */ agent_time.add(agent);
			}

			/* 342 */ agentDetail.setAgent_details_timing(agent_time);
			/* 343 */ agentDetail.setAgentMasking(bean.getAgentMasking());
			/* 344 */ agentDetail.setStickyAgent(bean.getStickyAgent());
			/* 345 */ agentDetail.setStickyDays(bean.getStickyDays());
			/* 346 */ agentDetail.setIsUpdated(Constants.ACTIVE_STATUS);
			/* 347 */ agentDetail.setAssignVoicemailCalls(bean.getAssignVoicemailCalls());
			/* 348 */ agentDetail.setAssignFailedCalls(bean.getAssignFailedCalls());
			if (bean.getAgentEmail() != null && !"".equals(bean.getAgentEmail()) &&
			/* 350 */ !bean.getAgentEmail().equals(agentDetail.getAgentEmail())) {
				/* 351 */ agentDetail.setAgentEmail(bean.getAgentEmail());
				/* 352 */ ServerResponse respose = createUserForAgent(bean);
				if (!respose.isSuccess()) {
					return respose;
				}
			}
			/* 357 */ res.setSuccess(true);
			/* 358 */ res.setResult("Successfully Updated");
		} else {
			/* 360 */ res.setSuccess(false);
			/* 361 */ ErrorDetails error = new ErrorDetails();
			/* 362 */ error.setErrorCode(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorCode());
			/* 363 */ error.setErrorString("createAgent::" + ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorMessage());
			/* 364 */ error.setDisplayMessage(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getDisplayMessage());
			/* 365 */ JSONSerializer serializer = new JSONSerializer();
			/* 366 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 367 */ res.setResult(response);
		}
		/* 369 */ return res;
	}

	public List<AgentDetail> fetchAgentDetailList(Long smeId) {
		/* 420 */ List<AgentDetailBean> detailBeans = new ArrayList<>();
		/* 421 */ Criteria criteriaAgent = getSession().createCriteria(AgentDetail.class);
		/* 422 */ criteriaAgent.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 423 */ criteriaAgent.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 424 */ criteriaAgent.addOrder(Order.asc("agentPosition"));
		/* 425 */ List<AgentDetail> agentDetails = criteriaAgent.list();
		/* 426 */ return agentDetails;
	}

	public void deleteSingleAgent(Long id) {
		/* 431 */ AgentDetail agentDetai = fetchSingleAgent(id);
		/* 432 */ if (agentDetai != null) {
			/* 433 */ agentDetai.setStatus(Byte.valueOf((byte) -9));
			/* 434 */ for (AgentDetailTime a : agentDetai.getAgent_details_timing()) {
				/* 435 */ a.setStatus(Byte.valueOf((byte) -9));
			}
		}
	}

	public AgentDetail fetchSingleAgent(Long id) {
		/* 442 */ return (AgentDetail) getSession().get(AgentDetail.class, id);
	}

	public AgentDetailTime fetchSingleAgentT(Long id) {
		/* 447 */ return (AgentDetailTime) getSession().get(AgentDetailTime.class, id);
	}

	public AgentDetail fetchSingleAgentByEmail(String email) {
		/* 452 */ Criteria criteriaAgent = getSession().createCriteria(AgentDetail.class);
		/* 453 */ criteriaAgent.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 454 */ criteriaAgent.add((Criterion) Restrictions.eq("agentEmail", email));
		/* 455 */ criteriaAgent.setMaxResults(1);
		/* 456 */ return (AgentDetail) criteriaAgent.uniqueResult();
	}

	public List<CallSchedulerBase> getBaseForAgent(Long agentDetail) {
		/* 461 */ Criteria criteria = getSession().createCriteria(CallSchedulerBase.class);
		/* 462 */ criteria.add((Criterion) Restrictions.eq("status", Byte.valueOf((byte) 0)));
		/* 463 */ criteria.add((Criterion) Restrictions.eq("agent", agentDetail));
		/* 464 */ criteria.addOrder(Order.asc("callCounter"));
		/* 465 */ criteria.addOrder(Order.desc("scheduleDateTime"));
		/* 466 */ return criteria.list();
	}

	public List<AgentCallingDetail> getAgentCallingDetail(StartEndDateRequest request) {
		/* 471 */ Criteria criteria = getSession().createCriteria(AgentCallingDetail.class);
		/* 472 */ if (request != null && request.getStartDate() != null && request.getEndDate() != null) {
			/* 473 */ criteria.add(Restrictions.between("insertDate", request.getStartDate(), request.getEndDate()));
		}
		/* 475 */ if (request.getSmeId() != null) {
			/* 476 */ criteria.add((Criterion) Restrictions.eq("smeId", request.getSmeId()));
		}
		/* 478 */ if (request.getId() != null && request.getId().longValue() > 0L) {
			/* 479 */ criteria.add((Criterion) Restrictions.eq("agentId", request.getId()));
		}
		/* 481 */ return criteria.list();
	}

	public void lunchInEntry(Long smeId, Long agentId, Date date, String message) {
		/* 486 */ AgentLunchDetail agentLunchDetail = new AgentLunchDetail();
		/* 487 */ agentLunchDetail.setAgentId(agentId);
		/* 488 */ agentLunchDetail.setSmeId(smeId);
		/* 489 */ agentLunchDetail.setInsertDate(date);
		/* 490 */ agentLunchDetail.setInTime(date);
		/* 491 */ agentLunchDetail.setMessage(message);
		/* 492 */ getSession().save(agentLunchDetail);
	}

	public AgentLunchDetail getLastInEntry(Long agentId) {
		/* 498 */ Criteria criteria = getSession().createCriteria(AgentLunchDetail.class);
		/* 499 */ criteria.add((Criterion) Restrictions.eq("agentId", agentId));
		/* 500 */ criteria.addOrder(Order.desc("inTime"));
		/* 501 */ criteria.setMaxResults(1);
		/* 502 */ return (AgentLunchDetail) criteria.uniqueResult();
	}

	public List<AgentLunchDetail> getSameDayBrkEntry(Long agentId) {
		/* 507 */ StartEndDateRequest startEndDateRequest = new StartEndDateRequest();
		/* 508 */ startEndDateRequest.setStartDate(new Date());
		/* 509 */ startEndDateRequest.setEndDate(new Date());
		/* 510 */ Criteria criteria = getSession().createCriteria(AgentLunchDetail.class);
		/* 511 */ criteria.add((Criterion) Restrictions.eq("agentId", agentId));
		/* 512 */ criteria.addOrder(Order.desc("outTime"));
		/* 513 */ criteria.add(Restrictions.between("insertDate", startEndDateRequest.getStartDate(),
				startEndDateRequest/* 514 */ .getEndDate()));
		/* 515 */ criteria.add(Restrictions.isNotNull("outTime"));
		/* 516 */ return criteria.list();
	}

	public AgentDetailTime getTimeForToday(Long agentId) {
		/* 521 */ Criteria criteria = getSession().createCriteria(AgentDetailTime.class);
		/* 522 */ String day = DaysofWeek.getDayName(LocalDate.now().getDayOfWeek().getValue());
		/* 523 */ criteria
				.add((Criterion) Restrictions.eq("agent_details", getSession().get(AgentDetail.class, agentId)));
		/* 524 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 525 */ criteria.add((Criterion) Restrictions.ne("daysFlag", day));
		/* 526 */ criteria.setMaxResults(1);
		/* 527 */ return (AgentDetailTime) criteria.uniqueResult();
	}

	public void saveAddressBook(AddressBook addressBook) {
		/* 532 */ getSession().saveOrUpdate(addressBook);
	}

	public AddressBook getAddressBookBySmeIdAndMobile(Long smeId, String mobile) {
		/* 537 */ Criteria criteria = getSession().createCriteria(AddressBook.class);
		/* 538 */ criteria.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 539 */ criteria.add((Criterion) Restrictions.eq("customerNumberPrimary", mobile));
		/* 540 */ criteria.setMaxResults(1);
		/* 541 */ return (AddressBook) criteria.uniqueResult();
	}

	public AddressBook getAddressBookById(Long id) {
		/* 546 */ return (AddressBook) getSession().get(AddressBook.class, id);
	}

	public List<AddressBook> getAddressBookBySmeId(Long smeId) {
		/* 551 */ Criteria criteria = getSession().createCriteria(AddressBook.class);
		/* 552 */ criteria.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 553 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 554 */ return criteria.list();
	}

	public List<AddressBook> getAddressBookByAgentId(Long agentId) {
		/* 559 */ Criteria criteria = getSession().createCriteria(AddressBook.class);
		/* 560 */ criteria.add((Criterion) Restrictions.eq("createdBy", agentId));
		/* 561 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 562 */ return criteria.list();
	}

	public void saveSmeCustomerRemarks(SmeCustomerRemarks customerRemarks) {
		/* 567 */ getSession().save(customerRemarks);
	}

	public SmeCustomerRemarks getSmeCustomerRemarks(Long id) {
		/* 573 */ return (SmeCustomerRemarks) getSession().get(SmeCustomerRemarks.class, id);
	}

	public List<SmeCustomerRemarks> getRemaksByAgentId(Long agentId) {
		/* 578 */ Criteria criteria = getSession().createCriteria(SmeCustomerRemarks.class);
		/* 579 */ criteria.add((Criterion) Restrictions.eq("agentId", agentId));
		/* 580 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 581 */ return criteria.list();
	}

	public List<SmeCustomerRemarks> getRemaksBySmeId(Long smeId) {
		/* 586 */ Criteria criteria = getSession().createCriteria(SmeCustomerRemarks.class);
		/* 587 */ criteria.add((Criterion) Restrictions.eq("smeId", smeId));
		/* 588 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 589 */ return criteria.list();
	}

	public List<SmeCustomerRemarks> getRemaksByAddressBookId(Long addressBookId) {
		/* 594 */ Criteria criteria = getSession().createCriteria(SmeCustomerRemarks.class);
		/* 595 */ criteria.add((Criterion) Restrictions.eq("addressBookId", addressBookId));
		/* 596 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 597 */ return criteria.list();
	}

	public void saveCallSheduleBase(CallBaseBean bean, Long agentId) {
		/* 602 */ Long smeId = fetchSingleAgent(agentId).getSmeId();
		/* 603 */ CallSchedulerBase base = new CallSchedulerBase();
		/* 604 */ base.setAgent(agentId);
		/* 605 */ base.setCallCounter(bean.getCallCounter());
		/* 606 */ base.setCallType(bean.getCallType());
		/* 607 */ base.setDescription(bean.getDesc());
		/* 608 */ base.setMobile(bean.getMoblie());
		/* 609 */ base.setScheduleDateTime(bean.getScheduleDateTime());
		/* 610 */ base.setStatus(Byte.valueOf((byte) 0));
		/* 611 */ base.setSme(smeId);
		/* 612 */ getSession().save(base);
	}

	public Long callSchBaseCounterOnCallType(Long agentId, Integer callType) {
		/* 618 */ Criteria criteria = getSession().createCriteria(CallSchedulerBase.class);
		/* 619 */ criteria.add((Criterion) Restrictions.eq("agent", agentId));
		/* 620 */ criteria.add((Criterion) Restrictions.eq("callType", callType));
		/* 621 */ criteria.add((Criterion) Restrictions.eq("status", Constants.BYTE_0));
		/* 622 */ criteria.add((Criterion) Restrictions.eq("callCounter", Integer.valueOf(0)));
		/* 623 */ criteria.setProjection(Projections.rowCount());
		/* 624 */ Long count = (Long) criteria.uniqueResult();
		/* 625 */ return count;
	}

	public CallSchedulerBase findById(Long id) {
		/* 630 */ return (CallSchedulerBase) getSession().get(CallSchedulerBase.class, id);
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\daoImpl\AgentProfileDaoImpl.
 * class Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */