package in.wringg.daoImpl;

import flexjson.JSONSerializer;
import in.wringg.dao.AbstractDao;
import in.wringg.dao.ComponentDao;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.IncomingIvrCallCdr;
import in.wringg.entity.MpbxChargingFail;
import in.wringg.entity.MpbxChargingSucc;
import in.wringg.entity.OutbondIvrCdr;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.VoicemailCallCdr;
import in.wringg.entity.VoicemailSchedule;
import in.wringg.pojo.CallCdrBean;
import in.wringg.pojo.ChargingResponseBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.utility.Constants;
import in.wringg.utility.Utility;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("componentRepo")
public class ComponentDaoImpl
  extends AbstractDao<Serializable, Void>
  implements ComponentDao {
/*  34 */   static final Logger logger = Logger.getLogger(in.wringg.daoImpl.ComponentDaoImpl.class); public void addCdrDetails(CallCdrBean cdr) { OutbondIvrCdr outbondIvrCdr;
    VoicemailCallCdr voicemailCallCdr;
    VoicemailSchedule voicemailSchedule;
    IncomingIvrCallCdr inCdr;
/*  38 */     logger.debug("In addCDR with mode:" + cdr.getCdrMode());
/*  39 */     switch (cdr.getCdrMode().byteValue()) {
      case 2:
      case 22:
      case 23:
      case 24:
/*  44 */         outbondIvrCdr = new OutbondIvrCdr();
/*  45 */         outbondIvrCdr.setSessionId(cdr.getCallId());
/*  46 */         outbondIvrCdr.setAgentGroup(cdr.getAgentGroup());
/*  47 */         outbondIvrCdr.setCallDirection(cdr.getCallDirection());
/*  48 */         outbondIvrCdr.setCallDirectionStatus(Integer.valueOf(cdr.getCallDirectionStatus()));
/*  49 */         outbondIvrCdr.setCalledNumber(cdr.getCalledNumber());
/*  50 */         outbondIvrCdr.setCallingNumber(cdr.getCallingNumber());
/*  51 */         outbondIvrCdr.setCallRecordedFile(cdr.getCallRecordedFile());
/*  52 */         outbondIvrCdr.setCallRecordingStatus(Integer.valueOf(cdr.getCallRecordingStatus()));
/*  53 */         outbondIvrCdr.setCdrMode(cdr.getCdrMode());
/*  54 */         outbondIvrCdr.setChannelNo(Integer.valueOf(cdr.getChannelNo()));
/*  55 */         outbondIvrCdr.setDuration(Integer.valueOf(cdr.getDuration()));
/*  56 */         outbondIvrCdr.setEndDateTime(cdr.getEndDateTime());
/*  57 */         outbondIvrCdr.setHlr(cdr.getHlr());
/*  58 */         outbondIvrCdr.setInsertDateTime(cdr.getInsertDateTime());
/*  59 */         outbondIvrCdr.setLongcode(cdr.getLongcode());
/*  60 */         outbondIvrCdr.setMasterShortcode(cdr.getMasterShortcode());
/*  61 */         outbondIvrCdr.setShortcodeMapping(cdr.getShortcodeMapping());
/*  62 */         outbondIvrCdr.setPatchedAgentId((AgentDetail)getSession().get(AgentDetail.class, cdr.getPatchedAgentId()));
/*  63 */         outbondIvrCdr.setServerIpAddress(cdr.getServerIpAddress());
/*  64 */         outbondIvrCdr.setSmeId((SmeProfile)getSession().get(SmeProfile.class, cdr.getSmeId()));
/*  65 */         outbondIvrCdr.setSmeIdentifier(cdr.getSmeIdentifier());
/*  66 */         outbondIvrCdr.setStartDateTime(cdr.getStartDateTime());
/*  67 */         outbondIvrCdr.setVoicemailRecordingFile(cdr.getVoicemailRecordingFile());
/*  68 */         outbondIvrCdr.setVoicemailRecordingStatus(cdr.getVoicemailRecordingStatus());
/*  69 */         outbondIvrCdr.setMergeStatus(Constants.BYTE_0);
/*  70 */         outbondIvrCdr.setAnswer(Byte.valueOf(cdr.getAnswer()));
/*  71 */         outbondIvrCdr.setAddressBookId(cdr.getAddressBookId());
/*  72 */         outbondIvrCdr.setCustomerName(cdr.getCustomerName());
/*  73 */         getSession().persist(outbondIvrCdr);
        break;
      
      case 3:
/*  77 */         voicemailCallCdr = new VoicemailCallCdr();
/*  78 */         voicemailCallCdr.setSessionId(cdr.getCallId());
/*  79 */         voicemailCallCdr.setAgentGroup(cdr.getAgentGroup());
/*  80 */         voicemailCallCdr.setCallDirection(cdr.getCallDirection());
/*  81 */         voicemailCallCdr.setCallDirectionStatus(Integer.valueOf(cdr.getCallDirectionStatus()));
/*  82 */         voicemailCallCdr.setCalledNumber(cdr.getCalledNumber());
/*  83 */         voicemailCallCdr.setCallingNumber(cdr.getCallingNumber());
/*  84 */         voicemailCallCdr.setCallRecordedFile(cdr.getCallRecordedFile());
/*  85 */         voicemailCallCdr.setCallRecordingStatus(Integer.valueOf(cdr.getCallRecordingStatus()));
/*  86 */         voicemailCallCdr.setCdrMode(cdr.getCdrMode());
/*  87 */         voicemailCallCdr.setChannelNo(Integer.valueOf(cdr.getChannelNo()));
/*  88 */         voicemailCallCdr.setDuration(Integer.valueOf(cdr.getDuration()));
/*  89 */         voicemailCallCdr.setEndDateTime(cdr.getEndDateTime());
/*  90 */         voicemailCallCdr.setHlr(cdr.getHlr());
/*  91 */         voicemailCallCdr.setInsertDateTime(cdr.getInsertDateTime());
/*  92 */         voicemailCallCdr.setLongcode(cdr.getLongcode());
/*  93 */         voicemailCallCdr.setMasterShortcode(cdr.getMasterShortcode());
/*  94 */         voicemailCallCdr.setShortcodeMapping(cdr.getShortcodeMapping());
/*  95 */         voicemailCallCdr.setPatchedAgentId((AgentDetail)getSession().get(AgentDetail.class, cdr.getPatchedAgentId()));
/*  96 */         voicemailCallCdr.setServerIpAddress(cdr.getServerIpAddress());
/*  97 */         voicemailCallCdr.setSmeId((SmeProfile)getSession().get(SmeProfile.class, cdr.getSmeId()));
/*  98 */         voicemailCallCdr.setSmeIdentifier(cdr.getSmeIdentifier());
/*  99 */         voicemailCallCdr.setStartDateTime(cdr.getStartDateTime());
/* 100 */         voicemailCallCdr.setVoicemailRecordingFile(cdr.getVoicemailRecordingFile());
/* 101 */         voicemailCallCdr.setVoicemailRecordingStatus(Integer.valueOf(cdr.getVoicemailRecordingStatus()));
/* 102 */         voicemailCallCdr.setMergeStatus(Constants.BYTE_0);
/* 103 */         voicemailCallCdr.setAnswer(Byte.valueOf(cdr.getAnswer()));
/* 104 */         voicemailCallCdr.setAddressBookId(cdr.getAddressBookId());
/* 105 */         voicemailCallCdr.setCustomerName(cdr.getCustomerName());


        
/* 109 */         voicemailSchedule = new VoicemailSchedule();
/* 110 */         voicemailSchedule.setCalledparty(cdr.getCalledNumber());
/* 111 */         voicemailSchedule.setCallingparty(cdr.getCallingNumber());
/* 112 */         voicemailSchedule.setCounter(0);
/* 113 */         voicemailSchedule.setDateTime(Utility.getDateTime());
/* 114 */         voicemailSchedule.setDescription("initiate");
/* 115 */         voicemailSchedule.setGroupId(cdr.getGroupId());
/* 116 */         voicemailSchedule.setGroupName(cdr.getAgentGroup());
/* 117 */         voicemailSchedule.setSentDatetime(Utility.getDateTime());
/* 118 */         voicemailSchedule.setStatus("0");
/* 119 */         voicemailSchedule.setVoicemailfile(cdr.getVoicemailRecordingFile());
/* 120 */         voicemailSchedule.setSmeId(cdr.getSmeId().toString());
/* 121 */         getSession().persist(voicemailCallCdr);
/* 122 */         voicemailSchedule.setVmCdrId(voicemailCallCdr.getId());
/* 123 */         getSession().persist(voicemailSchedule);
        break;
      
      case 1:
/* 127 */         inCdr = new IncomingIvrCallCdr();
/* 128 */         inCdr.setSessionId(cdr.getCallId());
/* 129 */         inCdr.setAgentGroup(cdr.getAgentGroup());
/* 130 */         inCdr.setCallDirection(cdr.getCallDirection());
/* 131 */         inCdr.setCallDirectionStatus(Integer.valueOf(cdr.getCallDirectionStatus()));
/* 132 */         inCdr.setCalledNumber(cdr.getCalledNumber());
/* 133 */         inCdr.setCallingNumber(cdr.getCallingNumber());
/* 134 */         inCdr.setCallRecordedFile(cdr.getCallRecordedFile());
/* 135 */         inCdr.setCallRecordingStatus(Integer.valueOf(cdr.getCallRecordingStatus()));
/* 136 */         inCdr.setCdrMode(cdr.getCdrMode());
/* 137 */         inCdr.setChannelNo(Integer.valueOf(cdr.getChannelNo()));
/* 138 */         inCdr.setDuration(Integer.valueOf(cdr.getDuration()));
/* 139 */         inCdr.setEndDateTime(cdr.getEndDateTime());
/* 140 */         inCdr.setHlr(cdr.getHlr());
/* 141 */         inCdr.setInsertDateTime(cdr.getInsertDateTime());
/* 142 */         inCdr.setLongcode(cdr.getLongcode());
/* 143 */         inCdr.setMasterShortcode(cdr.getMasterShortcode());
/* 144 */         inCdr.setShortcodeMapping(cdr.getShortcodeMapping());
/* 145 */         inCdr.setPatchedAgentId((AgentDetail)getSession().get(AgentDetail.class, cdr.getPatchedAgentId()));
/* 146 */         inCdr.setServerIpAddress(cdr.getServerIpAddress());
/* 147 */         inCdr.setSmeId((SmeProfile)getSession().get(SmeProfile.class, cdr.getSmeId()));
/* 148 */         inCdr.setSmeIdentifier(cdr.getSmeIdentifier());
/* 149 */         inCdr.setStartDateTime(cdr.getStartDateTime());
/* 150 */         inCdr.setVoicemailRecordingFile(cdr.getVoicemailRecordingFile());
/* 151 */         inCdr.setVoicemailRecordingStatus(Integer.valueOf(cdr.getVoicemailRecordingStatus()));
/* 152 */         inCdr.setMergeStatus(Constants.BYTE_0);
/* 153 */         inCdr.setAnswer(Byte.valueOf(cdr.getAnswer()));
/* 154 */         inCdr.setCallStatus(Byte.valueOf(cdr.getCallStatus()));
/* 155 */         inCdr.setDisconnectedBy(cdr.getDisconnectedBy());
/* 156 */         inCdr.setAddressBookId(cdr.getAddressBookId());
/* 157 */         inCdr.setCustomerName(cdr.getCustomerName());
/* 158 */         getSession().persist(inCdr);
        break;
    }  }


  
  public void changeSchuduleStatus(Long id) {
/* 165 */     logger.debug("In changeSchuduleStatus");
/* 166 */     ((VoicemailCallCdr)getSession().get(VoicemailCallCdr.class, id)).setVoicemailRecordingStatus(Integer.valueOf(2));
/* 167 */     Criteria crit = getSession().createCriteria(VoicemailSchedule.class);
/* 168 */     crit.add((Criterion)Restrictions.eq("vmCdrId", id));
/* 169 */     VoicemailSchedule schedule = (VoicemailSchedule)crit.uniqueResult();
/* 170 */     schedule.setStatus("1");
/* 171 */     schedule.setSentDatetime(Utility.getDateTime());
  }


  
  public ServerResponse chargingDetails(StartEndDateRequest filter, Long id) {
/* 177 */     ServerResponse response = new ServerResponse();
    
/* 179 */     Criteria chargingSucces = getSession().createCriteria(MpbxChargingSucc.class);
/* 180 */     Criteria chargingFail = getSession().createCriteria(MpbxChargingFail.class);

    
/* 183 */     if (id != null) {
/* 184 */       chargingSucces.add((Criterion)Restrictions.eq("smeId", id));
/* 185 */       chargingFail.add((Criterion)Restrictions.eq("smeId", id));
    } 
/* 187 */     if (filter != null && filter.getEndDate() != null && filter.getStartDate() != null) {
      
/* 189 */       chargingSucces.add(Restrictions.between("billingDate", filter.getStartDate(), filter.getEndDate()));
/* 190 */       chargingFail.add(Restrictions.between("billingDate", filter.getStartDate(), filter.getEndDate()));
    } 
    
/* 193 */     List<ChargingResponseBean> chargingListBean = new ArrayList<>();

    
/* 196 */     chargingSucces.addOrder(Order.desc("billingDate"));
/* 197 */     chargingFail.addOrder(Order.desc("billingDate"));
/* 198 */     List<MpbxChargingSucc> successList = chargingSucces.list();
/* 199 */     List<MpbxChargingFail> failList = chargingFail.list();

    
/* 202 */     for (MpbxChargingSucc obj : successList) {
      
/* 204 */       ChargingResponseBean bean = new ChargingResponseBean();
      
/* 206 */       bean.setAgentAllocate(obj.getAgentAllocate());
/* 207 */       bean.setAni(obj.getAni());
/* 208 */       bean.setBillingDate(obj.getBillingDate());
/* 209 */       bean.setCharegedAmt(obj.getCharegedAmt());
/* 210 */       bean.setChargingResp("Success");
/* 211 */       bean.setInFlag(obj.getInFlag());
/* 212 */       bean.setInMode(obj.getInMode());
/* 213 */       bean.setPackId(obj.getPackId());
/* 214 */       bean.setPrePost(obj.getPrePost());
/* 215 */       bean.setRenewDate(obj.getRenewDate());
/* 216 */       bean.setSmeId(obj.getSmeId());
/* 217 */       bean.setStatus(Constants.BYTE_0);
/* 218 */       bean.setSubDate(obj.getSubDate());
/* 219 */       bean.setTotalAgentAllocate(obj.getTotalAgentAllocate());
/* 220 */       bean.setUserBalance(obj.getUserBalance());
/* 221 */       bean.setUserType(obj.getUserType());
      
/* 223 */       chargingListBean.add(bean);
    } 
    
/* 226 */     for (MpbxChargingFail obj : failList) {
/* 227 */       ChargingResponseBean bean = new ChargingResponseBean();
      
/* 229 */       bean.setAgentAllocate(obj.getAgentAllocate());
/* 230 */       bean.setAni(obj.getAni());
/* 231 */       bean.setBillingDate(obj.getBillingDate());
/* 232 */       bean.setCharegedAmt(obj.getCharegedAmt());
/* 233 */       bean.setChargingResp("Fail");
/* 234 */       bean.setInFlag(obj.getInFlag());
/* 235 */       bean.setInMode(obj.getInMode());
/* 236 */       bean.setPackId(obj.getPackId());
/* 237 */       bean.setPrePost(obj.getPrePost());
/* 238 */       bean.setRenewDate(obj.getRenewDate());
/* 239 */       bean.setSmeId(obj.getSmeId());
/* 240 */       bean.setStatus(Constants.BYTE_1);
/* 241 */       bean.setSubDate(obj.getSubDate());
/* 242 */       bean.setTotalAgentAllocate(obj.getTotalAgentAllocate());
/* 243 */       bean.setUserBalance(Float.valueOf(obj.getUserBalance()));
/* 244 */       bean.setUserType(obj.getUserType());
      
/* 246 */       chargingListBean.add(bean);
    } 

    
/* 250 */     JSONSerializer serializer = new JSONSerializer();
/* 251 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(chargingListBean);
/* 252 */     response.setSuccess(true);
/* 253 */     response.setResult(res);
/* 254 */     return response;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\daoImpl\ComponentDaoImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */