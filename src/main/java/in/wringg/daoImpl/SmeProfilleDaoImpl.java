package in.wringg.daoImpl;

import flexjson.JSONSerializer;
import in.wringg.dao.AbstractDao;
import in.wringg.dao.AdminDao;
import in.wringg.dao.SmeProfileDao;
import in.wringg.email.MailSender;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.Longcode;
import in.wringg.entity.MpbxCharging;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.SystemSnapshot;
import in.wringg.entity.UserRole;
import in.wringg.entity.Users;
import in.wringg.entity.Zone;
import in.wringg.pojo.AdminSmeProfileBean;
import in.wringg.pojo.FilterBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import in.wringg.utility.Utility;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("smeProfileRepo")
public class SmeProfilleDaoImpl extends AbstractDao<Integer, SmeProfile> implements SmeProfileDao {
	@Autowired
	private AdminDao adminDao;

	public ServerResponse addSmeProfile(AdminSmeProfileBean smeprofile) throws Exception {
		/* 48 */ ServerResponse res = new ServerResponse();
		/* 49 */ Criteria criteria = getSession().createCriteria(SystemSnapshot.class);
		/* 50 */ criteria.addOrder(Order.desc("insertTime"));
		/* 51 */ criteria.setMaxResults(1);
		/* 52 */ SystemSnapshot snap = (SystemSnapshot) criteria.uniqueResult();
		/* 53 */ if (snap.getMaxSme() <= snap.getCurrentSme()) {
			/* 54 */ res.setSuccess(false);
			/* 55 */ ErrorDetails error = new ErrorDetails();
			/* 56 */ error.setErrorCode(ErrorConstants.ERROR_SYSTEM_CAPACITY_FULL.getErrorCode());
			/* 57 */ error.setErrorString("Add Sme" + ErrorConstants.ERROR_SYSTEM_CAPACITY_FULL.getErrorMessage());
			/* 58 */ error.setDisplayMessage(ErrorConstants.ERROR_SYSTEM_CAPACITY_FULL.getDisplayMessage());
			/* 59 */ JSONSerializer serializer = new JSONSerializer();
			/* 60 */ String result = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 61 */ res.setResult(result);
			/* 62 */ return res;
		}
		/* 64 */ int existingCapacity = snap.getMaxAgents() - snap.getCurrentAgents();
		/* 65 */ if (existingCapacity - smeprofile.getAllowedAgents() < 0) {
			/* 66 */ res.setSuccess(false);
			/* 67 */ ErrorDetails error = new ErrorDetails();
			/* 68 */ error.setErrorCode(ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getErrorCode());
			/* 69 */ error.setErrorString("Add Sme" + ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getErrorMessage());
			/* 70 */ error.setDisplayMessage(ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getDisplayMessage());
			/* 71 */ JSONSerializer serializer = new JSONSerializer();
			/* 72 */ String result = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 73 */ res.setResult(result);
			/* 74 */ return res;
		}
		/* 76 */ snap.setCurrentSme(snap.getCurrentSme() + 1);
		/* 77 */ snap.setCurrentAgents(snap.getCurrentAgents() + smeprofile.getAllowedAgents());
		/* 78 */ SmeProfile sme = new SmeProfile();
		/* 79 */ MpbxCharging charging = new MpbxCharging();
		/* 80 */ sme.setAllowedAgents(smeprofile.getAllowedAgents());
		/* 81 */ sme.setAlternateNumber(smeprofile.getAlternateNumber());
		/* 82 */ sme.setEmailId(smeprofile.getEmailId());
		/* 83 */ sme.setId(Long.valueOf(smeprofile.getId()));
		/* 84 */ sme.setInsertTime(new Date());
		sme.setInPermissionFlag(smeprofile.getInPermissionFlag());
		sme.setOutPermissionFlag(smeprofile.getOutPermissionFlag());
		sme.setEodReportFlag(smeprofile.getEodReportFlag());
		sme.setRoutingType(smeprofile.getRoutingType());
		sme.setInChannels(smeprofile.getInChannels());
		sme.setInQueueChannels(smeprofile.getInQueueChannels());
		sme.setOutChannels(smeprofile.getOutChannels());
		sme.setGuiTimer(smeprofile.getGuiTimer());
		sme.setAgentRelaxTime(smeprofile.getAgentRelaxTime());
//		if (smeprofile.getLongCodes() != null && smeprofile.getLongCodes().size() > 0) {
//			Set<Longcode> longcodes = new HashSet<Longcode>();
//			for (long longCode : smeprofile.getLongCodes()) {
//				Longcode longcode = getSession().get(Longcode.class, longCode);
//				if (longcode.getStatus() != Constants.LONGCODE_ACTIVE.byteValue()) {
//					res.setSuccess(false);
//					ErrorDetails error = new ErrorDetails();
//					error.setErrorCode(ErrorConstants.ERROR_LONGCODE_NOT_ACTIVE.getErrorCode());
//					error.setErrorString("Add Sme" + ErrorConstants.ERROR_LONGCODE_NOT_ACTIVE.getErrorMessage());
//					error.setDisplayMessage(ErrorConstants.ERROR_LONGCODE_NOT_ACTIVE.getDisplayMessage());
//					JSONSerializer serializer = new JSONSerializer();
//					String result = serializer.exclude(new String[] { "*.class" }).serialize(error);
//					res.setResult(result);
//					return res;
//				}
//				longcodes.add(longcode);
//			}
//			sme.setLongcodes(longcodes);
//		}

		/* 97 */ sme
				.setLongcodeId((Longcode) getSession().get(Longcode.class, Long.valueOf(smeprofile.getLongCodeId())));
		/* 98 */ sme.getLongcodeId().setStatus(Constants.LONG_CODE_ASSIGNED.byteValue());
		/* 99 */ sme.setServiceFlag(smeprofile.getServiceFlag());
		/* 100 */ sme.setSmeMobile(smeprofile.getSmeMobile());
		/* 101 */ sme.setName(smeprofile.getSmeName());
		/* 102 */ sme.setStatus(smeprofile.getStatus());
		/* 103 */ sme.setInsertTime(new Date());
		/* 104 */ sme.setZoneId((Zone) getSession().get(Zone.class, Long.valueOf(smeprofile.getZoneId())));
		/* 105 */ sme.setBalance(smeprofile.getBalance());
		/* 106 */ sme.setRecording(smeprofile.getRecording());
		/* 107 */ sme.setMasking(smeprofile.getMasking());
		/* 108 */ sme.setVoicemail(smeprofile.getVoicemail());
		/* 109 */ sme.setSelectionAlgo(smeprofile.getSelectionAlgo());
		/* 110 */ sme.setBizAddress(smeprofile.getBizAddress());
		/* 111 */ sme.setLanguage(smeprofile.getLanguage());
		/* 112 */ sme.setBillingStatus(Byte.valueOf((byte) 0));
		/* 113 */ sme.setRecValidity(smeprofile.getRecValidity());
		/* 114 */ sme.setAccountSid(smeprofile.getAccountSid());
		/* 115 */ sme.setQueueLimit(
				Integer.valueOf((smeprofile.getQueueLimit() != null && smeprofile.getQueueLimit().intValue() > 0)
						? smeprofile .getQueueLimit().intValue()
						: 99));

		/* 118 */ sme.setStickyAlgo(smeprofile.getStickyAlgo());
		/* 119 */ Session session = getSession();
		/* 120 */ session.save(sme);

		/* 122 */ Users user = new Users();
		/* 123 */ user.setUsername(sme.getId().toString());
		/* 124 */ user.setPassword("Wringg@" + sme.getId() + Utility.genrateDigit(4));
		/* 125 */ user.setEnabled(Constants.BYTE_1.byteValue());
		/* 126 */ user.setUserToken(Utility.genrateToken(64));
		/* 127 */ UserRole userRole = new UserRole();
		/* 128 */ userRole.setRole("Client");
		/* 129 */ userRole.setUser(user);
		/* 130 */ session.save(userRole);

		/* 132 */ charging.setAgentAllocate(smeprofile.getAllowedAgents());
		/* 133 */ charging.setTotalAgentAllocate(sme.getAllowedAgents());
		/* 134 */ charging.setAni(sme.getLongcodeId().getLongcode().toString());
		/* 135 */ charging.setPrePost(Byte.valueOf((byte) 0));
		/* 136 */ charging.setCharegedAmt(0.0F);
		/* 137 */ charging.setInFlag(1);
		/* 138 */ charging.setInMode("0");
		/* 139 */ charging.setUserType("newuser");
		/* 140 */ charging.setStatus(Byte.valueOf((byte) 0));
		/* 141 */ charging.setSubDate(Utility.currentDateTime());
		/* 142 */ charging.setPackId(Long.valueOf(0L));
		/* 143 */ charging.setSmeId(sme);
		/* 144 */ charging.setRecording(smeprofile.getRecording());

		/* 146 */ session.save(charging);
		/* 147 */ session.flush();
		/* 148 */ new MailSender(smeprofile.getEmailId(), "Wringg Account Detail",
				"Dear User, \n \n \t This is to inform you that your account has been created successfully by Wringg, \n Please use the following credentials to login. \n \n ",
				Constants.NEW_PROFILE_END_MSG, "Login ID: " + user

						/* 150 */ .getUsername() + " \n Login Password: " + user.getPassword());
		/* 151 */ res.setSuccess(true);
		/* 152 */ res.setResult("Inserted Successfully");
		/* 153 */ return res;
	}

	public ServerResponse updateSmeProfile(AdminSmeProfileBean smeprofile) throws ParseException {
		/* 158 */ ServerResponse res = new ServerResponse();
		/* 159 */ SmeProfile profile = findByID(Long.valueOf(smeprofile.getId()));
		/* 160 */ if (profile != null && profile.getStatus().byteValue() != -9) {

			/* 162 */ if (profile.getAllowedAgents() != smeprofile.getAllowedAgents()
					&& profile/* 163 */ .getBillingStatus().byteValue() == 0) {
				/* 164 */ res.setSuccess(false);
				/* 165 */ ErrorDetails errorDetails = new ErrorDetails();
				/* 166 */ errorDetails.setErrorCode(ErrorConstants.ERROR_BILLING_PENDING.getErrorCode());
				/* 167 */ errorDetails
						.setErrorString("Update Sme" + ErrorConstants.ERROR_BILLING_PENDING.getErrorMessage());
				/* 168 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_BILLING_PENDING.getDisplayMessage());
				/* 169 */ JSONSerializer jSONSerializer = new JSONSerializer();
				/* 170 */ String result = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
				/* 171 */ res.setResult(result);
				/* 172 */ return res;
			}
			if (smeprofile.getAllowedAgents() > profile.getAllowedAgents()
					&& profile/* 175 */ .getBillingStatus().byteValue() == 3) {
				/* 176 */ res.setSuccess(false);
				/* 177 */ ErrorDetails errorDetails = new ErrorDetails();
				/* 178 */ errorDetails.setErrorCode(ErrorConstants.ERROR_FAILED_BILLING_ADDING_AGENT.getErrorCode());
				/* 179 */ errorDetails.setErrorString(
						"Update Sme" + ErrorConstants.ERROR_FAILED_BILLING_ADDING_AGENT.getErrorMessage());
				/* 180 */ errorDetails
						.setDisplayMessage(ErrorConstants.ERROR_FAILED_BILLING_ADDING_AGENT.getDisplayMessage());
				/* 181 */ JSONSerializer jSONSerializer = new JSONSerializer();
				/* 182 */ String result = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
				/* 183 */ res.setResult(result);
				/* 184 */ return res;
			}
			if (profile.getAllowedAgents() < smeprofile.getAllowedAgents()) {

				/* 188 */ profile.setBillingStatus(profile.getBillingStatus());
			} else {

				/* 191 */ profile.setBillingStatus(profile.getBillingStatus());
			}

			/* 195 */ Criteria criteria = getSession().createCriteria(SystemSnapshot.class);
			/* 196 */ criteria.addOrder(Order.desc("insertTime"));
			/* 197 */ criteria.setMaxResults(1);
			/* 198 */ SystemSnapshot snap = (SystemSnapshot) criteria.uniqueResult();
			/* 199 */ int existingCapacity = snap.getMaxAgents() - snap.getCurrentAgents();
			if (existingCapacity - smeprofile.getAllowedAgents() < 0) {
				/* 201 */ res.setSuccess(false);
				/* 202 */ ErrorDetails errorDetails = new ErrorDetails();
				/* 203 */ errorDetails.setErrorCode(ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getErrorCode());
				/* 204 */ errorDetails
						.setErrorString("Add Sme" + ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getErrorMessage());
				/* 205 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_AGENT_LIMIT_EXCEED.getDisplayMessage());
				/* 206 */ JSONSerializer jSONSerializer = new JSONSerializer();
				/* 207 */ String result = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
				/* 208 */ res.setResult(result);
				/* 209 */ return res;
			}

			/* 212 */ int addedAgents = smeprofile.getAllowedAgents() - profile.getAllowedAgents();
			/* 213 */ snap.setCurrentAgents(snap.getCurrentAgents() + addedAgents);
			/* 214 */ boolean flag = false;
			if (profile.getAllowedAgents() > smeprofile.getAllowedAgents()) {
				/* 216 */ Criteria decrement = getSession().createCriteria(AgentDetail.class);
				/* 217 */ decrement.setProjection(Projections.rowCount());
				/* 218 */ decrement.add((Criterion) Restrictions.eq("smeId", Long.valueOf(smeprofile.getId())));
				/* 219 */ decrement.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
				/* 220 */ Integer createdAgents = Integer
						.valueOf(Integer.parseInt(/* 221 */ (decrement.uniqueResult().toString() == null) ? "0"
								: decrement.uniqueResult().toString()));
				/* 222 */ SmeProfile smeProfile = (SmeProfile) getSession().get(SmeProfile.class,
						Long.valueOf(smeprofile.getId()));

				if (createdAgents.intValue() > smeprofile.getAllowedAgents()) {
					/* 225 */ res.setSuccess(false);
					/* 226 */ ErrorDetails errorDetails = new ErrorDetails();
					/* 227 */ errorDetails.setErrorCode(ErrorConstants.ERROR_CREATION_EXCEED.getErrorCode());
					/* 228 */ errorDetails
							.setErrorString("Update sme::" + ErrorConstants.ERROR_CREATION_EXCEED.getErrorMessage());
					/* 229 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_CREATION_EXCEED.getDisplayMessage());
					/* 230 */ JSONSerializer jSONSerializer = new JSONSerializer();
					/* 231 */ String str = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
					/* 232 */ res.setResult(str);
					/* 233 */ return res;
				}

				flag = true;
				snap.setCurrentAgents(
						snap.getCurrentAgents() + profile.getAllowedAgents() - smeprofile.getAllowedAgents());
			}

			/* 243 */ profile.setEmailId(smeprofile.getEmailId());
			/* 244 */ profile.setName(smeprofile.getSmeName());
			/* 245 */ profile.setZoneId((Zone) getSession().get(Zone.class, Long.valueOf(smeprofile.getZoneId())));
			/* 246 */ profile.setServiceFlag(smeprofile.getServiceFlag());
			/* 247 */ profile.setAlternateNumber(smeprofile.getAlternateNumber());
			/* 248 */ profile.setSmeMobile(smeprofile.getSmeMobile());

			if (flag) {
				profile.setAllowedAgents(smeprofile.getAllowedAgents());
			} else {
				profile.setAllowedAgents(profile.getAllowedAgents());
			}
			/* 255 */ profile.setStatus(smeprofile.getStatus());
			/* 256 */ profile.setBalance(smeprofile.getBalance());
			/* 257 */ profile.setRecording(smeprofile.getRecording());
			/* 258 */ profile.setMasking(smeprofile.getMasking());
			/* 259 */ profile.setVoicemail(smeprofile.getVoicemail());
			/* 260 */ profile.setSelectionAlgo(smeprofile.getSelectionAlgo());
			/* 261 */ profile.setBizAddress(smeprofile.getBizAddress());
			/* 262 */ profile.setLanguage(smeprofile.getLanguage());
			/* 263 */ profile.setRecValidity(smeprofile.getRecValidity());
			profile.setInPermissionFlag(smeprofile.getInPermissionFlag());
			profile.setOutPermissionFlag(smeprofile.getOutPermissionFlag());
			profile.setEodReportFlag(smeprofile.getEodReportFlag());
			profile.setRoutingType(smeprofile.getRoutingType());
			profile.setInChannels(smeprofile.getInChannels());
			profile.setInQueueChannels(smeprofile.getInQueueChannels());
			profile.setOutChannels(smeprofile.getOutChannels());
			profile.setGuiTimer(smeprofile.getGuiTimer());
			profile.setAgentRelaxTime(smeprofile.getAgentRelaxTime());

			profile.setQueueLimit(
					Integer.valueOf((smeprofile.getQueueLimit() != null && smeprofile.getQueueLimit().intValue() > 0)
							? smeprofile/* 265 */ .getQueueLimit().intValue()
							: 99));

			/* 267 */ profile.setStickyAlgo(smeprofile.getStickyAlgo());

			if (addedAgents > 0) {
				/* 270 */ MpbxCharging charging = new MpbxCharging();
				/* 271 */ charging.setAgentAllocate(addedAgents);
				/* 272 */ charging.setTotalAgentAllocate(profile.getAllowedAgents());
				/* 273 */ charging.setAni(profile.getLongcodeId().getLongcode().toString());
				/* 274 */ charging.setPrePost(Byte.valueOf((byte) 0));
				/* 275 */ charging.setCharegedAmt(0.0F);
				/* 276 */ charging.setInFlag(1);
				/* 277 */ charging.setInMode("0");
				/* 278 */ charging.setUserType("topup");
				/* 279 */ charging.setStatus(Byte.valueOf((byte) 0));
				/* 280 */ charging.setRecording(smeprofile.getRecording());
				/* 281 */ charging.setSubDate(Utility.currentDateTime());
				/* 282 */ charging.setPackId(Long.valueOf(0L));
				/* 283 */ charging.setSmeId(profile);

				/* 285 */ getSession().save(charging);
			}
			/* 287 */ res.setSuccess(true);
			/* 288 */ res.setResult("Updated");
			/* 289 */ return res;
		}

		/* 292 */ res.setSuccess(false);
		/* 293 */ ErrorDetails error = new ErrorDetails();
		/* 294 */ error.setErrorCode(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorCode());
		/* 295 */ error.setErrorString("Update sme::" + ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorMessage());
		/* 296 */ error.setDisplayMessage(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getDisplayMessage());
		/* 297 */ JSONSerializer serializer = new JSONSerializer();
		/* 298 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 299 */ res.setResult(response);
		/* 300 */ return res;
	}

	public ServerResponse deleteSmeProfile(Long id) {
		/* 306 */ ServerResponse res = new ServerResponse();
		/* 307 */ SmeProfile profile = findByID(id);
		/* 308 */ if (profile != null && profile.getStatus().byteValue() != -9) {
			/* 309 */ profile.setStatus(Byte.valueOf((byte) -9));
			/* 310 */ Users users = this.adminDao.getUserInfo(id.toString());
			/* 311 */ if (users != null) {
				/* 312 */ users.setEnabled((byte) -9);
			}
			/* 314 */ res.setSuccess(true);
			/* 315 */ res.setResult("Successfully Deleted");
		} else {
			/* 317 */ res.setSuccess(false);
			/* 318 */ ErrorDetails error = new ErrorDetails();
			/* 319 */ error.setErrorCode(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorCode());
			/* 320 */ error.setErrorString("createAgent::" + ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorMessage());
			/* 321 */ error.setDisplayMessage(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getDisplayMessage());
			/* 322 */ JSONSerializer serializer = new JSONSerializer();
			/* 323 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 324 */ res.setResult(response);
		}
		/* 326 */ return res;
	}

	public List<SmeProfile> findAllSMEs(StartEndDateRequest filter) {
		/* 342 */ ManageClientPageRequest filterPage = new ManageClientPageRequest();
		/* 348 */ List<FilterBean> beanList = new ArrayList<>();
		/* 349 */ FilterBean beanObj = new FilterBean();
		/* 350 */ beanObj.setName("insertTime");
		/* 351 */ beanObj.setVal(Utility.getFormattedDateTime(filter.getStartDate(), "yyyy-MM-dd"));
		/* 352 */ beanObj.setOp(Integer.valueOf(34));
		/* 353 */ beanList.add(beanObj);
		/* 354 */ FilterBean beanObject = new FilterBean();
		/* 355 */ beanObject.setName("insertTime");
		/* 356 */ beanObject.setVal(Utility.getFormattedDateTime(filter.getEndDate(), "yyyy-MM-dd"));
		/* 357 */ beanObject.setOp(Integer.valueOf(33));
		/* 358 */ beanList.add(beanObject);
		/* 359 */ return findAllSMEs(" from SmeProfile sp left join fetch sp.longcodeId left join fetch sp.zoneId " +
		/* 360 */ createQueryForSME(filterPage), Integer.valueOf(-1), Integer.valueOf(-1));
	}

	public SmeProfile findByID(Long id) {
		/* 365 */ return (SmeProfile) getSession().get(SmeProfile.class, id);
	}

	public List<SmeProfile> findAllSMEs(String hql, Integer initialRecord, Integer batchSize) {
		/* 371 */ Query<SmeProfile> qry = getSession().createQuery(hql);
		/* 372 */ if (initialRecord.intValue() > 0)
			/* 373 */ qry.setFirstResult(initialRecord.intValue() - 1);
		/* 374 */ if (batchSize.intValue() > 0)
			/* 375 */ qry.setMaxResults(batchSize.intValue());
		/* 376 */ return qry.list();
	}

	public String createQueryForSME(ManageClientPageRequest filter) {
		/* 381 */ String hql = "";
		/* 382 */ hql = hql + " where sp.status != '-9' ";
		/* 383 */ if (filter.getFilterList() != null) {
			/* 384 */ for (FilterBean restrictionFilter : filter.getFilterList()) {
				/* 385 */ restrictionFilter.setName(getNameForSmeProfile(restrictionFilter.getName()));
				/* 386 */ hql = hql + " and " + Utility.genericFilterCriteria(restrictionFilter);
			}
		}

		/* 390 */ return hql;
	}

	public Long getRowCount(String hql) {
		/* 394 */ hql = "select count(sp.id) " + hql;
		/* 395 */ Query<Long> qry = getSession().createQuery(hql);
		/* 396 */ return (Long) qry.uniqueResult();
	}

	private String getNameForSmeProfile(String column) {
		/* 400 */ String result = "sp.";
		/* 401 */ switch (column) {
		case "id":
			/* 403 */ result = result + "id";

			/* 423 */ return result;
		case "smeName":
			result = result + "name";
			return result;
		case "startDate":
			result = result + "insertTime";
			return result;
		case "endDate":
			result = result + "insertTime";
			return result;
		case "zoneName":
			result = "sp.zoneId.zoneName";
			return result;
		case "longCode":
			result = "sp.longcodeId.longcode";
			return result;
		}
		result = result + column;
		return result;
	}
}
