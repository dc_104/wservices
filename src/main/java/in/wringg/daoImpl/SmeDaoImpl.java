/*      */
package in.wringg.daoImpl;

/*      */
/*      */ import flexjson.JSONSerializer;
/*      */ import in.wringg.dao.AbstractDao;
/*      */ import in.wringg.dao.SmeDao;
/*      */ import in.wringg.email.MailSender;
/*      */ import in.wringg.entity.AgentDetail;
/*      */ import in.wringg.entity.AgentGroupDetail;
/*      */ import in.wringg.entity.AgentReportDetail;
/*      */ import in.wringg.entity.AlertNotification;
/*      */ import in.wringg.entity.CallBaseHistory;
/*      */ import in.wringg.entity.CallSchedulerBase;
/*      */ import in.wringg.entity.ForgetPassword;
/*      */ import in.wringg.entity.IncomingIvrCallCdr;
/*      */ import in.wringg.entity.MissedCallMarketing;
/*      */ import in.wringg.entity.MpbxCallRecording;
/*      */ import in.wringg.entity.MpbxCategoryMaster;
/*      */ import in.wringg.entity.OutbondIvrCampaign;
/*      */ import in.wringg.entity.OutbondIvrCdr;
/*      */ import in.wringg.entity.RevenueDetail;
/*      */ import in.wringg.entity.SmeComplaint;
/*      */ import in.wringg.entity.SmeProfile;
import in.wringg.entity.SmePrompts;
/*      */ import in.wringg.entity.SmeSmsDetail;
/*      */ import in.wringg.entity.SmsCampaign;
/*      */ import in.wringg.entity.SmsContent;
/*      */ import in.wringg.entity.Users;
/*      */ import in.wringg.entity.VoicemailCallCdr;
/*      */ import in.wringg.pojo.AddBulkSmsBean;
/*      */ import in.wringg.pojo.AddOutBoundIvrBean;
/*      */ import in.wringg.pojo.AgentGroupDetailBean;
/*      */ import in.wringg.pojo.CallCdrResponseBean;
/*      */ import in.wringg.pojo.FilterBean;
/*      */ import in.wringg.pojo.IvrCallFlowBean;
/*      */ import in.wringg.pojo.LoginBean;
/*      */ import in.wringg.pojo.ManageClientPageRequest;
/*      */ import in.wringg.pojo.MissCallMarketingBean;
/*      */ import in.wringg.pojo.PassswordTokenBean;
import in.wringg.pojo.PromptDetailsBean;
/*      */ import in.wringg.pojo.ServerResponse;
/*      */ import in.wringg.pojo.SmeAgentDetailBean;
/*      */ import in.wringg.pojo.SmeCallDetailBean;
/*      */ import in.wringg.pojo.SmeComplaintBean;
/*      */ import in.wringg.pojo.SmeDashboardSystemDetail;
/*      */ import in.wringg.pojo.StartEndDateRequest;
/*      */ import in.wringg.pojo.UploadedBaseDesc;
/*      */ import in.wringg.utility.Constants;
/*      */ import in.wringg.utility.ErrorConstants;
/*      */ import in.wringg.utility.ErrorDetails;
/*      */ import in.wringg.utility.Utility;
/*      */ import java.io.File;
/*      */ import java.io.FileNotFoundException;
/*      */ import java.io.IOException;
/*      */ import java.io.Serializable;
/*      */ import java.math.BigInteger;
/*      */ import java.nio.file.CopyOption;
/*      */ import java.nio.file.Files;
/*      */ import java.nio.file.Path;
/*      */ import java.nio.file.Paths;
/*      */ import java.nio.file.StandardCopyOption;
/*      */ import java.nio.file.attribute.FileAttribute;
/*      */ import java.text.DateFormat;
/*      */ import java.text.ParseException;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Date;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ import java.util.Map;
/*      */ import java.util.Set;
/*      */ import java.util.UUID;
/*      */ import org.apache.commons.io.FileUtils;
/*      */ import org.hibernate.Criteria;
/*      */ import org.hibernate.Session;
/*      */ import org.hibernate.criterion.Criterion;
/*      */ import org.hibernate.criterion.Order;
/*      */ import org.hibernate.criterion.Projection;
/*      */ import org.hibernate.criterion.Projections;
/*      */ import org.hibernate.criterion.Restrictions;
/*      */ import org.hibernate.query.Query;
/*      */ import org.springframework.stereotype.Repository;

/*      */
/*      */
/*      */ @Repository("smeDao")
/*      */ public class SmeDaoImpl/*      */ extends AbstractDao<Serializable, Void>/*      */ implements SmeDao
/*      */ {
	/*      */ private Utility.getServiceName serviceName;

	public SmeDashboardSystemDetail smeDashboardDetail(StartEndDateRequest smeDetail, Long id) {
		/* 91 */ SmeDashboardSystemDetail smeDashboard = new SmeDashboardSystemDetail();
		Criteria criteria = getSession().createCriteria(RevenueDetail.class);
		criteria.setProjection(
				(Projection) Projections.projectionList().add((Projection) Projections.groupProperty("serviceId"))
						.add((Projection) Projections.sum("totalCalls")));
		/* 96 */ criteria.add(Restrictions.between("dateTime", smeDetail.getStartDate(), smeDetail.getEndDate()));
		/* 97 */ criteria.add((Criterion) Restrictions.eq("smeId", id));
		/* 98 */ List<Object[]> list = criteria.list();
		/* 99 */ List<SmeCallDetailBean> callDetailList = new ArrayList<>();
		/* 100 */ Map<Integer, Long> callDetailMap = new HashMap<>();
		for (Utility.getServiceName serviceCode : Utility.getServiceName.values()) {
			callDetailMap.put(Integer.valueOf(serviceCode.ordinal() + 1), Long.valueOf(0L));
		}
		callDetailMap.put(Integer.valueOf(15), Long.valueOf((callDetailMap.get(Integer.valueOf(15)) == null) ? 0L
				: ((Long) callDetailMap.get(Integer.valueOf(15))).longValue()));
		callDetailMap.put(Integer.valueOf(25), Long.valueOf((callDetailMap.get(Integer.valueOf(25)) == null) ? 0L
				: ((Long) callDetailMap.get(Integer.valueOf(25))).longValue()));
		callDetailMap.put(Integer.valueOf(16), Long.valueOf((callDetailMap.get(Integer.valueOf(16)) == null) ? 0L
				: ((Long) callDetailMap.get(Integer.valueOf(16))).longValue()));
		callDetailMap.put(Integer.valueOf(26), Long.valueOf((callDetailMap.get(Integer.valueOf(26)) == null) ? 0L
				: ((Long) callDetailMap.get(Integer.valueOf(26))).longValue()));
		callDetailMap.put(Integer.valueOf(11), Long.valueOf((callDetailMap.get(Integer.valueOf(11)) == null) ? 0L
				: ((Long) callDetailMap.get(Integer.valueOf(11))).longValue()));
		
		callDetailMap.put(3,0L);
		for (Object[] o : list) {
			int x = ((Integer) o[0]).intValue();
			if (x == 11) {
				Long calls = callDetailMap.get(Integer.valueOf(x));
				callDetailMap.put(Integer.valueOf(x), Long.valueOf(calls.longValue() + ((Long) o[1]).longValue()));
			}
			x = (x == 11) ? 1 : x;
			if (callDetailMap.containsKey(Integer.valueOf(x))) {
				Long calls = callDetailMap.get(Integer.valueOf(x));
				callDetailMap.put(Integer.valueOf(x), (calls + (Long) o[1]));
			}
		}
		for (Integer key : callDetailMap.keySet()) {
			/* 128 */ SmeCallDetailBean obj = new SmeCallDetailBean();
			/* 129 */ List<SmeCallDetailBean> sub = new ArrayList<>();
			/*      */
			if (key.intValue() != 11 && key.intValue() < 12 && key != 3) {
				this.serviceName = Utility.getServiceName.values()[key.intValue() - 1];
			}
			obj.setName((this.serviceName == null) ? "none" : this.serviceName.toString());
			obj.setValue(Integer.valueOf(((Long) callDetailMap.get(key)).intValue()));
			if (key.intValue() == 1) {
				SmeCallDetailBean callSubAns = new SmeCallDetailBean();
				callSubAns.setName("Failed");
				callSubAns.setValue(Integer.valueOf(((Long) callDetailMap.get(Integer.valueOf(15))).intValue()));

				SmeCallDetailBean callSubNotAns = new SmeCallDetailBean();
				callSubNotAns.setName("Answered");
				callSubNotAns.setValue(Integer.valueOf(((Long) callDetailMap.get(Integer.valueOf(16))).intValue()));

				sub.add(callSubAns);
				sub.add(callSubNotAns);
			}
			if (key.intValue() == 2) {
				SmeCallDetailBean callSubAns = new SmeCallDetailBean();
				callSubAns.setName("Failed");
				callSubAns.setValue(Integer.valueOf(((Long) callDetailMap.get(Integer.valueOf(25))).intValue()));
				SmeCallDetailBean callSubNotAns = new SmeCallDetailBean();
				callSubNotAns.setName("Answered");
				callSubNotAns.setValue(Integer.valueOf(((Long) callDetailMap.get(Integer.valueOf(26))).intValue()));
				SmeCallDetailBean callUnknown = new SmeCallDetailBean();
				callUnknown.setName("Unreachable");
				callUnknown.setValue(Integer
						.valueOf(obj.getValue().intValue() - ((Long) callDetailMap.get(Integer.valueOf(25))).intValue()
								- ((Long) callDetailMap.get(Integer.valueOf(26))).intValue()));
				sub.add(callSubAns);
				sub.add(callSubNotAns);
			}
			if (key.intValue() == 11) {
				obj.setName("Voicemail");
				obj.setValue(Integer.valueOf(((Long) callDetailMap.get(key)).intValue()));
			}
			if (key.intValue() == 3) {
				obj.setName("Recording");
				obj.setValue(Integer.valueOf(((Long) callDetailMap.get(key)).intValue()));
			}
			if (key.intValue() < 12) {
				obj.setSubs(sub);
				callDetailList.add(obj);
			}
		}
		smeDashboard.setCallDetail(callDetailList);
		return smeDashboard;
	}

	/*      */
	/*      */
	/*      */ public SmeAgentDetailBean smeDashboadForAgent(Long id) {
		/* 267 */ Criteria criteriaAgent = getSession().createCriteria(SmeProfile.class);
		/* 268 */ criteriaAgent.setProjection(
				(Projection) Projections.projectionList().add((Projection) Projections.property("allowedAgents")));
		/* 269 */ criteriaAgent.add((Criterion) Restrictions.eq("id", id));
		/*      */
		/* 271 */ Integer totalAgent = (Integer) criteriaAgent.uniqueResult();
		/* 272 */ Criteria criteriaAgentOccupy = getSession().createCriteria(AgentDetail.class);
		/* 273 */ criteriaAgentOccupy.setProjection(/* 274 */ (Projection) Projections.projectionList()
				.add((Projection) Projections.groupProperty("status")).add(Projections.rowCount()));
		/* 275 */ criteriaAgentOccupy.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 276 */ criteriaAgentOccupy.add((Criterion) Restrictions.eq("smeId", id));
		/*      */
		/* 278 */ List<Object[]> objects = criteriaAgentOccupy.list();
		/* 279 */ SmeAgentDetailBean agentDetail = new SmeAgentDetailBean();
		/* 280 */ Integer registeredAgent = Integer.valueOf(0);
		for (Object[] agentObj : objects) {
			registeredAgent = Integer.valueOf(registeredAgent.intValue()
					+ Integer.parseInt((agentObj[1].toString() == null) ? "0" : agentObj[1].toString()));
			switch (((Byte) agentObj[0]).byteValue()) {
			case 1:
				agentDetail.setFreeAgents(Integer
						.valueOf(Integer.parseInt((agentObj[1].toString() == null) ? "0" : agentObj[1].toString())));
				break;
			case 2:
				agentDetail.setBusyAgents(Integer
						.valueOf(Integer.parseInt((agentObj[1].toString() == null) ? "0" : agentObj[1].toString())));
				break;
			}
		}
		/* 296 */ agentDetail.setFreeAgents(
				Integer.valueOf((agentDetail.getFreeAgents() == null) ? 0 : agentDetail.getFreeAgents().intValue()));
		/* 297 */ agentDetail.setBusyAgents(
				Integer.valueOf((agentDetail.getBusyAgents() == null) ? 0 : agentDetail.getBusyAgents().intValue()));
		/* 298 */ agentDetail.setTotalAgents(Integer.valueOf((totalAgent == null) ? 0 : totalAgent.intValue()));
		/* 299 */ agentDetail.setRegisteredAgents(registeredAgent);
		/* 300 */ agentDetail
				.setUnregisteredAgents(Integer.valueOf(totalAgent.intValue() - registeredAgent.intValue()));
		/* 301 */ return agentDetail;
		/*      */ }

	/*      */
	/*      */
	/*      */ public void addSmsCampaign(AddBulkSmsBean bean, Long id) throws IOException {
		/* 306 */ SmeSmsDetail smeSmsDetail = new SmeSmsDetail();
		/* 307 */ SmsCampaign smsCampaign = new SmsCampaign();
		/* 308 */ SmsContent smsContent = new SmsContent();
		/*      */
		/* 310 */ File sourceFile = new File(bean.getFilePath());
		/* 311 */ File destinationDir = new File(Constants.UPLOAD_BASE_PATH + id + "/");
		/* 312 */ FileUtils.moveFileToDirectory(sourceFile, destinationDir, true);
		/* 313 */ smsCampaign.setBaseFile(destinationDir.getAbsolutePath() + File.separator + sourceFile.getName());
		/*      */
		/* 315 */ smsContent.setMessage(bean.getSmsContent());
		/* 316 */ smsCampaign.setSmsType(bean.getSmsContentType());
		/* 317 */ smsCampaign.setInsertDate(bean.getDate());
		/* 318 */ smsCampaign.setStartTime(bean.getStartTime());
		/* 319 */ smsCampaign.setEndTime(bean.getEndTime());
		/* 320 */ smsCampaign.setCli(bean.getCli());
		/* 321 */ smsCampaign.setMsgId(smsContent);
		/* 322 */ smsCampaign.setBaseCount(bean.getBaseCount().intValue());
		/* 323 */ smeSmsDetail.setSmeId((SmeProfile) getSession().get(SmeProfile.class, id));
		/* 324 */ smeSmsDetail.setCampaignId(smsCampaign);
		/* 325 */ smeSmsDetail.setBaseCount(bean.getBaseCount().intValue());
		/* 326 */ getSession().save(smeSmsDetail);
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AddBulkSmsBean> getBulkSmslist(String id) {
		/* 331 */ List<AddBulkSmsBean> getBulkSmsBeans = new ArrayList<>();
		/* 332 */ Criteria criteria = getSession().createCriteria(SmeSmsDetail.class);
		/* 333 */ criteria.add((Criterion) Restrictions.eq("smeId.id", Long.valueOf(Long.parseLong(id))));
		/* 334 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/*      */
		/* 336 */ List<SmeSmsDetail> list = criteria.list();
		/* 337 */ for (SmeSmsDetail obj : list) {
			/* 338 */ AddBulkSmsBean addBulkSmsBean = new AddBulkSmsBean();
			/* 339 */ addBulkSmsBean.setCampaignId(obj.getCampaignId().getId());
			/* 340 */ addBulkSmsBean.setCli(obj.getCampaignId().getCli());
			/* 341 */ addBulkSmsBean.setDate(obj.getCampaignId().getInsertDate());
			/* 342 */ addBulkSmsBean.setEndTime(obj.getCampaignId().getEndTime());
			/* 343 */ addBulkSmsBean.setStartTime(obj.getCampaignId().getStartTime());
			/* 344 */ addBulkSmsBean.setSmsContent(obj.getCampaignId().getMsgId().getMessage());
			/* 345 */ addBulkSmsBean.setSmsContentType(obj.getCampaignId().getSmsType());
			/* 346 */ addBulkSmsBean.setFilePath(obj.getCampaignId().getBaseFile());
			/* 347 */ addBulkSmsBean.setBaseCount(Integer.valueOf(obj.getCampaignId().getBaseCount()));
			/* 348 */ addBulkSmsBean.setStatus(obj.getCampaignId().getStatus());
			/* 349 */ getBulkSmsBeans.add(addBulkSmsBean);
			/*      */ }
		/*      */
		/* 352 */ return getBulkSmsBeans;
		/*      */ }

	/*      */
	/*      */
	/*      */ public AddBulkSmsBean fetchBulkSmsById(String id, Long campaignId) {
		/* 357 */ Criteria criteria = getSession().createCriteria(SmeSmsDetail.class);
		/* 358 */ criteria.add((Criterion) Restrictions.eq("smeId.id", Long.valueOf(Long.parseLong(id))));
		/* 359 */ criteria.add((Criterion) Restrictions.eq("campaignId.id", campaignId));
		/* 360 */ SmeSmsDetail obj = (SmeSmsDetail) criteria.uniqueResult();
		/* 361 */ AddBulkSmsBean addBulkSmsBean = new AddBulkSmsBean();
		/* 362 */ addBulkSmsBean.setCampaignId(campaignId);
		/* 363 */ addBulkSmsBean.setCli(obj.getCampaignId().getCli());
		/* 364 */ addBulkSmsBean.setDate(obj.getCampaignId().getInsertDate());
		/* 365 */ addBulkSmsBean.setEndTime(obj.getCampaignId().getEndTime());
		/* 366 */ addBulkSmsBean.setSmsContent(obj.getCampaignId().getMsgId().getMessage());
		/* 367 */ addBulkSmsBean.setSmsContentType(obj.getCampaignId().getSmsType());
		/* 368 */ addBulkSmsBean.setStartTime(obj.getCampaignId().getStartTime());
		/* 369 */ addBulkSmsBean.setBaseCount(Integer.valueOf(obj.getCampaignId().getBaseCount()));
		/* 370 */ addBulkSmsBean.setFilePath(obj.getCampaignId().getBaseFile());
		/* 371 */ addBulkSmsBean.setStatus(obj.getCampaignId().getStatus());
		/* 372 */ return addBulkSmsBean;
		/*      */ }

	/*      */
	/*      */
	/*      */ public void deleteBulkSmsList(String id, Long campaignId) {
		/* 377 */ Criteria criteria = getSession().createCriteria(SmeSmsDetail.class);
		/* 378 */ criteria.add((Criterion) Restrictions.eq("smeId.id", Long.valueOf(Long.parseLong(id))));
		/* 379 */ criteria.add((Criterion) Restrictions.eq("campaignId.id", campaignId));
		/*      */
		/* 381 */ SmeSmsDetail obj = (SmeSmsDetail) criteria.uniqueResult();
		/* 382 */ if (obj != null) {
			/* 383 */ obj.setStatus((byte) -9);
			/* 384 */ obj.getCampaignId().setStatus((byte) -9);
			/*      */ }
		/*      */ }

	/*      */
	/*      */
	/*      */ public void addIvrManager(List<IvrCallFlowBean> beanList, String id) throws IOException {
		/* 390 */ Session session = getSession();
		/* 391 */ deleteIvrManger(id, session);
		/* 392 */ Iterator<IvrCallFlowBean> itr = beanList.iterator();
		/* 393 */ while (itr.hasNext()) {
			/*      */
			/* 395 */ IvrCallFlowBean obj = itr.next();
			/* 396 */ MpbxCategoryMaster master = new MpbxCategoryMaster();
			/* 397 */ if (obj.getEventType().equals(Constants.FILE_CHECK)
					&& (obj/* 398 */ .getMediaFilePath() == null || obj.getMediaFilePath().trim().isEmpty()))
				/* 399 */ throw new IOException("Media file not specified");
			/* 400 */ if (obj.getMediaFilePath() != null && !obj.getMediaFilePath().trim().isEmpty()) {
				/* 401 */ File sourceFile = new File(obj.getMediaFilePath());
				/*      */
				/* 403 */ File destinationDir = new File(Constants.UPLOAD_BASE_PATH + id + "/" + sourceFile.getName());
				/* 404 */ Path path = Paths.get(Constants.UPLOAD_BASE_PATH + id, new String[0]);
				/* 405 */ if (!Files.exists(path, new java.nio.file.LinkOption[0])) {
					/* 406 */ Files.createDirectory(path, (FileAttribute<?>[]) new FileAttribute[0]);
					/*      */ }
				/* 408 */ Files.copy(sourceFile.toPath(), destinationDir.toPath(),
						new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
				/* 409 */ obj.setMediaFilePath(destinationDir.getAbsolutePath());
				/*      */ }
			/* 411 */ master.setCatDesc(obj.getCatDescription());
			/* 412 */ master.setCatId(obj.getCatId());
			/* 413 */ master.setChildren(obj.getChild());
			/* 414 */ master.setDtmf(obj.getDtmf());
			/* 415 */ master.setEventType(obj.getEventType());
			/* 416 */ master.setMediaFile(obj.getMediaFilePath());
			/* 417 */ master.setMediaFileStatus(obj.getMediaFileStatus());
			/* 418 */ master.setParentCatId(obj.getParentId());
			/* 419 */ master.setServiceType(obj.getServicrType());
			/* 420 */ master.setSmeId(id);
			/* 421 */ master.setTitle(obj.getCatTitle());
			/* 422 */ master.setType(obj.getType());
			/* 423 */ master.setDateTime(new Date());
			/* 424 */ session.save(master);
			/* 425 */ session.flush();
			/*      */ }
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */
	/*      */ public List<IvrCallFlowBean> getIvrManger(String id) {
		/* 433 */ List<IvrCallFlowBean> flowList = new ArrayList<>();
		/*      */
		/* 435 */ Criteria criteria = getSession().createCriteria(MpbxCategoryMaster.class);
		/* 436 */ criteria.add((Criterion) Restrictions.eq("smeId", id));
		/* 437 */ criteria.addOrder(Order.asc("id"));
		/* 438 */ List<MpbxCategoryMaster> list = criteria.list();
		/* 439 */ for (MpbxCategoryMaster mpbxCategoryMaster : list) {
			/* 440 */ IvrCallFlowBean callFlowBean = new IvrCallFlowBean();
			/* 441 */ callFlowBean.setId(mpbxCategoryMaster.getId().toString());
			/* 442 */ callFlowBean.setEventType(mpbxCategoryMaster.getEventType());
			/* 443 */ callFlowBean.setCatDescription(mpbxCategoryMaster.getCatDesc());
			/* 444 */ callFlowBean.setCatId(mpbxCategoryMaster.getCatId());
			/* 445 */ callFlowBean.setCatTitle(mpbxCategoryMaster.getTitle());
			/* 446 */ callFlowBean.setChild(mpbxCategoryMaster.getChildren());
			/* 447 */ callFlowBean.setDtmf(mpbxCategoryMaster.getDtmf());
			/* 448 */ callFlowBean.setMediaFilePath(mpbxCategoryMaster.getMediaFile());
			/* 449 */ callFlowBean.setMediaFileStatus(mpbxCategoryMaster.getMediaFileStatus());
			/* 450 */ callFlowBean.setParentId(mpbxCategoryMaster.getParentCatId());
			/* 451 */ callFlowBean.setServicrType(mpbxCategoryMaster.getServiceType());
			/*      */
			/* 453 */ callFlowBean.setType(mpbxCategoryMaster.getType());
			/* 454 */ callFlowBean.setServicrType(mpbxCategoryMaster.getServiceType());
			/* 455 */ flowList.add(callFlowBean);
			/*      */ }
		/*      */
		/* 458 */ return flowList;
		/*      */ }

	/*      */
	/*      */
	/*      */ public boolean isIvrManger(String id) {
		/* 463 */ Criteria criteria = getSession().createCriteria(MpbxCategoryMaster.class);
		/* 464 */ criteria.add((Criterion) Restrictions.eq("smeId", id));
		/* 465 */ if (criteria.list() != null && criteria.list().size() > 0) {
			/* 466 */ return true;
			/*      */ }
		/*      */
		/* 469 */ return false;
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */ public void deleteIvrManger(String smeId, Session session) {
		/* 475 */ String hql = "delete from MpbxCategoryMaster where smeId= :smeId";
		/* 476 */ session.createQuery(hql).setParameter("smeId", smeId).executeUpdate();
		/*      */ }

	/*      */
	/*      */
	/*      */ public void updateSmsCampaign(AddBulkSmsBean bean, Long id, Long campaignId) {
		/* 481 */ Criteria criteria = getSession().createCriteria(SmsCampaign.class);
		/* 482 */ criteria.add((Criterion) Restrictions.eq("id", campaignId));
		/*      */
		/* 484 */ SmsCampaign detail = (SmsCampaign) criteria.uniqueResult();
		/* 485 */ if (detail != null) {
			/* 486 */ detail.setBaseCount(bean.getBaseCount().intValue());
			/* 487 */ detail.setBaseFile(bean.getFilePath());
			/* 488 */ detail.setCli(bean.getCli());
			/* 489 */ detail.setEndTime(bean.getEndTime());
			/* 490 */ detail.setInsertDate(bean.getDate());
			/* 491 */ detail.setSmsType(bean.getSmsContentType());
			/* 492 */ detail.getMsgId().setMessage(bean.getSmsContent());
			/* 493 */ detail.setStartTime(bean.getStartTime());
			/*      */ }
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<MissCallMarketingBean> getMissCallList(StartEndDateRequest bean, long id) {
		/* 499 */ List<MissCallMarketingBean> marketingBeans = new ArrayList<>();
		/* 500 */ Criteria criteria = getSession().createCriteria(MissedCallMarketing.class);
		/* 501 */ criteria.add((Criterion) Restrictions.eq("smeId.id", Long.valueOf(id)));
		/* 502 */ if (bean != null && bean.getStartDate() != null && bean.getEndDate() != null) {
			/* 503 */ criteria.add(Restrictions.between("startDateTime", bean.getStartDate(), bean.getEndDate()));
			/*      */ }
		/* 505 */ List<MissedCallMarketing> cdrList = criteria.list();
		/*      */
		/* 507 */ for (MissedCallMarketing cdr : cdrList) {
			/* 508 */ MissCallMarketingBean missCallMarketingBean = new MissCallMarketingBean();
			/* 509 */ missCallMarketingBean.setAgentName("a1");
			/* 510 */ missCallMarketingBean/* 511 */ .setAsignedAgent(
					Integer.valueOf((cdr.getPatchedAgentId() == null) ? 0 : Integer.parseInt(cdr.getPatchedAgentId())));
			/* 512 */ missCallMarketingBean.setCallpartyNumber(cdr.getCallingNumber());
			/* 513 */ missCallMarketingBean.setDateTime(cdr.getStartDateTime());
			/* 514 */ missCallMarketingBean.setLongCode(cdr.getLongcode());
			/* 515 */ missCallMarketingBean.setMissCallid(cdr.getId().toString());
			/* 516 */ missCallMarketingBean.setStatus(cdr.getCdrMode());
			/* 517 */ marketingBeans.add(missCallMarketingBean);
			/*      */ }
		/*      */
		/* 520 */ return marketingBeans;
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AgentGroupDetailBean> getAgentGroupDetail(Long smeId) {
		/* 525 */ List<AgentGroupDetailBean> groupDetailBeans = new ArrayList<>();
		/* 526 */ Criteria criteria = getSession().createCriteria(AgentGroupDetail.class);
		/* 527 */ criteria.add((Criterion) Restrictions.ne("groupStatus", Byte.valueOf((byte) -9)));
		/* 528 */ List<AgentGroupDetail> groupDetails = criteria.list();
		/*      */
		/* 530 */ for (AgentGroupDetail obj : groupDetails) {
			/* 531 */ AgentGroupDetailBean groupDetailBean = new AgentGroupDetailBean();
			/* 532 */ groupDetailBean.setAgentGroupId(obj.getGroupId().toString());
			/* 533 */ groupDetailBean.setAgentGroupName(obj.getGroupName());
			/*      */
			/* 535 */ groupDetailBeans.add(groupDetailBean);
			/*      */ }
		/*      */
		/* 538 */ return groupDetailBeans;
		/*      */ }

	/*      */
	/*      */
	/*      */ public void addSmeComplaint(SmeComplaintBean bean, Long id) {
		/* 543 */ SmeComplaint smeComplaint = transformSmeComplaint(bean);
		/* 544 */ getSession().save(smeComplaint);
		/*      */ }

	/*      */
	/*      */
	/*      */ public ServerResponse updateSmeComplaint(SmeComplaintBean bean, Long id) {
		/* 549 */ ServerResponse res = new ServerResponse();
		/* 550 */ SmeComplaint smeComplaint = fetchSmeComplaintById(bean.getId());
		/* 551 */ if (smeComplaint != null && smeComplaint.getStatus().byteValue() != -9) {
			/* 552 */ transformSmeComplaint(bean);
			/* 553 */ res.setSuccess(true);
			/* 554 */ res.setResult("Successfully Updated");
			/*      */ } else {
			/* 556 */ res.setSuccess(false);
			/* 557 */ ErrorDetails error = new ErrorDetails();
			/* 558 */ error.setErrorCode(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getErrorCode());
			/* 559 */ error.setErrorString(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getErrorMessage());
			/* 560 */ error.setDisplayMessage(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getDisplayMessage());
			/* 561 */ JSONSerializer serializer = new JSONSerializer();
			/* 562 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 563 */ res.setResult(response);
			/*      */ }
		/*      */
		/* 566 */ return res;
		/*      */ }

	/*      */
	/*      */
	/*      */ public SmeComplaint transformSmeComplaint(SmeComplaintBean bean) {
		/* 571 */ SmeComplaint smeComplaint = (bean.getId() == null) ? new SmeComplaint()
				: fetchSmeComplaintById(bean.getId());
		/* 572 */ smeComplaint.setAssigneeName(bean.getAssigneeName());
		/* 573 */ smeComplaint.setCategory(bean.getCategory().intValue());
		/* 574 */ smeComplaint.setComplaintDateTime(bean.getComplaintDateTime());
		/* 575 */ smeComplaint.setComplaintDetail(bean.getComplaintDetail());
		/* 576 */ smeComplaint.setEmailId(bean.getEmailId());
		/* 577 */ smeComplaint.setParentId(bean.getParentId());
		/* 578 */ smeComplaint.setSmeId((SmeProfile) getSession().get(SmeProfile.class, bean.getSmeId()));
		/* 579 */ smeComplaint.setSubject(bean.getSubject());
		/* 580 */ smeComplaint.setSolution(bean.getSolution());
		/* 581 */ smeComplaint.setStatus(bean.getStatus());
		/* 582 */ return smeComplaint;
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<SmeComplaint> getSmeComplaints(Long id) {
		/* 587 */ List<SmeComplaint> smeComplaints = new ArrayList<>();
		/* 588 */ Criteria criteria = getSession().createCriteria(SmeComplaint.class);
		/* 589 */ if (id.longValue() != 0L)
			/* 590 */ criteria.add((Criterion) Restrictions.eq("smeId.id", id));
		/* 591 */ criteria.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 592 */ criteria.addOrder(Order.desc("complaintDateTime"));
		/* 593 */ smeComplaints = criteria.list();
		/* 594 */ return smeComplaints;
		/*      */ }

	/*      */
	/*      */
	/*      */ public ServerResponse deleteSmeComplaint(Long smeId, Long compalintId) {
		/* 599 */ ServerResponse res = new ServerResponse();
		/* 600 */ SmeComplaint complaint = fetchSmeComplaintById(compalintId);
		/* 601 */ if (complaint != null && complaint.getStatus().byteValue() != -9) {
			/* 602 */ complaint.setStatus(Byte.valueOf((byte) -9));
			/* 603 */ res.setSuccess(true);
			/* 604 */ res.setResult("Successfully Deleted");
			/*      */ } else {
			/* 606 */ res.setSuccess(false);
			/* 607 */ ErrorDetails error = new ErrorDetails();
			/* 608 */ error.setErrorCode(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getErrorCode());
			/* 609 */ error.setErrorString(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getErrorMessage());
			/* 610 */ error.setDisplayMessage(ErrorConstants.ERROR_COMPLAINT_NOT_FOUND.getDisplayMessage());
			/* 611 */ JSONSerializer serializer = new JSONSerializer();
			/* 612 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 613 */ res.setResult(response);
			/*      */ }
		/*      */
		/*      */
		/* 617 */ return res;
		/*      */ }

	/*      */
	/*      */
	/*      */ public SmeComplaint fetchSmeComplaintById(Long complainId) {
		/* 622 */ return (SmeComplaint) getSession().get(SmeComplaint.class, complainId);
		/*      */ }

	/*      */
	/*      */
	/*      */ public Boolean addOutboundIvrCampaign(AddOutBoundIvrBean bean) {
		/* 627 */ OutbondIvrCampaign outbondIvrCampaign = new OutbondIvrCampaign();
		/* 628 */ outbondIvrCampaign.setBaseFileName(bean.getBaseFileName());
		/* 629 */ outbondIvrCampaign.setCampaignName(bean.getIvrName());
		/* 630 */ outbondIvrCampaign.setCampaignStatus(bean.getStatus());
		/* 631 */ outbondIvrCampaign.setCampaignType(bean.getIvrType());
		/* 632 */ outbondIvrCampaign.setCli(bean.getCli());
		/* 633 */ outbondIvrCampaign.setInsertDateTime(bean.getDate());
		/* 634 */ outbondIvrCampaign.setMediaFile(bean.getMediaFile());
		/* 635 */ outbondIvrCampaign.setScheduleEndTime(bean.getEndTime());
		/* 636 */ outbondIvrCampaign.setScheduleStartTime(bean.getStartTime());
		/* 637 */ outbondIvrCampaign.setSmeId(bean.getSmeId());
		/* 638 */ outbondIvrCampaign.setSmeSimultaneouslyCalls(bean.getSimontanouslyCalls().intValue());
		/* 639 */ outbondIvrCampaign.setTotalCalls(bean.getNumberOfCalls().intValue());
		/* 640 */ getSession().save(outbondIvrCampaign);
		/* 641 */ return Boolean.valueOf(true);
		/*      */ }

	/*      */
	/*      */
	/*      */ public Boolean updateOutboundIvrCampaign(AddOutBoundIvrBean bean, BigInteger id) {
		/* 646 */ OutbondIvrCampaign outbondIvrCampaign = findOutboundCampaignByID(id);
		/* 647 */ if (outbondIvrCampaign != null && outbondIvrCampaign.getCampaignStatus().byteValue() == 0) {
			/* 648 */ outbondIvrCampaign.setBaseFileName(bean.getBaseFileName());
			/* 649 */ outbondIvrCampaign.setCampaignName(bean.getIvrName());
			/* 650 */ outbondIvrCampaign.setCampaignStatus(bean.getStatus());
			/* 651 */ outbondIvrCampaign.setCampaignType(bean.getIvrType());
			/* 652 */ outbondIvrCampaign.setCli(bean.getCli());
			/* 653 */ outbondIvrCampaign.setInsertDateTime(bean.getDate());
			/* 654 */ outbondIvrCampaign.setMediaFile(bean.getMediaFile());
			/* 655 */ outbondIvrCampaign.setScheduleEndTime(bean.getEndTime());
			/* 656 */ outbondIvrCampaign.setScheduleStartTime(bean.getStartTime());
			/* 657 */ outbondIvrCampaign.setSmeId(bean.getSmeId());
			/* 658 */ outbondIvrCampaign.setSmeSimultaneouslyCalls(bean.getSimontanouslyCalls().intValue());
			/* 659 */ outbondIvrCampaign.setTotalCalls(bean.getNumberOfCalls().intValue());
			/* 660 */ return Boolean.valueOf(true);
			/*      */ }
		/*      */
		/* 663 */ return Boolean.valueOf(false);
		/*      */ }

	/*      */
	/*      */
	/*      */ public Boolean deleteOutboundIvrCampaign(BigInteger id) {
		/* 668 */ OutbondIvrCampaign campaign = findOutboundCampaignByID(id);
		/* 669 */ if (campaign != null) {
			/* 670 */ campaign.setCampaignStatus(Byte.valueOf((byte) -9));
			/* 671 */ return Boolean.valueOf(true);
			/*      */ }
		/* 673 */ return Boolean.valueOf(false);
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AddOutBoundIvrBean> getOutboundCampaignlit() {
		/* 678 */ List<AddOutBoundIvrBean> outboundIvrBeans = new ArrayList<>();
		/* 679 */ Criteria criteria = getSession().createCriteria(OutbondIvrCampaign.class);
		/* 680 */ criteria.add((Criterion) Restrictions.ne("campaignStatus", Byte.valueOf((byte) -9)));
		/* 681 */ List<OutbondIvrCampaign> ivrCampaigns = criteria.list();
		/* 682 */ for (OutbondIvrCampaign campaign : ivrCampaigns) {
			/* 683 */ AddOutBoundIvrBean addOutBoundIvrBean = new AddOutBoundIvrBean();
			/* 684 */ addOutBoundIvrBean.setId(campaign.getId());
			/* 685 */ addOutBoundIvrBean.setBaseFileName(campaign.getBaseFileName());
			/* 686 */ addOutBoundIvrBean.setCli(campaign.getCli());
			/* 687 */ addOutBoundIvrBean.setDate(campaign.getInsertDateTime());
			/* 688 */ addOutBoundIvrBean.setEndTime(campaign.getScheduleEndTime());
			/* 689 */ addOutBoundIvrBean.setIvrName(campaign.getCampaignName());
			/* 690 */ addOutBoundIvrBean.setIvrType(campaign.getCampaignType());
			/* 691 */ addOutBoundIvrBean.setMediaFile(campaign.getMediaFile());
			/* 692 */ addOutBoundIvrBean.setNumberOfCalls(Integer.valueOf(campaign.getTotalCalls()));
			/* 693 */ addOutBoundIvrBean.setSimontanouslyCalls(Integer.valueOf(campaign.getSmeSimultaneouslyCalls()));
			/* 694 */ outboundIvrBeans.add(addOutBoundIvrBean);
			/*      */ }
		/*      */
		/* 697 */ return outboundIvrBeans;
		/*      */ }

	/*      */
	/*      */
	/*      */ public OutbondIvrCampaign findOutboundCampaignByID(BigInteger id) {
		/* 702 */ OutbondIvrCampaign outbondIvrCampaign = (OutbondIvrCampaign) getSession()
				.get(OutbondIvrCampaign.class, id);
		/* 703 */ return outbondIvrCampaign;
		/*      */ }

	/*      */
	/*      */ private CallCdrResponseBean inCDRToBean(IncomingIvrCallCdr obj)
			throws FileNotFoundException, IOException {
		/* 707 */ CallCdrResponseBean bean = new CallCdrResponseBean();
		/* 708 */ bean.setCallId((obj.getSessionId() == null) ? "Not available" : obj.getSessionId());
		/* 709 */ bean.setId(obj.getId().toString());
		/* 710 */ bean.setAgentGroup(obj.getAgentGroup());
		/* 711 */ bean.setCallDirectionStatus(/* 712 */ (Utility
				.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString()) != null) ?
		/* 713 */ Utility.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString())
						: obj/* 714 */ .getCallDirectionStatus().toString());
		/* 715 */ bean.setCallDirection(obj.getCallDirection());
		/* 716 */ bean.setCalledNumber(obj.getCalledNumber());
		/* 717 */ bean.setCallingNumber(obj.getCallingNumber());
		/* 718 */ bean.setCallRecordedFile(obj.getCallRecordedFile());
		/* 719 */ bean.setCallRecordingStatus(/* 720 */ (Utility
				.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString()) != null) ?
		/* 721 */ Utility.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString())
						: obj/* 722 */ .getCallRecordingStatus().toString());
		/* 723 */ bean.setCdrMode((Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString()) != null) ?
		/* 724 */ Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString())
				: obj/* 725 */ .getCdrMode().toString());
		/* 726 */ bean.setChannelNo(obj.getChannelNo().toString());
		/* 727 */ bean.setDuration(obj.getDuration().toString());
		/* 728 */ bean.setStartDateTimeS(obj.getStartDateTime().toString());
		/* 729 */ bean.setEndDateTimeS(obj.getEndDateTime().toString());
		/* 730 */ bean.setInsertDateTimeS(obj.getInsertDateTime().toString());
		/* 731 */ bean.setHlr(obj.getHlr());
		/* 732 */ bean.setLongcode(obj.getLongcode().toString());
		/* 733 */ bean.setMasterShortcode(obj.getMasterShortcode());
		/* 734 */ bean.setPatchedAgentId(obj.getPatchedAgentId().getAgentId().toString());
		/* 735 */ bean.setAgentName(obj.getPatchedAgentId().getAgentName());
		/* 736 */ bean.setServerIpAddress(obj.getServerIpAddress());
		/* 737 */ bean.setShortcodeMapping(obj.getShortcodeMapping());
		/* 738 */ bean.setSmeIdentifier(obj.getSmeIdentifier());
		/* 739 */ bean.setVoicemailRecordingFile(obj.getVoicemailRecordingFile());
		/* 740 */ bean.setVoicemailRecordingStatus(/* 741 */ (Utility
				.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus()) != null) ?
		/* 742 */ Utility.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus())
						: obj/* 743 */ .getVoicemailRecordingStatus().toString());
		/* 744 */ bean.setMergeStatus(obj.getMergeStatus());
		/* 745 */ bean.setAnswer(((obj.getAnswer() == null) ? Constants.BYTE_0 : obj.getAnswer()).byteValue());
		/* 746 */ bean
				.setCallStatus(((obj.getCallStatus() == null) ? Constants.BYTE_1 : obj.getCallStatus()).byteValue());
		/*      */
		/*      */
		/*      */
		/* 750 */ bean.setDisconnectedBy((obj.getDisconnectedBy() == null) ? "" : obj.getDisconnectedBy());
		/* 751 */ bean.setAddressBookId(obj.getAddressBookId());
		/* 752 */ bean.setCustomerName(obj.getCustomerName());
		/* 753 */ return bean;
		/*      */ }

	/*      */
	/*      */ private CallCdrResponseBean outCDRToBean(OutbondIvrCdr obj) throws FileNotFoundException, IOException {
		/* 757 */ CallCdrResponseBean bean = new CallCdrResponseBean();
		/* 758 */ bean.setCallId((obj.getSessionId() == null) ? "Not available" : obj.getSessionId());
		/* 759 */ bean.setId(obj.getId().toString());
		/* 760 */ bean.setAgentGroup(obj.getAgentGroup());
		/* 761 */ bean.setCallDirectionStatus(/* 762 */ (Utility
				.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString()) != null) ?
		/* 763 */ Utility.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString())
						: obj/* 764 */ .getCallDirectionStatus().toString());
		/* 765 */ bean.setCallDirection(obj.getCallDirection());
		/* 766 */ bean.setCalledNumber(obj.getCalledNumber());
		/* 767 */ bean.setCallingNumber(obj.getCallingNumber());
		/* 768 */ bean.setCallRecordedFile(obj.getCallRecordedFile());
		/* 769 */ bean.setCallRecordingStatus(/* 770 */ (Utility
				.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString()) != null) ?
		/* 771 */ Utility.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString())
						: obj/* 772 */ .getCallRecordingStatus().toString());
		/* 773 */ bean.setCdrMode((Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString()) != null) ?
		/* 774 */ Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString())
				: obj/* 775 */ .getCdrMode().toString());
		/* 776 */ bean.setChannelNo(obj.getChannelNo().toString());
		/* 777 */ bean.setDuration(obj.getDuration().toString());
		/* 778 */ bean.setStartDateTimeS(obj.getStartDateTime().toString());
		/* 779 */ bean.setEndDateTimeS(obj.getEndDateTime().toString());
		/* 780 */ bean.setInsertDateTimeS(obj.getInsertDateTime().toString());
		/* 781 */ bean.setHlr(obj.getHlr());
		/* 782 */ bean.setLongcode(obj.getLongcode().toString());
		/* 783 */ bean.setMasterShortcode(obj.getMasterShortcode());
		/* 784 */ bean.setPatchedAgentId(obj.getPatchedAgentId().getAgentId().toString());
		/* 785 */ bean.setAgentName(obj.getPatchedAgentId().getAgentName());
		/* 786 */ bean.setServerIpAddress(obj.getServerIpAddress());
		/* 787 */ bean.setShortcodeMapping(obj.getShortcodeMapping());
		/* 788 */ bean.setSmeIdentifier(obj.getSmeIdentifier());
		/* 789 */ bean.setVoicemailRecordingFile(obj.getVoicemailRecordingFile());
		/* 790 */ bean.setVoicemailRecordingStatus(/* 791 */ (Utility
				.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus()) != null) ?
		/* 792 */ Utility.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus())
						: obj/* 793 */ .getVoicemailRecordingStatus().toString());
		/* 794 */ bean.setMergeStatus(obj.getMergeStatus());
		/* 795 */ bean.setAnswer(((obj.getAnswer() == null) ? Constants.BYTE_0 : obj.getAnswer()).byteValue());
		/* 796 */ bean.setAddressBookId(obj.getAddressBookId());
		/* 797 */ bean.setCustomerName(obj.getCustomerName());
		/* 798 */ return bean;
		/*      */ }

	/*      */
	/*      */ private CallCdrResponseBean voiceCDRToBean(VoicemailCallCdr obj)
			throws FileNotFoundException, IOException {
		/* 802 */ CallCdrResponseBean bean = new CallCdrResponseBean();
		/* 803 */ bean.setCallId((obj.getSessionId() == null) ? "Not available" : obj.getSessionId());
		/* 804 */ bean.setId(obj.getId().toString());
		/* 805 */ bean.setAgentGroup(obj.getAgentGroup());
		/* 806 */ bean.setCallDirectionStatus(/* 807 */ (Utility
				.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString()) != null) ?
		/* 808 */ Utility.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString())
						: obj/* 809 */ .getCallDirectionStatus().toString());
		/* 810 */ bean.setCallDirection(obj.getCallDirection());
		/* 811 */ bean.setCalledNumber(obj.getCalledNumber());
		/* 812 */ bean.setCallingNumber(obj.getCallingNumber());
		/* 813 */ bean.setCallRecordedFile(obj.getCallRecordedFile());
		/* 814 */ bean.setCallRecordingStatus(/* 815 */ (Utility
				.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString()) != null) ?
		/* 816 */ Utility.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString())
						: obj/* 817 */ .getCallRecordingStatus().toString());
		/* 818 */ bean.setCdrMode((Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString()) != null) ?
		/* 819 */ Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString())
				: obj/* 820 */ .getCdrMode().toString());
		/* 821 */ bean.setChannelNo(obj.getChannelNo().toString());
		/* 822 */ bean.setDuration(obj.getDuration().toString());
		/* 823 */ bean.setStartDateTimeS(obj.getStartDateTime().toString());
		/* 824 */ bean.setEndDateTimeS(obj.getEndDateTime().toString());
		/* 825 */ bean.setInsertDateTimeS(obj.getInsertDateTime().toString());
		/* 826 */ bean.setHlr(obj.getHlr());
		/* 827 */ bean.setLongcode(obj.getLongcode().toString());
		/* 828 */ bean.setMasterShortcode(obj.getMasterShortcode());
		/* 829 */ bean.setPatchedAgentId(obj.getPatchedAgentId().getAgentId().toString());
		/* 830 */ bean.setAgentName(obj.getPatchedAgentId().getAgentName());
		/* 831 */ bean.setServerIpAddress(obj.getServerIpAddress());
		/* 832 */ bean.setShortcodeMapping(obj.getShortcodeMapping());
		/* 833 */ bean.setSmeIdentifier(obj.getSmeIdentifier());
		/* 834 */ bean.setVoicemailRecordingFile(obj.getVoicemailRecordingFile());
		/* 835 */ bean.setVoicemailRecordingStatus(/* 836 */ (Utility
				.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus()) != null) ?
		/* 837 */ Utility.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus())
						: obj/* 838 */ .getVoicemailRecordingStatus().toString());
		/*      */
		/* 840 */ bean.setMergeStatus(obj.getMergeStatus());
		/* 841 */ bean.setAnswer(((obj.getAnswer() == null) ? Constants.BYTE_0 : obj.getAnswer()).byteValue());
		/* 842 */ bean.setAddressBookId(obj.getAddressBookId());
		/* 843 */ bean.setCustomerName(obj.getCustomerName());
		/* 844 */ return bean;
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */
	/*      */ public List<CallCdrResponseBean> getCDRListForService(ManageClientPageRequest pageRequest, Long smeId,
			Byte catagory) throws FileNotFoundException, IOException {
		/* 851 */ List<CallCdrResponseBean> cdrList = new ArrayList<>();
		/*      */
		/*      */
		/* 854 */ String hql = "";
		/* 855 */ if (pageRequest.getFilterList() != null) {
			/* 856 */ for (FilterBean restrgictionFilter : pageRequest.getFilterList()) {
				/* 857 */ restrgictionFilter.setName(cdrListMapper(restrgictionFilter.getName()));
				/* 858 */ hql = hql + " and " + Utility.genericFilterCriteria(restrgictionFilter);
			}
			/*      */ }
		/*      */
		/* 862 */ if (catagory != null) {
			/* 863 */ Query<IncomingIvrCallCdr> qry;
			List<IncomingIvrCallCdr> entityList;
			Query<OutbondIvrCdr> qryOut;
			List<OutbondIvrCdr> entityListO;
			Query<VoicemailCallCdr> qryVoice;
			List<VoicemailCallCdr> entityListV;
			switch (catagory.byteValue()) {
			/*      */
			/*      */
			/*      */ case 1:
				/*      */ case 4:
				/* 868 */ hql = "from IncomingIvrCallCdr cdr  left join fetch cdr.patchedAgentId  where cdr.smeId.id='"
						+ smeId + "' " + hql + " order by cdr.endDateTime desc ";
				/*      */
				/*      */
				/* 871 */ qry = getSession().createQuery(hql);
				/* 872 */ if (pageRequest.getInitialRecord() > 0)
					/* 873 */ qry.setFirstResult(pageRequest.getInitialRecord() - 1);
				/* 874 */ if (pageRequest.getBatchSize() > 0)
					/* 875 */ qry.setMaxResults(pageRequest.getBatchSize());
				/* 876 */ entityList = qry.list();
				/* 877 */ for (IncomingIvrCallCdr obj : entityList) {
					/* 878 */ cdrList.add(inCDRToBean(obj));
					/*      */ }
				/*      */ break;
			/*      */
			/*      */ case 2:
				/* 883 */ hql = "from OutbondIvrCdr cdr  left join fetch cdr.patchedAgentId  where cdr.smeId.id='"
						+ smeId + "' " + hql + " order by cdr.endDateTime desc ";
				/*      */
				/* 885 */ qryOut = getSession().createQuery(hql);
				/* 886 */ if (pageRequest.getInitialRecord() > 0)
					/* 887 */ qryOut.setFirstResult(pageRequest.getInitialRecord() - 1);
				/* 888 */ if (pageRequest.getBatchSize() > 0) {
					/* 889 */ qryOut.setMaxResults(pageRequest.getBatchSize());
					/*      */ }
				/*      */
				/* 892 */ entityListO = qryOut.list();
				/* 893 */ for (OutbondIvrCdr obj : entityListO) {
					/* 894 */ cdrList.add(outCDRToBean(obj));
					/*      */ }
				/*      */ break;
			/*      */ case 3:
				/* 898 */ hql = "from VoicemailCallCdr  cdr  left join fetch cdr.patchedAgentId  where cdr.smeId.id='"
						+ smeId + "' " + hql + " order by cdr.endDateTime desc ";
				/*      */
				/* 900 */ qryVoice = getSession().createQuery(hql);
				/* 901 */ entityListV = qryVoice.list();
				/* 902 */ for (VoicemailCallCdr obj : entityListV) {
					/* 903 */ cdrList.add(voiceCDRToBean(obj));
					/*      */ }
				/*      */ break;
			/*      */ }
			/*      */
			/*      */
			/*      */ }
		/* 910 */ return cdrList;
		/*      */ }

	/*      */
	/*      */
	/*      */ public Users fetchUser(String userName) {
		/* 915 */ Criteria criteria = getSession().createCriteria(Users.class);
		/* 916 */ criteria.add((Criterion) Restrictions.eq("username", userName));
		/* 917 */ Users user = (Users) criteria.uniqueResult();
		/* 918 */ return user;
		/*      */ }

	/*      */
	/*      */
	/*      */ public ServerResponse resetPassword(LoginBean login) {
		/* 923 */ ServerResponse response = new ServerResponse();
		/* 924 */ Users user = fetchUser(login.getUserName());
		/* 925 */ if (user == null) {
			/* 926 */ response.setSuccess(false);
			/* 927 */ ErrorDetails error = new ErrorDetails();
			/* 928 */ error.setErrorCode(ErrorConstants.ERROR_USER_NOT_EXIST.getErrorCode());
			/* 929 */ error.setErrorString("reset Password::" + ErrorConstants.ERROR_USER_NOT_EXIST.getErrorMessage());
			/* 930 */ error.setDisplayMessage(ErrorConstants.ERROR_USER_NOT_EXIST.getDisplayMessage());
			/* 931 */ JSONSerializer serializer = new JSONSerializer();
			/* 932 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 933 */ response.setResult(res);
			/* 934 */ } else if (user.getPassword().equals(login.getPassword()) && login.getNewPassword() != null &&
		/* 935 */ !login.getNewPassword().equals("")) {
			/* 936 */ user.setPassword(login.getNewPassword());
			/* 937 */ response.setSuccess(true);
			/* 938 */ response.setResult("Password Updated");
			/*      */ } else {
			/* 940 */ response.setSuccess(false);
			/* 941 */ ErrorDetails error = new ErrorDetails();
			/* 942 */ error.setErrorCode(ErrorConstants.ERROR_PASSWORD_INCORRRECT.getErrorCode());
			/* 943 */ error
					.setErrorString("reset Password::" + ErrorConstants.ERROR_PASSWORD_INCORRRECT.getErrorMessage());
			/* 944 */ error.setDisplayMessage(ErrorConstants.ERROR_PASSWORD_INCORRRECT.getDisplayMessage());
			/* 945 */ JSONSerializer serializer = new JSONSerializer();
			/* 946 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 947 */ response.setResult(res);
			/*      */ }
		/* 949 */ return response;
		/*      */ }

	/*      */
	/*      */
	/*      */ public ServerResponse forgetPassword(String smeId, String token) throws ParseException {
		/* 954 */ ServerResponse response = new ServerResponse();
		/* 955 */ Criteria crit = getSession().createCriteria(ForgetPassword.class);
		/* 956 */ crit.add((Criterion) Restrictions.eq("token", token));
		/* 957 */ crit.add((Criterion) Restrictions.eq("status", Byte.valueOf((byte) 0)));
		/* 958 */ ForgetPassword forgetPassword = (ForgetPassword) crit.uniqueResult();
		/* 959 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/* 960 */ if (forgetPassword == null) {
			/* 961 */ response.setSuccess(false);
			/* 962 */ ErrorDetails error = new ErrorDetails();
			/* 963 */ error.setErrorCode(ErrorConstants.ERROR_LINK_INVALID.getErrorCode());
			/* 964 */ error.setErrorString("new Password::" + ErrorConstants.ERROR_LINK_INVALID.getErrorMessage());
			/* 965 */ error.setDisplayMessage(ErrorConstants.ERROR_LINK_INVALID.getDisplayMessage());
			/* 966 */ JSONSerializer serializer = new JSONSerializer();
			/* 967 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 968 */ response.setResult(res);
			/* 969 */ } else if ((int) (df.parse(Utility.getDateTime()).getTime()
					- forgetPassword.getInsertionDate().getTime()) / 1000 > 3600) {
			/*      */
			/* 971 */ response.setSuccess(false);
			/* 972 */ ErrorDetails error = new ErrorDetails();
			/* 973 */ error.setErrorCode(ErrorConstants.ERROR_LINK_EXPIRED.getErrorCode());
			/* 974 */ error.setErrorString("new Password::" + ErrorConstants.ERROR_LINK_EXPIRED.getErrorMessage());
			/* 975 */ error.setDisplayMessage(ErrorConstants.ERROR_LINK_EXPIRED.getDisplayMessage());
			/* 976 */ JSONSerializer serializer = new JSONSerializer();
			/* 977 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 978 */ response.setResult(res);
			/*      */ } else {
			/* 980 */ response.setResult("Proceed Further");
			/* 981 */ response.setSuccess(true);
			/*      */ }
		/* 983 */ return response;
		/*      */ }

	/*      */
	/*      */
	/*      */ public ServerResponse genForgotToken(String smeId) throws ParseException, IOException {
		/* 988 */ ServerResponse response = new ServerResponse();
		/* 989 */ Users user = fetchUser(smeId);
		/* 990 */ if (user == null) {
			/* 991 */ response.setSuccess(false);
			/* 992 */ ErrorDetails error = new ErrorDetails();
			/* 993 */ error.setErrorCode(ErrorConstants.ERROR_USER_NOT_EXIST.getErrorCode());
			/* 994 */ error.setErrorString("reset Password::" + ErrorConstants.ERROR_USER_NOT_EXIST.getErrorMessage());
			/* 995 */ error.setDisplayMessage(ErrorConstants.ERROR_USER_NOT_EXIST.getDisplayMessage());
			/* 996 */ JSONSerializer serializer = new JSONSerializer();
			/* 997 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 998 */ response.setResult(res);
			/*      */ } else {
			/* 1000 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			/* 1001 */ ForgetPassword forgetPassword = new ForgetPassword();
			/* 1002 */ String token = UUID.randomUUID().toString();
			/* 1003 */ forgetPassword.setToken(token);
			/* 1004 */ forgetPassword.setSmeId(user);
			/* 1005 */ forgetPassword.setInsertionDate(df.parse(Utility.getDateTime()));
			/* 1006 */ forgetPassword.setStatus((byte) 0);
			/* 1007 */ String link = Constants.APP_URL + Constants.GUI_URL + smeId + "&activationkey=" + token;
			/* 1008 */ new MailSender(retriveEmail(smeId), "Wringg Account Password Reset",
					"Dear User, \n \n This is to inform you that we got a password change request from you, \n Please use the following link to change password. \n \n ",
					"\n \n If this was not you ,then please ignore this mail.\n\n Thanks and Regards, \n Team Wringg",
					link);
			/*      */
			/* 1010 */ getSession().save(forgetPassword);
			/* 1011 */ response.setSuccess(true);
			/* 1012 */ response.setResult(link);
			/*      */ }
		/*      */
		/* 1015 */ return response;
		/*      */ }

	/*      */
	/*      */ private String retriveEmail(String id) {
		/*      */ try {
			/* 1020 */ Long smeId = Long.valueOf(Long.parseLong(id));
			/* 1021 */ Criteria crit = getSession().createCriteria(SmeProfile.class);
			/* 1022 */ crit.setProjection((Projection) Projections.property("emailId"));
			/* 1023 */ crit.add((Criterion) Restrictions.eq("id", smeId));
			/* 1024 */ Object mailID = crit.uniqueResult();
			/* 1025 */ return mailID.toString();
			/* 1026 */ } catch (NumberFormatException numberFormatException) {
			/*      */
			/* 1028 */ return id;
			/*      */ }
		/*      */ }

	/*      */
	/*      */ public ServerResponse newPassword(String smeId, PassswordTokenBean bean) throws ParseException {
		/* 1033 */ ServerResponse response = new ServerResponse();
		/* 1034 */ JSONSerializer serializer = new JSONSerializer();
		/* 1035 */ Criteria crit = getSession().createCriteria(ForgetPassword.class);
		/* 1036 */ crit.add((Criterion) Restrictions.eq("token", bean.getToken()));
		/* 1037 */ crit.add((Criterion) Restrictions.eq("status", Byte.valueOf((byte) 0)));
		/* 1038 */ ForgetPassword forgetPassword = (ForgetPassword) crit.uniqueResult();
		/* 1039 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/* 1040 */ if (forgetPassword == null) {
			/* 1041 */ response.setSuccess(false);
			/* 1042 */ ErrorDetails error = new ErrorDetails();
			/* 1043 */ error.setErrorCode(ErrorConstants.ERROR_LINK_INVALID.getErrorCode());
			/* 1044 */ error.setErrorString("new Password::" + ErrorConstants.ERROR_LINK_INVALID.getErrorMessage());
			/* 1045 */ error.setDisplayMessage(ErrorConstants.ERROR_LINK_INVALID.getDisplayMessage());
			/* 1046 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 1047 */ response.setResult(res);
			/* 1048 */ } else if ((int) (df.parse(Utility.getDateTime()).getTime()
					- forgetPassword.getInsertionDate().getTime()) / 1000 > 3600) {
			/*      */
			/* 1050 */ response.setSuccess(false);
			/* 1051 */ ErrorDetails error = new ErrorDetails();
			/* 1052 */ error.setErrorCode(ErrorConstants.ERROR_LINK_EXPIRED.getErrorCode());
			/* 1053 */ error.setErrorString("new Password::" + ErrorConstants.ERROR_LINK_EXPIRED.getErrorMessage());
			/* 1054 */ error.setDisplayMessage(ErrorConstants.ERROR_LINK_EXPIRED.getDisplayMessage());
			/* 1055 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 1056 */ response.setResult(res);
			/*      */ } else {
			/* 1058 */ Users user = fetchUser(smeId);
			/* 1059 */ if (user == null) {
				/* 1060 */ response.setSuccess(false);
				/* 1061 */ ErrorDetails error = new ErrorDetails();
				/* 1062 */ error.setErrorCode(ErrorConstants.ERROR_USER_NOT_EXIST.getErrorCode());
				/* 1063 */ error
						.setErrorString("reset Password::" + ErrorConstants.ERROR_USER_NOT_EXIST.getErrorMessage());
				/* 1064 */ error.setDisplayMessage(ErrorConstants.ERROR_USER_NOT_EXIST.getDisplayMessage());
				/* 1065 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
				/* 1066 */ response.setResult(res);
				/*      */ } else {
				/* 1068 */ user.setPassword(bean.getPassword());
				/* 1069 */ response.setSuccess(true);
				/* 1070 */ response.setResult("Password Updated Successfully");
				/* 1071 */ forgetPassword.setStatus((byte) -9);
				/* 1072 */ forgetPassword.setUpdationDate(df.parse(Utility.getDateTime()));
				/*      */ }
			/*      */ }
		/* 1075 */ return response;
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AgentReportDetail> fetchAgentReportDetail(Long smeId, ManageClientPageRequest pageRequest) {
		/* 1080 */ String hql = "from AgentReportDetail ard  left join fetch ard.agentId ";
		/* 1081 */ hql = hql + " where ard.smeId='" + smeId + "' ";
		/*      */
		/* 1083 */ if (pageRequest.getFilterList() != null) {
			/* 1084 */ for (FilterBean restrgictionFilter : pageRequest.getFilterList()) {
				/* 1085 */ restrgictionFilter.setName(agentReportColumnMapper(restrgictionFilter.getName()));
				/* 1086 */ hql = hql + " and " + Utility.genericFilterCriteria(restrgictionFilter);
				/*      */ }
			/*      */ }
		/*      */
		/* 1090 */ hql = hql + " order by ard.endDate ";
		/* 1091 */ Query<AgentReportDetail> qry = getSession().createQuery(hql);
		/* 1092 */ if (pageRequest.getInitialRecord() > 0)
			/* 1093 */ qry.setFirstResult(pageRequest.getInitialRecord() - 1);
		/* 1094 */ if (pageRequest.getBatchSize() > 0)
			/* 1095 */ qry.setMaxResults(pageRequest.getBatchSize());
		/* 1096 */ return qry.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public String genricFilterAppend(ManageClientPageRequest request, String allias) {
		/* 1101 */ String hql = " ";
		/* 1102 */ if (request.getFilterList() != null) {
			/* 1103 */ for (FilterBean restrgictionFilter : request.getFilterList()) {
				/* 1104 */ hql = hql + " and " + Utility.genericFilterCriteria(restrgictionFilter);
				/*      */ }
			/*      */ }
		/*      */
		/* 1108 */ return hql;
		/*      */ }

	/*      */
	/*      */
	/*      */ public Long getAgetntReportCount(Long smeId, ManageClientPageRequest pageRequest) {
		/* 1113 */ String hql = "select count(*) from AgentReportDetail ard ";
		/* 1114 */ hql = hql + " where smeId='" + smeId + "' ";
		/*      */
		/* 1116 */ if (pageRequest.getFilterList() != null) {
			/* 1117 */ for (FilterBean restrgictionFilter : pageRequest.getFilterList()) {
				/* 1118 */ hql = hql + " and " + Utility.genericFilterCriteria(restrgictionFilter);
				/*      */ }
			/*      */ }
		/*      */
		/* 1122 */ Query<Long> qry = getSession().createQuery(hql);
		/* 1123 */ return (Long) qry.uniqueResult();
		/*      */ }

	/*      */
	/*      */ private String agentReportColumnMapper(String column) {
		/* 1127 */ String result = "ard.";
		/* 1128 */ switch (column) {
		/*      */ case "agentNumber":
			/* 1130 */ return "ard.agentId.agentMobile";
		/*      */ case "agentName":
			/* 1132 */ return "ard.agentId.agentName";
		/*      */ }
		/* 1134 */ return result = result + column;
		/*      */ }

	/*      */
	/*      */
	/*      */ private String cdrListMapper(String column) {
		/* 1139 */ String result = "cdr.";
		/* 1140 */ switch (column) {
		/*      */ case "callId":
			/* 1142 */ return result + "sessionId";
		/*      */ case "endDateTimeS":
			/*      */ case "startDate":
			/*      */ case "endDate":
			/* 1146 */ return result + "endDateTime";
		/*      */ case "startDateTimeS":
			/* 1148 */ return result + "startDateTime";
		/*      */ case "insertDateTimeS":
			/* 1150 */ return result + "insertDateTime";
		/*      */ case "patchedAgentId":
			/* 1152 */ return "cdr.patchedAgentId.id";
		/*      */ case "agentName":
			/* 1154 */ return "cdr.patchedAgentId.agentName";
		/*      */ }
		/* 1156 */ return result + column;
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */ public Long getCdrListCount(ManageClientPageRequest pageRequest, Long smeId, Byte catagory) {
		/* 1162 */ String hql = "";
		/* 1163 */ if (pageRequest.getFilterList() != null) {
			/* 1164 */ for (FilterBean restrgictionFilter : pageRequest.getFilterList()) {
				/* 1165 */ hql = hql + " and " + Utility.genericFilterCriteria(restrgictionFilter);
				/*      */ }
			/*      */ }
		/*      */
		/* 1169 */ if (catagory != null) {
			/* 1170 */ Query<Long> qry;
			Query<Long> qryOut;
			Query<Long> qryVoice;
			switch (catagory.byteValue()) {
			/*      */ case 1:
				/*      */ case 4:
				/* 1173 */ hql = "select count(*) from IncomingIvrCallCdr cdr  left join cdr.patchedAgentId  where cdr.smeId='"
						+ smeId + "' " + hql;
				/*      */
				/*      */
				/* 1176 */ qry = getSession().createQuery(hql);
				/* 1177 */ return (Long) qry.uniqueResult();
			/*      */ case 2:
				/* 1179 */ hql = "select count(*) from OutbondIvrCdr cdr  left join cdr.patchedAgentId where cdr.smeId='"
						+ smeId + "' " + hql;
				/*      */
				/* 1181 */ qryOut = getSession().createQuery(hql);
				/* 1182 */ return (Long) qryOut.uniqueResult();
			/*      */ case 3:
				/* 1184 */ hql = "select count(*) from VoicemailCallCdr cdr  left join cdr.patchedAgentId where cdr.smeId='"
						+ smeId + "' " + hql;
				/*      */
				/* 1186 */ qryVoice = getSession().createQuery(hql);
				/* 1187 */ return (Long) qryVoice.uniqueResult();
			/*      */ }
			/* 1189 */ return Long.valueOf(0L);
			/*      */ }
		/*      */
		/* 1192 */ return Long.valueOf(0L);
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AgentDetail> agentDashboardSummary(Long id, Byte status) {
		/* 1197 */ Criteria crit = getSession().createCriteria(AgentDetail.class);
		/* 1198 */ crit.add((Criterion) Restrictions.eq("smeId", id));
		/* 1199 */ crit.add((Criterion) Restrictions.eq("status", status));
		/* 1200 */ return crit.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AgentDetail> agentDashboardSummary(Long id, List<Byte> status) {
		/* 1205 */ Criteria crit = getSession().createCriteria(AgentDetail.class);
		/* 1206 */ crit.add((Criterion) Restrictions.eq("smeId", id));
		/* 1207 */ crit.add(Restrictions.in("status", status));
		/* 1208 */ return crit.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public void updateAlertNotification(AlertNotification notification) {
		/* 1213 */ notification.setStatus(Constants.ALERT_SEEN.byteValue());
		/*      */ }

	/*      */
	/*      */
	/*      */ public AlertNotification getAlertNotification(int id) {
		/* 1218 */ return (AlertNotification) getSession().get(AlertNotification.class, Integer.valueOf(id));
		/*      */ }

	/*      */
	/*      */
	/*      */ public void deleteAlertNotification(AlertNotification notification) {
		/* 1223 */ notification.setStatus((byte) -9);
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<AlertNotification> alertNotificationList(String userId, int seen) {
		/* 1228 */ Criteria crit = getSession().createCriteria(AlertNotification.class);
		/* 1229 */ crit.add((Criterion) Restrictions.eq("userId", getSession().get(Users.class, userId)));
		/* 1230 */ crit.add((Criterion) Restrictions.ne("status", Byte.valueOf((byte) -9)));
		/* 1231 */ if (seen < 0) {
			/* 1232 */ crit.add((Criterion) Restrictions.ne("status", Constants.ALERT_SEEN));
			/*      */ }
		/* 1234 */ crit.addOrder(Order.desc("insertedDate"));
		/* 1235 */ crit.setMaxResults(50);
		/* 1236 */ return crit.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public MpbxCallRecording recordingFileList(String id) {
		/* 1241 */ Criteria criteria = getSession().createCriteria(MpbxCallRecording.class);
		/* 1242 */ criteria.add((Criterion) Restrictions.eq("callId", id));
		/* 1243 */ return (MpbxCallRecording) criteria.uniqueResult();
		/*      */ }

	/*      */
	/*      */ public CallCdrResponseBean fetchOutLeg(Long smeId, String sessionId, byte flag)
			throws FileNotFoundException, IOException {
		/*      */ IncomingIvrCallCdr incomingIvrCallCdr;
		/*      */ OutbondIvrCdr outbondIvrCdr;
		/* 1249 */ CallCdrResponseBean bean = new CallCdrResponseBean();
		/* 1250 */ Criteria crit = null;
		/* 1251 */ switch (flag)
		/*      */ {
		case 1:
			/* 1253 */ crit = getSession().createCriteria(IncomingIvrCallCdr.class);
			/* 1254 */ crit.add((Criterion) Restrictions.eq("sessionId", sessionId));
			/* 1255 */ crit.add((Criterion) Restrictions.eq("smeId", getSession().get(SmeProfile.class, smeId)));
			/* 1256 */ crit.add((Criterion) Restrictions.eq("callDirectionStatus", Integer.valueOf(2)));
			/* 1257 */ crit.setMaxResults(1);
			/* 1258 */ incomingIvrCallCdr = (IncomingIvrCallCdr) crit.uniqueResult();
			/* 1259 */ if (incomingIvrCallCdr != null) {
				/* 1260 */ bean.setCallId((incomingIvrCallCdr.getSessionId() == null) ? "Not available"
						: incomingIvrCallCdr.getSessionId());
				/*      */
				/* 1262 */ bean.setId(incomingIvrCallCdr.getId().toString());
				/* 1263 */ bean.setAgentGroup(incomingIvrCallCdr.getAgentGroup());
				/* 1264 */ bean.setCallDirectionStatus(/* 1265 */ (Utility.getProjectProperty(
						"callDirectionStatus." + incomingIvrCallCdr.getCallDirectionStatus().toString()) != null) ?
				/* 1266 */ Utility.getProjectProperty(
						"callDirectionStatus." + incomingIvrCallCdr/* 1267 */ .getCallDirectionStatus().toString())
								: incomingIvrCallCdr/* 1268 */ .getCallDirectionStatus().toString());
				/* 1269 */ bean.setCallDirection(incomingIvrCallCdr.getCallDirection());
				/* 1270 */ bean.setCalledNumber(incomingIvrCallCdr.getCalledNumber());
				/* 1271 */ bean.setCallingNumber(incomingIvrCallCdr.getCallingNumber());
				/* 1272 */ bean.setCallRecordedFile(incomingIvrCallCdr.getCallRecordedFile());
				/* 1273 */ bean.setCallRecordingStatus(/* 1274 */ (Utility.getProjectProperty(
						"callRecordingStatus." + incomingIvrCallCdr.getCallRecordingStatus().toString()) != null) ?
				/* 1275 */ Utility.getProjectProperty(
						"callRecordingStatus." + incomingIvrCallCdr/* 1276 */ .getCallRecordingStatus().toString())
								: incomingIvrCallCdr/* 1277 */ .getCallRecordingStatus().toString());
				/* 1278 */ bean.setCdrMode(
						(Utility.getProjectProperty("cdrMode." + incomingIvrCallCdr.getCdrMode().toString()) != null) ?
						/* 1279 */ Utility.getProjectProperty("cdrMode." + incomingIvrCallCdr.getCdrMode().toString())
								: incomingIvrCallCdr/* 1280 */ .getCdrMode().toString());
				/* 1281 */ bean.setChannelNo(incomingIvrCallCdr.getChannelNo().toString());
				/* 1282 */ bean.setDuration(incomingIvrCallCdr.getDuration().toString());
				/* 1283 */ bean.setStartDateTimeS(incomingIvrCallCdr.getStartDateTime().toString());
				/* 1284 */ bean.setEndDateTimeS(incomingIvrCallCdr.getEndDateTime().toString());
				/* 1285 */ bean.setInsertDateTimeS(incomingIvrCallCdr.getInsertDateTime().toString());
				/* 1286 */ bean.setHlr(incomingIvrCallCdr.getHlr());
				/* 1287 */ bean.setLongcode(incomingIvrCallCdr.getLongcode().toString());
				/* 1288 */ bean.setMasterShortcode(incomingIvrCallCdr.getMasterShortcode());
				/* 1289 */ bean.setPatchedAgentId(incomingIvrCallCdr.getPatchedAgentId().getAgentId().toString());
				/* 1290 */ bean.setAgentName(incomingIvrCallCdr.getPatchedAgentId().getAgentName());
				/* 1291 */ bean.setServerIpAddress(incomingIvrCallCdr.getServerIpAddress());
				/* 1292 */ bean.setShortcodeMapping(incomingIvrCallCdr.getShortcodeMapping());
				/* 1293 */ bean.setSmeIdentifier(incomingIvrCallCdr.getSmeIdentifier());
				/* 1294 */ bean.setVoicemailRecordingFile(incomingIvrCallCdr.getVoicemailRecordingFile());
				/* 1295 */ bean.setVoicemailRecordingStatus(/* 1296 */ (Utility.getProjectProperty(
						"voiceMailRecordingStatus." + incomingIvrCallCdr.getVoicemailRecordingStatus()) != null) ?
				/* 1297 */ Utility.getProjectProperty(
						"voiceMailRecordingStatus." + incomingIvrCallCdr/* 1298 */ .getVoicemailRecordingStatus())
								: incomingIvrCallCdr/* 1299 */ .getVoicemailRecordingStatus().toString());
				/* 1300 */ bean.setMergeStatus(incomingIvrCallCdr.getMergeStatus());
				/* 1301 */ bean.setAnswer(
						((incomingIvrCallCdr.getAnswer() == null) ? Constants.BYTE_0 : incomingIvrCallCdr.getAnswer())
								.byteValue());
				/*      */
				/*      */
				/* 1304 */ bean.setCallStatus(((incomingIvrCallCdr.getCallStatus() == null) ? Constants.BYTE_1
						: incomingIvrCallCdr.getCallStatus()).byteValue());
				/*      */
				/*      */
				/* 1307 */ bean.setDisconnectedBy(
						(incomingIvrCallCdr.getDisconnectedBy() == null) ? "" : incomingIvrCallCdr.getDisconnectedBy());
				/*      */ }
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/*      */
			/* 1416 */ return bean;
		case 2:
			crit = getSession().createCriteria(OutbondIvrCdr.class);
			crit.add((Criterion) Restrictions.eq("sessionId", sessionId));
			crit.add((Criterion) Restrictions.eq("smeId", getSession().get(SmeProfile.class, smeId)));
			crit.add((Criterion) Restrictions.eq("callDirectionStatus", Integer.valueOf(2)));
			crit.setMaxResults(1);
			outbondIvrCdr = (OutbondIvrCdr) crit.uniqueResult();
			if (outbondIvrCdr != null) {
				bean.setCallId((outbondIvrCdr.getSessionId() == null) ? "Not available" : outbondIvrCdr.getSessionId());
				bean.setId(outbondIvrCdr.getId().toString());
				bean.setAgentGroup(outbondIvrCdr.getAgentGroup());
				bean.setCallDirectionStatus((Utility.getProjectProperty(
						"callDirectionStatus." + outbondIvrCdr.getCallDirectionStatus().toString()) != null)
								? Utility.getProjectProperty(
										"callDirectionStatus." + outbondIvrCdr.getCallDirectionStatus().toString())
								: outbondIvrCdr.getCallDirectionStatus().toString());
				bean.setCallDirection(outbondIvrCdr.getCallDirection());
				bean.setCalledNumber(outbondIvrCdr.getCalledNumber());
				bean.setCallingNumber(outbondIvrCdr.getCallingNumber());
				bean.setCallRecordedFile(outbondIvrCdr.getCallRecordedFile());
				bean.setCallRecordingStatus((Utility.getProjectProperty(
						"callRecordingStatus." + outbondIvrCdr.getCallRecordingStatus().toString()) != null)
								? Utility.getProjectProperty(
										"callRecordingStatus." + outbondIvrCdr.getCallRecordingStatus().toString())
								: outbondIvrCdr.getCallRecordingStatus().toString());
				bean.setCdrMode((Utility.getProjectProperty("cdrMode." + outbondIvrCdr.getCdrMode().toString()) != null)
						? Utility.getProjectProperty("cdrMode." + outbondIvrCdr.getCdrMode().toString())
						: outbondIvrCdr.getCdrMode().toString());
				bean.setChannelNo(outbondIvrCdr.getChannelNo().toString());
				bean.setDuration(outbondIvrCdr.getDuration().toString());
				bean.setStartDateTimeS(outbondIvrCdr.getStartDateTime().toString());
				bean.setEndDateTimeS(outbondIvrCdr.getEndDateTime().toString());
				bean.setInsertDateTimeS(outbondIvrCdr.getInsertDateTime().toString());
				bean.setHlr(outbondIvrCdr.getHlr());
				bean.setLongcode(outbondIvrCdr.getLongcode().toString());
				bean.setMasterShortcode(outbondIvrCdr.getMasterShortcode());
				bean.setPatchedAgentId(outbondIvrCdr.getPatchedAgentId().getAgentId().toString());
				bean.setAgentName(outbondIvrCdr.getPatchedAgentId().getAgentName());
				bean.setServerIpAddress(outbondIvrCdr.getServerIpAddress());
				bean.setShortcodeMapping(outbondIvrCdr.getShortcodeMapping());
				bean.setSmeIdentifier(outbondIvrCdr.getSmeIdentifier());
				bean.setVoicemailRecordingFile(outbondIvrCdr.getVoicemailRecordingFile());
				bean.setVoicemailRecordingStatus((Utility.getProjectProperty(
						"voiceMailRecordingStatus." + outbondIvrCdr.getVoicemailRecordingStatus()) != null)
								? Utility.getProjectProperty(
										"voiceMailRecordingStatus." + outbondIvrCdr.getVoicemailRecordingStatus())
								: outbondIvrCdr.getVoicemailRecordingStatus().toString());
				bean.setMergeStatus(outbondIvrCdr.getMergeStatus());
				bean.setAnswer(((outbondIvrCdr.getAnswer() == null) ? Constants.BYTE_0 : outbondIvrCdr.getAnswer())
						.byteValue());
			}
			return bean;
		}
		crit = getSession().createCriteria(VoicemailCallCdr.class);
		crit.add((Criterion) Restrictions.eq("sessionId", sessionId));
		crit.add((Criterion) Restrictions.eq("smeId", getSession().get(SmeProfile.class, smeId)));
		crit.add((Criterion) Restrictions.eq("callDirectionStatus", Integer.valueOf(2)));
		crit.setMaxResults(1);
		VoicemailCallCdr obj = (VoicemailCallCdr) crit.uniqueResult();
		if (obj != null) {
			bean.setCallId((obj.getSessionId() == null) ? "Not available" : obj.getSessionId());
			bean.setId(obj.getId().toString());
			bean.setAgentGroup(obj.getAgentGroup());
			bean.setCallDirectionStatus((Utility
					.getProjectProperty("callDirectionStatus." + obj.getCallDirectionStatus().toString()) != null)
							? Utility.getProjectProperty(
									"callDirectionStatus." + obj.getCallDirectionStatus().toString())
							: obj.getCallDirectionStatus().toString());
			bean.setCallDirection(obj.getCallDirection());
			bean.setCalledNumber(obj.getCalledNumber());
			bean.setCallingNumber(obj.getCallingNumber());
			bean.setCallRecordedFile(obj.getCallRecordedFile());
			bean.setCallRecordingStatus((Utility
					.getProjectProperty("callRecordingStatus." + obj.getCallRecordingStatus().toString()) != null)
							? Utility.getProjectProperty(
									"callRecordingStatus." + obj.getCallRecordingStatus().toString())
							: obj.getCallRecordingStatus().toString());
			bean.setCdrMode((Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString()) != null)
					? Utility.getProjectProperty("cdrMode." + obj.getCdrMode().toString())
					: obj.getCdrMode().toString());
			bean.setChannelNo(obj.getChannelNo().toString());
			bean.setDuration(obj.getDuration().toString());
			bean.setStartDateTimeS(obj.getStartDateTime().toString());
			bean.setEndDateTimeS(obj.getEndDateTime().toString());
			bean.setInsertDateTimeS(obj.getInsertDateTime().toString());
			bean.setHlr(obj.getHlr());
			bean.setLongcode(obj.getLongcode().toString());
			bean.setMasterShortcode(obj.getMasterShortcode());
			bean.setPatchedAgentId(obj.getPatchedAgentId().getAgentId().toString());
			bean.setAgentName(obj.getPatchedAgentId().getAgentName());
			bean.setServerIpAddress(obj.getServerIpAddress());
			bean.setShortcodeMapping(obj.getShortcodeMapping());
			bean.setSmeIdentifier(obj.getSmeIdentifier());
			bean.setVoicemailRecordingFile(obj.getVoicemailRecordingFile());
			bean.setVoicemailRecordingStatus((Utility
					.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus()) != null)
							? Utility
									.getProjectProperty("voiceMailRecordingStatus." + obj.getVoicemailRecordingStatus())
							: obj.getVoicemailRecordingStatus().toString());
			bean.setMergeStatus(obj.getMergeStatus());
			bean.setAnswer(((obj.getAnswer() == null) ? Constants.BYTE_0 : obj.getAnswer()).byteValue());
		}
		return bean;
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */ public SmeProfile getSmeProfile(long id) {
		/* 1422 */ return (SmeProfile) getSession().get(SmeProfile.class, Long.valueOf(id));
		/*      */ }

	/*      */
	/*      */
	/*      */ public void saveBase(List<UploadedBaseDesc> list, long smeId) {
		/* 1427 */ for (int i = 0; i < list.size(); i++) {
			/* 1428 */ CallSchedulerBase entity = new CallSchedulerBase();
			/* 1429 */ UploadedBaseDesc baseDesc = list.get(i);
			/* 1430 */ entity.setAgent(Long.valueOf(baseDesc.getAgentId()));
			/* 1431 */ entity.setInsertTime(new Date());
			/* 1432 */ entity.setDescription(baseDesc.getDesc());
			/* 1433 */ entity.setMobile(baseDesc.getMobile());
			/* 1434 */ entity.setSme(Long.valueOf(smeId));
			/* 1435 */ entity.setStatus(Byte.valueOf((byte) 0));
			/* 1436 */ entity.setCallType(Integer.valueOf(0));
			/* 1437 */ entity.setScheduleDateTime(new Date());
			/* 1438 */ entity.setCallCounter(Integer.valueOf(0));
			/* 1439 */ getSession().save(entity);
			/* 1440 */ if (i % 50 == 0) {
				/* 1441 */ getSession().flush();
				/* 1442 */ getSession().clear();
				/*      */ }
			/*      */ }
		/*      */ }

	/*      */
	/*      */
	/*      */ public void saveCallBaseHistory(CallBaseHistory callBaseHistory) {
		/* 1449 */ getSession().save(callBaseHistory);
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */ public List<CallBaseHistory> getBaseHistory(long smeId) {
		/* 1455 */ Criteria criteria = getSession().createCriteria(CallBaseHistory.class);
		/* 1456 */ return criteria.add((Criterion) Restrictions.eq("smeId", Long.valueOf(smeId))).list();
		/*      */ }

	/*      */
	/*      */ private AgentDetail getAgent(long id) {
		/* 1460 */ return (AgentDetail) getSession().get(AgentDetail.class, Long.valueOf(id));
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<IncomingIvrCallCdr> getCDRLInForAgent(long agentId) {
		/* 1465 */ Criteria critIn = getSession().createCriteria(IncomingIvrCallCdr.class);
		/* 1466 */ critIn.add((Criterion) Restrictions.eq("patchedAgentId", getAgent(agentId)));
		/* 1467 */ critIn.addOrder(Order.desc("insertDateTime"));
		/* 1468 */ critIn.setMaxResults(250);
		/* 1469 */ return critIn.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<OutbondIvrCdr> getCDRLOutForAgent(long agentId) {
		/* 1474 */ Criteria critOut = getSession().createCriteria(OutbondIvrCdr.class);
		/* 1475 */ critOut.add((Criterion) Restrictions.eq("patchedAgentId", getAgent(agentId)));
		/* 1476 */ critOut.addOrder(Order.desc("insertDateTime"));
		/* 1477 */ critOut.setMaxResults(250);
		/* 1478 */ return critOut.list();
		/*      */ }

	/*      */
	/*      */
	/*      */ public List<VoicemailCallCdr> getCDRVoiceForAgent(long agentId) {
		/* 1483 */ Criteria critOut = getSession().createCriteria(VoicemailCallCdr.class);
		/* 1484 */ critOut.add((Criterion) Restrictions.eq("patchedAgentId", getAgent(agentId)));
		/* 1485 */ critOut.addOrder(Order.desc("insertDateTime"));
		/* 1486 */ critOut.setMaxResults(250);
		/* 1487 */ return critOut.list();
		/*      */ }

	/*      */
	/*      */
	/*      */
	/*      */
	/*      */ public List<CallCdrResponseBean> getCDRLForAgent(long agentId, byte catagory)
			throws FileNotFoundException, IOException {
		/* 1494 */ List<CallCdrResponseBean> cdrList = new ArrayList<>();
		/* 1495 */ if (1 == catagory) {
			/* 1496 */ for (IncomingIvrCallCdr obj : getCDRLInForAgent(agentId)) {
				/* 1497 */ cdrList.add(inCDRToBean(obj));
				/*      */ }
			/*      */ }
		/* 1500 */ else if (3 == catagory) {
			/* 1501 */ for (VoicemailCallCdr obj : getCDRVoiceForAgent(agentId)) {
				/* 1502 */ cdrList.add(voiceCDRToBean(obj));
				/*      */ }
			/*      */ } else {
			/* 1505 */ for (OutbondIvrCdr obj : getCDRLOutForAgent(agentId)) {
				/* 1506 */ cdrList.add(outCDRToBean(obj));
				/*      */ }
			/*      */ }
		/* 1509 */ return cdrList;
		/*      */ }

	/*      */
	/*      */ public List<CallCdrResponseBean> getNotAnsweredMissCall(Set<String> sessionIdSet)
			throws FileNotFoundException, IOException {
		/* 1513 */ List<CallCdrResponseBean> cdrList = new ArrayList<>();
		/* 1514 */ Criteria critMissCall = getSession().createCriteria(IncomingIvrCallCdr.class);
		/* 1515 */ critMissCall.add((Criterion) Restrictions.eq("patchedAgentId", getAgent(0L)));
		/* 1516 */ critMissCall.add(Restrictions.in("sessionId", sessionIdSet));
		/* 1517 */ critMissCall.addOrder(Order.desc("insertDateTime"));
		/* 1518 */ critMissCall.setMaxResults(250);
		/* 1519 */ List<IncomingIvrCallCdr> cdr = critMissCall.list();
		/* 1520 */ for (IncomingIvrCallCdr obj : cdr) {
			/* 1521 */ cdrList.add(inCDRToBean(obj));
			/*      */ }
		/* 1523 */ return cdrList;
		/*      */ }

	public List<AgentReportDetail> fetchAgentReportDetail(long agentId) {
		Criteria criteria = getSession().createCriteria(AgentReportDetail.class);
		criteria.add((Criterion) Restrictions.eq("agentId", getAgent(agentId)));
		criteria.addOrder(Order.desc("endDate"));
		criteria.add(Restrictions.isNotNull("sessionId"));
		criteria.setMaxResults(800);
		return criteria.list();
	}

	@Override
	public void saveSmePrompt(PromptDetailsBean promptDetails) {
		SmePrompts smePrompt = new SmePrompts();
		smePrompt.setId(promptDetails.getPromptId());
		smePrompt.setSmeId((SmeProfile) getSession().get(SmeProfile.class, promptDetails.getSmeId()));
		smePrompt.setPromptKey(promptDetails.getPromptName());
		smePrompt.setPromptPath(promptDetails.getPromptPath());
		smePrompt.setInsertDateTime(new Date());
		smePrompt.setUpdateDateTime(new Date());
		smePrompt.setPromptName(promptDetails.getPromptName());
		smePrompt.setPromptUrl(promptDetails.getPromptUrl());
		smePrompt.setPromptDescription(promptDetails.getPromptDescription());
		smePrompt.setCategory(promptDetails.getPromptCategory());
		getSession().update(smePrompt);
	}

	@Override
	public List<SmePrompts> getSmePrompt(Long smeId, Long promptId, int pageNum, int pageSize) {
		String hqlQuery = "from SmePrompts where smeId.id=" + smeId;

		Criteria criteria = getSession().createCriteria(SmePrompts.class);

//		criteria.setMaxResults(pageSize);
//		criteria.add((Criterion) Restrictions.eq("smeId", Long.valueOf(smeId)));
		if (promptId != null) {
//			criteria.add((Criterion) Restrictions.eq("id", promptId));
			hqlQuery += " and id=" + promptId;
		}
		Query<SmePrompts> qry = getSession().createQuery(hqlQuery);
		if (pageNum > 1) {
//			criteria.setFirstResult((pageNum - 1) * pageSize);
			qry.setFirstResult((pageNum - 1) * pageSize);
		}
		qry.setMaxResults(pageSize);

		return qry.list();
	}
}
