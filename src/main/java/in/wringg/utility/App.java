package in.wringg.utility;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class App
{
  public static void main(String[] args) {
/* 14 */     String string1 = "21:11:13";
    
    try {
/* 17 */       Date time1 = (new SimpleDateFormat("HH:mm:ss")).parse(string1);
      
/* 19 */       Calendar calendar1 = Calendar.getInstance();
/* 20 */       calendar1.setTime(time1);
      
/* 22 */       String string2 = "00:49:00";
/* 23 */       Date time2 = (new SimpleDateFormat("HH:mm:ss")).parse(string2);
/* 24 */       Calendar calendar2 = Calendar.getInstance();
/* 25 */       calendar2.setTime(time2);
      
/* 27 */       System.out.println(calendar2.compareTo(calendar1));
/* 28 */     } catch (ParseException e) {
      
/* 30 */       e.printStackTrace();
    } 
    
/* 33 */     SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    
    try {
/* 37 */       Date d1 = format.parse("21:11:13");
/* 38 */       Time ppstime = new Time(d1.getTime());
/* 39 */       System.out.println(ppstime);
/* 40 */     } catch (ParseException e) {
/* 41 */       e.printStackTrace();
    } 
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wring\\utility\App.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */