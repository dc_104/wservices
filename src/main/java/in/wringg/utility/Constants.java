package in.wringg.utility;

import java.util.TimeZone;

public class Constants {
	public static String APP_URL;
	/* 11 */ public static String GUI_URL = "/#!/login?action=resetpassowrd&userid=";

	public static final String NEW_PROFILE_MAIL_SUB = "Wringg Account Detail";
	public static final String NEW_PROFILE_START_MSG = "Dear User, \n \n \t This is to inform you that your account has been created successfully by Wringg, \n Please use the following credentials to login. \n \n ";
	/* 15 */ public static final String NEW_PROFILE_END_MSG = "\n \n We recommend you to reset your password through logging at "
			+ APP_URL + " \n\n Thanks and Regards, \n Team Wringg";

	public static final String EDVAANTAGE_DEMO_MAILS = "letsconnect@edvaantage.com,sales@edvaantage.com";

	public static final String EDVAANTAGE_DEMO_REQUEST = "Edvaantage Demo Request";

	public static final String EDVAANTAGE_START_MSG = "Hi Edvaantage , \n \n \t This is to inform you that someone request for a demo.\n";

	public static final String EDVAANTAGE_END_MSG = "\n\n Thanks and Regards, \n Team Wringg";

	public static final String FORGET_PASSWORD_MAIL_SUB = "Wringg Account Password Reset";

	public static final String FORGET_PASSWORD_START_MSG = "Dear User, \n \n This is to inform you that we got a password change request from you, \n Please use the following link to change password. \n \n ";

	public static final String FORGET_PASSWORD_END_MSG = "\n \n If this was not you ,then please ignore this mail.\n\n Thanks and Regards, \n Team Wringg";
	public static TimeZone SYSTEM_TIME_ZONE;
	public static String UPLOAD_BASE_PATH;
	public static String MEDIA_BASE_PATH;
	public static String MEDIA_BASE_URL;
	
	/* 32 */ public static String FILE_CHECK = "play";

	public static final String CALL_ANSWERED = "Answered";

	public static final String NOT_ANSWERED = "Failed";

	public static final String CALL_UNREACHABLE = "Unreachable";

	public static final String BLANK = "";

	public static final int INT_0 = 0;

	public static final int INT_1 = 1;

	public static final int INT_2 = 2;

	public static final int INT_3 = 3;

	public static final int INT_4 = 4;

	public static final int INT_5 = 5;

	public static final int INT_6 = 6;

	public static final int INT_7 = 7;

	public static final int INT_8 = 8;

	public static final int INT_16 = 16;
	public static final int INT_32 = 32;
	public static final int INT_64 = 64;
	public static final int AGENT_EXT_START = 1001;
	public static final int VOICEMAIL_CALL_PROCESSING = 2;
	public static final int USER_ACTIVE_STATUS = 1;
	public static final int PENDING_STATUS = 0;
	public static final int RUNNING_STATUS = 1;
	public static final int COMPLETE_STATUS = 2;
	public static final int CANCELED_STATUS = 3;
	public static final byte BLOCKED_STATUS = -7;
	public static final byte DELETE_STATUS = -9;
	public static final byte INITIAL_STATUS = 0;
	public static final byte IDLE_STATUS = 1;
	public static final byte BUSY_STATUS = 2;
	public static final byte INCOMING_IVR_CDR = 1;
	public static final byte OUTGOING_IVR_CDR = 2;
	public static final byte OUTGOING_OFFLINE_CDR = 22;
	public static final byte OUTGOING_VOICEMAIL_CDR = 23;
	public static final byte OUTGOING_VMC2CALL_CDR = 24;
	public static final byte VOICEMAIL_CALL_CDR = 3;
	public static final byte RECORDING_CALL_CDR = 4;
	public static final byte BILLING_PENDING = 0;
	public static final byte BILLING_SUCCESS = 1;
	public static final byte BILLING_FAIL = 3;
	/* 85 */ public static final Byte BYTE_0 = Byte.valueOf((byte) 0);
	/* 86 */ public static final Byte BYTE_1 = Byte.valueOf((byte) 1);
	/* 87 */ public static final Byte BYTE_2 = Byte.valueOf((byte) 2);
	/* 88 */ public static final Byte BYTE_3 = Byte.valueOf((byte) 3);
	/* 89 */ public static final Byte BYTE_4 = Byte.valueOf((byte) 4);
	/* 90 */ public static final Byte ALERT_SEEN = BYTE_0;
	/* 91 */ public static final Byte ALERT_UNSEEN = BYTE_1;

	/* 93 */ public static final Byte LONGCODE_ACTIVE = BYTE_0;
	/* 94 */ public static final Byte LONG_CODE_ASSIGNED = BYTE_1;
	/* 95 */ public static final Byte LONGCODE_IN_ACTIVE = BYTE_2;
	/* 96 */ public static final Byte VIRTUAL_LONGCODE_STATUS = BYTE_1;
	/* 97 */ public static final Byte OFF_HOUR = BYTE_3;
	/* 98 */ public static final Byte LUNCH = BYTE_4;
	/* 99 */ public static final Byte ACTIVE_STATUS = BYTE_1;
	/* 100 */ public static final Byte IN_ACTIVE = BYTE_0;

	public static final String STR_TOTAL = "Total";

	public static final String STR_RUNNING = "Runnng";

	public static final String STR_DELIVERED = "Delivered";

	public static final String STR_CANCELED = "Canceled";

	public static final String STR_PENDING = "Pending";

	public static final String STR_PROCESSED = "Processed";

	public static final String STR_FAILED = "Failed";

	public static final String AGENT = "Agent";

	public static final String ADMIN = "ADMIN";

	public static final String CLIENT = "Client";

	public static final String VOICE_MAIL = "Voicemail";

	public static final String API_SUCCESS = "Successfully Done";

	public static final String AUTHORIZATION = "Authorization";
public static enum Services {
		
		RECORDING_SERVICE("Recording", "Call Recording", INT_1),
		VOICEMAIL_SERVICE("VoiceMail", "Voicemail", INT_2),
		MASKING_SERVICE("Masking", "CLI Masking", INT_4),
		OUTBOUND_SERVICE("OUTBOUND", "Outgoing Call Campaign", INT_8),
		BULKSMS_SERVICE("BulkSms", "SMS Campaign", INT_16),
		MISSEDCALL_SERVICE("MissCall", "Missed Call Marketing", INT_32),
		CONFERENCE_SERVICE("Conference", "Conference", INT_64);
		
		private final String name;
		private final String displayName;
		private final int serviceId;

		private Services(String name, String displayName, int serviceId) {
			this.name = name;
			this.displayName = displayName;
			this.serviceId = serviceId;
		}

		public String getName() {
			return name;
		}

		public String getDisplayName() {
			return displayName;
		}

		public int getServiceId() {
			return serviceId;
		}
	}

	public static final Services[] settingServices = { 
			Services.RECORDING_SERVICE, 
			Services.VOICEMAIL_SERVICE,
			Services.MASKING_SERVICE, 
			Services.CONFERENCE_SERVICE };
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wring\\utility\Constants.class Java
 * compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */