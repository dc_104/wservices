package in.wringg.utility;

import in.wringg.pojo.FilterBean;
import in.wringg.utility.Constants;
import in.wringg.utility.DaysofWeek;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class Utility {
	/* 29 */ public static Properties projectProperties = null;

	private static Date stringToDate(String str) {
		/* 33 */ Date date = null;
		/* 34 */ DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			/* 36 */ date = formatter.parse(str);
			/* 37 */ } catch (ParseException e) {
			/* 38 */ e.printStackTrace();
		}
		/* 40 */ return date;
	}

	public static Date currentDateTime() throws ParseException {
		/* 44 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/* 45 */ Calendar calobj = Calendar.getInstance();
		/* 46 */ SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		/* 47 */ return formatter.parse(df.format(calobj.getTime()));
	}

	public static String getDateTime() {
		/* 51 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/* 52 */ df.setTimeZone(Constants.SYSTEM_TIME_ZONE);
		/* 53 */ Calendar calobj = Calendar.getInstance();
		/* 54 */ return df.format(calobj.getTime());
	}

	public static String getFormattedDateTime(Date date, String format) {
		/* 58 */ DateFormat df = new SimpleDateFormat(format);
		/* 59 */ df.setTimeZone(Constants.SYSTEM_TIME_ZONE);
		/* 60 */ Calendar calobj = Calendar.getInstance();
		/* 61 */ calobj.setTime(date);
		/* 62 */ return df.format(calobj.getTime());
	}

	public static Date timeToDate(Time time) {
		/* 67 */ Date date = new Date();
		/* 68 */ Calendar cal = Calendar.getInstance();
		/* 69 */ cal.setTime(date);
		/* 70 */ cal.set(11, time.getHours());
		/* 71 */ cal.set(12, time.getMinutes());
		/* 72 */ cal.set(13, time.getSeconds());
		/* 73 */ Date dateFinal = cal.getTime();
		/* 74 */ return dateFinal;
	}

	public static Date getFormattedFromDateTime(Date date) {
		/* 82 */ if (date == null) {
			/* 83 */ return null;
		}
		try {
			/* 86 */ Calendar cal = Calendar.getInstance();
			/* 87 */ cal.setTime(date);
			/* 88 */ cal.set(11, 0);
			/* 89 */ cal.set(12, 0);
			/* 90 */ cal.set(13, 0);
			/* 91 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			/* 92 */ Date date1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(df.format(cal.getTime()));
			/* 93 */ return date1;
			/* 94 */ } catch (ParseException e) {
			/* 95 */ e.printStackTrace();

			/* 97 */ return date;
		}
	}

	public static Date getFormattedToDateTime(Date date) {
		try {
			/* 102 */ DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			/* 103 */ Calendar cal = Calendar.getInstance();
			/* 104 */ cal.setTime(date);
			/* 105 */ cal.set(11, 23);
			/* 106 */ cal.set(12, 59);
			/* 107 */ cal.set(13, 59);
			/* 108 */ Date date1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(df.format(cal.getTime()));
			/* 109 */ return date1;
			/* 110 */ } catch (Exception e) {
			/* 111 */ e.printStackTrace();

			/* 113 */ return date;
		}
	}

	public static Date addDays(Date date, int days) {
		/* 117 */ Calendar c = Calendar.getInstance();
		/* 118 */ c.setTime(date);
		/* 119 */ c.add(5, days);
		/* 120 */ Date newdt = c.getTime();
		/* 121 */ return newdt;
	}

	public static Criteria genericFilterCriteria(Criteria criteria, FilterBean bean) {
		/* 125 */ switch (bean.getOp().intValue()) {
		case 1:
			/* 127 */ criteria.add((Criterion) Restrictions.eq(bean.getName(), bean.getVal()));
			break;
		case 2:
			/* 130 */ criteria.add((Criterion) Restrictions.ne(bean.getName(), bean.getVal()));
			break;
		case 3:
			/* 133 */ criteria.add(Restrictions.sqlRestriction(bean.getName() + " like '%" + bean.getVal() + "%'"));
			break;
		case 4:
			/* 136 */ criteria.add((Criterion) Restrictions.like(bean.getName(), bean.getVal(), MatchMode.ANYWHERE));
			break;
		case 5:
			/* 139 */ criteria.add(Restrictions.not(Restrictions.in(bean.getName(), new Object[] { bean.getVal() })));
			break;
		case 6:
			/* 142 */ criteria.add((Criterion) Restrictions.like(bean.getName(), bean.getVal(), MatchMode.END));
			break;

		case 11:
			/* 153 */ criteria.add(/* 154 */ (Criterion) Restrictions.eq(bean.getName(),
					Integer.valueOf(Integer.parseInt((bean.getVal() == null) ? "0" : bean.getVal()))));
			break;
		case 12:
			/* 157 */ criteria.add(/* 158 */ (Criterion) Restrictions.ne(bean.getName(),
					Integer.valueOf(Integer.parseInt((bean.getVal() == null) ? "0" : bean.getVal()))));
			break;
		case 13:
			/* 161 */ criteria.add(/* 162 */ (Criterion) Restrictions.gt(bean.getName(),
					Integer.valueOf(Integer.parseInt((bean.getVal() == null) ? "0" : bean.getVal()))));
			break;
		case 14:
			/* 165 */ criteria.add(/* 166 */ (Criterion) Restrictions.lt(bean.getName(),
					Integer.valueOf(Integer.parseInt((bean.getVal() == null) ? "0" : bean.getVal()))));
			break;

		case 31:
			/* 201 */ criteria.add(
					(Criterion) Restrictions.gt(bean.getName(), getFormattedFromDateTime(stringToDate(bean.getVal()))));
			/* 202 */ criteria.add(
					(Criterion) Restrictions.lt(bean.getName(), getFormattedToDateTime(stringToDate(bean.getVal()))));
			break;
		case 32:
			/* 205 */ criteria.add(
					(Criterion) Restrictions.ne(bean.getName(), getFormattedFromDateTime(stringToDate(bean.getVal()))));
			/* 206 */ criteria.add(
					(Criterion) Restrictions.ne(bean.getName(), getFormattedToDateTime(stringToDate(bean.getVal()))));
			break;
		case 33:
			/* 209 */ criteria.add(
					(Criterion) Restrictions.gt(bean.getName(), getFormattedFromDateTime(stringToDate(bean.getVal()))));
			break;
		case 34:
			/* 212 */ criteria.add(
					(Criterion) Restrictions.lt(bean.getName(), getFormattedToDateTime(stringToDate(bean.getVal()))));
			break;

		case 41:
			/* 219 */ criteria
					.add((Criterion) Restrictions.like(bean.getName(), Long.valueOf(Long.parseLong(bean.getVal()))));
			break;
		}

		/* 233 */ return criteria;
	}

	public static String genericFilterCriteria(FilterBean bean) {
		/* 238 */ switch (bean.getOp().intValue()) {
		case 1:
			/* 240 */ return bean.getName() + "='" + bean.getVal() + "'";
		case 2:
			/* 242 */ return bean.getName() + "!='" + bean.getVal() + "'";
		case 3:
			/* 244 */ return bean.getName() + " like '" + bean.getVal() + "%'";
		case 4:
			/* 246 */ return bean.getName() + " like '%" + bean.getVal() + "%'";
		case 5:
			/* 248 */ return bean.getName() + " not like '%" + bean.getVal() + "%'";
		case 6:
			/* 250 */ return bean.getName() + " like '%" + bean.getVal() + "'";
		case 11:
			/* 252 */ return bean.getName() + "=" + bean.getVal();
		case 12:
			/* 254 */ return bean.getName() + "!=" + bean.getVal();
		case 13:
			/* 256 */ return bean.getName() + ">" + bean.getVal();
		case 14:
			/* 258 */ return bean.getName() + "<" + bean.getVal();

		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
			/* 304 */ return "";
		case 31:
			return bean.getName() + "<" + getFormattedFromDateTime(stringToDate(bean.getVal())) + bean.getName() + ">"
					+ getFormattedFromDateTime(stringToDate(bean.getVal()));
		case 32:
			return bean.getName() + "!=" + bean.getVal();
		case 33:
			return bean.getName() + ">'" + bean.getVal() + "'";
		case 34:
			/* 309 */ return bean.getName() + "<'" + bean.getVal() + "'";
		}
		return "";
	}

	private static void loadProperties() throws FileNotFoundException, IOException {
		if (projectProperties == null) {
			/* 310 */ ClassLoader classLoader = in.wringg.utility.Utility.class.getClassLoader();
			/* 311 */ File file = new File(classLoader.getResource("pbxservices.properties").getFile());
			/* 312 */ projectProperties = new Properties();
			/* 313 */ projectProperties.load(new FileInputStream(file));
		}
	}

	public static String getProjectProperty(String key) throws FileNotFoundException, IOException {
		/* 318 */ if (projectProperties == null) {
			/* 319 */ loadProperties();
		}
		/* 321 */ return projectProperties.getProperty(key);
	}

	public static Set<Object> getProjectPropertyKeySet() throws FileNotFoundException, IOException {
		/* 325 */ if (projectProperties == null) {
			/* 326 */ loadProperties();
		}
		/* 328 */ return projectProperties.keySet();
	}

	public static List<String> getWeekdays(int val) {
		List<String> days = new ArrayList<>();
		for (int day = 0; day < 7; day++) {
			int dayValue = (int) Math.pow(2, day);
			if (dayValue > val)
				break;
			if ((val & dayValue) > 0) {
				switch (dayValue) {
				case 64:
					days.add("SUN");
					break;
				case 32:
					days.add("SAT");
					break;
				case 16:
					days.add("FRI");
					break;
				case 8:
					days.add("THU");
					break;
				case 4:
					days.add("WED");
					break;
				case 2:
					days.add("TUE");
					break;
				case 1:
					days.add("MON");
					break;
				}
			}
		}
///* 332 */     int[] week = { 64, 32, 16, 8, 4, 2, 1 };
//    
///* 334 */     boolean[] weekday = { false, false, false, false, false, false, false };
///* 335 */     
//
//    
///* 338 */     for (int i = 0; i < week.length; i++) {
///* 339 */       int f = week[i];
///* 340 */       if (f <= val) {
///* 341 */         for (int j = 0; j < week.length; j++) {
///* 342 */           if (week[j] == f) {
///* 343 */             weekday[j] = true;
//          }
//        } 
//        
///* 347 */         val -= f;
//      } 
//    } 
///* 350 */     int c = 0;
//    
///* 352 */     Map<Integer, String> enumMap = new HashMap<>();
///* 353 */     enumMap.put(Integer.valueOf(DaysofWeek.SUN.value), "SUN");
///* 354 */     enumMap.put(Integer.valueOf(DaysofWeek.SAT.value), "SAT");
///* 355 */     enumMap.put(Integer.valueOf(DaysofWeek.FRI.value), "FRI");
///* 356 */     enumMap.put(Integer.valueOf(DaysofWeek.THU.value), "THU");
///* 357 */     enumMap.put(Integer.valueOf(DaysofWeek.WED.value), "WED");
///* 358 */     enumMap.put(Integer.valueOf(DaysofWeek.TUE.value), "TUE");
///* 359 */     enumMap.put(Integer.valueOf(DaysofWeek.MON.value), "MON");
///* 360 */     for (boolean a : weekday) {
///* 361 */       if (a == true) {
///* 362 */         System.out.println(c);
///* 363 */         days.add(enumMap.get(Integer.valueOf(c)));
//      } 
///* 365 */       c++;
//    } 
		return days;
	}

	public enum getServiceName {
		Inbound, OutBound// , MissCall, Conference,BulkSms
	}

	public static String getNextDay(String currentDay) {
		/* 371 */ String[] days = { "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN" };
		/* 372 */ String returnDay = "";
		/* 373 */ for (int i = 0; i < days.length; i++) {
			/* 374 */ if (days[i].equalsIgnoreCase(currentDay)) {
				/* 375 */ if (i == days.length - 1) {
					/* 376 */ returnDay = "MON";
					break;
				}
				/* 378 */ returnDay = days[i + 1];

				break;
			}
		}
		/* 383 */ return returnDay;
	}

	public static int getDateTimeDiff(String start, String end) {
		/* 388 */ int c = 0;

		try {
			/* 391 */ Date startDate = (new SimpleDateFormat("HH:mm:ss")).parse(start);

			/* 393 */ Calendar calStart = Calendar.getInstance();
			/* 394 */ calStart.setTime(startDate);

			/* 397 */ Date endDate = (new SimpleDateFormat("HH:mm:ss")).parse(end);
			/* 398 */ Calendar calEnd = Calendar.getInstance();
			/* 399 */ calEnd.setTime(endDate);

			/* 401 */ c = calEnd.compareTo(calStart);
			/* 402 */ } catch (ParseException e) {
			/* 403 */ e.printStackTrace();
		}
		/* 405 */ return c;
	}

	public static String genrateToken(int n) {
		/* 409 */ String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvxyz";
		/* 410 */ return randomPick(alphaNumericString, n);
	}

	private static String randomPick(String str, int n) {
		/* 414 */ StringBuilder sb = new StringBuilder(n);
		/* 415 */ for (int i = 0; i < n; i++) {
			/* 416 */ int index = (int) (str.length() * Math.random());
			/* 417 */ sb.append(str.charAt(index));
		}
		/* 419 */ return sb.toString();
	}

	public static String genrateDigit(int n) {
		/* 423 */ String digit = "0123456789";
		/* 424 */ return randomPick(digit, n);
	}

	public static void main(String[] args) {
		/* 428 */ System.out.println(genrateToken(64));
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wring\\utility\Utility.class Java
 * compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */