package in.wringg.utility;

public enum ErrorConstants
{
/*  5 */   ERROR_GENERAL(Integer.valueOf(2001), "Problem in response.", "Problem while submitting request."),
/*  6 */   ERROR_AGENT_RESTRICTION(Integer.valueOf(1001), "Number of allocated agents can't be reduced.", "Number of allocated agents can't be reduced."),
/*  7 */   ERROR_AGENT_CAPACITY(Integer.valueOf(1002), "Agent capacity of Client is full.", "Can't create new agent, maximum allocated agents already created."),
/*  8 */   ERROR_USER_NOT_EXIST(Integer.valueOf(1003), "User Not Exist", "User Not Exist"),
/*  9 */   ERROR_PASSWORD_INCORRRECT(Integer.valueOf(1004), "Incorrect Password", "Incorrect Password"),
/* 10 */   ERROR_LINK_INVALID(Integer.valueOf(1005), "Invalid Activation Link", "Invalid Activation Link"),
/* 11 */   ERROR_LINK_EXPIRED(Integer.valueOf(1006), "Activation Link Expired ", "Activation Link Expired"),
/* 12 */   ERROR_SYSTEM_CAPACITY_FULL(Integer.valueOf(1007), "System Capacity full", "System Capacity full"),
/* 13 */   ERROR_AGENT_LIMIT_EXCEED(Integer.valueOf(1008), "Agent limit exceed", "Agent limit exceed"),
/* 14 */   ERROR_CAN_DECREASE_AGENTS(Integer.valueOf(1009), "You can't decrease agents", "You can't decrease agents"),
/* 15 */   ERROR_PROFILE_NOT_FOUND(Integer.valueOf(1010), "Profile Not Found", "Profile Not Found"),
/* 16 */   ERROR_COMPLAINT_NOT_FOUND(Integer.valueOf(1011), "Complaint Not Found", "Complaint Not Found"),
/* 17 */   ERROR_BILLING_PENDING(Integer.valueOf(1012), "Your billing is under process, can not alter agents", "Your billing is under process, can not alter agents"),
/* 18 */   ERROR_CREATION_EXCEED(Integer.valueOf(1013), "You are not allowed to remove the agents because SME agents are still active", "You are not allowed to remove the agents because SME agents are still active"),
/* 19 */   ERROR_BILLING_PENDING_AENET(Integer.valueOf(1014), "Your billing is under process, can not create agents", "Your billing is under process, can not create agents"),
/* 20 */   ERROR_NOTIFICATION(Integer.valueOf(1015), "Notification does not exist", "Notification does not exist"),
/* 21 */   ERROR_NOT_ONLY_CONFERENCE(Integer.valueOf(1016), "Can't change to conference only", "Can't change to conference only"),
/* 22 */   ERROR_LONGCODE_NOT_EXIST(Integer.valueOf(1017), "Long code not exist", "Long code not exist"),
/* 23 */   ERROR_LONGCODE_ASSIGNED(Integer.valueOf(1018), "Can't updated,Longcode already assigned", "Can't updated,Longcode already assigned"),
/* 24 */   ERROR_INVALID_LONGCODE_STATUS(Integer.valueOf(1019), "Invalid updattion status passed", "Invalid updattion status passed"),
/* 25 */   ERROR_LONGCODE_NOT_ACTIVE(Integer.valueOf(1020), "Long code is not active", "Long code is not active"),
/* 26 */   ERROR_FAILED_BILLING_ADDING_AGENT(Integer.valueOf(1021), "Already failed Billing can't add agents ", "Already failed Billing, Can't add more agents, only decrease agent in failed billing case"),
/* 27 */   ERROR_FAILED_AGENT_EMAIL_ALREADY_USED(Integer.valueOf(1022), "This email is already used via another agent", "This email is already used via another agent"),
/* 28 */   ERROR_FAILED_AGENT_MOBILE_ALREADY_USED(Integer.valueOf(1023), "This mobile number is already used via another agent", "This mobile number is already used via another agent"),
/* 29 */   ERROR_MAX_BASE(Integer.valueOf(1024), "Max 1000 contact upload at a time", "Max 1000 contact upload at a time"),
/* 30 */   ERROR_ALREADY_LUNCH_IN(Integer.valueOf(1025), "Agent is already on lunch", "Agent is already on lunch"),
/* 31 */   ERROR_NOT_LUNCH_IN(Integer.valueOf(1026), "Agent is not on lunch", "Agent is not on lunch"),
/* 32 */   ERROR_AGENT_NOT_ELIGIBLE(Integer.valueOf(1027), "Agent not eligible for lunch entry", "Agent not eligible for lunch entry"),
/* 33 */   DUPLICATE_ADDRESS_BOOK(Integer.valueOf(1028), "The primary moblie number is already in use", "The primary moblie number is already in use"),
/* 34 */   ADDRESS_BOOK_NOT_EXIST(Integer.valueOf(1029), "Address book not exist", "Address book not exist"),
/* 35 */   NOT_PERMITTED(Integer.valueOf(1030), "You are not permitted to perform this action", "You are not permitted to perform this action"),
/* 36 */   REMARK_NOT_EXIST(Integer.valueOf(1032), "Remark not exist", "Remark not exist"),
/* 37 */   SCHEDULE_CALL_NOT_EXIST(Integer.valueOf(1033), "Schedule call not exist", "Schedule call not exist");
  
  private final String errorMessage;
  
  private final String displayMessage;
  
  private final Integer errorCode;
  
  ErrorConstants(Integer errorCode, String errorMessage, String displayMessage) {
/* 46 */     this.errorMessage = errorMessage;
/* 47 */     this.displayMessage = displayMessage;
/* 48 */     this.errorCode = errorCode;
  }
  
  public String getErrorMessage() {
/* 52 */     return this.errorMessage;
  }
  
  public String getDisplayMessage() {
/* 56 */     return this.displayMessage;
  }
  
  public Integer getErrorCode() {
/* 60 */     return this.errorCode;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wring\\utility\ErrorConstants.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */