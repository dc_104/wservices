package in.wringg.utility;

import in.wringg.utility.ErrorConstants;

public class ErrorDetails
{
  Integer errorCode;
  String errorString;
  String displayMessage;
  
  public ErrorDetails() {}
  
  public ErrorDetails(ErrorConstants error) {
/* 14 */     this.errorCode = error.getErrorCode();
/* 15 */     this.errorString = error.getErrorMessage();
/* 16 */     this.displayMessage = error.getDisplayMessage();
  }
  
  public Integer getErrorCode() {
/* 20 */     return this.errorCode;
  }
  
  public void setErrorCode(Integer errorCode) {
/* 24 */     this.errorCode = errorCode;
  }
  
  public String getErrorString() {
/* 28 */     return this.errorString;
  }
  
  public void setErrorString(String errorString) {
/* 32 */     this.errorString = errorString;
  }
  
  public String getDisplayMessage() {
/* 36 */     return this.displayMessage;
  }
  
  public void setDisplayMessage(String displayMessage) {
/* 40 */     this.displayMessage = displayMessage;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wring\\utility\ErrorDetails.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */