package in.wringg.utility;

public enum DaysofWeek {
	MON(1), TUE(2), WED(4), THU(8), FRI(16), SAT(32), SUN(64);

	public int value;

	DaysofWeek(int value) {
		this.value = value;
	}

	public static String getDayName(int index) {
		/* 23 */ if (index > 7 && index < 1)
			/* 24 */ return null;
		/* 25 */ return values()[index - 1].toString();
	}
}
