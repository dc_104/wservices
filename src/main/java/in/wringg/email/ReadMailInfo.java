package in.wringg.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class ReadMailInfo
{
  protected static String id;
  protected static String pass;
  public static String sub;
  public static String msgStart;
  public static String msgEnd;
  
  ReadMailInfo(String sub, String msgStart, String msgEnd) throws IOException {
/* 18 */     Properties prpts = new Properties();
/* 19 */     ClassLoader classLoader = getClass().getClassLoader();
/* 20 */     File file = new File(classLoader.getResource("mailInfo.cfg").getFile());
/* 21 */     prpts.load(new FileInputStream(file));
/* 22 */     id = prpts.getProperty("id");
/* 23 */     pass = prpts.getProperty("pass");
/* 24 */     in.wringg.email.ReadMailInfo.sub = sub;
/* 25 */     in.wringg.email.ReadMailInfo.msgStart = msgStart;
/* 26 */     in.wringg.email.ReadMailInfo.msgEnd = msgEnd;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\email\ReadMailInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */