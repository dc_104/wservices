package in.wringg.email;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSender {
	public MailSender(String to, String sub, String startMsg, String endMsg, String info) throws IOException {
		new ReadMailInfo(sub, startMsg, endMsg);
		/* 22 */ String username = ReadMailInfo.id;
		/* 23 */ String password = ReadMailInfo.pass;
		/* 24 */ Properties properties = new Properties();
		/* 25 */ properties.put("mail.smtp.auth", "true");
		/* 26 */ properties.put("mail.smtp.starttls", "true");
		/* 27 */ properties.put("mail.smtp.host", "smtp.gmail.com");
		/* 28 */ properties.put("mail.smtp.starttls.enable", "true");
		/* 29 */ properties.put("mail.smtp.port", "587");
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			/* 37 */ MimeMessage mimeMessage = new MimeMessage(session);
			/* 38 */ mimeMessage.setFrom((Address) new InternetAddress(username));
			/* 39 */ mimeMessage.setRecipients(Message.RecipientType.TO, (Address[]) InternetAddress.parse(to));
			/* 40 */ mimeMessage.setSubject(ReadMailInfo.sub);
			/* 41 */ MimeBodyPart mimeBodyPart = new MimeBodyPart();
			/* 42 */ mimeBodyPart.setText(startMsg + info + endMsg);
			/* 43 */ MimeMultipart mimeMultipart = new MimeMultipart();
			/* 44 */ mimeMultipart.addBodyPart((BodyPart) mimeBodyPart);
			/* 45 */ mimeMessage.setContent((Multipart) mimeMultipart);
			/* 46 */ Transport.send((Message) mimeMessage);
			/* 47 */ System.out.println("Sent Message Successfully");
			/* 48 */ } catch (Exception ex) {
			/* 49 */ ex.printStackTrace();
			/* 50 */ System.out.println("Error is" + ex);
		}
	}

	public MailSender(String evaanStartMsg, String evaanEndMsg, String evaanInfo) throws IOException {
		/* 55 */ new ReadMailInfo("Edvaantage Demo Request", evaanStartMsg, evaanEndMsg);
		/* 56 */ String username = ReadMailInfo.id;
		/* 57 */ String password = ReadMailInfo.pass;
		/* 58 */ Properties properties = new Properties();
		/* 59 */ properties.put("mail.smtp.auth", "true");
		/* 60 */ properties.put("mail.smtp.starttls", "true");
		/* 61 */ properties.put("mail.smtp.host", "smtp.gmail.com");
		/* 62 */ properties.put("mail.smtp.starttls.enable", "true");
		/* 63 */ properties.put("mail.smtp.port", "587");
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			/* 71 */ MimeMessage mimeMessage = new MimeMessage(session);
			/* 72 */ mimeMessage.setFrom((Address) new InternetAddress(username));
			/* 73 */ mimeMessage.setRecipients(Message.RecipientType.TO,
					(Address[]) InternetAddress.parse("letsconnect@edvaantage.com,sales@edvaantage.com"));
			/* 74 */ mimeMessage.setSubject(ReadMailInfo.sub);
			/* 75 */ MimeBodyPart mimeBodyPart = new MimeBodyPart();
			/* 76 */ mimeBodyPart.setText(evaanStartMsg + evaanInfo + evaanEndMsg);
			/* 77 */ MimeMultipart mimeMultipart = new MimeMultipart();
			/* 78 */ mimeMultipart.addBodyPart((BodyPart) mimeBodyPart);
			/* 79 */ mimeMessage.setContent((Multipart) mimeMultipart);
			/* 80 */ Transport.send((Message) mimeMessage);
			/* 81 */ System.out.println("Sent Message Successfully");
			/* 82 */ } catch (Exception ex) {
			/* 83 */ ex.printStackTrace();
			/* 84 */ System.out.println("Error is" + ex);
		}
	}

	public static void main(String[] args) throws IOException {
		/* 89 */ new in.wringg.email.MailSender("akshaywr@gmail.com", "Sub ", "Start", "End", "Info");
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\email\MailSender.class Java
 * compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */