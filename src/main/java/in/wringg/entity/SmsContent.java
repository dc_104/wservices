package in.wringg.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;





@Entity
@Table(name = "sms_content")
@NamedQuery(name = "SmsContent.findAll", query = "SELECT s FROM SmsContent s")
public class SmsContent
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "sms_content_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sms_content_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "sms_content_gen")
  private Long id;
  private String message;
  
  public Long getId() {
/* 32 */     return this.id;
  }
  
  public void setId(Long id) {
/* 36 */     this.id = id;
  }
  
  public String getMessage() {
/* 40 */     return this.message;
  }
  
  public void setMessage(String message) {
/* 44 */     this.message = message;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmsContent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */