package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;














@Entity
@Table(name = "outbond_ivr_campaign")
@NamedQuery(name = "OutbondIvrCampaign.findAll", query = "SELECT o FROM OutbondIvrCampaign o")
public class OutbondIvrCampaign
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "base_file_name")
  private String baseFileName;
  @Column(name = "campaign_name")
  private String campaignName;
  @Column(name = "campaign_status")
  private Byte campaignStatus;
  @Column(name = "campaign_type")
  private String campaignType;
  private String cli;
  @Id
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Column(name = "media_file")
  private String mediaFile;
  @Column(name = "schedule_end_time")
  private Time scheduleEndTime;
  @Column(name = "schedule_start_time")
  private Time scheduleStartTime;
  @Column(name = "sme_id")
  private Long smeId;
  @Column(name = "sme_simultaneously_calls")
  private int smeSimultaneouslyCalls;
  @Column(name = "total_calls")
  private int totalCalls;
  
  public String getBaseFileName() {
/*  63 */     return this.baseFileName;
  }
  
  public void setBaseFileName(String baseFileName) {
/*  67 */     this.baseFileName = baseFileName;
  }
  
  public String getCampaignName() {
/*  71 */     return this.campaignName;
  }
  
  public void setCampaignName(String campaignName) {
/*  75 */     this.campaignName = campaignName;
  }
  
  public Byte getCampaignStatus() {
/*  79 */     return this.campaignStatus;
  }
  
  public void setCampaignStatus(Byte campaignStatus) {
/*  83 */     this.campaignStatus = campaignStatus;
  }
  
  public String getCampaignType() {
/*  87 */     return this.campaignType;
  }
  
  public void setCampaignType(String campaignType) {
/*  91 */     this.campaignType = campaignType;
  }
  
  public String getCli() {
/*  95 */     return this.cli;
  }
  
  public void setCli(String cli) {
/*  99 */     this.cli = cli;
  }
  
  public BigInteger getId() {
/* 103 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 107 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 111 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 115 */     this.insertDateTime = insertDateTime;
  }
  
  public String getMediaFile() {
/* 119 */     return this.mediaFile;
  }
  
  public void setMediaFile(String mediaFile) {
/* 123 */     this.mediaFile = mediaFile;
  }
  
  public Time getScheduleEndTime() {
/* 127 */     return this.scheduleEndTime;
  }
  
  public void setScheduleEndTime(Time scheduleEndTime) {
/* 131 */     this.scheduleEndTime = scheduleEndTime;
  }
  
  public Date getScheduleStartTime() {
/* 135 */     return this.scheduleStartTime;
  }
  
  public void setScheduleStartTime(Time scheduleStartTime) {
/* 139 */     this.scheduleStartTime = scheduleStartTime;
  }
  
  public Long getSmeId() {
/* 143 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 147 */     this.smeId = smeId;
  }
  
  public int getSmeSimultaneouslyCalls() {
/* 151 */     return this.smeSimultaneouslyCalls;
  }
  
  public void setSmeSimultaneouslyCalls(int smeSimultaneouslyCalls) {
/* 155 */     this.smeSimultaneouslyCalls = smeSimultaneouslyCalls;
  }
  
  public int getTotalCalls() {
/* 159 */     return this.totalCalls;
  }
  
  public void setTotalCalls(int totalCalls) {
/* 163 */     this.totalCalls = totalCalls;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\OutbondIvrCampaign.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */