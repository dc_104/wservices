package in.wringg.entity;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "voicemail_call_cdr")
@NamedQuery(name = "VoicemailCallCdr.findAll", query = "SELECT v FROM VoicemailCallCdr v")
public class VoicemailCallCdr implements Serializable { private static final long serialVersionUID = 1L; @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "voicemail_call_cdr_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "voicemail_call_cdr_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "voicemail_call_cdr_gen")
  private Long id; @Column(name = "answer")
  private Byte answer; @Column(name = "agent_group")
  private String agentGroup; @Column(name = "merge_status")
  private Byte mergeStatus; @Column(name = "session_id")
  private String sessionId; @Column(name = "call_direction")
  private String callDirection; @Column(name = "call_direction_status")
  private Integer callDirectionStatus;
  @Column(name = "call_recorded_file")
  private String callRecordedFile;
  @Column(name = "call_recording_status")
  private Integer callRecordingStatus;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private Byte cdrMode;
  @Column(name = "channel_no")
  private Integer channelNo;
  private Integer duration;
  
/*  30 */   public Byte getAnswer() { return this.answer; } @Temporal(TemporalType.TIMESTAMP) @Column(name = "end_date_time") private Date endDateTime; private String hlr; @Temporal(TemporalType.TIMESTAMP) @Column(name = "insert_date_time") private Date insertDateTime; private Long longcode; @Column(name = "master_shortcode") private String masterShortcode; @ManyToOne @JoinColumn(name = "patched_agent_id") private AgentDetail patchedAgentId; @Column(name = "server_ip_address") private String serverIpAddress; @Column(name = "shortcode_mapping") private String shortcodeMapping; @ManyToOne @JoinColumn(name = "sme_id") private SmeProfile smeId; @Column(name = "sme_identifier") private String smeIdentifier; @Temporal(TemporalType.TIMESTAMP) @Column(name = "start_date_time") private Date startDateTime; @Column(name = "voicemail_recording_file")
  private String voicemailRecordingFile; @Column(name = "voicemail_recording_status")
  private Integer voicemailRecordingStatus; @Column(name = "address_book_id")
  private Long addressBookId; @Column(name = "customer_name")
/*  34 */   private String customerName; public void setAnswer(Byte answer) { this.answer = answer; }







  
  public Byte getMergeStatus() {
/*  44 */     return this.mergeStatus;
  }
  
  public void setMergeStatus(Byte mergeStatus) {
/*  48 */     this.mergeStatus = mergeStatus;
  }



  
  public String getSessionId() {
/*  55 */     return this.sessionId;
  }
  
  public void setSessionId(String sessionId) {
/*  59 */     this.sessionId = sessionId;
  }











































































  
  public Long getAddressBookId() {
/* 138 */     return this.addressBookId;
  }
  
  public void setAddressBookId(Long addressBookId) {
/* 142 */     this.addressBookId = addressBookId;
  }
  
  public String getCustomerName() {
/* 146 */     return this.customerName;
  }
  
  public void setCustomerName(String customerName) {
/* 150 */     this.customerName = customerName;
  }



  
  public Long getId() {
/* 157 */     return this.id;
  }
  
  public void setId(Long id) {
/* 161 */     this.id = id;
  }
  
  public String getAgentGroup() {
/* 165 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/* 169 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/* 173 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 177 */     this.callDirection = callDirection;
  }
  
  public Integer getCallDirectionStatus() {
/* 181 */     return this.callDirectionStatus;
  }
  
  public void setCallDirectionStatus(Integer callDirectionStatus) {
/* 185 */     this.callDirectionStatus = callDirectionStatus;
  }
  
  public String getCallRecordedFile() {
/* 189 */     return this.callRecordedFile;
  }
  
  public void setCallRecordedFile(String callRecordedFile) {
/* 193 */     this.callRecordedFile = callRecordedFile;
  }
  
  public Integer getCallRecordingStatus() {
/* 197 */     return this.callRecordingStatus;
  }
  
  public void setCallRecordingStatus(Integer callRecordingStatus) {
/* 201 */     this.callRecordingStatus = callRecordingStatus;
  }
  
  public String getCalledNumber() {
/* 205 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 209 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 213 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 217 */     this.callingNumber = callingNumber;
  }
  
  public Byte getCdrMode() {
/* 221 */     return this.cdrMode;
  }
  
  public void setCdrMode(Byte cdrMode) {
/* 225 */     this.cdrMode = cdrMode;
  }
  
  public Integer getChannelNo() {
/* 229 */     return this.channelNo;
  }
  
  public void setChannelNo(Integer channelNo) {
/* 233 */     this.channelNo = channelNo;
  }
  
  public Integer getDuration() {
/* 237 */     return this.duration;
  }
  
  public void setDuration(Integer duration) {
/* 241 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 245 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 249 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 253 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 257 */     this.hlr = hlr;
  }
  
  public Date getInsertDateTime() {
/* 261 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 265 */     this.insertDateTime = insertDateTime;
  }
  
  public Long getLongcode() {
/* 269 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/* 273 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 277 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 281 */     this.masterShortcode = masterShortcode;
  }

  
  public String getServerIpAddress() {
/* 286 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 290 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 294 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 298 */     this.shortcodeMapping = shortcodeMapping;
  }


  
  public AgentDetail getPatchedAgentId() {
/* 304 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(AgentDetail patchedAgentId) {
/* 308 */     this.patchedAgentId = patchedAgentId;
  }
  
  public SmeProfile getSmeId() {
/* 312 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 316 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 320 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 324 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 328 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 332 */     this.startDateTime = startDateTime;
  }
  
  public String getVoicemailRecordingFile() {
/* 336 */     return this.voicemailRecordingFile;
  }
  
  public void setVoicemailRecordingFile(String voicemailRecordingFile) {
/* 340 */     this.voicemailRecordingFile = voicemailRecordingFile;
  }
  
  public Integer getVoicemailRecordingStatus() {
/* 344 */     return this.voicemailRecordingStatus;
  }
  
  public void setVoicemailRecordingStatus(Integer voicemailRecordingStatus) {
/* 348 */     this.voicemailRecordingStatus = voicemailRecordingStatus;
  } }


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\VoicemailCallCdr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */