package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;







@Entity
@Table(name = "agent_group_mapping")
@NamedQuery(name = "AgentGroupMapping.findAll", query = "SELECT a FROM AgentGroupMapping a")
public class AgentGroupMapping
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "group_id")
  private BigInteger agentGroupId;
  @Column(name = "agent_id")
  private BigInteger agentId;
  @Id
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_date_time")
  private Date updateDateTime;
  
  public BigInteger getAgentGroupId() {
/* 41 */     return this.agentGroupId;
  }
  
  public void setAgentGroupId(BigInteger agentGroupId) {
/* 45 */     this.agentGroupId = agentGroupId;
  }
  
  public BigInteger getAgentId() {
/* 49 */     return this.agentId;
  }
  
  public void setAgentId(BigInteger agentId) {
/* 53 */     this.agentId = agentId;
  }
  
  public BigInteger getId() {
/* 57 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 61 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 65 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 69 */     this.insertDateTime = insertDateTime;
  }

  
  public Date getUpdateDateTime() {
/* 74 */     return this.updateDateTime;
  }
  
  public void setUpdateDateTime(Date updateDateTime) {
/* 78 */     this.updateDateTime = updateDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentGroupMapping.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */