package in.wringg.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "outbond_ivr_cdr")
@NamedQuery(name = "OutbondIvrCdr.findAll", query = "SELECT o FROM OutbondIvrCdr o")
public class OutbondIvrCdr implements Serializable { private static final long serialVersionUID = 1L; @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "outbond_ivr_cdr_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "outbond_ivr_cdr_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "outbond_ivr_cdr_gen")
  private Long id; @Column(name = "merge_status")
  private Byte mergeStatus; @Column(name = "answer")
  private Byte answer;
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "session_id")
  private String sessionId;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "call_direction_status")
  private Integer callDirectionStatus;
  @Column(name = "call_recorded_file")
  private String callRecordedFile;
  @Column(name = "call_recording_status")
  private Integer callRecordingStatus;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private Byte cdrMode;
  @Column(name = "channel_no")
  private Integer channelNo;
  private Integer duration;
  
/*  34 */   public Byte getAnswer() { return this.answer; } @Temporal(TemporalType.TIMESTAMP) @Column(name = "end_date_time") private Date endDateTime; private String hlr; @Temporal(TemporalType.TIMESTAMP) @Column(name = "insert_date_time") private Date insertDateTime; private Long longcode; @Column(name = "master_shortcode") private String masterShortcode; @ManyToOne @JoinColumn(name = "patched_agent_id") private AgentDetail patchedAgentId; @Column(name = "server_ip_address") private String serverIpAddress; @Column(name = "shortcode_mapping") private String shortcodeMapping; @ManyToOne @JoinColumn(name = "sme_id") private SmeProfile smeId; @Column(name = "sme_identifier") private String smeIdentifier; @Temporal(TemporalType.TIMESTAMP) @Column(name = "start_date_time") private Date startDateTime; @Column(name = "voicemail_recording_file")
  private String voicemailRecordingFile; @Column(name = "voicemail_recording_status")
  private int voicemailRecordingStatus; @Column(name = "address_book_id")
  private Long addressBookId; @Column(name = "customer_name")
/*  38 */   private String customerName; public void setAnswer(Byte answer) { this.answer = answer; }

  
  public Byte getMergeStatus() {
/*  42 */     return this.mergeStatus;
  }
  
  public void setMergeStatus(Byte mergeStatus) {
/*  46 */     this.mergeStatus = mergeStatus;
  }






  
  public String getSessionId() {
/*  56 */     return this.sessionId;
  }
  
  public void setSessionId(String sessionId) {
/*  60 */     this.sessionId = sessionId;
  }
  
  public void setVoicemailRecordingStatus(int voicemailRecordingStatus) {
/*  64 */     this.voicemailRecordingStatus = voicemailRecordingStatus;
  }











































































  
  public Long getAddressBookId() {
/* 143 */     return this.addressBookId;
  }
  
  public void setAddressBookId(Long addressBookId) {
/* 147 */     this.addressBookId = addressBookId;
  }
  
  public String getCustomerName() {
/* 151 */     return this.customerName;
  }
  
  public void setCustomerName(String customerName) {
/* 155 */     this.customerName = customerName;
  }



  
  public Long getId() {
/* 162 */     return this.id;
  }
  
  public void setId(Long id) {
/* 166 */     this.id = id;
  }
  
  public String getAgentGroup() {
/* 170 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/* 174 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/* 178 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 182 */     this.callDirection = callDirection;
  }
  
  public Integer getCallDirectionStatus() {
/* 186 */     return this.callDirectionStatus;
  }
  
  public void setCallDirectionStatus(Integer callDirectionStatus) {
/* 190 */     this.callDirectionStatus = callDirectionStatus;
  }
  
  public String getCallRecordedFile() {
/* 194 */     return this.callRecordedFile;
  }
  
  public void setCallRecordedFile(String callRecordedFile) {
/* 198 */     this.callRecordedFile = callRecordedFile;
  }
  
  public Integer getCallRecordingStatus() {
/* 202 */     return this.callRecordingStatus;
  }
  
  public void setCallRecordingStatus(Integer callRecordingStatus) {
/* 206 */     this.callRecordingStatus = callRecordingStatus;
  }
  
  public String getCalledNumber() {
/* 210 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 214 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 218 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 222 */     this.callingNumber = callingNumber;
  }
  
  public Byte getCdrMode() {
/* 226 */     return this.cdrMode;
  }
  
  public void setCdrMode(Byte cdrMode) {
/* 230 */     this.cdrMode = cdrMode;
  }
  
  public Integer getChannelNo() {
/* 234 */     return this.channelNo;
  }
  
  public void setChannelNo(Integer channelNo) {
/* 238 */     this.channelNo = channelNo;
  }
  
  public Integer getDuration() {
/* 242 */     return this.duration;
  }
  
  public void setDuration(Integer duration) {
/* 246 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 250 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 254 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 258 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 262 */     this.hlr = hlr;
  }
  
  public Date getInsertDateTime() {
/* 266 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 270 */     this.insertDateTime = insertDateTime;
  }
  
  public Long getLongcode() {
/* 274 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/* 278 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 282 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 286 */     this.masterShortcode = masterShortcode;
  }

  
  public String getServerIpAddress() {
/* 291 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 295 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 299 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 303 */     this.shortcodeMapping = shortcodeMapping;
  }

  
  public AgentDetail getPatchedAgentId() {
/* 308 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(AgentDetail patchedAgentId) {
/* 312 */     this.patchedAgentId = patchedAgentId;
  }
  
  public SmeProfile getSmeId() {
/* 316 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 320 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 324 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 328 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 332 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 336 */     this.startDateTime = startDateTime;
  }
  
  public String getVoicemailRecordingFile() {
/* 340 */     return this.voicemailRecordingFile;
  }
  
  public void setVoicemailRecordingFile(String voicemailRecordingFile) {
/* 344 */     this.voicemailRecordingFile = voicemailRecordingFile;
  }
  
  public Integer getVoicemailRecordingStatus() {
/* 348 */     return Integer.valueOf(this.voicemailRecordingStatus);
  }
  
  public void setVoicemailRecordingStatus(Integer voicemailRecordingStatus) {
/* 352 */     this.voicemailRecordingStatus = voicemailRecordingStatus.intValue();
  } }


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\OutbondIvrCdr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */