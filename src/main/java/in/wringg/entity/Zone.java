package in.wringg.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;




@Entity
@Table(name = "zones")
@NamedQuery(name = "Zone.findAll", query = "SELECT z FROM Zone z")
public class Zone
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private Long id;
  private byte status;
  @Column(name = "zone_code")
  private String zoneCode;
  @Column(name = "zone_name")
  private String zoneName;
  
  public Zone() {}
  
  public Zone(Long id) {
/* 31 */     this.id = id;
  }
  public Long getId() {
/* 34 */     return this.id;
  }
  
  public void setId(Long id) {
/* 38 */     this.id = id;
  }
  
  public byte getStatus() {
/* 42 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 46 */     this.status = status;
  }
  
  public String getZoneCode() {
/* 50 */     return this.zoneCode;
  }
  
  public void setZoneCode(String zoneCode) {
/* 54 */     this.zoneCode = zoneCode;
  }
  
  public String getZoneName() {
/* 58 */     return this.zoneName;
  }
  
  public void setZoneName(String zoneName) {
/* 62 */     this.zoneName = zoneName;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\Zone.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */