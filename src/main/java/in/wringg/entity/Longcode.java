package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "longcodes")
@NamedQuery(name = "Longcode.findAll", query = "SELECT l FROM Longcode l")
public class Longcode
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  private Long longcode;
  private byte status;
  @Column(name = "in_short_code")
  private Long inShortCode;
  @Column(name = "out_short_code")
  private Long outShortCode;
  @Column(name = "hlr")
  private String hlr;
  @Column(name = "sme_Identifier")
  private String smeIdentifier;
  
  public String getHlr() {
/*  40 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/*  44 */     this.hlr = hlr;
  }
  
  public String getSmeIdentifier() {
/*  48 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/*  52 */     this.smeIdentifier = smeIdentifier;
  }

  
  public Longcode() {}
  
  public Longcode(Long id) {
/*  59 */     this.id = id;
  }
  
  public Long getId() {
/*  63 */     return this.id;
  }
  
  public void setId(Long id) {
/*  67 */     this.id = id;
  }
  
  public Date getInsertTime() {
/*  71 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/*  75 */     this.insertTime = insertTime;
  }
  
  public Long getLongcode() {
/*  79 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/*  83 */     this.longcode = longcode;
  }
  
  public byte getStatus() {
/*  87 */     return this.status;
  }
  
  public void setStatus(byte status) {
/*  91 */     this.status = status;
  }
  
  public Long getInShortCode() {
/*  95 */     return this.inShortCode;
  }
  
  public void setInShortCode(Long inShortCode) {
/*  99 */     this.inShortCode = inShortCode;
  }
  
  public Long getOutShortCode() {
/* 103 */     return this.outShortCode;
  }
  
  public void setOutShortCode(Long outShortCode) {
/* 107 */     this.outShortCode = outShortCode;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\Longcode.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */