package in.wringg.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table(name = "day_weeks")
@NamedQuery(name = "WeekDayName.findAll", query = "SELECT a FROM WeekDayName a")
public class WeekDayName
{
  @Id
  @Column(name = "day_id")
  private Long id;
  @Column(name = "day_num")
  private String day_num;
  @Column(name = "day_name")
  private String day_name;
  
  public Long getId() {
/* 25 */     return this.id;
  }
  
  public void setId(Long id) {
/* 29 */     this.id = id;
  }
  
  public String getDay_num() {
/* 33 */     return this.day_num;
  }
  
  public void setDay_num(String day_num) {
/* 37 */     this.day_num = day_num;
  }
  
  public String getDay_name() {
/* 41 */     return this.day_name;
  }
  
  public void setDay_name(String day_name) {
/* 45 */     this.day_name = day_name;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\WeekDayName.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */