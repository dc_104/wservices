package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;











@Entity
@Table(name = "sme_customer_remarks")
@NamedQuery(name = "SmeCustomerRemarks.findAll", query = "SELECT a FROM SmeCustomerRemarks a")
public class SmeCustomerRemarks
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "address_book_id")
  private Long addressBookId;
  @Column(name = "sme_id")
  private Long smeId;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "updated_date_time")
  private Date updatedDateTime;
  @Column(name = "agent_id")
  private Long agentId;
  @Column(name = "remarks")
  private String remarks;
  @Column(name = "status")
  private Byte status;
  
  public Long getId() {
/*  54 */     return this.id;
  }
  
  public void setId(Long id) {
/*  58 */     this.id = id;
  }
  
  public Long getAddressBookId() {
/*  62 */     return this.addressBookId;
  }
  
  public void setAddressBookId(Long addressBookId) {
/*  66 */     this.addressBookId = addressBookId;
  }
  
  public Long getSmeId() {
/*  70 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/*  74 */     this.smeId = smeId;
  }
  
  public Date getInsertDateTime() {
/*  78 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/*  82 */     this.insertDateTime = insertDateTime;
  }
  
  public Date getUpdatedDateTime() {
/*  86 */     return this.updatedDateTime;
  }
  
  public void setUpdatedDateTime(Date updatedDateTime) {
/*  90 */     this.updatedDateTime = updatedDateTime;
  }
  
  public Long getAgentId() {
/*  94 */     return this.agentId;
  }
  
  public void setAgentId(Long agentId) {
/*  98 */     this.agentId = agentId;
  }
  
  public String getRemarks() {
/* 102 */     return this.remarks;
  }
  
  public void setRemarks(String remarks) {
/* 106 */     this.remarks = remarks;
  }
  
  public Byte getStatus() {
/* 110 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 114 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmeCustomerRemarks.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */