package in.wringg.entity;

import in.wringg.entity.SmsContent;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



















@Entity
@Table(name = "sms_campaign")
@NamedQuery(name = "SmsCampaign.findAll", query = "SELECT s FROM SmsCampaign s")
public class SmsCampaign
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "base_count")
  private int baseCount;
  @Column(name = "base_file")
  private String baseFile;
  private String cli;
  @Column(name = "end_time")
  private Time endTime;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "sms_campaign_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sms_campaign_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "sms_campaign_gen")
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date")
  private Date insertDate;
  @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinColumn(name = "msg_id")
  private SmsContent msgId;
  @Column(name = "sms_type")
  private byte smsType;
  @Column(name = "start_time")
  private Time startTime;
  @Column(name = "status")
  private byte status;
  
  public int getBaseCount() {
/*  73 */     return this.baseCount;
  }
  
  public void setBaseCount(int baseCount) {
/*  77 */     this.baseCount = baseCount;
  }
  
  public String getBaseFile() {
/*  81 */     return this.baseFile;
  }
  
  public void setBaseFile(String baseFile) {
/*  85 */     this.baseFile = baseFile;
  }
  
  public String getCli() {
/*  89 */     return this.cli;
  }
  
  public void setCli(String cli) {
/*  93 */     this.cli = cli;
  }
  
  public Time getEndTime() {
/*  97 */     return this.endTime;
  }
  
  public void setEndTime(Time endTime) {
/* 101 */     this.endTime = endTime;
  }
  
  public Long getId() {
/* 105 */     return this.id;
  }
  
  public void setId(Long id) {
/* 109 */     this.id = id;
  }
  
  public Date getInsertDate() {
/* 113 */     return this.insertDate;
  }
  
  public void setInsertDate(Date insertDate) {
/* 117 */     this.insertDate = insertDate;
  }

  
  public byte getSmsType() {
/* 122 */     return this.smsType;
  }
  
  public void setSmsType(byte smsType) {
/* 126 */     this.smsType = smsType;
  }
  
  public SmsContent getMsgId() {
/* 130 */     return this.msgId;
  }
  
  public void setMsgId(SmsContent msgId) {
/* 134 */     this.msgId = msgId;
  }
  
  public Time getStartTime() {
/* 138 */     return this.startTime;
  }
  
  public void setStartTime(Time startTime) {
/* 142 */     this.startTime = startTime;
  }
  
  public byte getStatus() {
/* 146 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 150 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmsCampaign.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */