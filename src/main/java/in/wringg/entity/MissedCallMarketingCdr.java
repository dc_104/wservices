package in.wringg.entity;

import in.wringg.entity.SmeProfile;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

















@Entity
@Table(name = "missed_call_marketing_cdr")
@NamedQuery(name = "MissedCallMarketingCdr.findAll", query = "SELECT m FROM MissedCallMarketingCdr m")
public class MissedCallMarketingCdr
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "call_direction_status")
  private int callDirectionStatus;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private String cdrMode;
  @Column(name = "channel_no")
  private int channelNo;
  private int duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  private String hlr;
  @Id
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  private BigInteger longcode;
  @Column(name = "master_shortcode")
  private String masterShortcode;
  @Column(name = "recording_file")
  private String recordingFile;
  @Column(name = "recording_status")
  private int recordingStatus;
  @Column(name = "server_ip_address")
  private String serverIpAddress;
  @Column(name = "shortcode_mapping")
  private String shortcodeMapping;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "sme_id")
  private SmeProfile smeId;
  @Column(name = "sme_identifier")
  private String smeIdentifier;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date_time")
  private Date startDateTime;
  
  public String getCallDirection() {
/*  84 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/*  88 */     this.callDirection = callDirection;
  }
  
  public int getCallDirectionStatus() {
/*  92 */     return this.callDirectionStatus;
  }
  
  public void setCallDirectionStatus(int callDirectionStatus) {
/*  96 */     this.callDirectionStatus = callDirectionStatus;
  }
  
  public String getCalledNumber() {
/* 100 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 104 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 108 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 112 */     this.callingNumber = callingNumber;
  }
  
  public String getCdrMode() {
/* 116 */     return this.cdrMode;
  }
  
  public void setCdrMode(String cdrMode) {
/* 120 */     this.cdrMode = cdrMode;
  }
  
  public int getChannelNo() {
/* 124 */     return this.channelNo;
  }
  
  public void setChannelNo(int channelNo) {
/* 128 */     this.channelNo = channelNo;
  }
  
  public int getDuration() {
/* 132 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 136 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 140 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 144 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 148 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 152 */     this.hlr = hlr;
  }
  
  public BigInteger getId() {
/* 156 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 160 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 164 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 168 */     this.insertDateTime = insertDateTime;
  }
  
  public BigInteger getLongcode() {
/* 172 */     return this.longcode;
  }
  
  public void setLongcode(BigInteger longcode) {
/* 176 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 180 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 184 */     this.masterShortcode = masterShortcode;
  }
  
  public String getRecordingFile() {
/* 188 */     return this.recordingFile;
  }
  
  public void setRecordingFile(String recordingFile) {
/* 192 */     this.recordingFile = recordingFile;
  }
  
  public int getRecordingStatus() {
/* 196 */     return this.recordingStatus;
  }
  
  public void setRecordingStatus(int recordingStatus) {
/* 200 */     this.recordingStatus = recordingStatus;
  }
  
  public String getServerIpAddress() {
/* 204 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 208 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 212 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 216 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public SmeProfile getSmeId() {
/* 220 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 224 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 228 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 232 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 236 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 240 */     this.startDateTime = startDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MissedCallMarketingCdr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */