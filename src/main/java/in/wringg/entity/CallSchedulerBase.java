package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "call_scheduler_base")
@NamedQuery(name = "CallSchedulerBase.findAll", query = "SELECT a FROM CallSchedulerBase a")
public class CallSchedulerBase
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "base_id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long baseId;
  @Column(name = "mobile")
  private String mobile;
  @Column(name = "description")
  private String description;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "sme_id")
  private Long sme;
  @Column(name = "agent_id")
  private Long agent;
  @Column(name = "call_counter")
  private Integer callCounter;
  @Column(name = "schedule_date_time")
  private Date scheduleDateTime;
  @Column(name = "call_type")
  private Integer callType;
  private Byte status;
  
  public Integer getCallCounter() {
/*  57 */     return this.callCounter;
  }
  
  public Date getScheduleDateTime() {
/*  61 */     return this.scheduleDateTime;
  }
  
  public void setScheduleDateTime(Date scheduleDateTime) {
/*  65 */     this.scheduleDateTime = scheduleDateTime;
  }
  
  public Integer getCallType() {
/*  69 */     return this.callType;
  }
  
  public void setCallType(Integer callType) {
/*  73 */     this.callType = callType;
  }
  
  public void setCallCounter(Integer callCounter) {
/*  77 */     this.callCounter = callCounter;
  }


  
  public Long getBaseId() {
/*  83 */     return this.baseId;
  }
  
  public void setBaseId(Long baseId) {
/*  87 */     this.baseId = baseId;
  }
  
  public String getMobile() {
/*  91 */     return this.mobile;
  }
  
  public void setMobile(String mobile) {
/*  95 */     this.mobile = mobile;
  }
  
  public String getDescription() {
/*  99 */     return this.description;
  }
  
  public void setDescription(String description) {
/* 103 */     this.description = description;
  }
  
  public Date getInsertTime() {
/* 107 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 111 */     this.insertTime = insertTime;
  }
  
  public static long getSerialversionuid() {
/* 115 */     return 1L;
  }
  
  public Long getSme() {
/* 119 */     return this.sme;
  }
  
  public void setSme(Long sme) {
/* 123 */     this.sme = sme;
  }
  
  public Long getAgent() {
/* 127 */     return this.agent;
  }
  
  public void setAgent(Long agent) {
/* 131 */     this.agent = agent;
  }
  
  public Byte getStatus() {
/* 135 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 139 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\CallSchedulerBase.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */