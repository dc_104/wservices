package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


























@Entity
@Table(name = "agent_group_detail")
@NamedQuery(name = "AgentGroupDetail.findAll", query = "SELECT a FROM AgentGroupDetail a")
public class AgentGroupDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date_time")
  private Date createDateTime;
  @Id
  @Column(name = "group_id")
  private Long groupId;
  @Column(name = "group_name")
  private String groupName;
  @Column(name = "group_status")
  private Byte groupStatus;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_date_time")
  private Date updateDateTime;
  
  public Date getCreateDateTime() {
/* 60 */     return this.createDateTime;
  }
  
  public void setCreateDateTime(Date createDateTime) {
/* 64 */     this.createDateTime = createDateTime;
  }
  
  public Long getGroupId() {
/* 68 */     return this.groupId;
  }
  
  public void setGroupId(Long groupId) {
/* 72 */     this.groupId = groupId;
  }
  
  public String getGroupName() {
/* 76 */     return this.groupName;
  }
  
  public void setGroupName(String groupName) {
/* 80 */     this.groupName = groupName;
  }
  
  public Byte getGroupStatus() {
/* 84 */     return this.groupStatus;
  }
  
  public void setGroupStatus(Byte groupStatus) {
/* 88 */     this.groupStatus = groupStatus;
  }
  
  public Date getUpdateDateTime() {
/* 92 */     return this.updateDateTime;
  }
  
  public void setUpdateDateTime(Date updateDateTime) {
/* 96 */     this.updateDateTime = updateDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentGroupDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */