package in.wringg.entity;

import in.wringg.entity.Users;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;













@Entity
@Table(name = "user_roles")
@NamedQuery(name = "UserRole.findAll", query = "SELECT u FROM UserRole u")
public class UserRole
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String role;
  @Id
  @Column(name = "user_role_id", unique = true, nullable = false)
  @TableGenerator(name = "userrole_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "userrole_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "userrole_gen")
  private int userRoleId;
  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
  @JoinColumn(name = "username", nullable = false)
  private Users user;
  
  public String getRole() {
/* 48 */     return this.role;
  }
  
  public void setRole(String role) {
/* 52 */     this.role = role;
  }
  
  public int getUserRoleId() {
/* 56 */     return this.userRoleId;
  }
  
  public void setUserRoleId(int userRoleId) {
/* 60 */     this.userRoleId = userRoleId;
  }
  
  public Users getUser() {
/* 64 */     return this.user;
  }
  
  public void setUser(Users user) {
/* 68 */     this.user = user;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\UserRole.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */