package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mpbx_category_master")
@NamedQuery(name = "MpbxCategoryMaster.findAll", query = "SELECT m FROM MpbxCategoryMaster m")
public class MpbxCategoryMaster implements Serializable {
  private static final long serialVersionUID = 1L;
  @Column(name = "date_time")
  private Date dateTime;
  @Column(name = "cat_desc")
  private String catDesc;
  @Column(name = "cat_id")
  private String catId;
  private byte children;
  private byte dtmf;
  @Column(name = "event_type")
  private String eventType;
  
/*  25 */   public Date getDateTime() { return this.dateTime; } @Id @Column(name = "id") @TableGenerator(name = "mpbx_category_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "mpbx_category_seq", allocationSize = 1) @GeneratedValue(strategy = GenerationType.TABLE, generator = "mpbx_category_gen") private BigInteger id; @Column(name = "media_file") private String mediaFile; @Column(name = "media_file_status")
  private String mediaFileStatus; @Column(name = "parent_cat_id")
  private String parentCatId; @Column(name = "service_type")
  private String serviceType; @Column(name = "sme_id")
/*  29 */   private String smeId; private String title; private String type; public void setDateTime(Date dateTime) { this.dateTime = dateTime; }













































  
  public String getCatDesc() {
/*  77 */     return this.catDesc;
  }
  
  public void setCatDesc(String catDesc) {
/*  81 */     this.catDesc = catDesc;
  }
  
  public String getCatId() {
/*  85 */     return this.catId;
  }
  
  public void setCatId(String catId) {
/*  89 */     this.catId = catId;
  }
  
  public byte getChildren() {
/*  93 */     return this.children;
  }
  
  public void setChildren(byte children) {
/*  97 */     this.children = children;
  }
  
  public byte getDtmf() {
/* 101 */     return this.dtmf;
  }
  
  public void setDtmf(byte dtmf) {
/* 105 */     this.dtmf = dtmf;
  }
  
  public String getEventType() {
/* 109 */     return this.eventType;
  }
  
  public void setEventType(String eventType) {
/* 113 */     this.eventType = eventType;
  }
  
  public BigInteger getId() {
/* 117 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 121 */     this.id = id;
  }
  
  public String getMediaFile() {
/* 125 */     return this.mediaFile;
  }
  
  public void setMediaFile(String mediaFile) {
/* 129 */     this.mediaFile = mediaFile;
  }
  
  public String getMediaFileStatus() {
/* 133 */     return this.mediaFileStatus;
  }
  
  public void setMediaFileStatus(String mediaFileStatus) {
/* 137 */     this.mediaFileStatus = mediaFileStatus;
  }
  
  public String getParentCatId() {
/* 141 */     return this.parentCatId;
  }
  
  public void setParentCatId(String parentCatId) {
/* 145 */     this.parentCatId = parentCatId;
  }
  
  public String getServiceType() {
/* 149 */     return this.serviceType;
  }
  
  public void setServiceType(String serviceType) {
/* 153 */     this.serviceType = serviceType;
  }
  
  public String getSmeId() {
/* 157 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 161 */     this.smeId = smeId;
  }
  
  public String getTitle() {
/* 165 */     return this.title;
  }
  
  public void setTitle(String title) {
/* 169 */     this.title = title;
  }
  
  public String getType() {
/* 173 */     return this.type;
  }
  
  public void setType(String type) {
/* 177 */     this.type = type;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MpbxCategoryMaster.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */