package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;








@Entity
@Table(name = "call_base_history")
@NamedQuery(name = "CallBaseHistory.findAll", query = "SELECT a FROM CallBaseHistory a")
public class CallBaseHistory
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "history_id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long historyId;
  @Column(name = "data")
  private String data;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "sme_id")
  private Long smeId;
  
  public String getData() {
/* 42 */     return this.data;
  }
  
  public void setData(String data) {
/* 46 */     this.data = data;
  }
  
  public Date getInsertTime() {
/* 50 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 54 */     this.insertTime = insertTime;
  }
  
  public static long getSerialversionuid() {
/* 58 */     return 1L;
  }

  
  public Long getSmeId() {
/* 63 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 67 */     this.smeId = smeId;
  }
  
  public Long getHistoryId() {
/* 71 */     return this.historyId;
  }
  
  public void setHistoryId(Long historyId) {
/* 75 */     this.historyId = historyId;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\CallBaseHistory.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */