package in.wringg.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;





@Entity
@Table(name = "mpbx_call_recording")
@NamedQuery(name = "MpbxCallRecording.findAll", query = "SELECT m FROM MpbxCallRecording m")
public class MpbxCallRecording
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private int id;
  @Column(name = "agent_id")
  private int agentId;
  @Column(name = "call_id")
  private String callId;
  @Column(name = "customer_ani")
  private String customerAni;
  @Column(name = "merged_file")
  private String mergedFile;
  
  public String getMergedFile() {
/*  34 */     return this.mergedFile;
  } private int duration; private String filename; private String flag; @Column(name = "insert_date")
  private Timestamp insertDate; @Column(name = "sme_id")
  private String smeId; private int status; public void setMergedFile(String mergedFile) {
/*  38 */     this.mergedFile = mergedFile;
  }

















  
  public int getId() {
/*  59 */     return this.id;
  }
  
  public void setId(int id) {
/*  63 */     this.id = id;
  }
  
  public int getAgentId() {
/*  67 */     return this.agentId;
  }
  
  public void setAgentId(int agentId) {
/*  71 */     this.agentId = agentId;
  }
  
  public String getCallId() {
/*  75 */     return this.callId;
  }
  
  public void setCallId(String callId) {
/*  79 */     this.callId = callId;
  }
  
  public String getCustomerAni() {
/*  83 */     return this.customerAni;
  }
  
  public void setCustomerAni(String customerAni) {
/*  87 */     this.customerAni = customerAni;
  }
  
  public int getDuration() {
/*  91 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/*  95 */     this.duration = duration;
  }
  
  public String getFilename() {
/*  99 */     return this.filename;
  }
  
  public void setFilename(String filename) {
/* 103 */     this.filename = filename;
  }
  
  public String getFlag() {
/* 107 */     return this.flag;
  }
  
  public void setFlag(String flag) {
/* 111 */     this.flag = flag;
  }
  
  public Timestamp getInsertDate() {
/* 115 */     return this.insertDate;
  }
  
  public void setInsertDate(Timestamp insertDate) {
/* 119 */     this.insertDate = insertDate;
  }
  
  public String getSmeId() {
/* 123 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 127 */     this.smeId = smeId;
  }
  
  public int getStatus() {
/* 131 */     return this.status;
  }
  
  public void setStatus(int status) {
/* 135 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MpbxCallRecording.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */