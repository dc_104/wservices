package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "forget_password")
@NamedQuery(name = "ForgetPassword.findAll", query = "SELECT f FROM ForgetPassword f")
public class ForgetPassword implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "forget_password_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "forget_password_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "forget_password_gen")
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insertion_date")
  private Date insertionDate;
  
/* 31 */   public Users getSmeId() { return this.smeId; } @ManyToOne(optional = false, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY) @JoinColumn(name = "sme_id")
  private Users smeId; private Byte status; @Column(unique = true, name = "token")
  private String token; @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "updation_date")
/* 35 */   private Date updationDate; public void setSmeId(Users smeId) { this.smeId = smeId; }










  
  public Date getInsertionDate() {
/* 48 */     return this.insertionDate;
  }
  
  public void setInsertionDate(Date insertionDate) {
/* 52 */     this.insertionDate = insertionDate;
  }
  
  public byte getStatus() {
/* 56 */     return this.status.byteValue();
  }
  
  public void setStatus(byte status) {
/* 60 */     this.status = Byte.valueOf(status);
  }
  
  public String getToken() {
/* 64 */     return this.token;
  }
  
  public void setToken(String token) {
/* 68 */     this.token = token;
  }
  
  public Date getUpdationDate() {
/* 72 */     return this.updationDate;
  }
  
  public void setUpdationDate(Date updationDate) {
/* 76 */     this.updationDate = updationDate;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\ForgetPassword.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */