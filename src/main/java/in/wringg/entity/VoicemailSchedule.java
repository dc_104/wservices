package in.wringg.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


















@Entity
@Table(name = "voicemail_schedule")
@NamedQuery(name = "VoicemailSchedule.findAll", query = "SELECT v FROM VoicemailSchedule v")
public class VoicemailSchedule
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private int id;
  private String calledparty;
  private String callingparty;
  private int counter;
  @Column(name = "date_time")
  private String dateTime;
  private String description;
  @Column(name = "group_id")
  private String groupId;
  @Column(name = "group_name")
  private String groupName;
  @Column(name = "sent_datetime")
  private String sentDatetime;
  @Column(name = "sme_id")
  private String smeId;
  private String status;
  private String voicemailfile;
  @Column(name = "vm_cdr_id")
  private Long vmCdrId;
  
  public Long getVmCdrId() {
/*  56 */     return this.vmCdrId;
  }
  
  public void setVmCdrId(Long vmCdrId) {
/*  60 */     this.vmCdrId = vmCdrId;
  }



  
  public int getId() {
/*  67 */     return this.id;
  }
  
  public void setId(int id) {
/*  71 */     this.id = id;
  }
  
  public String getCalledparty() {
/*  75 */     return this.calledparty;
  }
  
  public void setCalledparty(String calledparty) {
/*  79 */     this.calledparty = calledparty;
  }
  
  public String getCallingparty() {
/*  83 */     return this.callingparty;
  }
  
  public void setCallingparty(String callingparty) {
/*  87 */     this.callingparty = callingparty;
  }
  
  public int getCounter() {
/*  91 */     return this.counter;
  }
  
  public void setCounter(int counter) {
/*  95 */     this.counter = counter;
  }
  
  public String getDateTime() {
/*  99 */     return this.dateTime;
  }
  
  public void setDateTime(String string) {
/* 103 */     this.dateTime = string;
  }
  
  public String getDescription() {
/* 107 */     return this.description;
  }
  
  public void setDescription(String description) {
/* 111 */     this.description = description;
  }
  
  public String getGroupId() {
/* 115 */     return this.groupId;
  }
  
  public void setGroupId(String groupId) {
/* 119 */     this.groupId = groupId;
  }
  
  public String getGroupName() {
/* 123 */     return this.groupName;
  }
  
  public void setGroupName(String groupName) {
/* 127 */     this.groupName = groupName;
  }
  
  public String getSentDatetime() {
/* 131 */     return this.sentDatetime;
  }
  
  public void setSentDatetime(String sentDatetime) {
/* 135 */     this.sentDatetime = sentDatetime;
  }
  
  public String getSmeId() {
/* 139 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 143 */     this.smeId = smeId;
  }
  
  public String getStatus() {
/* 147 */     return this.status;
  }
  
  public void setStatus(String status) {
/* 151 */     this.status = status;
  }
  
  public String getVoicemailfile() {
/* 155 */     return this.voicemailfile;
  }
  
  public void setVoicemailfile(String voicemailfile) {
/* 159 */     this.voicemailfile = voicemailfile;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\VoicemailSchedule.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */