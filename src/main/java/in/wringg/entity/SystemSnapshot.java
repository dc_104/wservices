package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
















@Entity
@Table(name = "system_snapshot")
@NamedQuery(name = "SystemSnapshot.findAll", query = "SELECT s FROM SystemSnapshot s")
public class SystemSnapshot
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "current_agents")
  private int currentAgents;
  @Column(name = "current_sme")
  private int currentSme;
  @Id
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "max_agents")
  private int maxAgents;
  @Column(name = "max_sme")
  private int maxSme;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_time")
  private Date updateTime;
  
  public int getCurrentAgents() {
/*  53 */     return this.currentAgents;
  }
  
  public void setCurrentAgents(int currentAgents) {
/*  57 */     this.currentAgents = currentAgents;
  }
  
  public int getCurrentSme() {
/*  61 */     return this.currentSme;
  }
  
  public void setCurrentSme(int currentSme) {
/*  65 */     this.currentSme = currentSme;
  }


  
  public Date getInsertTime() {
/*  71 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/*  75 */     this.insertTime = insertTime;
  }
  
  public int getMaxAgents() {
/*  79 */     return this.maxAgents;
  }
  
  public void setMaxAgents(int maxAgents) {
/*  83 */     this.maxAgents = maxAgents;
  }
  
  public int getMaxSme() {
/*  87 */     return this.maxSme;
  }
  
  public void setMaxSme(int maxSme) {
/*  91 */     this.maxSme = maxSme;
  }


  
  public Date getUpdateTime() {
/*  97 */     return this.updateTime;
  }
  
  public void setUpdateTime(Date updateTime) {
/* 101 */     this.updateTime = updateTime;
  }
  
  public Long getId() {
/* 105 */     return this.id;
  }
  
  public void setId(Long id) {
/* 109 */     this.id = id;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SystemSnapshot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */