package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "agent_calling_details")
@NamedQuery(name = "AgentCallingDetail.findAll", query = "SELECT a FROM AgentCallingDetail a")
public class AgentCallingDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "agent_id")
  private Long agentId;
  @Column(name = "sme_id")
  private Long smeId;
  @Column(name = "total_calls")
  private Long totalCalls;
  @Column(name = "total_in_calls")
  private Long totalInCalls;
  @Column(name = "total_out_calls")
  private Long totalOutCalls;
  @Column(name = "no_answer")
  private Long noAnswer;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date")
  private Date insertDate;
  @Column(name = "in_success_calls")
  private Long inSuccessCalls;
  @Column(name = "in_failed_calls")
  private Long inFailedCalls;
  @Column(name = "out_success_calls")
  private Long outSuccessCalls;
  @Column(name = "out_failed_calls")
  private Long outFailedCalls;
  @Column(name = "avg_call_duration")
  private Long avgCallDuration;
  @Column(name = "total_call_duration")
  private Long totalCallDuration;
  @Column(name = "office_hours")
  private Long officeHours;
  @Column(name = "lunch_hours")
  private Long lunchHours;
  
  public Long getId() {
/*  61 */     return this.id;
  }
  public void setId(Long id) {
/*  64 */     this.id = id;
  }
  public Long getAgentId() {
/*  67 */     return this.agentId;
  }
  public void setAgentId(Long agentId) {
/*  70 */     this.agentId = agentId;
  }
  public Long getSmeId() {
/*  73 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/*  76 */     this.smeId = smeId;
  }
  public Long getTotalCalls() {
/*  79 */     return this.totalCalls;
  }
  public void setTotalCalls(Long totalCalls) {
/*  82 */     this.totalCalls = totalCalls;
  }
  public Long getNoAnswer() {
/*  85 */     return this.noAnswer;
  }
  public void setNoAnswer(Long noAnswer) {
/*  88 */     this.noAnswer = noAnswer;
  }
  public Date getInsertDate() {
/*  91 */     return this.insertDate;
  }
  public void setInsertDate(Date insertDate) {
/*  94 */     this.insertDate = insertDate;
  }
  public Long getInSuccessCalls() {
/*  97 */     return this.inSuccessCalls;
  }
  public void setInSuccessCalls(Long inSuccessCalls) {
/* 100 */     this.inSuccessCalls = inSuccessCalls;
  }
  public Long getInFailedCalls() {
/* 103 */     return this.inFailedCalls;
  }
  public void setInFailedCalls(Long inFailedCalls) {
/* 106 */     this.inFailedCalls = inFailedCalls;
  }
  public Long getOutSuccessCalls() {
/* 109 */     return this.outSuccessCalls;
  }
  public void setOutSuccessCalls(Long outSuccessCalls) {
/* 112 */     this.outSuccessCalls = outSuccessCalls;
  }
  public Long getOutFailedCalls() {
/* 115 */     return this.outFailedCalls;
  }
  public void setOutFailedCalls(Long outFailedCalls) {
/* 118 */     this.outFailedCalls = outFailedCalls;
  }
  public Long getAvgCallDuration() {
/* 121 */     return this.avgCallDuration;
  }
  public void setAvgCallDuration(Long avgCallDuration) {
/* 124 */     this.avgCallDuration = avgCallDuration;
  }
  public Long getTotalCallDuration() {
/* 127 */     return this.totalCallDuration;
  }
  public void setTotalCallDuration(Long totalCallDuration) {
/* 130 */     this.totalCallDuration = totalCallDuration;
  }
  public Long getOfficeHours() {
/* 133 */     return this.officeHours;
  }
  public void setOfficeHours(Long officeHours) {
/* 136 */     this.officeHours = officeHours;
  }
  public Long getLunchHours() {
/* 139 */     return this.lunchHours;
  }
  public void setLunchHours(Long lunchHours) {
/* 142 */     this.lunchHours = lunchHours;
  }
  public Long getTotalInCalls() {
/* 145 */     return this.totalInCalls;
  }
  public void setTotalInCalls(Long totalInCalls) {
/* 148 */     this.totalInCalls = totalInCalls;
  }
  public Long getTotalOutCalls() {
/* 151 */     return this.totalOutCalls;
  }
  public void setTotalOutCalls(Long totalOutCalls) {
/* 154 */     this.totalOutCalls = totalOutCalls;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentCallingDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */