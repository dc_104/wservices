package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;























@Entity
@Table(name = "missed_call_marketing_logs")
@NamedQuery(name = "MissedCallMarketingLog.findAll", query = "SELECT m FROM MissedCallMarketingLog m")
public class MissedCallMarketingLog
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private String cdrMode;
  @Column(name = "channel_no")
  private int channelNo;
  private int duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  private String hlr;
  @Id
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "logs_date_time")
  private Date logsDateTime;
  private BigInteger longcode;
  @Column(name = "master_shortcode")
  private String masterShortcode;
  @Column(name = "patched_agent_id")
  private String patchedAgentId;
  @Column(name = "recorded_file")
  private String recordedFile;
  @Column(name = "recording_status")
  private int recordingStatus;
  @Column(name = "server_ip_address")
  private String serverIpAddress;
  @Column(name = "shortcode_mapping")
  private String shortcodeMapping;
  @Column(name = "sme_id")
  private String smeId;
  @Column(name = "sme_identifier")
  private String smeIdentifier;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date_time")
  private Date startDateTime;
  
  public String getAgentGroup() {
/*  90 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/*  94 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/*  98 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 102 */     this.callDirection = callDirection;
  }
  
  public String getCalledNumber() {
/* 106 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 110 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 114 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 118 */     this.callingNumber = callingNumber;
  }
  
  public String getCdrMode() {
/* 122 */     return this.cdrMode;
  }
  
  public void setCdrMode(String cdrMode) {
/* 126 */     this.cdrMode = cdrMode;
  }
  
  public int getChannelNo() {
/* 130 */     return this.channelNo;
  }
  
  public void setChannelNo(int channelNo) {
/* 134 */     this.channelNo = channelNo;
  }
  
  public int getDuration() {
/* 138 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 142 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 146 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 150 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 154 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 158 */     this.hlr = hlr;
  }
  
  public BigInteger getId() {
/* 162 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 166 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 170 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 174 */     this.insertDateTime = insertDateTime;
  }
  
  public Date getLogsDateTime() {
/* 178 */     return this.logsDateTime;
  }
  
  public void setLogsDateTime(Date logsDateTime) {
/* 182 */     this.logsDateTime = logsDateTime;
  }
  
  public BigInteger getLongcode() {
/* 186 */     return this.longcode;
  }
  
  public void setLongcode(BigInteger longcode) {
/* 190 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 194 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 198 */     this.masterShortcode = masterShortcode;
  }
  
  public String getPatchedAgentId() {
/* 202 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(String patchedAgentId) {
/* 206 */     this.patchedAgentId = patchedAgentId;
  }
  
  public String getRecordedFile() {
/* 210 */     return this.recordedFile;
  }
  
  public void setRecordedFile(String recordedFile) {
/* 214 */     this.recordedFile = recordedFile;
  }
  
  public int getRecordingStatus() {
/* 218 */     return this.recordingStatus;
  }
  
  public void setRecordingStatus(int recordingStatus) {
/* 222 */     this.recordingStatus = recordingStatus;
  }
  
  public String getServerIpAddress() {
/* 226 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 230 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 234 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 238 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public String getSmeId() {
/* 242 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 246 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 250 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 254 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 258 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 262 */     this.startDateTime = startDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MissedCallMarketingLog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */