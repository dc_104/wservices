package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;









@Entity
@Table(name = "revenue_details")
@NamedQuery(name = "RevenueDetail.findAll", query = "SELECT r FROM RevenueDetail r")
public class RevenueDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_time")
  private Date dateTime;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "revenue_detail_seq", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "revenue_detail_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "revenue_detail_seq")
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  private Integer mou;
  private double revenue;
  @Column(name = "service_id")
  private Integer serviceId;
  @Column(name = "sme_id")
  private Long smeId;
  @Column(name = "total_calls")
  private Integer totalCalls;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_time")
  private Date updateTime;
  
  public Date getDateTime() {
/*  55 */     return this.dateTime;
  }
  
  public void setDateTime(Date dateTime) {
/*  59 */     this.dateTime = dateTime;
  }
  
  public Long getId() {
/*  63 */     return this.id;
  }
  
  public void setId(Long id) {
/*  67 */     this.id = id;
  }
  
  public Date getInsertTime() {
/*  71 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/*  75 */     this.insertTime = insertTime;
  }
  
  public Integer getMou() {
/*  79 */     return this.mou;
  }
  
  public void setMou(Integer mou) {
/*  83 */     this.mou = mou;
  }
  
  public double getRevenue() {
/*  87 */     return this.revenue;
  }
  
  public void setRevenue(double revenue) {
/*  91 */     this.revenue = revenue;
  }
  
  public Integer getServiceId() {
/*  95 */     return this.serviceId;
  }
  
  public void setServiceId(Integer serviceId) {
/*  99 */     this.serviceId = serviceId;
  }
  
  public Long getSmeId() {
/* 103 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 107 */     this.smeId = smeId;
  }
  
  public Integer getTotalCalls() {
/* 111 */     return this.totalCalls;
  }
  
  public void setTotalCalls(Integer totalCalls) {
/* 115 */     this.totalCalls = totalCalls;
  }
  
  public Date getUpdateTime() {
/* 119 */     return this.updateTime;
  }
  
  public void setUpdateTime(Date updateTime) {
/* 123 */     this.updateTime = updateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\RevenueDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */