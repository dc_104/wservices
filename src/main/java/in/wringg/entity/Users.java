package in.wringg.entity;

import in.wringg.entity.UserRole;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM Users u")
public class Users implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "username", unique = true, nullable = false, length = 50)
  private String username;
  
  public String getUserToken() {
/* 25 */     return this.userToken; } @Column(name = "password", nullable = false, length = 50)
  private String password; @Column(name = "enabled", nullable = false)
  private byte enabled; @Column(name = "user_token")
  private String userToken; public void setUserToken(String userToken) {
/* 29 */     this.userToken = userToken;
  }

















  
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
/* 50 */   private Set<UserRole> userRole = new HashSet<>(0);




  
  public byte getEnabled() {
/* 57 */     return this.enabled;
  }
  
  public void setEnabled(byte enabled) {
/* 61 */     this.enabled = enabled;
  }
  
  public String getPassword() {
/* 65 */     return this.password;
  }
  
  public void setPassword(String password) {
/* 69 */     this.password = password;
  }
  
  public String getUsername() {
/* 73 */     return this.username;
  }
  
  public void setUsername(String username) {
/* 77 */     this.username = username;
  }
  
  public Set<UserRole> getUserRole() {
/* 81 */     return this.userRole;
  }
  
  public void setUserRole(Set<UserRole> userRole) {
/* 85 */     this.userRole = userRole;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\Users.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */