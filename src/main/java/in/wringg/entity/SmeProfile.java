package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sme_profile")
@NamedQuery(name = "SmeProfile.findAll", query = "SELECT s FROM SmeProfile s")
public class SmeProfile implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "ivr_flow_status")
	private String ivrFlowStatus;
	@Column(name = "allowed_agents")
	private int allowedAgents;
	@Column(name = "alternate_number")
	private String alternateNumber;
	@Column(name = "email_id")
	private String emailId;
	@Column(name = "balance")
	private Double balance;
	@Column(name = "recording")
	private Boolean recording;
	@Column(name = "in_permission_flag")
	private Boolean inPermissionFlag;
	@Column(name = "out_permission_flag")
	private Boolean outPermissionFlag;
	@Column(name = "eod_report_flag")
	private Boolean eodReportFlag;
	@Column(name = "routing_type")
	private int routingType;
	@Column(name = "in_channels")
	private int inChannels;
	@Column(name = "in_queue_channels")
	private int inQueueChannels;
	@Column(name = "out_channels")
	private int outChannels;
	@Column(name = "gui_timer")
	private int guiTimer;
	@Column(name = "agent_relax_time")
	private int agentRelaxTime;

	public SmeProfile(Long id) {
		this.id = id;
	}

	@Column(name = "rec_validity")
	private int recValidity;
	@Column(name = "masking")
	private Boolean masking;
	@Column(name = "voicemail")
	private Boolean voicemail;
	@Column(name = "selection_algo")
	private String selectionAlgo;
	@Column(name = "biz_address")
	private String bizAddress;
	@Id
	@Column(name = "id", nullable = false)
	@TableGenerator(name = "table_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sme_profile_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "table_gen")
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_time")
	private Date insertTime;
	@JoinColumn(name = "longcode_id", unique = true)
	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Longcode longcodeId;
//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinTable(name = "longcodes_sme_mapping", joinColumns = {
//			@JoinColumn(name = "sme_id", referencedColumnName = "id") }, inverseJoinColumns = {
//					@JoinColumn(name = "longcode_id", referencedColumnName = "id") })
//	private Set<Longcode> longcodes;

	private String name;
	@Column(name = "service_flag")
	private int serviceFlag;
	@Column(name = "sme_mobile")
	private String smeMobile;
	@Column(name = "status")
	private Byte status;
	@Column(name = "language")
	private Byte language;
	@Column(name = "billing_status")
	private Byte billingStatus;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time", nullable = true)
	private Date updateTime;
	@JoinColumn(name = "zone_id")
	@ManyToOne(cascade = { CascadeType.ALL })
	private Zone zoneId;
	@Column(name = "account_sid")
	private String accountSid;
	@Column(name = "queue_limit")
	private Integer queueLimit;
	@Column(name = "call_back_url")
	private String callBackUrl;
	@Column(name = "sticky_algo")
	private Byte stickyAlgo;

	public SmeProfile() {
	}

	public String getIvrFlowStatus() {
		return this.ivrFlowStatus;
	}

	public void setIvrFlowStatus(String ivrFlowStatus) {
		this.ivrFlowStatus = ivrFlowStatus;
	}

	public Boolean getInPermissionFlag() {
		return inPermissionFlag;
	}

	public void setInPermissionFlag(Boolean inPermissionFlag) {
		this.inPermissionFlag = inPermissionFlag;
	}

	public Boolean getOutPermissionFlag() {
		return outPermissionFlag;
	}

	public void setOutPermissionFlag(Boolean outPermissionFlag) {
		this.outPermissionFlag = outPermissionFlag;
	}

	public String getSelectionAlgo() {
		/* 74 */ return this.selectionAlgo;
	}

	public void setSelectionAlgo(String selectionAlgo) {
		/* 77 */ this.selectionAlgo = selectionAlgo;
	}

	public Boolean getMasking() {
		/* 80 */ return this.masking;
	}

	public void setMasking(Boolean masking) {
		/* 83 */ this.masking = masking;
	}

	public Boolean getRecording() {
		/* 86 */ return this.recording;
	}

	public void setRecording(Boolean recording) {
		/* 89 */ this.recording = recording;
	}

	public Byte getBillingStatus() {
		/* 126 */ return this.billingStatus;
	}

	public void setBillingStatus(Byte billingStatus) {
		/* 129 */ this.billingStatus = billingStatus;
	}

	public Byte getLanguage() {
		/* 132 */ return this.language;
	}

	public void setLanguage(Byte language) {
		/* 135 */ this.language = language;
	}

	public Byte getStickyAlgo() {
		/* 163 */ return this.stickyAlgo;
	}

	public void setStickyAlgo(Byte stickyAlgo) {
		/* 166 */ this.stickyAlgo = stickyAlgo;
	}

	public String getCallBackUrl() {
		/* 169 */ return this.callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		/* 172 */ this.callBackUrl = callBackUrl;
	}

	public Integer getQueueLimit() {
		/* 175 */ return this.queueLimit;
	}

	public void setQueueLimit(Integer queueLimit) {
		/* 178 */ this.queueLimit = queueLimit;
	}

	public String getAccountSid() {
		/* 181 */ return this.accountSid;
	}

	public void setAccountSid(String accountSid) {
		/* 184 */ this.accountSid = accountSid;
	}

	public int getAllowedAgents() {
		/* 187 */ return this.allowedAgents;
	}

	public void setAllowedAgents(int allowedAgents) {
		/* 191 */ this.allowedAgents = allowedAgents;
	}

	public String getAlternateNumber() {
		/* 195 */ return this.alternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		/* 199 */ this.alternateNumber = alternateNumber;
	}

	public String getEmailId() {
		/* 203 */ return this.emailId;
	}

	public void setEmailId(String emailId) {
		/* 207 */ this.emailId = emailId;
	}

	public Long getId() {
		/* 211 */ return this.id;
	}

	public void setId(Long id) {
		/* 215 */ this.id = id;
	}

	public Date getInsertTime() {
		/* 219 */ return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		/* 223 */ this.insertTime = insertTime;
	}

	public Longcode getLongcodeId() {
		/* 227 */ return this.longcodeId;
	}

	public void setLongcodeId(Longcode longcodeId) {
		/* 231 */ this.longcodeId = longcodeId;
	}

	public String getName() {
		/* 235 */ return this.name;
	}

	public void setName(String name) {
		/* 239 */ this.name = name;
	}

	public int getServiceFlag() {
		/* 243 */ return this.serviceFlag;
	}

	public void setServiceFlag(int serviceFlag) {
		/* 247 */ this.serviceFlag = serviceFlag;
	}

	public String getSmeMobile() {
		/* 251 */ return this.smeMobile;
	}

	public void setSmeMobile(String smeMobile) {
		/* 255 */ this.smeMobile = smeMobile;
	}

	public Byte getStatus() {
		/* 259 */ return this.status;
	}

	public void setStatus(Byte status) {
		/* 263 */ this.status = status;
	}

	public Date getUpdateTime() {
		/* 267 */ return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		/* 271 */ this.updateTime = updateTime;
	}

	public Zone getZoneId() {
		/* 275 */ return this.zoneId;
	}

	public void setZoneId(Zone zoneId) {
		/* 279 */ this.zoneId = zoneId;
	}

	public Double getBalance() {
		/* 282 */ return this.balance;
	}

	public void setBalance(Double balance) {
		/* 285 */ this.balance = balance;
	}

	public String getBizAddress() {
		/* 288 */ return this.bizAddress;
	}

	public void setBizAddress(String bizAddress) {
		/* 291 */ this.bizAddress = bizAddress;
	}

	public Boolean getVoicemail() {
		/* 294 */ return this.voicemail;
	}

	public void setVoicemail(Boolean voicemail) {
		/* 297 */ this.voicemail = voicemail;
	}

	public int getRecValidity() {
		/* 300 */ return this.recValidity;
	}

	public void setRecValidity(int recValidity) {
		/* 303 */ this.recValidity = recValidity;
	}

	public Boolean getEodReportFlag() {
		return eodReportFlag;
	}

	public void setEodReportFlag(Boolean eodReportFlag) {
		this.eodReportFlag = eodReportFlag;
	}

	public int getRoutingType() {
		return routingType;
	}

	public void setRoutingType(int routingType) {
		this.routingType = routingType;
	}

	public int getInChannels() {
		return inChannels;
	}

	public void setInChannels(int inChannels) {
		this.inChannels = inChannels;
	}

	public int getInQueueChannels() {
		return inQueueChannels;
	}

	public void setInQueueChannels(int inQueueChannels) {
		this.inQueueChannels = inQueueChannels;
	}

	public int getOutChannels() {
		return outChannels;
	}

	public void setOutChannels(int outChannels) {
		this.outChannels = outChannels;
	}

	public int getGuiTimer() {
		return guiTimer;
	}

	public void setGuiTimer(int guiTimer) {
		this.guiTimer = guiTimer;
	}

	public int getAgentRelaxTime() {
		return agentRelaxTime;
	}

	public void setAgentRelaxTime(int agentRelaxTime) {
		this.agentRelaxTime = agentRelaxTime;
	}

//	public Set<Longcode> getLongcodes() {
//		return longcodes;
//	}
//
//	public void setLongcodes(Set<Longcode> longcodes) {
//		this.longcodes = longcodes;
//	}
}
