package in.wringg.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;









@Entity
@Table(name = "obd_campaign")
@NamedQuery(name = "ObdCampaign.findAll", query = "SELECT o FROM ObdCampaign o")
public class ObdCampaign
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "base_count")
  private int baseCount;
  @Column(name = "base_file")
  private String baseFile;
  @Column(name = "end_time")
  private Timestamp endTime;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "obd_campaign_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "obd_campaign_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "obd_campaign_gen")
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date")
  private Date insertDate;
  @Column(name = "obd_name")
  private String obdName;
  @Column(name = "start_time")
  private Timestamp startTime;
  private byte status;
  
  public int getBaseCount() {
/*  53 */     return this.baseCount;
  }
  
  public void setBaseCount(int baseCount) {
/*  57 */     this.baseCount = baseCount;
  }
  
  public String getBaseFile() {
/*  61 */     return this.baseFile;
  }
  
  public void setBaseFile(String baseFile) {
/*  65 */     this.baseFile = baseFile;
  }
  
  public Timestamp getEndTime() {
/*  69 */     return this.endTime;
  }
  
  public void setEndTime(Timestamp endTime) {
/*  73 */     this.endTime = endTime;
  }
  
  public Long getId() {
/*  77 */     return this.id;
  }
  
  public void setId(Long id) {
/*  81 */     this.id = id;
  }
  
  public Date getInsertDate() {
/*  85 */     return this.insertDate;
  }
  
  public void setInsertDate(Date insertDate) {
/*  89 */     this.insertDate = insertDate;
  }
  
  public String getObdName() {
/*  93 */     return this.obdName;
  }
  
  public void setObdName(String obdName) {
/*  97 */     this.obdName = obdName;
  }
  
  public Timestamp getStartTime() {
/* 101 */     return this.startTime;
  }
  
  public void setStartTime(Timestamp startTime) {
/* 105 */     this.startTime = startTime;
  }
  
  public byte getStatus() {
/* 109 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 113 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\ObdCampaign.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */