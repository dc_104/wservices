package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "missed_call_marketing")
@NamedQuery(name = "MissedCallMarketing.findAll", query = "SELECT m FROM MissedCallMarketing m")
public class MissedCallMarketing
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private String cdrMode;
  @Column(name = "channel_no")
  private int channelNo;
  private int duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  private String hlr;
  @Id
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  private Long longcode;
  @Column(name = "master_shortcode")
  private String masterShortcode;
  @Column(name = "patched_agent_id")
  private String patchedAgentId;
  @Column(name = "recorded_file")
  private String recordedFile;
  @Column(name = "recording_status")
  private int recordingStatus;
  @Column(name = "server_ip_address")
  private String serverIpAddress;
  @Column(name = "shortcode_mapping")
  private String shortcodeMapping;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "sme_id")
  private SmeProfile smeId;
  @Column(name = "sme_identifier")
  private String smeIdentifier;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date_time")
  private Date startDateTime;
  
  public String getAgentGroup() {
/*  86 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/*  90 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/*  94 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/*  98 */     this.callDirection = callDirection;
  }
  
  public String getCalledNumber() {
/* 102 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 106 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 110 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 114 */     this.callingNumber = callingNumber;
  }
  
  public String getCdrMode() {
/* 118 */     return this.cdrMode;
  }
  
  public void setCdrMode(String cdrMode) {
/* 122 */     this.cdrMode = cdrMode;
  }
  
  public int getChannelNo() {
/* 126 */     return this.channelNo;
  }
  
  public void setChannelNo(int channelNo) {
/* 130 */     this.channelNo = channelNo;
  }
  
  public int getDuration() {
/* 134 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 138 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 142 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 146 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 150 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 154 */     this.hlr = hlr;
  }
  
  public Long getId() {
/* 158 */     return this.id;
  }
  
  public void setId(Long id) {
/* 162 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 166 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 170 */     this.insertDateTime = insertDateTime;
  }
  
  public Long getLongcode() {
/* 174 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/* 178 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 182 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 186 */     this.masterShortcode = masterShortcode;
  }
  
  public String getPatchedAgentId() {
/* 190 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(String patchedAgentId) {
/* 194 */     this.patchedAgentId = patchedAgentId;
  }
  
  public String getRecordedFile() {
/* 198 */     return this.recordedFile;
  }
  
  public void setRecordedFile(String recordedFile) {
/* 202 */     this.recordedFile = recordedFile;
  }
  
  public int getRecordingStatus() {
/* 206 */     return this.recordingStatus;
  }
  
  public void setRecordingStatus(int recordingStatus) {
/* 210 */     this.recordingStatus = recordingStatus;
  }
  
  public String getServerIpAddress() {
/* 214 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 218 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 222 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 226 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public SmeProfile getSmeId() {
/* 230 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 234 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 238 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 242 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 246 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 250 */     this.startDateTime = startDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MissedCallMarketing.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */