package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sme_prompts")
@NamedQuery(name = "SmePrompts.findAll", query = "SELECT s FROM SmePrompts s")
public class SmePrompts implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "prompt_name")
	private String promptName;

	@Id
	@Column(name = "id", nullable = false)
	@TableGenerator(name = "sme_prompts_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sme_prompts_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "sme_prompts_gen")
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_date_time")
	private Date insertDateTime;
	@Column(name = "updated_date_time")
	private Date updateDateTime;
	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@JoinColumn(name = "sme_id")
	private SmeProfile smeId;
	@Column(name = "status")
	private int status;
	@Column(name = "prompt_description")
	private String promptDescription;
	@Column(name = "prompt_path")
	private String promptPath;

	@Column(name = "server_ip_address")
	private String serverIpAddress;
	@Column(name = "category")
	private String category;
	@Column(name = "prompt_key")
	private String promptKey;
	@Column(name = "prompt_url")
	private String promptUrl;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SmeProfile getSmeId() {
		return this.smeId;
	}

	public void setSmeId(SmeProfile smeId) {
		this.smeId = smeId;
	}

	public String getPromptName() {
		return promptName;
	}

	public void setPromptName(String promptName) {
		this.promptName = promptName;
	}

	public Date getInsertDateTime() {
		return insertDateTime;
	}

	public void setInsertDateTime(Date insertDateTime) {
		this.insertDateTime = insertDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getPromptDescription() {
		return promptDescription;
	}

	public void setPromptDescription(String promptDescription) {
		this.promptDescription = promptDescription;
	}

	public String getPromptPath() {
		return promptPath;
	}

	public void setPromptPath(String promptPath) {
		this.promptPath = promptPath;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPromptKey() {
		return promptKey;
	}

	public void setPromptKey(String promptKey) {
		this.promptKey = promptKey;
	}

	public String getPromptUrl() {
		return promptUrl;
	}

	public void setPromptUrl(String promptUrl) {
		this.promptUrl = promptUrl;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
