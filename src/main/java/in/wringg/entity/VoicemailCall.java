package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "voicemail_call")
@NamedQuery(name = "VoicemailCall.findAll", query = "SELECT v FROM VoicemailCall v")
public class VoicemailCall implements Serializable {
  private static final long serialVersionUID = 1L;
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "merge_status")
  private Byte mergeStatus;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private String cdrMode;
  @Column(name = "channel_no")
  private int channelNo;
  private int duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  private String hlr;
  
/*  36 */   public Byte getMergeStatus() { return this.mergeStatus; } @Id private BigInteger id; @Temporal(TemporalType.TIMESTAMP) @Column(name = "insert_date_time") private Date insertDateTime; private BigInteger longcode; @Column(name = "master_shortcode") private String masterShortcode; @Column(name = "patched_agent_id") private String patchedAgentId; @Column(name = "recorded_file") private String recordedFile; @Column(name = "recording_status") private int recordingStatus; @Column(name = "server_ip_address") private String serverIpAddress; @Column(name = "shortcode_mapping") private String shortcodeMapping; @Column(name = "sme_id")
  private String smeId; @Column(name = "sme_identifier")
  private String smeIdentifier; @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date_time")
/*  40 */   private Date startDateTime; public void setMergeStatus(Byte mergeStatus) { this.mergeStatus = mergeStatus; }
























































  
  public String getAgentGroup() {
/*  99 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/* 103 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/* 107 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 111 */     this.callDirection = callDirection;
  }
  
  public String getCalledNumber() {
/* 115 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 119 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 123 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 127 */     this.callingNumber = callingNumber;
  }
  
  public String getCdrMode() {
/* 131 */     return this.cdrMode;
  }
  
  public void setCdrMode(String cdrMode) {
/* 135 */     this.cdrMode = cdrMode;
  }
  
  public int getChannelNo() {
/* 139 */     return this.channelNo;
  }
  
  public void setChannelNo(int channelNo) {
/* 143 */     this.channelNo = channelNo;
  }
  
  public int getDuration() {
/* 147 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 151 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 155 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 159 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 163 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 167 */     this.hlr = hlr;
  }
  
  public BigInteger getId() {
/* 171 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 175 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 179 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 183 */     this.insertDateTime = insertDateTime;
  }
  
  public BigInteger getLongcode() {
/* 187 */     return this.longcode;
  }
  
  public void setLongcode(BigInteger longcode) {
/* 191 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 195 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 199 */     this.masterShortcode = masterShortcode;
  }
  
  public String getPatchedAgentId() {
/* 203 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(String patchedAgentId) {
/* 207 */     this.patchedAgentId = patchedAgentId;
  }
  
  public String getRecordedFile() {
/* 211 */     return this.recordedFile;
  }
  
  public void setRecordedFile(String recordedFile) {
/* 215 */     this.recordedFile = recordedFile;
  }
  
  public int getRecordingStatus() {
/* 219 */     return this.recordingStatus;
  }
  
  public void setRecordingStatus(int recordingStatus) {
/* 223 */     this.recordingStatus = recordingStatus;
  }
  
  public String getServerIpAddress() {
/* 227 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 231 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 235 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 239 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public String getSmeId() {
/* 243 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 247 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 251 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 255 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 259 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 263 */     this.startDateTime = startDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\VoicemailCall.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */