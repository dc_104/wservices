package in.wringg.entity;

import in.wringg.entity.Users;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;





@Entity
@Table(name = "alert_notifications")
@NamedQuery(name = "AlertNotification.findAll", query = "SELECT a FROM AlertNotification a")
public class AlertNotification
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private int id;
  private String description;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "inserted_date")
  private Date insertedDate;
  private byte status;
  @Column(name = "title")
  private String title;
  @ManyToOne
  @JoinColumn(name = "user_id")
  private Users userId;
  
  public int getId() {
/* 41 */     return this.id;
  }
  
  public void setId(int id) {
/* 45 */     this.id = id;
  }
  
  public String getDescription() {
/* 49 */     return this.description;
  }
  
  public void setDescription(String description) {
/* 53 */     this.description = description;
  }
  
  public Date getInsertedDate() {
/* 57 */     return this.insertedDate;
  }
  
  public void setInsertedDate(Date insertedDate) {
/* 61 */     this.insertedDate = insertedDate;
  }
  
  public byte getStatus() {
/* 65 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 69 */     this.status = status;
  }
  
  public String getTitle() {
/* 73 */     return this.title;
  }
  
  public void setTitle(String title) {
/* 77 */     this.title = title;
  }
  
  public Users getUserId() {
/* 81 */     return this.userId;
  }
  
  public void setUserId(Users userId) {
/* 85 */     this.userId = userId;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AlertNotification.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */