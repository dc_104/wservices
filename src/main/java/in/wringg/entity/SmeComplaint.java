package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sme_complaints")
@NamedQuery(name = "SmeComplaint.findAll", query = "SELECT s FROM SmeComplaint s")
public class SmeComplaint implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "assignee_name")
	private String assigneeName;
	@Column(name = "category")
	private int category;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "complaint_date_time")
	private Date complaintDateTime;
	@Column(name = "complaint_detail")
	private String complaintDetail;
	@Column(name = "email_id")
	private String emailId;
	@Id
	@Column(name = "id", nullable = false)
	@TableGenerator(name = "sme_complaints_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sme_complaints_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "sme_complaints_gen")
	private Long id;
	@Column(name = "parent_id")
	private Long parentId;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sme_id")
	private SmeProfile smeId;
	@Column(name = "solution")
	private String solution;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "solution_date_time")
	private Date solutionDateTime;
	@Column(name = "status")
	private Byte status;
	@Column(name = "subject")
	private String subject;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date_time")
	private Date updateDateTime;

	public String getAssigneeName() {
		return this.assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public int getCategory() {
		return this.category;
	}

	public void setCategory(int category) {
		/* 82 */ this.category = category;
	}

	public Date getComplaintDateTime() {
		/* 86 */ return this.complaintDateTime;
	}

	public void setComplaintDateTime(Date complaintDateTime) {
		/* 90 */ this.complaintDateTime = complaintDateTime;
	}

	public String getComplaintDetail() {
		/* 94 */ return this.complaintDetail;
	}

	public void setComplaintDetail(String complaintDetail) {
		/* 98 */ this.complaintDetail = complaintDetail;
	}

	public String getEmailId() {
		/* 102 */ return this.emailId;
	}

	public void setEmailId(String emailId) {
		/* 106 */ this.emailId = emailId;
	}

	public Long getId() {
		/* 110 */ return this.id;
	}

	public void setId(Long id) {
		/* 114 */ this.id = id;
	}

	public Long getParentId() {
		/* 118 */ return this.parentId;
	}

	public void setParentId(Long parentId) {
		/* 122 */ this.parentId = parentId;
	}

	public SmeProfile getSmeId() {
		/* 126 */ return this.smeId;
	}

	public void setSmeId(SmeProfile smeId) {
		/* 130 */ this.smeId = smeId;
	}

	public String getSolution() {
		/* 134 */ return this.solution;
	}

	public void setSolution(String solution) {
		/* 138 */ this.solution = solution;
	}

	public Date getSolutionDateTime() {
		/* 142 */ return this.solutionDateTime;
	}

	public void setSolutionDateTime(Date solutionDateTime) {
		/* 146 */ this.solutionDateTime = solutionDateTime;
	}

	public Byte getStatus() {
		/* 150 */ return this.status;
	}

	public void setStatus(Byte status) {
		/* 154 */ this.status = status;
	}

	public String getSubject() {
		/* 158 */ return this.subject;
	}

	public void setSubject(String subject) {
		/* 162 */ this.subject = subject;
	}

	public Date getUpdateDateTime() {
		/* 166 */ return this.updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		/* 170 */ this.updateDateTime = updateDateTime;
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmeComplaint.class
 * Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */