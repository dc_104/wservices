package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


















@Entity
@Table(name = "virtual_longcodes")
@NamedQuery(name = "VirtualLongcode.findAll", query = "SELECT v FROM VirtualLongcode v")
public class VirtualLongcode
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  private Long longcode;
  private byte status;
  
  public Date getInsertTime() {
/* 46 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 50 */     this.insertTime = insertTime;
  }

  
  public Long getLongcode() {
/* 55 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/* 59 */     this.longcode = longcode;
  }
  
  public Long getId() {
/* 63 */     return this.id;
  }
  
  public void setId(Long id) {
/* 67 */     this.id = id;
  }
  
  public byte getStatus() {
/* 71 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 75 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\VirtualLongcode.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */