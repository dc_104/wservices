package in.wringg.entity;

import in.wringg.entity.SmeProfile;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



























@Entity
@Table(name = "mpbx_charging")
@NamedQuery(name = "MpbxCharging.findAll", query = "SELECT m FROM MpbxCharging m")
public class MpbxCharging
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "agent_allocate")
  private int agentAllocate;
  private String ani;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "billing_date")
  private Date billingDate;
  @Column(name = "chareged_amt")
  private float charegedAmt;
  @Column(name = "charging_resp")
  private String chargingResp;
  @Column(name = "in_flag")
  private int inFlag;
  @Column(name = "in_mode")
  private String inMode;
  @Column(name = "pack_id")
  private Long packId;
  @Column(name = "pre_post")
  private Byte prePost;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "renew_date")
  private Date renewDate;
  @JoinColumn(name = "sme_id", referencedColumnName = "id")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private SmeProfile smeId;
  private Byte status;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "sub_date")
  private Date subDate;
  @Column(name = "total_agent_allocate")
  private int totalAgentAllocate;
  @Column(name = "user_balance")
  private float userBalance;
  @Column(name = "user_type")
  private String userType;
  @Column(name = "recording")
  private Boolean recording;
  
  public Long getId() {
/*  94 */     return this.id;
  }
  
  public void setId(Long id) {
/*  98 */     this.id = id;
  }
  
  public int getAgentAllocate() {
/* 102 */     return this.agentAllocate;
  }
  
  public void setAgentAllocate(int agentAllocate) {
/* 106 */     this.agentAllocate = agentAllocate;
  }
  
  public String getAni() {
/* 110 */     return this.ani;
  }
  
  public void setAni(String ani) {
/* 114 */     this.ani = ani;
  }
  
  public Date getBillingDate() {
/* 118 */     return this.billingDate;
  }
  
  public void setBillingDate(Date billingDate) {
/* 122 */     this.billingDate = billingDate;
  }
  
  public float getCharegedAmt() {
/* 126 */     return this.charegedAmt;
  }
  
  public void setCharegedAmt(float charegedAmt) {
/* 130 */     this.charegedAmt = charegedAmt;
  }
  
  public String getChargingResp() {
/* 134 */     return this.chargingResp;
  }
  
  public void setChargingResp(String chargingResp) {
/* 138 */     this.chargingResp = chargingResp;
  }
  
  public int getInFlag() {
/* 142 */     return this.inFlag;
  }
  
  public void setInFlag(int inFlag) {
/* 146 */     this.inFlag = inFlag;
  }
  
  public String getInMode() {
/* 150 */     return this.inMode;
  }
  
  public void setInMode(String inMode) {
/* 154 */     this.inMode = inMode;
  }
  
  public Long getPackId() {
/* 158 */     return this.packId;
  }
  
  public void setPackId(Long packId) {
/* 162 */     this.packId = packId;
  }
  
  public Byte getPrePost() {
/* 166 */     return this.prePost;
  }
  
  public void setPrePost(Byte prePost) {
/* 170 */     this.prePost = prePost;
  }
  
  public Date getRenewDate() {
/* 174 */     return this.renewDate;
  }
  
  public void setRenewDate(Date renewDate) {
/* 178 */     this.renewDate = renewDate;
  }
  
  public SmeProfile getSmeId() {
/* 182 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 186 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/* 190 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 194 */     this.status = status;
  }
  
  public Date getSubDate() {
/* 198 */     return this.subDate;
  }
  
  public void setSubDate(Date subDate) {
/* 202 */     this.subDate = subDate;
  }
  
  public int getTotalAgentAllocate() {
/* 206 */     return this.totalAgentAllocate;
  }
  
  public void setTotalAgentAllocate(int totalAgentAllocate) {
/* 210 */     this.totalAgentAllocate = totalAgentAllocate;
  }
  
  public float getUserBalance() {
/* 214 */     return this.userBalance;
  }
  
  public void setUserBalance(float userBalance) {
/* 218 */     this.userBalance = userBalance;
  }
  
  public String getUserType() {
/* 222 */     return this.userType;
  }
  
  public void setUserType(String userType) {
/* 226 */     this.userType = userType;
  }
  
  public Boolean getRecording() {
/* 230 */     return this.recording;
  }
  
  public void setRecording(Boolean recording) {
/* 234 */     this.recording = recording;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MpbxCharging.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */