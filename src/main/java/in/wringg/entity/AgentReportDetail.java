package in.wringg.entity;

import in.wringg.entity.AgentDetail;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;











@Entity
@Table(name = "agent_report_details")
@NamedQuery(name = "AgentReportDetail.findAll", query = "SELECT a FROM AgentReportDetail a")
public class AgentReportDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  private Integer id;
  @Column(name = "agent_group")
  private String agentGroup;
  @JoinColumn(name = "agent_id")
  @ManyToOne
  private AgentDetail agentId;
  @Column(name = "customer_ani")
  private String customerAni;
  @Column(name = "session_id")
  private String sessionId;
  @Column(name = "call_mode")
  private String callMode;
  @Column(name = "call_info")
  private String callInfo;
  
  public String getCallInfo() {
/*  50 */     return this.callInfo; } private int duration; @Temporal(TemporalType.TIMESTAMP) @Column(name = "end_date") private Date endDate; @Temporal(TemporalType.TIMESTAMP) @Column(name = "insert_date") private Date insertDate; @Column(name = "response_code") private String responseCode; @Column(name = "response_message")
  private String responseMessage; @Column(name = "sme_id")
  private Long smeId; @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date")
/*  54 */   private Date startDate; private int status; public void setCallInfo(String callInfo) { this.callInfo = callInfo; }

  
  public String getCallMode() {
/*  58 */     return this.callMode;
  }
  
  public void setCallMode(String callMode) {
/*  62 */     this.callMode = callMode;
  }
  
  public String getSessionId() {
/*  66 */     return this.sessionId;
  }
  
  public void setSessionId(String sessionId) {
/*  70 */     this.sessionId = sessionId;
  }




























  
  public Integer getId() {
/* 102 */     return this.id;
  }
  
  public void setId(Integer id) {
/* 106 */     this.id = id;
  }
  
  public String getAgentGroup() {
/* 110 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/* 114 */     this.agentGroup = agentGroup;
  }

  
  public AgentDetail getAgentId() {
/* 119 */     return this.agentId;
  }
  
  public void setAgentId(AgentDetail agentId) {
/* 123 */     this.agentId = agentId;
  }
  
  public String getCustomerAni() {
/* 127 */     return this.customerAni;
  }
  
  public void setCustomerAni(String customerAni) {
/* 131 */     this.customerAni = customerAni;
  }
  
  public int getDuration() {
/* 135 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 139 */     this.duration = duration;
  }
  
  public Date getEndDate() {
/* 143 */     return this.endDate;
  }
  
  public void setEndDate(Date endDate) {
/* 147 */     this.endDate = endDate;
  }
  
  public Date getInsertDate() {
/* 151 */     return this.insertDate;
  }
  
  public void setInsertDate(Date insertDate) {
/* 155 */     this.insertDate = insertDate;
  }
  
  public String getResponseCode() {
/* 159 */     return this.responseCode;
  }
  
  public void setResponseCode(String responseCode) {
/* 163 */     this.responseCode = responseCode;
  }
  
  public Long getSmeId() {
/* 167 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 171 */     this.smeId = smeId;
  }
  
  public Date getStartDate() {
/* 175 */     return this.startDate;
  }
  
  public void setStartDate(Date startDate) {
/* 179 */     this.startDate = startDate;
  }
  
  public int getStatus() {
/* 183 */     return this.status;
  }
  
  public void setStatus(int status) {
/* 187 */     this.status = status;
  }
  
  public String getResponseMessage() {
/* 191 */     return this.responseMessage;
  }
  
  public void setResponseMessage(String responseMessage) {
/* 195 */     this.responseMessage = responseMessage;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentReportDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */