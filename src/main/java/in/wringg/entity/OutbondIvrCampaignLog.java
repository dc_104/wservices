package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;















@Entity
@Table(name = "outbond_ivr_campaign_logs")
@NamedQuery(name = "OutbondIvrCampaignLog.findAll", query = "SELECT o FROM OutbondIvrCampaignLog o")
public class OutbondIvrCampaignLog
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "base_file_name")
  private String baseFileName;
  @Column(name = "campaign_name")
  private String campaignName;
  @Column(name = "campaign_status")
  private int campaignStatus;
  @Column(name = "campaign_type")
  private String campaignType;
  private String cli;
  @Id
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "log_time")
  private Date logTime;
  @Column(name = "media_file")
  private String mediaFile;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "schedule_end_time")
  private Date scheduleEndTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "schedule_start_time")
  private Date scheduleStartTime;
  @Column(name = "sme_id")
  private BigInteger smeId;
  @Column(name = "sme_simultaneously_calls")
  private int smeSimultaneouslyCalls;
  @Column(name = "total_calls")
  private int totalCalls;
  
  public String getBaseFileName() {
/*  68 */     return this.baseFileName;
  }
  
  public void setBaseFileName(String baseFileName) {
/*  72 */     this.baseFileName = baseFileName;
  }
  
  public String getCampaignName() {
/*  76 */     return this.campaignName;
  }
  
  public void setCampaignName(String campaignName) {
/*  80 */     this.campaignName = campaignName;
  }
  
  public int getCampaignStatus() {
/*  84 */     return this.campaignStatus;
  }
  
  public void setCampaignStatus(int campaignStatus) {
/*  88 */     this.campaignStatus = campaignStatus;
  }
  
  public String getCampaignType() {
/*  92 */     return this.campaignType;
  }
  
  public void setCampaignType(String campaignType) {
/*  96 */     this.campaignType = campaignType;
  }
  
  public String getCli() {
/* 100 */     return this.cli;
  }
  
  public void setCli(String cli) {
/* 104 */     this.cli = cli;
  }
  
  public BigInteger getId() {
/* 108 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 112 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 116 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 120 */     this.insertDateTime = insertDateTime;
  }
  
  public Date getLogTime() {
/* 124 */     return this.logTime;
  }
  
  public void setLogTime(Date logTime) {
/* 128 */     this.logTime = logTime;
  }
  
  public String getMediaFile() {
/* 132 */     return this.mediaFile;
  }
  
  public void setMediaFile(String mediaFile) {
/* 136 */     this.mediaFile = mediaFile;
  }
  
  public Date getScheduleEndTime() {
/* 140 */     return this.scheduleEndTime;
  }
  
  public void setScheduleEndTime(Date scheduleEndTime) {
/* 144 */     this.scheduleEndTime = scheduleEndTime;
  }
  
  public Date getScheduleStartTime() {
/* 148 */     return this.scheduleStartTime;
  }
  
  public void setScheduleStartTime(Date scheduleStartTime) {
/* 152 */     this.scheduleStartTime = scheduleStartTime;
  }
  
  public BigInteger getSmeId() {
/* 156 */     return this.smeId;
  }
  
  public void setSmeId(BigInteger smeId) {
/* 160 */     this.smeId = smeId;
  }
  
  public int getSmeSimultaneouslyCalls() {
/* 164 */     return this.smeSimultaneouslyCalls;
  }
  
  public void setSmeSimultaneouslyCalls(int smeSimultaneouslyCalls) {
/* 168 */     this.smeSimultaneouslyCalls = smeSimultaneouslyCalls;
  }
  
  public int getTotalCalls() {
/* 172 */     return this.totalCalls;
  }
  
  public void setTotalCalls(int totalCalls) {
/* 176 */     this.totalCalls = totalCalls;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\OutbondIvrCampaignLog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */