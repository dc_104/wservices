package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;















@Entity
@Table(name = "sme_complaints_logs")
@NamedQuery(name = "SmeComplaintsLog.findAll", query = "SELECT s FROM SmeComplaintsLog s")
public class SmeComplaintsLog
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "assignee_name")
  private String assigneeName;
  private int category;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "complaint_date_time")
  private Date complaintDateTime;
  @Column(name = "complaint_detail")
  private String complaintDetail;
  @Column(name = "email_id")
  private String emailId;
  @Id
  @Column(name = "id")
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "log_date_time")
  private Date logDateTime;
  @Column(name = "parent_id")
  private BigInteger parentId;
  @Column(name = "sme_id")
  private BigInteger smeId;
  private String solution;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "solution_date_time")
  private Date solutionDateTime;
  private int status;
  private String subject;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "update_date_time")
  private Date updateDateTime;
  
  public String getAssigneeName() {
/*  66 */     return this.assigneeName;
  }
  
  public void setAssigneeName(String assigneeName) {
/*  70 */     this.assigneeName = assigneeName;
  }
  
  public int getCategory() {
/*  74 */     return this.category;
  }
  
  public void setCategory(int category) {
/*  78 */     this.category = category;
  }
  
  public Date getComplaintDateTime() {
/*  82 */     return this.complaintDateTime;
  }
  
  public void setComplaintDateTime(Date complaintDateTime) {
/*  86 */     this.complaintDateTime = complaintDateTime;
  }
  
  public String getComplaintDetail() {
/*  90 */     return this.complaintDetail;
  }
  
  public void setComplaintDetail(String complaintDetail) {
/*  94 */     this.complaintDetail = complaintDetail;
  }
  
  public String getEmailId() {
/*  98 */     return this.emailId;
  }
  
  public void setEmailId(String emailId) {
/* 102 */     this.emailId = emailId;
  }
  
  public BigInteger getId() {
/* 106 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 110 */     this.id = id;
  }
  
  public Date getLogDateTime() {
/* 114 */     return this.logDateTime;
  }
  
  public void setLogDateTime(Date logDateTime) {
/* 118 */     this.logDateTime = logDateTime;
  }
  
  public BigInteger getParentId() {
/* 122 */     return this.parentId;
  }
  
  public void setParentId(BigInteger parentId) {
/* 126 */     this.parentId = parentId;
  }
  
  public BigInteger getSmeId() {
/* 130 */     return this.smeId;
  }
  
  public void setSmeId(BigInteger smeId) {
/* 134 */     this.smeId = smeId;
  }
  
  public String getSolution() {
/* 138 */     return this.solution;
  }
  
  public void setSolution(String solution) {
/* 142 */     this.solution = solution;
  }
  
  public Date getSolutionDateTime() {
/* 146 */     return this.solutionDateTime;
  }
  
  public void setSolutionDateTime(Date solutionDateTime) {
/* 150 */     this.solutionDateTime = solutionDateTime;
  }
  
  public int getStatus() {
/* 154 */     return this.status;
  }
  
  public void setStatus(int status) {
/* 158 */     this.status = status;
  }
  
  public String getSubject() {
/* 162 */     return this.subject;
  }
  
  public void setSubject(String subject) {
/* 166 */     this.subject = subject;
  }
  
  public Date getUpdateDateTime() {
/* 170 */     return this.updateDateTime;
  }
  
  public void setUpdateDateTime(Date updateDateTime) {
/* 174 */     this.updateDateTime = updateDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmeComplaintsLog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */