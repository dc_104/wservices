package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




@Entity
@Table(name = "agent_lunch_details")
@NamedQuery(name = "AgentLunchDetail.findAll", query = "SELECT a FROM AgentLunchDetail a")
public class AgentLunchDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "agent_id")
  private Long agentId;
  @Column(name = "sme_id")
  private Long smeId;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date")
  private Date insertDate;
  @Column(name = "in_time")
  private Date inTime;
  @Column(name = "out_time")
  private Date outTime;
  @Column(name = "message")
  private String message;
  
  public String getMessage() {
/* 44 */     return this.message;
  }
  public void setMessage(String message) {
/* 47 */     this.message = message;
  }
  public Long getId() {
/* 50 */     return this.id;
  }
  public void setId(Long id) {
/* 53 */     this.id = id;
  }
  public Long getAgentId() {
/* 56 */     return this.agentId;
  }
  public void setAgentId(Long agentId) {
/* 59 */     this.agentId = agentId;
  }
  public Long getSmeId() {
/* 62 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/* 65 */     this.smeId = smeId;
  }
  public Date getInsertDate() {
/* 68 */     return this.insertDate;
  }
  public void setInsertDate(Date insertDate) {
/* 71 */     this.insertDate = insertDate;
  }
  public Date getInTime() {
/* 74 */     return this.inTime;
  }
  public void setInTime(Date inTime) {
/* 77 */     this.inTime = inTime;
  }
  public Date getOutTime() {
/* 80 */     return this.outTime;
  }
  public void setOutTime(Date outTime) {
/* 83 */     this.outTime = outTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentLunchDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */