package in.wringg.entity;

import in.wringg.entity.AgentDetail;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "agent_details_timing")
@NamedQuery(name = "AgentDetailTime.findAll", query = "SELECT a FROM AgentDetailTime a")
public class AgentDetailTime
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "agent_mobile")
  private String agentMobile;
  @Column(name = "agent_name")
  private String agentName;
  @Column(name = "in_time")
  private Time inTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "out_time")
  private Time outTime;
  @Column(name = "sme_id")
  private Long smeId;
  private Byte status;
  @Column(name = "days_week")
  private String daysFlag;
  @Column(name = "agent_position")
  private Integer agentPosition;
  @ManyToOne
  @JoinColumn(name = "agent_id")
  private AgentDetail agent_details;
  
  public Integer getAgentPosition() {
/*  66 */     return this.agentPosition;
  }
  
  public void setAgentPosition(Integer agentPosition) {
/*  70 */     this.agentPosition = agentPosition;
  }
  
  public String getDaysFlag() {
/*  74 */     return this.daysFlag;
  }
  
  public void setDaysFlag(String daysFlag) {
/*  78 */     this.daysFlag = daysFlag;
  }





  
  public String getAgentMobile() {
/*  87 */     return this.agentMobile;
  }
  
  public void setAgentMobile(String agentMobile) {
/*  91 */     this.agentMobile = agentMobile;
  }
  
  public String getAgentName() {
/*  95 */     return this.agentName;
  }
  
  public void setAgentName(String agentName) {
/*  99 */     this.agentName = agentName;
  }
  
  public Time getInTime() {
/* 103 */     return this.inTime;
  }
  
  public void setInTime(Time inTime) {
/* 107 */     this.inTime = inTime;
  }
  
  public Date getInsertTime() {
/* 111 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 115 */     this.insertTime = insertTime;
  }
  
  public Time getOutTime() {
/* 119 */     return this.outTime;
  }
  
  public void setOutTime(Time outTime) {
/* 123 */     this.outTime = outTime;
  }
  
  public Long getSmeId() {
/* 127 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 131 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/* 135 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 139 */     this.status = status;
  }
  
  public Long getId() {
/* 143 */     return this.id;
  }
  
  public void setId(Long id) {
/* 147 */     this.id = id;
  }
  
  public AgentDetail getAgent_details() {
/* 151 */     return this.agent_details;
  }
  
  public void setAgent_details(AgentDetail agent_details) {
/* 155 */     this.agent_details = agent_details;
  }

  
  public String toString() {
/* 160 */     return "AgentDetailTime [id=" + this.id + ", agentMobile=" + this.agentMobile + ", agentName=" + this.agentName + ", inTime=" + this.inTime + ", insertTime=" + this.insertTime + ", outTime=" + this.outTime + ", smeId=" + this.smeId + ", status=" + this.status + ", daysFlag=" + this.daysFlag + ", agentPosition=" + this.agentPosition + ", agent_details=" + this.agent_details + "]";
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AgentDetailTime.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */