package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




















@Entity
@Table(name = "mpbx_charging_succ")
@NamedQuery(name = "MpbxChargingSucc.findAll", query = "SELECT m FROM MpbxChargingSucc m")
public class MpbxChargingSucc
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "agent_allocate")
  private Integer agentAllocate;
  private String ani;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "billing_date")
  private Date billingDate;
  @Column(name = "chareged_amt")
  private Float charegedAmt;
  @Column(name = "charging_resp")
  private String chargingResp;
  @Id
  private Long id;
  @Column(name = "in_flag")
  private String inFlag;
  @Column(name = "in_mode")
  private String inMode;
  @Column(name = "pack_id")
  private Long packId;
  @Column(name = "pre_post")
  private Byte prePost;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "renew_date")
  private Date renewDate;
  @Column(name = "sme_id")
  private Long smeId;
  private Byte status;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "sub_date")
  private Date subDate;
  @Column(name = "total_agent_allocate")
  private Integer totalAgentAllocate;
  @Column(name = "user_balance")
  private Float userBalance;
  @Column(name = "user_type")
  private String userType;
  @Column(name = "recording")
  private Boolean recording;
  
  public Integer getAgentAllocate() {
/*  78 */     return this.agentAllocate;
  }
  
  public void setAgentAllocate(Integer agentAllocate) {
/*  82 */     this.agentAllocate = agentAllocate;
  }
  
  public String getAni() {
/*  86 */     return this.ani;
  }
  
  public void setAni(String ani) {
/*  90 */     this.ani = ani;
  }
  
  public Date getBillingDate() {
/*  94 */     return this.billingDate;
  }
  
  public void setBillingDate(Date billingDate) {
/*  98 */     this.billingDate = billingDate;
  }
  
  public Float getCharegedAmt() {
/* 102 */     return this.charegedAmt;
  }
  
  public void setCharegedAmt(Float charegedAmt) {
/* 106 */     this.charegedAmt = charegedAmt;
  }
  
  public String getChargingResp() {
/* 110 */     return this.chargingResp;
  }
  
  public void setChargingResp(String chargingResp) {
/* 114 */     this.chargingResp = chargingResp;
  }
  
  public Long getId() {
/* 118 */     return this.id;
  }
  
  public void setId(Long id) {
/* 122 */     this.id = id;
  }
  
  public String getInFlag() {
/* 126 */     return this.inFlag;
  }
  
  public void setInFlag(String inFlag) {
/* 130 */     this.inFlag = inFlag;
  }
  
  public String getInMode() {
/* 134 */     return this.inMode;
  }
  
  public void setInMode(String inMode) {
/* 138 */     this.inMode = inMode;
  }
  
  public Long getPackId() {
/* 142 */     return this.packId;
  }
  
  public void setPackId(Long packId) {
/* 146 */     this.packId = packId;
  }
  
  public Byte getPrePost() {
/* 150 */     return this.prePost;
  }
  
  public void setPrePost(Byte prePost) {
/* 154 */     this.prePost = prePost;
  }
  
  public Date getRenewDate() {
/* 158 */     return this.renewDate;
  }
  
  public void setRenewDate(Date renewDate) {
/* 162 */     this.renewDate = renewDate;
  }
  
  public Long getSmeId() {
/* 166 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 170 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/* 174 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 178 */     this.status = status;
  }
  
  public Date getSubDate() {
/* 182 */     return this.subDate;
  }
  
  public void setSubDate(Date subDate) {
/* 186 */     this.subDate = subDate;
  }
  
  public Integer getTotalAgentAllocate() {
/* 190 */     return this.totalAgentAllocate;
  }
  
  public void setTotalAgentAllocate(Integer totalAgentAllocate) {
/* 194 */     this.totalAgentAllocate = totalAgentAllocate;
  }
  
  public Float getUserBalance() {
/* 198 */     return this.userBalance;
  }
  
  public void setUserBalance(Float userBalance) {
/* 202 */     this.userBalance = userBalance;
  }
  
  public String getUserType() {
/* 206 */     return this.userType;
  }
  
  public void setUserType(String userType) {
/* 210 */     this.userType = userType;
  }
  
  public Boolean getRecording() {
/* 214 */     return this.recording;
  }
  
  public void setRecording(Boolean recording) {
/* 218 */     this.recording = recording;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\MpbxChargingSucc.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */