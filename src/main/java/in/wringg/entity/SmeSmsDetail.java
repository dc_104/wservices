package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "sme_sms_details")
@NamedQuery(name = "SmeSmsDetail.findAll", query = "SELECT s FROM SmeSmsDetail s")
public class SmeSmsDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Column(name = "base_count")
  private int baseCount;
  @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinColumn(name = "campaign_id")
  private SmsCampaign campaignId;
  @Column(name = "fail_base")
  private int failBase;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "sme_sms_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "sme_sms_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "sme_sms_gen")
  private Long id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "processed_base")
  private int processedBase;
  @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinColumn(name = "sme_id")
  private SmeProfile smeId;
  private byte status;
  @Column(name = "success_base")
  private int successBase;
  
  public int getBaseCount() {
/*  56 */     return this.baseCount;
  }
  
  public void setBaseCount(int baseCount) {
/*  60 */     this.baseCount = baseCount;
  }
  
  public SmsCampaign getCampaignId() {
/*  64 */     return this.campaignId;
  }
  
  public void setCampaignId(SmsCampaign campaignId) {
/*  68 */     this.campaignId = campaignId;
  }
  
  public int getFailBase() {
/*  72 */     return this.failBase;
  }
  
  public void setFailBase(int failBase) {
/*  76 */     this.failBase = failBase;
  }
  
  public Long getId() {
/*  80 */     return this.id;
  }
  
  public void setId(Long id) {
/*  84 */     this.id = id;
  }
  
  public Date getInsertTime() {
/*  88 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/*  92 */     this.insertTime = insertTime;
  }
  
  public int getProcessedBase() {
/*  96 */     return this.processedBase;
  }
  
  public void setProcessedBase(int processedBase) {
/* 100 */     this.processedBase = processedBase;
  }
  
  public SmeProfile getSmeId() {
/* 104 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 108 */     this.smeId = smeId;
  }
  
  public byte getStatus() {
/* 112 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 116 */     this.status = status;
  }
  
  public int getSuccessBase() {
/* 120 */     return this.successBase;
  }
  
  public void setSuccessBase(int successBase) {
/* 124 */     this.successBase = successBase;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SmeSmsDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */