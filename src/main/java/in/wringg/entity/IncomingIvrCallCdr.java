package in.wringg.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@Table(name = "incoming_ivr_call_cdr")
@NamedQuery(name = "IncomingIvrCallCdr.findAll", query = "SELECT i FROM IncomingIvrCallCdr i")
public class IncomingIvrCallCdr implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", nullable = false)
  @TableGenerator(name = "incoming_ivr_call_cdr_gen", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "incoming_ivr_call_cdr_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "incoming_ivr_call_cdr_gen")
  private Long id;
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "answer")
  private Byte answer;
  @Column(name = "call_status")
  private Byte callStatus;
  @Column(name = "disconnected_by ")
  private String disconnectedBy;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "call_direction_status")
  private Integer callDirectionStatus;
  @Column(name = "call_recorded_file")
  private String callRecordedFile;
  @Column(name = "call_recording_status")
  private Integer callRecordingStatus;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private Byte cdrMode;
  @Column(name = "channel_no")
  private Integer channelNo;
  private Integer duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  
/*  51 */   public Byte getAnswer() { return this.answer; } private String hlr; @Temporal(TemporalType.TIMESTAMP) @Column(name = "insert_date_time") private Date insertDateTime; private Long longcode; @Column(name = "master_shortcode") private String masterShortcode; @ManyToOne @JoinColumn(name = "patched_agent_id") private AgentDetail patchedAgentId; @Column(name = "server_ip_address") private String serverIpAddress; @Column(name = "shortcode_mapping") private String shortcodeMapping; @Column(name = "merge_status") private Byte mergeStatus; @ManyToOne @JoinColumn(name = "sme_id") private SmeProfile smeId; @Column(name = "session_id") private String sessionId; @Column(name = "sme_identifier") private String smeIdentifier; @Temporal(TemporalType.TIMESTAMP) @Column(name = "start_date_time") private Date startDateTime; @Column(name = "voicemail_recording_file")
  private String voicemailRecordingFile; @Column(name = "voicemail_recording_status")
  private Integer voicemailRecordingStatus; @Column(name = "address_book_id")
  private Long addressBookId; @Column(name = "customer_name")
/*  55 */   private String customerName; public void setAnswer(Byte answer) { this.answer = answer; }

















































  
  public Byte getMergeStatus() {
/* 107 */     return this.mergeStatus;
  }
  
  public void setMergeStatus(Byte mergeStatus) {
/* 111 */     this.mergeStatus = mergeStatus;
  }













  
  public String getSessionId() {
/* 128 */     return this.sessionId;
  }
  
  public void setSessionId(String sessionId) {
/* 132 */     this.sessionId = sessionId;
  }



















  
  public Long getAddressBookId() {
/* 155 */     return this.addressBookId;
  }
  
  public void setAddressBookId(Long addressBookId) {
/* 159 */     this.addressBookId = addressBookId;
  }
  
  public String getCustomerName() {
/* 163 */     return this.customerName;
  }
  
  public void setCustomerName(String customerName) {
/* 167 */     this.customerName = customerName;
  }



  
  public Long getId() {
/* 174 */     return this.id;
  }
  
  public void setId(Long id) {
/* 178 */     this.id = id;
  }
  
  public String getAgentGroup() {
/* 182 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/* 186 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/* 190 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 194 */     this.callDirection = callDirection;
  }
  
  public Integer getCallDirectionStatus() {
/* 198 */     return this.callDirectionStatus;
  }
  
  public void setCallDirectionStatus(Integer callDirectionStatus) {
/* 202 */     this.callDirectionStatus = callDirectionStatus;
  }
  
  public String getCallRecordedFile() {
/* 206 */     return this.callRecordedFile;
  }
  
  public void setCallRecordedFile(String callRecordedFile) {
/* 210 */     this.callRecordedFile = callRecordedFile;
  }
  
  public Integer getCallRecordingStatus() {
/* 214 */     return this.callRecordingStatus;
  }
  
  public void setCallRecordingStatus(Integer callRecordingStatus) {
/* 218 */     this.callRecordingStatus = callRecordingStatus;
  }
  
  public String getCalledNumber() {
/* 222 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 226 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 230 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 234 */     this.callingNumber = callingNumber;
  }
  
  public Byte getCdrMode() {
/* 238 */     return this.cdrMode;
  }
  
  public void setCdrMode(Byte cdrMode) {
/* 242 */     this.cdrMode = cdrMode;
  }
  
  public Integer getChannelNo() {
/* 246 */     return this.channelNo;
  }
  
  public void setChannelNo(Integer channelNo) {
/* 250 */     this.channelNo = channelNo;
  }
  
  public Integer getDuration() {
/* 254 */     return this.duration;
  }
  
  public void setDuration(Integer duration) {
/* 258 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 262 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 266 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 270 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 274 */     this.hlr = hlr;
  }
  
  public Date getInsertDateTime() {
/* 278 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 282 */     this.insertDateTime = insertDateTime;
  }
  
  public Long getLongcode() {
/* 286 */     return this.longcode;
  }
  
  public void setLongcode(Long longcode) {
/* 290 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 294 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 298 */     this.masterShortcode = masterShortcode;
  }
  
  public String getServerIpAddress() {
/* 302 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 306 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 310 */     return this.shortcodeMapping;
  }
  
  public AgentDetail getPatchedAgentId() {
/* 314 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(AgentDetail patchedAgentId) {
/* 318 */     this.patchedAgentId = patchedAgentId;
  }
  
  public SmeProfile getSmeId() {
/* 322 */     return this.smeId;
  }
  
  public void setSmeId(SmeProfile smeId) {
/* 326 */     this.smeId = smeId;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 330 */     this.shortcodeMapping = shortcodeMapping;
  }


  
  public String getSmeIdentifier() {
/* 336 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 340 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 344 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 348 */     this.startDateTime = startDateTime;
  }
  
  public String getVoicemailRecordingFile() {
/* 352 */     return this.voicemailRecordingFile;
  }
  
  public void setVoicemailRecordingFile(String voicemailRecordingFile) {
/* 356 */     this.voicemailRecordingFile = voicemailRecordingFile;
  }
  
  public Integer getVoicemailRecordingStatus() {
/* 360 */     return this.voicemailRecordingStatus;
  }
  
  public void setVoicemailRecordingStatus(Integer voicemailRecordingStatus) {
/* 364 */     this.voicemailRecordingStatus = voicemailRecordingStatus;
  }
  
  public Byte getCallStatus() {
/* 368 */     return this.callStatus;
  }
  
  public void setCallStatus(Byte callStatus) {
/* 372 */     this.callStatus = callStatus;
  }
  
  public String getDisconnectedBy() {
/* 376 */     return this.disconnectedBy;
  }
  
  public void setDisconnectedBy(String disconnectedBy) {
/* 380 */     this.disconnectedBy = disconnectedBy;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\IncomingIvrCallCdr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */