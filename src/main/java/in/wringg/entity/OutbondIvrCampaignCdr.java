package in.wringg.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;























@Entity
@Table(name = "outbond_ivr_campaign_cdr")
@NamedQuery(name = "OutbondIvrCampaignCdr.findAll", query = "SELECT o FROM OutbondIvrCampaignCdr o")
public class OutbondIvrCampaignCdr
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "agent_group")
  private String agentGroup;
  @Column(name = "call_direction")
  private String callDirection;
  @Column(name = "call_direction_status")
  private int callDirectionStatus;
  @Column(name = "called_number")
  private String calledNumber;
  @Column(name = "calling_number")
  private String callingNumber;
  @Column(name = "cdr_mode")
  private String cdrMode;
  @Column(name = "channel_no")
  private int channelNo;
  private int duration;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date_time")
  private Date endDateTime;
  private String hlr;
  private BigInteger id;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  private BigInteger longcode;
  @Column(name = "master_shortcode")
  private String masterShortcode;
  @Column(name = "patched_agent_id")
  private String patchedAgentId;
  @Column(name = "recorded_file")
  private String recordedFile;
  @Column(name = "recording_status")
  private int recordingStatus;
  @Column(name = "server_ip_address")
  private String serverIpAddress;
  @Column(name = "shortcode_mapping")
  private String shortcodeMapping;
  @Column(name = "sme_id")
  private String smeId;
  @Column(name = "sme_identifier")
  private String smeIdentifier;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date_time")
  private Date startDateTime;
  
  public String getAgentGroup() {
/*  89 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/*  93 */     this.agentGroup = agentGroup;
  }
  
  public String getCallDirection() {
/*  97 */     return this.callDirection;
  }
  
  public void setCallDirection(String callDirection) {
/* 101 */     this.callDirection = callDirection;
  }
  
  public int getCallDirectionStatus() {
/* 105 */     return this.callDirectionStatus;
  }
  
  public void setCallDirectionStatus(int callDirectionStatus) {
/* 109 */     this.callDirectionStatus = callDirectionStatus;
  }
  
  public String getCalledNumber() {
/* 113 */     return this.calledNumber;
  }
  
  public void setCalledNumber(String calledNumber) {
/* 117 */     this.calledNumber = calledNumber;
  }
  
  public String getCallingNumber() {
/* 121 */     return this.callingNumber;
  }
  
  public void setCallingNumber(String callingNumber) {
/* 125 */     this.callingNumber = callingNumber;
  }
  
  public String getCdrMode() {
/* 129 */     return this.cdrMode;
  }
  
  public void setCdrMode(String cdrMode) {
/* 133 */     this.cdrMode = cdrMode;
  }
  
  public int getChannelNo() {
/* 137 */     return this.channelNo;
  }
  
  public void setChannelNo(int channelNo) {
/* 141 */     this.channelNo = channelNo;
  }
  
  public int getDuration() {
/* 145 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/* 149 */     this.duration = duration;
  }
  
  public Date getEndDateTime() {
/* 153 */     return this.endDateTime;
  }
  
  public void setEndDateTime(Date endDateTime) {
/* 157 */     this.endDateTime = endDateTime;
  }
  
  public String getHlr() {
/* 161 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 165 */     this.hlr = hlr;
  }
  
  public BigInteger getId() {
/* 169 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 173 */     this.id = id;
  }
  
  public Date getInsertDateTime() {
/* 177 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 181 */     this.insertDateTime = insertDateTime;
  }
  
  public BigInteger getLongcode() {
/* 185 */     return this.longcode;
  }
  
  public void setLongcode(BigInteger longcode) {
/* 189 */     this.longcode = longcode;
  }
  
  public String getMasterShortcode() {
/* 193 */     return this.masterShortcode;
  }
  
  public void setMasterShortcode(String masterShortcode) {
/* 197 */     this.masterShortcode = masterShortcode;
  }
  
  public String getPatchedAgentId() {
/* 201 */     return this.patchedAgentId;
  }
  
  public void setPatchedAgentId(String patchedAgentId) {
/* 205 */     this.patchedAgentId = patchedAgentId;
  }
  
  public String getRecordedFile() {
/* 209 */     return this.recordedFile;
  }
  
  public void setRecordedFile(String recordedFile) {
/* 213 */     this.recordedFile = recordedFile;
  }
  
  public int getRecordingStatus() {
/* 217 */     return this.recordingStatus;
  }
  
  public void setRecordingStatus(int recordingStatus) {
/* 221 */     this.recordingStatus = recordingStatus;
  }
  
  public String getServerIpAddress() {
/* 225 */     return this.serverIpAddress;
  }
  
  public void setServerIpAddress(String serverIpAddress) {
/* 229 */     this.serverIpAddress = serverIpAddress;
  }
  
  public String getShortcodeMapping() {
/* 233 */     return this.shortcodeMapping;
  }
  
  public void setShortcodeMapping(String shortcodeMapping) {
/* 237 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public String getSmeId() {
/* 241 */     return this.smeId;
  }
  
  public void setSmeId(String smeId) {
/* 245 */     this.smeId = smeId;
  }
  
  public String getSmeIdentifier() {
/* 249 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 253 */     this.smeIdentifier = smeIdentifier;
  }
  
  public Date getStartDateTime() {
/* 257 */     return this.startDateTime;
  }
  
  public void setStartDateTime(Date startDateTime) {
/* 261 */     this.startDateTime = startDateTime;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\OutbondIvrCampaignCdr.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */