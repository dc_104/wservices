package in.wringg.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;





@Entity
@Table(name = "sequence_gen")
@NamedQuery(name = "SequenceGen.findAll", query = "SELECT s FROM SequenceGen s")
public class SequenceGen
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "seq_col_name")
  private String seqColName;
  @Column(name = "seq_col_value")
  private int seqColValue;
  
  public String getSeqColName() {
/* 28 */     return this.seqColName;
  }
  
  public void setSeqColName(String seqColName) {
/* 32 */     this.seqColName = seqColName;
  }
  
  public int getSeqColValue() {
/* 36 */     return this.seqColValue;
  }
  
  public void setSeqColValue(int seqColValue) {
/* 40 */     this.seqColValue = seqColValue;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\SequenceGen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */