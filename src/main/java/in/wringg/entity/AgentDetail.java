package in.wringg.entity;

import in.wringg.entity.AgentDetailTime;
import in.wringg.entity.AgentGroupDetail;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "agent_details")
@NamedQuery(name = "AgentDetail.findAll", query = "SELECT a FROM AgentDetail a")
public class AgentDetail
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "agent_id")
  @TableGenerator(name = "agent_details_seq", table = "sequence_gen", pkColumnName = "seq_col_name", valueColumnName = "seq_col_value", pkColumnValue = "agent_details_seq", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "agent_details_seq")
  private Long agentId;
  @Column(name = "agent_mobile")
  private String agentMobile;
  @Column(name = "agent_name")
  private String agentName;
  @Column(name = "in_time")
  private Time inTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_time")
  private Date insertTime;
  @Column(name = "out_time")
  private Time outTime;
  @Column(name = "sme_id")
  private Long smeId;
  private Byte status;
  @Column(name = "days_flag")
  private Integer daysFlag;
  @Column(name = "agent_position")
  private Integer agentPosition;
  @Column(name = "agent_extention")
  private Integer agentExtention;
  @Column(name = "agent_email")
  private String agentEmail;
  @Column(name = "sticky_agent")
  private Byte stickyAgent;
  @Column(name = "agent_masking")
  private Byte agentMasking;
  @Column(name = "agent_score")
  private Integer agentScore;
  @Column(name = "sticky_days")
  private Integer stickyDays;
  @Column(name = "is_updated")
  private Byte isUpdated;
  @Column(name = "assign_failed_calls")
  private Byte assignFailedCalls;
  @Column(name = "assign_voicemail_calls")
  private Byte assignVoicemailCalls;
  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinTable(name = "agent_group_mapping", joinColumns = {@JoinColumn(name = "agent_id")}, inverseJoinColumns = {@JoinColumn(name = "group_id")})
  private Set<AgentGroupDetail> groups;
  @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "agent_details")
  private Set<AgentDetailTime> agent_details_timing;
  @Column(name = "in_permission_flag")
  private Boolean inPermissionFlag;
  @Column(name = "out_permission_flag")
  private Boolean outPermissionFlag;

  public Byte getAssignFailedCalls() {
/*  97 */     return this.assignFailedCalls;
  }
  
  public void setAssignFailedCalls(Byte assignFailedCalls) {
/* 101 */     this.assignFailedCalls = assignFailedCalls;
  }
  
  public Byte getAssignVoicemailCalls() {
/* 105 */     return this.assignVoicemailCalls;
  }
  
  public void setAssignVoicemailCalls(Byte assignVoicemailCalls) {
/* 109 */     this.assignVoicemailCalls = assignVoicemailCalls;
  }
  
  public Byte getIsUpdated() {
/* 113 */     return this.isUpdated;
  }
  
  public void setIsUpdated(Byte isUpdated) {
/* 117 */     this.isUpdated = isUpdated;
  }
  
  public Integer getStickyDays() {
/* 121 */     return this.stickyDays;
  }
  
  public void setStickyDays(Integer stickyDays) {
/* 125 */     this.stickyDays = stickyDays;
  }
  
  public Integer getAgentScore() {
/* 129 */     return this.agentScore;
  }
  
  public void setAgentScore(Integer agentScore) {
/* 133 */     this.agentScore = agentScore;
  }
  
  public Byte getAgentMasking() {
/* 137 */     return this.agentMasking;
  }
  
  public void setAgentMasking(Byte agentMasking) {
/* 141 */     this.agentMasking = agentMasking;
  }
  
  public Byte getStickyAgent() {
/* 145 */     return this.stickyAgent;
  }
  
  public void setStickyAgent(Byte stickyAgent) {
/* 149 */     this.stickyAgent = stickyAgent;
  }
  
  public static long getSerialversionuid() {
/* 153 */     return 1L;
  }
  
  public Integer getAgentExtention() {
/* 157 */     return this.agentExtention;
  }
  
  public void setAgentExtention(Integer agentExtention) {
/* 161 */     this.agentExtention = agentExtention;
  }
  
  public String getAgentEmail() {
/* 165 */     return this.agentEmail;
  }
  
  public void setAgentEmail(String agentEmail) {
/* 169 */     this.agentEmail = agentEmail;
  }
  
  public Integer getAgentPosition() {
/* 173 */     return this.agentPosition;
  }
  
  public void setAgentPosition(Integer agentPosition) {
/* 177 */     this.agentPosition = agentPosition;
  }
  
  public Integer getDaysFlag() {
/* 181 */     return this.daysFlag;
  }
  
  public void setDaysFlag(Integer daysFlag) {
/* 185 */     this.daysFlag = daysFlag;
  }


  
  public AgentDetail() {
/* 191 */     this.groups = new HashSet<>(0);

/* 204 */     this.agent_details_timing = new HashSet<>();
  } public Set<AgentGroupDetail> getGroups() {
    return this.groups;
  } public Long getAgentId() {
/* 208 */     return this.agentId;
  } public void setGroups(Set<AgentGroupDetail> groups) {
    this.groups = groups;
  } public void setAgentId(Long agentId) {
/* 212 */     this.agentId = agentId;
  }
  
  public String getAgentMobile() {
/* 216 */     return this.agentMobile;
  }
  
  public void setAgentMobile(String agentMobile) {
/* 220 */     this.agentMobile = agentMobile;
  }
  
  public String getAgentName() {
/* 224 */     return this.agentName;
  }
  
  public void setAgentName(String agentName) {
/* 228 */     this.agentName = agentName;
  }
  
  public Time getInTime() {
/* 232 */     return this.inTime;
  }
  
  public void setInTime(Time inTime) {
/* 236 */     this.inTime = inTime;
  }
  
  public Date getInsertTime() {
/* 240 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 244 */     this.insertTime = insertTime;
  }
  
  public Time getOutTime() {
/* 248 */     return this.outTime;
  }
  
  public void setOutTime(Time outTime) {
/* 252 */     this.outTime = outTime;
  }
  
  public Long getSmeId() {
/* 256 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 260 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/* 264 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 268 */     this.status = status;
  }
  
  public Set<AgentDetailTime> getAgent_details_timing() {
/* 272 */     return this.agent_details_timing;
  }
  
  public void setAgent_details_timing(Set<AgentDetailTime> agent_details_timing) {
/* 276 */     this.agent_details_timing = agent_details_timing;
  }

  public Boolean getInPermissionFlag() {
	return inPermissionFlag;
}

public void setInPermissionFlag(Boolean inPermissionFlag) {
	this.inPermissionFlag = inPermissionFlag;
}

public Boolean getOutPermissionFlag() {
	return outPermissionFlag;
}

public void setOutPermissionFlag(Boolean outPermissionFlag) {
	this.outPermissionFlag = outPermissionFlag;
}

@Override
public String toString() {
	return "AgentDetail [agentId=" + agentId + ", agentMobile=" + agentMobile + ", agentName=" + agentName + ", inTime="
			+ inTime + ", insertTime=" + insertTime + ", outTime=" + outTime + ", smeId=" + smeId + ", status=" + status
			+ ", daysFlag=" + daysFlag + ", agentPosition=" + agentPosition + ", agentExtention=" + agentExtention
			+ ", agentEmail=" + agentEmail + ", stickyAgent=" + stickyAgent + ", agentMasking=" + agentMasking
			+ ", agentScore=" + agentScore + ", stickyDays=" + stickyDays + ", isUpdated=" + isUpdated
			+ ", assignFailedCalls=" + assignFailedCalls + ", assignVoicemailCalls=" + assignVoicemailCalls
			+ ", groups=" + groups + ", agent_details_timing=" + agent_details_timing + ", inPermissionFlag="
			+ inPermissionFlag + ", outPermissionFlag=" + outPermissionFlag + "]";
}

//public String toString() {
///* 281 */     return "AgentDetail [agentId=" + this.agentId + ", agentMobile=" + this.agentMobile + ", agentName=" + this.agentName + ", inTime=" + this.inTime + ", insertTime=" + this.insertTime + ", outTime=" + this.outTime + ", smeId=" + this.smeId + ", status=" + this.status + ", daysFlag=" + this.daysFlag + ", agentPosition=" + this.agentPosition + ", agentExtention=" + this.agentExtention + ", agentEmail=" + this.agentEmail + ", stickyAgent=" + this.stickyAgent + ", agentMasking=" + this.agentMasking + ", groups=" + this.groups + ", agent_details_timing=" + this.agent_details_timing + "]";
//  }



}

