package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "address_book")
@NamedQuery(name = "AddressBook.findAll", query = "SELECT a FROM AddressBook a")
public class AddressBook
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "sme_id")
  private Long smeId;
  @Column(name = "customer_name")
  private String customerName;
  @Column(name = "customer_number_primary")
  private String customerNumberPrimary;
  @Column(name = "customer_number_secondary")
  private String customerNumberSecondary;
  private Byte status;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "insert_date_time")
  private Date insertDateTime;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "updated_date_time")
  private Date updatedDateTime;
  private Byte mode;
  @Column(name = "company_name")
  private String companyName;
  @Column(name = "email_id")
  private String emailId;
  @Column(name = "created_by")
  private Long createdBy;
  @Column(name = "visibility_flag")
  private Byte visibilityFlag;
  @Column(name = "is_updated")
  private Byte isUpdated;
  
  public Byte getIsUpdated() {
/*  70 */     return this.isUpdated;
  }
  
  public void setIsUpdated(Byte isUpdated) {
/*  74 */     this.isUpdated = isUpdated;
  }
  
  public Long getId() {
/*  78 */     return this.id;
  }
  
  public void setId(Long id) {
/*  82 */     this.id = id;
  }
  
  public Long getSmeId() {
/*  86 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/*  90 */     this.smeId = smeId;
  }
  
  public String getCustomerName() {
/*  94 */     return this.customerName;
  }
  
  public void setCustomerName(String customerName) {
/*  98 */     this.customerName = customerName;
  }
  
  public String getCustomerNumberPrimary() {
/* 102 */     return this.customerNumberPrimary;
  }
  
  public void setCustomerNumberPrimary(String customerNumberPrimary) {
/* 106 */     this.customerNumberPrimary = customerNumberPrimary;
  }
  
  public String getCustomerNumberSecondary() {
/* 110 */     return this.customerNumberSecondary;
  }
  
  public void setCustomerNumberSecondary(String customerNumberSecondary) {
/* 114 */     this.customerNumberSecondary = customerNumberSecondary;
  }
  
  public Byte getStatus() {
/* 118 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 122 */     this.status = status;
  }
  
  public Date getInsertDateTime() {
/* 126 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 130 */     this.insertDateTime = insertDateTime;
  }
  
  public Date getUpdatedDateTime() {
/* 134 */     return this.updatedDateTime;
  }
  
  public void setUpdatedDateTime(Date updatedDateTime) {
/* 138 */     this.updatedDateTime = updatedDateTime;
  }
  
  public Byte getMode() {
/* 142 */     return this.mode;
  }
  
  public void setMode(Byte mode) {
/* 146 */     this.mode = mode;
  }
  
  public String getCompanyName() {
/* 150 */     return this.companyName;
  }
  
  public void setCompanyName(String companyName) {
/* 154 */     this.companyName = companyName;
  }
  
  public String getEmailId() {
/* 158 */     return this.emailId;
  }
  
  public void setEmailId(String emailId) {
/* 162 */     this.emailId = emailId;
  }
  
  public Long getCreatedBy() {
/* 166 */     return this.createdBy;
  }
  
  public void setCreatedBy(Long createdBy) {
/* 170 */     this.createdBy = createdBy;
  }
  
  public Byte getVisibilityFlag() {
/* 174 */     return this.visibilityFlag;
  }
  
  public void setVisibilityFlag(Byte visibilityFlag) {
/* 178 */     this.visibilityFlag = visibilityFlag;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\AddressBook.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */