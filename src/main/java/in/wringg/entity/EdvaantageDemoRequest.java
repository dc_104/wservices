package in.wringg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;







@Entity
@Table(name = "edvaantage_demo_request")
@NamedQuery(name = "EdvaantageDemoRequest.findAll", query = "SELECT a FROM EdvaantageDemoRequest a")
public class EdvaantageDemoRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "name")
  private String name;
  @Column(name = "email_id")
  private String emailId;
  @Column(name = "mobile_number")
  private String mobileNumber;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_time")
  private Date date_time;
  @Column(name = "message")
  private String message;
  @Column(name = "status")
  private Byte status;
  @Column(name = "pack_id")
  private String packId;
  
  public String getPackId() {
/*  49 */     return this.packId;
  }
  
  public void setPackId(String packId) {
/*  53 */     this.packId = packId;
  }
  
  public Long getId() {
/*  57 */     return this.id;
  }
  
  public void setId(Long id) {
/*  61 */     this.id = id;
  }
  
  public String getName() {
/*  65 */     return this.name;
  }
  
  public void setName(String name) {
/*  69 */     this.name = name;
  }
  
  public String getEmailId() {
/*  73 */     return this.emailId;
  }
  
  public void setEmailId(String emailId) {
/*  77 */     this.emailId = emailId;
  }
  
  public String getMobileNumber() {
/*  81 */     return this.mobileNumber;
  }
  
  public void setMobileNumber(String mobileNumber) {
/*  85 */     this.mobileNumber = mobileNumber;
  }
  
  public Date getDate_time() {
/*  89 */     return this.date_time;
  }
  
  public void setDate_time(Date date_time) {
/*  93 */     this.date_time = date_time;
  }
  
  public String getMessage() {
/*  97 */     return this.message;
  }
  
  public void setMessage(String message) {
/* 101 */     this.message = message;
  }
  
  public Byte getStatus() {
/* 105 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 109 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\entity\EdvaantageDemoRequest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */