package in.wringg.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
	/* 19 */ private static String REALM = "MY_OAUTH_REALM";

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserApprovalHandler userApprovalHandler;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		/* 32 */ clients/* 33 */ .inMemory()/* 34 */ .withClient("edvaantage")/* 35 */ .authorizedGrantTypes(
				new String[] { "password", "authorization_code", "refresh_token", "implicit", "generate"
				/* 36 */ }).authorities(new String[] { "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT"
				/* 37 */ }).scopes(new String[] { "read", "write", "trust"
				/* 38 */ }).secret("teamwirngg")/* 39 */ .accessTokenValiditySeconds(300)
				/* 40 */ .refreshTokenValiditySeconds(600);
	}

	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(this.tokenStore).userApprovalHandler(this.userApprovalHandler).reuseRefreshTokens(false)
				.authenticationManager(this.authenticationManager);
	}

	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.realm(REALM + "/client");
	}
}
