package in.wringg.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	private static final String RESOURCE_ID = "my_rest_api";

	public void configure(ResourceServerSecurityConfigurer resources) {
		/* 29 */ resources.resourceId("my_rest_api").stateless(false);
	}

	public void configure(HttpSecurity http) throws Exception {
		http.addFilterBefore(new Filter() {
			@Override
			public void init(FilterConfig filterConfig) throws ServletException {
			}

			@Override
			public void doFilter(ServletRequest request, ServletResponse res, FilterChain chain)
					throws IOException, ServletException {
				HttpServletResponse response = (HttpServletResponse) res;
				response.setHeader("Access-Control-Allow-Origin", "*");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
				response.setHeader("Access-Control-Max-Age", "3600");
				response.setHeader("Access-Control-Allow-Headers",
						"X-Requested-With, Content-Type, Authorization, Origin, Accept, Access-Control-Request-Method, Access-Control-Request-Headers,AUTH-TOKEN");
				chain.doFilter(request, response);
			}

			@Override
			public void destroy() {

			}
		}, ChannelProcessingFilter.class)

				.authorizeRequests().antMatchers("/admin/**").authenticated().antMatchers("/sme/**").authenticated();
	}
}

/*
 * Location: D:\Work\wservices.war!\WEB-INF\classes\in\wringg\security\
 * ResourceServerConfiguration.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */