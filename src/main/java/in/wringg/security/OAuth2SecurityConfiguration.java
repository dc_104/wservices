package in.wringg.security;

import java.io.IOException;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityBuilder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class OAuth2SecurityConfiguration extends WebSecurityConfigurerAdapter implements AuthenticationEntryPoint {
	@Resource(name = "userDetailService")
	private UserDetailsService userDetailsService;
	@Autowired
	TokenStore tokenStore;
	@Autowired
	private ClientDetailsService clientDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		/* 50 */ return (PasswordEncoder) new BCryptPasswordEncoder();
	}

	@Autowired
	public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
		/* 67 */ auth.userDetailsService(this.userDetailsService);
	}

	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		/* 72 */ HttpStatus responseStatus = HttpStatus.UNAUTHORIZED;
		/* 73 */ response.sendError(responseStatus.value(), responseStatus.getReasonPhrase());
	}

	public void configure(WebSecurity web) throws Exception {
		/* 81 */ web.ignoring().antMatchers(HttpMethod.OPTIONS, new String[] { "/**" });
	}

	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		/* 89 */ return super.authenticationManagerBean();
	}

	@Bean
	public TokenStore tokenStore() {
		InMemoryTokenStore tokenService = new InMemoryTokenStore();
		tokenService.setAuthenticationKeyGenerator(new AuthenticationKeyGenerator() {

			@Override
			public String extractKey(OAuth2Authentication authentication) {
				return UUID.randomUUID().toString();
			}
		});
		return (TokenStore) tokenService;
	}

	@Bean
	@Autowired
	public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore) {
		/* 108 */ TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		/* 109 */ handler.setTokenStore(tokenStore);
		/* 110 */ handler
				.setRequestFactory((OAuth2RequestFactory) new DefaultOAuth2RequestFactory(this.clientDetailsService));
		/* 111 */ handler.setClientDetailsService(this.clientDetailsService);
		/* 112 */ return handler;
	}

	@Bean
	@Autowired
	public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
		/* 118 */ TokenApprovalStore store = new TokenApprovalStore();
		/* 119 */ store.setTokenStore(tokenStore);
		/* 120 */ return (ApprovalStore) store;
	}

	@Bean
	public WebResponseExceptionTranslator webResponseExceptionTranslator() {
		return new DefaultWebResponseExceptionTranslator() {

			@Override
			public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
				ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
				OAuth2Exception body = responseEntity.getBody();
				HttpHeaders headers = new HttpHeaders();
				headers.setAll(responseEntity.getHeaders().toSingleValueMap());
				headers.set("Access-Control-Allow-Origin", "*");
				headers.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
				headers.set("Access-Control-Max-Age", "3600");
				headers.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
				return new ResponseEntity<>(body, headers, responseEntity.getStatusCode());
			}
		};
	}
}

/*
 * Location: D:\Work\wservices.war!\WEB-INF\classes\in\wringg\security\
 * OAuth2SecurityConfiguration.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */