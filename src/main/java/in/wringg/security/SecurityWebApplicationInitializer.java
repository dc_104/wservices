package in.wringg.security;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.multipart.support.MultipartFilter;

public class SecurityWebApplicationInitializer
  extends AbstractSecurityWebApplicationInitializer {
  protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
/* 11 */     insertFilters(servletContext, new Filter[] { (Filter)new MultipartFilter() });
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\security\SecurityWebApplicationInitializer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */