package in.wringg.security;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;



public class UniqueAuthenticationKeyGenerator
  implements AuthenticationKeyGenerator
{
  private static final String CLIENT_ID = "client_id";
  private static final String SCOPE = "scope";
  private static final String USERNAME = "username";
  private static final String UUID_KEY = "uuid";
  
  public String extractKey(OAuth2Authentication authentication) {
    MessageDigest digest;
/* 29 */     Map<String, String> values = new LinkedHashMap<>();
/* 30 */     OAuth2Request authorizationRequest = authentication.getOAuth2Request();
/* 31 */     if (!authentication.isClientOnly()) {
/* 32 */       values.put("username", authentication.getName());
    }
/* 34 */     values.put("client_id", authorizationRequest.getClientId());
/* 35 */     if (authorizationRequest.getScope() != null) {
/* 36 */       values.put("scope", OAuth2Utils.formatParameterList(authorizationRequest.getScope()));
    }
/* 38 */     Map<String, Serializable> extentions = authorizationRequest.getExtensions();
/* 39 */     String uuid = null;
/* 40 */     if (extentions == null) {
/* 41 */       extentions = new HashMap<>(1);
/* 42 */       uuid = UUID.randomUUID().toString();
/* 43 */       extentions.put("uuid", uuid);
    } else {
/* 45 */       uuid = (String)extentions.get("uuid");
/* 46 */       if (uuid == null) {
/* 47 */         uuid = UUID.randomUUID().toString();
/* 48 */         extentions.put("uuid", uuid);
      } 
    } 
/* 51 */     values.put("uuid", uuid);

    
    try {
/* 55 */       digest = MessageDigest.getInstance("MD5");
    }
/* 57 */     catch (NoSuchAlgorithmException e) {
/* 58 */       throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
    } 
    
    try {
/* 62 */       byte[] bytes = digest.digest(values.toString().getBytes("UTF-8"));
/* 63 */       return String.format("%032x", new Object[] { new BigInteger(1, bytes) });
    }
/* 65 */     catch (UnsupportedEncodingException e) {
/* 66 */       throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).");
    } 
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\security\UniqueAuthenticationKeyGenerator.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */