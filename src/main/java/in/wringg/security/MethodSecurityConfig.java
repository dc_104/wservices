package in.wringg.security;

import in.wringg.security.OAuth2SecurityConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class MethodSecurityConfig
  extends GlobalMethodSecurityConfiguration {
  @Autowired
  private OAuth2SecurityConfiguration securityConfig;
  
  protected MethodSecurityExpressionHandler createExpressionHandler() {
/* 19 */     return (MethodSecurityExpressionHandler)new OAuth2MethodSecurityExpressionHandler();
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\security\MethodSecurityConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */