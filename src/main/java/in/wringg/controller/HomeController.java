package in.wringg.controller;

import flexjson.JSONSerializer;
import in.wringg.pojo.EdvaantageDemoBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.service.AdminService;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class HomeController
{
  @Autowired
  AdminService admin;
  
  @GetMapping({"/"})
  public String home() {
/* 31 */     return "Success";
  }
  @ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
/* 36 */     ex.printStackTrace();
/* 37 */     ServerResponse response = new ServerResponse();
/* 38 */     response.setSuccess(false);
/* 39 */     ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
/* 40 */     errorDetails.setErrorString("Home ::" + ex.getMessage());
/* 41 */     JSONSerializer serializer = new JSONSerializer();
/* 42 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
/* 43 */     response.setResult(res);
/* 44 */     res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 45 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  @RequestMapping(value = {"/demo/request"}, method = {RequestMethod.POST})
  public ResponseEntity<String> demoRequest(@RequestBody EdvaantageDemoBean request) throws Exception {
/* 49 */     ServerResponse response = this.admin.addDemoRequest(request);
/* 50 */     JSONSerializer serializer = new JSONSerializer();
/* 51 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 52 */     return new ResponseEntity(res, HttpStatus.OK);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\controller\HomeController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */