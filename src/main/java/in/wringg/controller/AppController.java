package in.wringg.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import flexjson.JSONSerializer;
import in.wringg.dao.AdminDao;
import in.wringg.entity.Users;
import in.wringg.pojo.AddressBookBean;
import in.wringg.pojo.AgentInsightPojo;
import in.wringg.pojo.CallBaseBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SmeCustomerRemarksBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UserRoleDetailBean;
import in.wringg.service.AdminService;
import in.wringg.service.AgentProfileService;
import in.wringg.service.ComponentService;
import in.wringg.service.SmeService;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;












@RestController
@Api(value = "Wringg ", description = "Rest API for app operations", tags = {"WRINGG APP API"})
@RequestMapping({"/app/"})
public class AppController
{
  private static final String TOKEN = "khjewgciqhbxkjxbakjxqwuohxecgechvk";
  @Autowired
  AdminDao dao;
  @Autowired
  AdminService admin;
  @Autowired
  SmeService smeService;
  @Autowired
  ComponentService componentService;
  @Autowired
  AgentProfileService agentProfileService;
/*  71 */   static final Logger logger = Logger.getLogger(in.wringg.controller.AppController.class);
  
  @ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
/*  76 */     logger.error("Exception in AppController " + ex.getMessage());
/*  77 */     ex.printStackTrace();
/*  78 */     ServerResponse response = new ServerResponse();
/*  79 */     response.setSuccess(false);
/*  80 */     ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
/*  81 */     errorDetails.setErrorString("App Controller Error::" + ex.getMessage());
/*  82 */     JSONSerializer serializer = new JSONSerializer();
/*  83 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
/*  84 */     response.setResult(res);
/*  85 */     res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/*  86 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Login Api for app", response = String.class)
  @RequestMapping(value = {"login"}, method = {RequestMethod.POST})
  public ResponseEntity<String> login(@RequestBody LoginBean bean) {
/*  93 */     JSONSerializer serializer = new JSONSerializer();
/*  94 */     logger.info("Login request for " + bean.getUserName());
/*  95 */     Users user = this.admin.getUserInfo(bean.getUserName());
/*  96 */     if (user != null && user.getPassword().equals(bean.getPassword()) && Constants.BYTE_1.byteValue() == user.getEnabled()) {
/*  97 */       Map<String, String> map = new HashMap<>();
/*  98 */       map.put("token", user.getUserToken());
/*  99 */       String res = serializer.exclude(new String[] { "*.class" }).serialize(map);
/* 100 */       logger.info("Login request for " + bean.getUserName());
/* 101 */       return new ResponseEntity(res, HttpStatus.OK);
    } 
/* 103 */     return new ResponseEntity("Login fail", HttpStatus.UNAUTHORIZED);
  }

  
  @ApiOperation(value = "Returns logined user info ", response = UserRoleDetailBean.class)
  @RequestMapping(value = {"/user/{id}/typedetail"}, method = {RequestMethod.POST})
  public ResponseEntity<String> serviceList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable String id) throws JsonProcessingException {
/* 110 */     logger.info("User type detail for" + id);
/* 111 */     ServerResponse response = this.admin.getUserRoleDetail(id);
/* 112 */     JSONSerializer serializer = new JSONSerializer();
/* 113 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 114 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Get base for agent ", response = CallBaseBean.class)
  @RequestMapping(value = {"/user/{id}/call/base"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callBaseList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) throws JsonProcessingException {
/* 121 */     logger.info("User type detail for" + id);
/* 122 */     ServerResponse response = this.agentProfileService.getBaseForAgent(id);
/* 123 */     JSONSerializer serializer = new JSONSerializer();
/* 124 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 125 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Password reset API", response = String.class)
  @RequestMapping(value = {"/reset/password"}, method = {RequestMethod.POST})
  public ResponseEntity<String> resetPassword(@RequestHeader("Authorization") @NotNull @NotBlank String token, @RequestBody LoginBean login) {
/* 132 */     logger.info("Agent " + login.getUserName() + " request for password reset");
/* 133 */     JSONSerializer serializer = new JSONSerializer();
/* 134 */     ServerResponse response = this.smeService.resetPassword(login);
/* 135 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 136 */     logger.info("Sme id " + login.getUserName() + " request for password reset and got response " + res);
/* 137 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Get latest 250  incoming call", response = CallBaseBean.class)
  @RequestMapping(value = {"/user/{id}/cdr/incoming"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callCDRInList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) throws FileNotFoundException, IOException {
/* 144 */     logger.info("User type detail for" + id);
/* 145 */     ServerResponse response = this.smeService.getCDRForAgent(id.longValue(), (byte)1);
/* 146 */     JSONSerializer serializer = new JSONSerializer();
/* 147 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 148 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Get latest 250 outgoing call", response = CallBaseBean.class)
  @RequestMapping(value = {"/user/{id}/cdr/outgoing"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callCDROutList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) throws FileNotFoundException, IOException {
/* 156 */     logger.info("User type detail for" + id);
/* 157 */     ServerResponse response = this.smeService.getCDRForAgent(id.longValue(), (byte)2);
/* 158 */     JSONSerializer serializer = new JSONSerializer();
/* 159 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 160 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Get latest 250 voicemail call", response = String.class)
  @RequestMapping(value = {"/user/{id}/voicemail/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> voiceMailForAgent(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody ManageClientPageRequest request) throws FileNotFoundException, IOException {
/* 168 */     ServerResponse re = this.smeService.getCDRForAgent(id.longValue(), (byte)3);
/* 169 */     JSONSerializer serializer = new JSONSerializer();
/* 170 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 171 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Get agent insight info", response = AgentInsightPojo.class)
  @RequestMapping(value = {"/user/{id}/insight"}, method = {RequestMethod.POST})
  public ResponseEntity<String> getAgentInsight(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 178 */     logger.info("Get agent insight info " + id);
/* 179 */     callingDetailRequest.setId(id);
/* 180 */     ServerResponse response = this.agentProfileService.getAgentInsighnt(callingDetailRequest);
/* 181 */     JSONSerializer serializer = new JSONSerializer();
/* 182 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 183 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Agent goes for lunch", response = String.class)
  @RequestMapping(value = {"user/{id}/break/in"}, method = {RequestMethod.POST})
  public ResponseEntity<String> goForLunch(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 190 */     ServerResponse response = this.agentProfileService.lunchIn(callingDetailRequest.getSmeId(), id, callingDetailRequest
/* 191 */         .getActualStartDate(), callingDetailRequest.getMessage());
/* 192 */     JSONSerializer serializer = new JSONSerializer();
/* 193 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 194 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Agent back from lunch", response = String.class)
  @RequestMapping(value = {"user/{id}/break/out"}, method = {RequestMethod.POST})
  public ResponseEntity<String> backFromLunch(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 201 */     ServerResponse response = this.agentProfileService.lunchOut(id, callingDetailRequest.getActualEndDate());
/* 202 */     JSONSerializer serializer = new JSONSerializer();
/* 203 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 204 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @RequestMapping(value = {"user/{id}/break/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> breakList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) {
/* 210 */     ServerResponse response = this.agentProfileService.breakList(id);
/* 211 */     JSONSerializer serializer = new JSONSerializer();
/* 212 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 213 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Get Miss Call CDRs", response = CallBaseBean.class)
  @RequestMapping(value = {"/user/{id}/cdr/misscall"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callMissCallCdr(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) throws FileNotFoundException, IOException {
/* 221 */     ServerResponse response = this.smeService.getMissCallCDRForAgent(id.longValue());
/* 222 */     JSONSerializer serializer = new JSONSerializer();
/* 223 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 224 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @RequestMapping(value = {"/user/{id}/recording/{callId}/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> recordingList(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable String callId) {
/* 230 */     ServerResponse re = this.smeService.getRecordingList(callId);
/* 231 */     JSONSerializer serializer = new JSONSerializer();
/* 232 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 233 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Create Addressbook", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/create"}, method = {RequestMethod.POST})
  public ResponseEntity<String> createAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody AddressBookBean bean) {
/* 241 */     bean.setCreatedBy(id);
/* 242 */     ServerResponse re = this.agentProfileService.addAddressBook(bean);
/* 243 */     JSONSerializer serializer = new JSONSerializer();
/* 244 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 245 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Get Addressbook By addressBookId", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/{addressBookId}/get"}, method = {RequestMethod.POST})
  public ResponseEntity<String> getAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable Long addressBookId) {
/* 253 */     ServerResponse re = this.agentProfileService.getAddressBookById(addressBookId);
/* 254 */     JSONSerializer serializer = new JSONSerializer();
/* 255 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 256 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Update Addressbook", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/update"}, method = {RequestMethod.POST})
  public ResponseEntity<String> updateAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody AddressBookBean bean) {
/* 264 */     bean.setCreatedBy(id);
/* 265 */     ServerResponse re = this.agentProfileService.updateAddressBook(bean);
/* 266 */     JSONSerializer serializer = new JSONSerializer();
/* 267 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 268 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Delete Addressbook", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/{addressBookId}/delete"}, method = {RequestMethod.POST})
  public ResponseEntity<String> createAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable Long addressBookId) {
/* 276 */     ServerResponse re = this.agentProfileService.deleteAddressBook(addressBookId);
/* 277 */     JSONSerializer serializer = new JSONSerializer();
/* 278 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 279 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Own Addressbook", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/own"}, method = {RequestMethod.POST})
  public ResponseEntity<String> ownAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) {
/* 286 */     ServerResponse re = this.agentProfileService.getAddressBookForAgent(id);
/* 287 */     JSONSerializer serializer = new JSONSerializer();
/* 288 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 289 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Own Addressbook", response = String.class)
  @RequestMapping(value = {"/user/{id}/address/book/all"}, method = {RequestMethod.POST})
  public ResponseEntity<String> allAddressBook(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id) {
/* 296 */     ServerResponse re = this.agentProfileService.getAddressBookForSme(id);
/* 297 */     JSONSerializer serializer = new JSONSerializer();
/* 298 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 299 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Add Customer Remark", response = String.class)
  @RequestMapping(value = {"/user/{id}/customer/remark/add"}, method = {RequestMethod.POST})
  public ResponseEntity<String> addCustomerRemark(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody SmeCustomerRemarksBean bean) {
/* 307 */     bean.setAgentId(id);
/* 308 */     ServerResponse re = this.agentProfileService.addRemaks(bean);
/* 309 */     JSONSerializer serializer = new JSONSerializer();
/* 310 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 311 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Update Customer Remark", response = String.class)
  @RequestMapping(value = {"/user/{id}/customer/remark/update"}, method = {RequestMethod.POST})
  public ResponseEntity<String> updateCustomerRemark(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody SmeCustomerRemarksBean bean) {
/* 319 */     bean.setAgentId(id);
/* 320 */     ServerResponse re = null;
/* 321 */     if (this.agentProfileService.canAgentUpdateOrDelete(id, bean.getId(), null)) {
/* 322 */       re = this.agentProfileService.updateRemaks(bean);
    } else {
/* 324 */       re = this.agentProfileService.notPermitted();
    } 
/* 326 */     JSONSerializer serializer = new JSONSerializer();
/* 327 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 328 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Delete Customer Remark", response = String.class)
  @RequestMapping(value = {"/user/{id}/customer/remark/{remarkId}/delete"}, method = {RequestMethod.POST})
  public ResponseEntity<String> deleteSingleCustomerRemark(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable Long remarkId) {
/* 336 */     ServerResponse re = null;
/* 337 */     if (this.agentProfileService.canAgentUpdateOrDelete(id, remarkId, null)) {
/* 338 */       re = this.agentProfileService.deleteRemaks(remarkId);
    } else {
/* 340 */       re = this.agentProfileService.notPermitted();
    } 
/* 342 */     JSONSerializer serializer = new JSONSerializer();
/* 343 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 344 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Delete Customers All Remarks", response = String.class)
  @RequestMapping(value = {"/user/{id}/customer/remark/{addressBookId}/delete/all"}, method = {RequestMethod.POST})
  public ResponseEntity<String> deleteAllCustomerRemark(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable Long addressBookId) {
/* 352 */     ServerResponse re = null;
/* 353 */     if (this.agentProfileService.canAgentUpdateOrDelete(id, null, addressBookId)) {
/* 354 */       re = this.agentProfileService.deleteRemaksByAddressBook(addressBookId);
    } else {
/* 356 */       re = this.agentProfileService.notPermitted();
    } 
/* 358 */     JSONSerializer serializer = new JSONSerializer();
/* 359 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 360 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Get all remarks for address book", response = String.class)
  @RequestMapping(value = {"/user/{id}/customer/remark/{addressBookId}/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> listCustomerRemark(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @PathVariable Long addressBookId) {
/* 368 */     ServerResponse re = this.agentProfileService.getRemaksByAddressBook(addressBookId, id);
/* 369 */     JSONSerializer serializer = new JSONSerializer();
/* 370 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 371 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Call Scheduler API for Agent", response = String.class)
  @RequestMapping(value = {"/user/{id}/call/schedule"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callSchedule(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody CallBaseBean baseBean) {
/* 379 */     ServerResponse re = this.agentProfileService.callSchedule(baseBean, id);
/* 380 */     JSONSerializer serializer = new JSONSerializer();
/* 381 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 382 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Call Scheduler API for Agent", response = String.class)
  @RequestMapping(value = {"/user/{id}/call/schedule/update"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callScheduleUpdate(@RequestHeader("Authorization") @NotNull @NotBlank String token, @PathVariable Long id, @RequestBody CallBaseBean baseBean) {
/* 390 */     ServerResponse re = this.agentProfileService.callScheduleUpdate(baseBean, id);
/* 391 */     JSONSerializer serializer = new JSONSerializer();
/* 392 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 393 */     return new ResponseEntity(res, HttpStatus.OK);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\controller\AppController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */