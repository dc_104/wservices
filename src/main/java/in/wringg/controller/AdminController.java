package in.wringg.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import flexjson.JSONSerializer;
import in.wringg.pojo.AdminDashboardRequest;
import in.wringg.pojo.AdminDashboardRevenue;
import in.wringg.pojo.AdminDashboardSystemDetail;
import in.wringg.pojo.AdminMaxAgents;
import in.wringg.pojo.AdminSmeProfileBean;
import in.wringg.pojo.AgentInsightPojo;
import in.wringg.pojo.ChargingResponseBean;
import in.wringg.pojo.LongCodeBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.ServiceListPojo;
import in.wringg.pojo.SmeComplaintBean;
import in.wringg.pojo.SmeProfilePaginationBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UserRoleDetailBean;
import in.wringg.pojo.ZoneBean;
import in.wringg.service.AdminService;
import in.wringg.service.AgentProfileService;
import in.wringg.service.ComponentService;
import in.wringg.service.SmeService;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;











@RestController
@Api(value = "Wringg ", description = "Rest API for admin operations", tags = {"WRINGG ADMIN API"})
public class AdminController
{
  @Autowired
  AdminService admin;
  @Autowired
  SmeService smeService;
  @Autowired
  ComponentService componentService;
  @Autowired
  AgentProfileService agentService;
/*  68 */   static final Logger logger = Logger.getLogger(in.wringg.controller.AdminController.class);
  
  @Autowired
  private TokenStore tokenStore;

  
  @ApiOperation(value = "Logout Api to terminate seesion, token is required in path varibale", response = String.class)
  @RequestMapping(value = {"/logout/token/revoke"}, method = {RequestMethod.POST})
  public ResponseEntity<String> logout(@RequestParam("token") String value) {
/*  77 */     OAuth2AccessToken accessToken = this.tokenStore.readAccessToken(value);
/*  78 */     ServerResponse response = new ServerResponse();
/*  79 */     JSONSerializer serializer = new JSONSerializer();
/*  80 */     if (accessToken == null) {
/*  81 */       response.setResult("Logout Failed");
/*  82 */       String str = serializer.serialize(response);
/*  83 */       return new ResponseEntity(str, HttpStatus.OK);
    } 
/*  85 */     if (accessToken.getRefreshToken() != null) {
/*  86 */       this.tokenStore.removeRefreshToken(accessToken.getRefreshToken());
    }
/*  88 */     this.tokenStore.removeAccessToken(accessToken);
/*  89 */     response.setSuccess(true);
/*  90 */     response.setResult("Logout Successfully");
/*  91 */     String res = serializer.serialize(response);
/*  92 */     return new ResponseEntity(res, HttpStatus.OK);
  }









  
  @ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
/* 107 */     logger.error("Exception in AdminController " + ex.getMessage());
/* 108 */     ex.printStackTrace();
/* 109 */     ServerResponse response = new ServerResponse();
/* 110 */     response.setSuccess(false);
/* 111 */     ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
/* 112 */     errorDetails.setErrorString("Admin Controller Error::" + ex.getMessage());
/* 113 */     JSONSerializer serializer = new JSONSerializer();
/* 114 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
/* 115 */     response.setResult(res);
/* 116 */     res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 117 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  @ApiOperation(value = "Returns logined user info ", response = UserRoleDetailBean.class)
  @RequestMapping(value = {"/user/{id}/typedetail"}, method = {RequestMethod.POST})
  public ResponseEntity<String> serviceList(@PathVariable String id) throws JsonProcessingException {
/* 122 */     logger.info("User type detail with sme id " + id);
/* 123 */     ServerResponse response = this.admin.getUserRoleDetail(id);
/* 124 */     JSONSerializer serializer = new JSONSerializer();
/* 125 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 126 */     logger.info("User type detail with sme id " + id + " got correct response");
/* 127 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Get agents insight info from admin portal", response = AgentInsightPojo.class)
  @RequestMapping(value = {"/admin/insight"}, method = {RequestMethod.POST})
  public ResponseEntity<String> getAgentInsight(@RequestBody StartEndDateRequest callingDetailRequest) {
/* 134 */     logger.info("Get agent insight info " + callingDetailRequest.getId());
/* 135 */     callingDetailRequest.setId(callingDetailRequest.getAgentId());
/* 136 */     ServerResponse response = this.agentService.getAgentInsighnt(callingDetailRequest);
/* 137 */     JSONSerializer serializer = new JSONSerializer();
/* 138 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 139 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  @ApiOperation(value = "Returns service list ", response = ServiceListPojo.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/service/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> serviceList() throws JsonProcessingException {
/* 144 */     logger.info("Admin request for service list");
/* 145 */     ServerResponse response = this.admin.getServiceList();
/* 146 */     JSONSerializer serializer = new JSONSerializer();
/* 147 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 148 */     logger.info("Admin got response for service list");
/* 149 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "SME list as per page limit and fileration ", response = SmeProfilePaginationBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/sme/fetchPage"}, method = {RequestMethod.POST})
  public ResponseEntity<String> fetchPageData(@RequestBody ManageClientPageRequest filter) {
/* 155 */     logger.info("Admin requesut for all sme with filteration");
/* 156 */     ServerResponse response = this.admin.fetchSmePage(filter);
/* 157 */     JSONSerializer serializer = new JSONSerializer();
/* 158 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 159 */     logger.info("Admin request for service list and got Response ");
/* 160 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "SME list without filteration", response = AdminSmeProfileBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/sme/fetch"}, method = {RequestMethod.POST})
  public ResponseEntity<String> fetchData(@RequestBody StartEndDateRequest filter) {
/* 166 */     logger.info("Admin request for all sme list");
/* 167 */     ServerResponse response = this.admin.fetchSme(filter);
/* 168 */     JSONSerializer serializer = new JSONSerializer();
/* 169 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 170 */     logger.info("Admin request for service list and get response");
/* 171 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Sme creation or updation based on action", response = String.class)
  @RequestMapping(value = {"/admin/sme/{action}"}, method = {RequestMethod.POST})
  public ResponseEntity<String> smeProfile(@PathVariable("action") String action, @RequestBody AdminSmeProfileBean profile, UriComponentsBuilder ucBuilder) throws Exception {
/* 177 */     logger.info("Admin request for sme " + action);
/* 178 */     ServerResponse response = new ServerResponse();
/* 179 */     JSONSerializer serializer = new JSONSerializer();
/* 180 */     response = this.admin.smeProcessor(action, profile);
/* 181 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 182 */     logger.info("Admin request for sme " + action + " and got response");
/* 183 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Revenve on admin dashboard ", response = AdminDashboardRevenue.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/dashboard/revenue"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminDashboardRevenue(@RequestBody AdminDashboardRequest revenueDateRange, UriComponentsBuilder ucBuilder) throws Exception {
/* 189 */     logger.info("Admin request for dasboard revenue");
/* 190 */     ServerResponse response = this.admin.adminDashboardRevenue(revenueDateRange);
/* 191 */     response.setSuccess(true);
/* 192 */     JSONSerializer serializer = new JSONSerializer();
/* 193 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 194 */     logger.info("Admin dashboard revenue response " + res);
/* 195 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "System detail on admin dashboard ", response = AdminDashboardSystemDetail.class)
  @RequestMapping(value = {"/admin/dashboard/systemdetail"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminDashboardSystem(@RequestBody AdminDashboardRequest dashboardDate, UriComponentsBuilder ucBuilder) throws Exception {
/* 203 */     logger.info("Admin request for dasboard systemdetail");
/* 204 */     ServerResponse response = this.admin.adminSystemSnapshot(dashboardDate);
/* 205 */     JSONSerializer serializer = new JSONSerializer();
/* 206 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 207 */     return new ResponseEntity(res, HttpStatus.OK);
  }


  
  @ApiOperation(value = "Zone drop-down ", response = ZoneBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/zone"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminGetZone(@RequestBody AdminDashboardRequest dashboardDate, UriComponentsBuilder ucBuilder) throws Exception {
/* 215 */     ServerResponse response = this.admin.getZone();
/* 216 */     JSONSerializer serializer = new JSONSerializer();
/* 217 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 218 */     logger.info("Admin dashboard systemdetail response " + res);
/* 219 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Longcode drop-down list", response = LongCodeBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/longcode"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminGetLongcode(UriComponentsBuilder ucBuilder) throws Exception {
/* 225 */     logger.info("Admin request for longcode list");
/* 226 */     ServerResponse response = this.admin.getLongCode(Constants.LONGCODE_ACTIVE);
/* 227 */     JSONSerializer serializer = new JSONSerializer();
/* 228 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 229 */     logger.info("Admin got longcode list");
/* 230 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Long code list with status", response = ZoneBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/longcode/status"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminGetLongcodeStatus(UriComponentsBuilder ucBuilder) throws Exception {
/* 236 */     logger.info("Admin request for longcode active and inactive ");
/* 237 */     ServerResponse response = this.admin.getLongCode(Constants.BYTE_1);
/* 238 */     JSONSerializer serializer = new JSONSerializer();
/* 239 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 240 */     logger.info("Admin got longcode list qctive and in-active ");
/* 241 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Longcode status update", response = String.class)
  @RequestMapping(value = {"/admin/longcode/{id}/update/{status}"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminGetLongcodeUpdate(@PathVariable Long id, @PathVariable Byte status) throws Exception {
/* 247 */     logger.info("Admin request to update longoce with id " + id + " with stats " + status);
/* 248 */     ServerResponse response = this.admin.updateLongCode(id, status);
/* 249 */     JSONSerializer serializer = new JSONSerializer();
/* 250 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 251 */     logger.info("Admin get response for longcode updation  " + response);
/* 252 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "New longcode insertion ", response = String.class)
  @RequestMapping(value = {"/admin/longcode/add"}, method = {RequestMethod.POST})
  public ResponseEntity<String> adminGetLongcodeAdd(@RequestBody LongCodeBean bean) throws Exception {
/* 258 */     logger.info("Admin request for longcode active and inactive ");
/* 259 */     ServerResponse response = this.admin.saveLongCode(bean);
/* 260 */     JSONSerializer serializer = new JSONSerializer();
/* 261 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 262 */     logger.info("Admin got longcode list qctive and in-active ");
/* 263 */     return new ResponseEntity(res, HttpStatus.OK);
  }









  
  @ApiOperation(value = "Max allowed agent count ", response = AdminMaxAgents.class)
  @RequestMapping(value = {"/admin/agent/maxallowed"}, method = {RequestMethod.POST})
  public ResponseEntity<String> maxAllowedAgent() throws Exception {
/* 278 */     logger.info("Admin request for max allowed aggents");
/* 279 */     ServerResponse response = this.admin.getMaxAgent();
/* 280 */     JSONSerializer serializer = new JSONSerializer();
/* 281 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 282 */     logger.info("Admin response for max allowed aggents " + res);
/* 283 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Complaint updation  ", response = String.class)
  @RequestMapping(value = {"/admin/complaint/update"}, method = {RequestMethod.POST})
  public ResponseEntity<String> complaintUpdate(@RequestBody SmeComplaintBean smeComplaintBean) {
/* 289 */     logger.info("Admin request for sme complaint update");
/* 290 */     ServerResponse response = this.smeService.updateSmeComplaint(smeComplaintBean, Long.valueOf(0L));
/* 291 */     JSONSerializer serializer = new JSONSerializer();
/* 292 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 293 */     logger.info("Admin response for sme complaint update ");
/* 294 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Complaint list on admin portal ", response = SmeComplaintBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/complaint/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> complaintFilter() {
/* 301 */     logger.info("Admin request for sme complaint list");
/* 302 */     ServerResponse re = this.smeService.getSmeComplaints(Long.valueOf(0L));
/* 303 */     JSONSerializer serializer = new JSONSerializer();
/* 304 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 305 */     logger.info("Admin response for sme complaints list");
/* 306 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Sme charging historty on admin portsl", response = ChargingResponseBean.class, responseContainer = "List")
  @RequestMapping(value = {"/admin/charging/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> chargingList(@RequestBody StartEndDateRequest request) {
/* 313 */     logger.info("Admin request for sme chatging list");
/* 314 */     ServerResponse re = this.componentService.chargingDetails(request, null);
/* 315 */     JSONSerializer serializer = new JSONSerializer();
/* 316 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
/* 317 */     logger.info("Admin response for sme charging list");
/* 318 */     return new ResponseEntity(res, HttpStatus.OK);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\controller\AdminController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */