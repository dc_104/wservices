package in.wringg.controller;

import flexjson.JSONSerializer;
import in.wringg.pojo.AgentDetailBean;
import in.wringg.pojo.AgentInsightPojo;
import in.wringg.pojo.AgentOrderBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.service.AgentProfileService;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;







@RestController
@Api(value = "Wringg ", description = "Rest API for SME portal for Agent related operation", tags = {"SME-Agent Opertion"})
public class AgentProifeController
{
  @Autowired
  AgentProfileService agentService;
/*  40 */   static final Logger logger = Logger.getLogger(in.wringg.controller.AgentProifeController.class);
  
  @ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
/*  45 */     logger.error("Exception in AgentProifeController " + ex.getMessage());
/*  46 */     ex.printStackTrace();
/*  47 */     ServerResponse response = new ServerResponse();
/*  48 */     response.setSuccess(false);
/*  49 */     ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
/*  50 */     errorDetails.setErrorString("AgentProfile Controller Error::" + ex.getMessage());
/*  51 */     JSONSerializer serializer = new JSONSerializer();
/*  52 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
/*  53 */     response.setResult(res);
/*  54 */     res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/*  55 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Adding new agent on sme portal ", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/agent/add"}, method = {RequestMethod.POST})
  public ResponseEntity<String> addAgentDetail(@PathVariable Long smeId, @RequestBody AgentDetailBean agentDetailBean) throws IOException {
/*  62 */     logger.info("Sme with id " + smeId + " request to add agents");
    
/*  64 */     agentDetailBean.setSmeId(smeId);
/*  65 */     ServerResponse response = this.agentService.addAgentProfile(agentDetailBean);
/*  66 */     JSONSerializer serializer = new JSONSerializer();
/*  67 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  68 */     logger.info("Sme with id " + smeId + " request to add agent and got response " + res);
/*  69 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Updating existing agent on sme portal ", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/agent/update/{id}"}, method = {RequestMethod.POST})
  public ResponseEntity<String> updateAgentDetail(@PathVariable Long smeId, @PathVariable Long id, @RequestBody AgentDetailBean agentDetailBean) throws IOException {
/*  76 */     logger.info("Sme with id " + smeId + " request to update agents with id " + id);
/*  77 */     ServerResponse response = this.agentService.updateAgentProfile(agentDetailBean, id);
/*  78 */     JSONSerializer serializer = new JSONSerializer();
/*  79 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  80 */     logger.info("Sme with id " + smeId + " request to update  agents with id " + id + "and got response " + res);
/*  81 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Fetcching agent list on sme portal", response = AgentDetailBean.class, responseContainer = "List")
  @RequestMapping(value = {"/sme/{smeId}/agent/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> listAgentDetail(@PathVariable Long smeId) {
/*  87 */     logger.info("Sme with id " + smeId + " request for all agents list");
/*  88 */     ServerResponse response = this.agentService.listAgentProfile(smeId);
/*  89 */     JSONSerializer serializer = new JSONSerializer();
/*  90 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  91 */     logger.info("Sme with id " + smeId + " request for agents list got response ");
/*  92 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Deleting agent  on sme portal", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/agent/delete/{id}"}, method = {RequestMethod.POST})
  public ResponseEntity<String> deleteAgentDetail(@PathVariable Long id, @PathVariable Long smeId) {
/*  98 */     logger.info("Sme with id " + smeId + " request to delete agent with id " + id);
/*  99 */     ServerResponse response = this.agentService.deleteAgentProfile(id);
/* 100 */     JSONSerializer serializer = new JSONSerializer();
/* 101 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 102 */     logger.info("Sme with id " + smeId + " request to delete with id " + id + " agent and got response" + res);
/* 103 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Checking whether sme can create agent on portal", response = Boolean.class)
  @RequestMapping(value = {"/sme/{smeId}/agent/cancreate"}, method = {RequestMethod.POST})
  public ResponseEntity<String> canCreateAgent(@PathVariable Long smeId) {
/* 109 */     logger.info("Sme with id " + smeId + " check if can create more agents");
/* 110 */     ServerResponse response = this.agentService.canCreateAgent(smeId);
/* 111 */     JSONSerializer serializer = new JSONSerializer();
/* 112 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 113 */     logger.info("Sme with id " + smeId + " check if can create more agents and got response " + res);
/* 114 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Maintaining agent order as per selection algorith for IVR calling", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/maintain/order"}, method = {RequestMethod.POST})
  public ResponseEntity<String> maintainOrder(@PathVariable Long smeId, @RequestBody List<AgentOrderBean> order) {
/* 120 */     logger.info("Sme with id " + smeId + " change order of agents");
/* 121 */     ServerResponse response = this.agentService.maintainOrder(smeId, order);
/* 122 */     JSONSerializer serializer = new JSONSerializer();
/* 123 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 124 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Get agent insight info", response = AgentInsightPojo.class)
  @RequestMapping(value = {"/sme/{smeId}/insight"}, method = {RequestMethod.POST})
  public ResponseEntity<String> getAgentInsight(@PathVariable Long smeId, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 131 */     logger.info("Get agent insight info " + callingDetailRequest.getId());
/* 132 */     callingDetailRequest.setId(callingDetailRequest.getAgentId());
/* 133 */     callingDetailRequest.setSmeId(smeId);
/* 134 */     ServerResponse response = this.agentService.getAgentInsighnt(callingDetailRequest);
/* 135 */     JSONSerializer serializer = new JSONSerializer();
/* 136 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/* 137 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Agent goes for lunch", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/break/in"}, method = {RequestMethod.POST})
  public ResponseEntity<String> goForLunch(@PathVariable Long smeId, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 143 */     logger.info("Sme with id " + smeId + " request lunch in for agent with id ");
/* 144 */     ServerResponse response = this.agentService.lunchIn(smeId, callingDetailRequest.getAgentId(), callingDetailRequest.getActualStartDate(), callingDetailRequest.getMessage());
/* 145 */     JSONSerializer serializer = new JSONSerializer();
/* 146 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 147 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Agent back from lunch", response = String.class)
  @RequestMapping(value = {"/sme/{smeId}/break/out"}, method = {RequestMethod.POST})
  public ResponseEntity<String> backFromLunch(@PathVariable Long smeId, @RequestBody StartEndDateRequest callingDetailRequest) {
/* 153 */     logger.info("Sme with id " + smeId + " request request lunch iout for  agent");
/* 154 */     ServerResponse response = this.agentService.lunchOut(callingDetailRequest.getAgentId(), callingDetailRequest.getActualEndDate());
/* 155 */     JSONSerializer serializer = new JSONSerializer();
/* 156 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 157 */     return new ResponseEntity(res, HttpStatus.OK);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\controller\AgentProifeController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */