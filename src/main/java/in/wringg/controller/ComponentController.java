package in.wringg.controller;

import flexjson.JSONSerializer;
import in.wringg.pojo.CallCdrBean;
import in.wringg.pojo.IdValueBean;
import in.wringg.pojo.PassswordTokenBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.service.ComponentService;
import in.wringg.service.SmeService;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;





@Api(value = "Wringg ", description = "Rest API for IVR and forget password ", tags = {"Wringg API WITHOUT AUTHORIZATION "})
@RestController
public class ComponentController
{
  @Autowired
  ComponentService componentService;
  @Autowired
  SmeService smeService;
/*  43 */   static final Logger logger = Logger.getLogger(in.wringg.controller.ComponentController.class);
  
  @ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
  @ExceptionHandler({Exception.class})
  public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
/*  48 */     logger.error("Exception in ComponentController " + ex.getMessage());
/*  49 */     ex.printStackTrace();
/*  50 */     ServerResponse response = new ServerResponse();
/*  51 */     response.setSuccess(false);
/*  52 */     ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
/*  53 */     errorDetails.setErrorString("Component Controller Error::" + ex.getMessage());
/*  54 */     JSONSerializer serializer = new JSONSerializer();
/*  55 */     String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
/*  56 */     response.setResult(res);
/*  57 */     res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
/*  58 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "To post cdr in data base sent via IVR", response = String.class)
  @RequestMapping(value = {"/component/ivrincomg/cdr/add"}, method = {RequestMethod.POST})
  public ResponseEntity<String> addCDR(@RequestBody CallCdrBean callCdrBean) {
/*  64 */     logger.info("CDR pushed via IVR with data " + callCdrBean.toString());
/*  65 */     JSONSerializer serializer = new JSONSerializer();
/*  66 */     ServerResponse response = this.componentService.addCallCdr(callCdrBean);
/*  67 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  68 */     logger.info("CDR pushed via IVR with data " + callCdrBean.toString() + " got response " + res);
/*  69 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Language list ", response = IdValueBean.class, responseContainer = "List")
  @RequestMapping(value = {"/component/language/list"}, method = {RequestMethod.POST})
  public ResponseEntity<String> getAllLanguages() {
/*  75 */     logger.info("Language list request");
/*  76 */     JSONSerializer serializer = new JSONSerializer();
/*  77 */     List<IdValueBean> langList = new ArrayList<>();
/*  78 */     langList.add(new IdValueBean(1, "English"));
/*  79 */     langList.add(new IdValueBean(2, "Bangla"));
/*  80 */     ServerResponse response = new ServerResponse();
/*  81 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(langList);
/*  82 */     response.setSuccess(true);
/*  83 */     response.setResult(res);
/*  84 */     res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  85 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Change voice mail schedule status ", response = String.class)
  @RequestMapping(value = {"/component/{id}/change/status"}, method = {RequestMethod.POST})
  public ResponseEntity<String> callStatuschnge(@PathVariable Long id) {
/*  91 */     logger.info("Resquest for changing the status of cdr having id " + id);
/*  92 */     JSONSerializer serializer = new JSONSerializer();
/*  93 */     ServerResponse response = this.componentService.changeSchduleStatus(id);
/*  94 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/*  95 */     logger.info("Resquest for changing the status of cdr having id " + id + " got response " + res);
/*  96 */     return new ResponseEntity(res, HttpStatus.OK);
  }
  
  @ApiOperation(value = "Generating token for forget password ", response = String.class)
  @RequestMapping(value = {"/component/{id}/gentoken/password"}, method = {RequestMethod.POST})
  public ResponseEntity<String> gentPasswordToken(@PathVariable String id) throws ParseException, IOException {
/* 102 */     logger.info("Sme id " + id + "forget password ask for email link to change password");
/* 103 */     JSONSerializer serializer = new JSONSerializer();
/* 104 */     ServerResponse response = this.smeService.genPaswordToken(id);
/* 105 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 106 */     logger.info("Sme id " + id + "forget password ask for email link to change password and got response " + res);
/* 107 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Idenditifying  token for forget password ", response = String.class)
  @RequestMapping(value = {"/component/{id}/forget/password/{token}"}, method = {RequestMethod.POST})
  public ResponseEntity<String> forgetPassword(@PathVariable String id, @PathVariable String token) throws ParseException {
/* 114 */     logger.info("Sme id " + id + "forget password send token " + token + " to change password ");
/* 115 */     JSONSerializer serializer = new JSONSerializer();
/* 116 */     ServerResponse response = this.smeService.validateToken(id, token);
/* 117 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 118 */     logger.info("Sme id " + id + "forget password send token " + token + " to change password and got response" + res);
    
/* 120 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Allowing new password after toekn verification", response = String.class)
  @RequestMapping(value = {"/component/{id}/new/password"}, method = {RequestMethod.POST})
  public ResponseEntity<String> forgetPasswordChange(@PathVariable String id, @RequestBody PassswordTokenBean bean) throws ParseException {
/* 127 */     logger.info("Sme id " + id + "forget password finaly resquest to change password after token verifivcation");
/* 128 */     JSONSerializer serializer = new JSONSerializer();
/* 129 */     ServerResponse response = this.smeService.newPasword(id, bean);
/* 130 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 131 */     logger.info("Sme id " + id + "forget password finaly resquest to change password after token verifivcation got response " + res);
    
/* 133 */     return new ResponseEntity(res, HttpStatus.OK);
  }

  
  @ApiOperation(value = "Generating token for forget password for agent ", response = String.class)
  @RequestMapping(value = {"/component/gentoken/password"}, method = {RequestMethod.POST})
  public ResponseEntity<String> gentPasswordTokenViaMail(@RequestParam(value = "userName", required = true) String userName) throws ParseException, IOException {
/* 140 */     logger.info("Sme id " + userName + "forget password ask for email link to change password");
/* 141 */     JSONSerializer serializer = new JSONSerializer();
/* 142 */     ServerResponse response = this.smeService.genPaswordToken(userName);
/* 143 */     String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
/* 144 */     logger.info("Sme id " + userName + "forget password ask for email link to change password and got response " + res);
    
/* 146 */     return new ResponseEntity(res, HttpStatus.OK);
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\controller\ComponentController.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */