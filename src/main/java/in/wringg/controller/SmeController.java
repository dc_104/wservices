package in.wringg.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import flexjson.JSONSerializer;
import in.wringg.pojo.AgentDetailSummaryDashboard;
import in.wringg.pojo.AgentGroupDetailBean;
import in.wringg.pojo.AgentReportFilterPagination;
import in.wringg.pojo.AlertNotificationBean;
import in.wringg.pojo.CallCdrResponseBean;
import in.wringg.pojo.CdrDropDownFilter;
import in.wringg.pojo.CdrPaginationFilerationList;
import in.wringg.pojo.ChargingResponseBean;
import in.wringg.pojo.FileUpload;
import in.wringg.pojo.IvrCallFlowBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.PromptDetailsBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SettingWapperBean;
import in.wringg.pojo.SmeAgentDetailBean;
import in.wringg.pojo.SmeComplaintBean;
import in.wringg.pojo.SmeDashboardSystemDetail;
import in.wringg.pojo.SmeSettingBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UploadedBaseDesc;
import in.wringg.service.ComponentService;
import in.wringg.service.SmeService;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Wringg", description = "Rest API for SME portal operation", tags = { "SME PORTAL" })
public class SmeController {
	@Autowired
	SmeService smeService;
	@Autowired
	ComponentService componentService;

	static final Logger logger = Logger.getLogger(in.wringg.controller.SmeController.class);

	@ApiOperation(value = "Parent exception handler ", response = ErrorDetails.class)
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> handleError(HttpServletRequest req, Exception ex) {
		/* 95 */ logger.error("Exception in SmeController " + ex.getMessage());
		/* 96 */ ex.printStackTrace();
		/* 97 */ ServerResponse response = new ServerResponse();
		/* 98 */ response.setSuccess(false);
		/* 99 */ ErrorDetails errorDetails = new ErrorDetails(ErrorConstants.ERROR_GENERAL);
		/* 100 */ errorDetails.setErrorString("Sme Controller Error::" + ex.getMessage());
		/* 101 */ JSONSerializer serializer = new JSONSerializer();
		/* 102 */ String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(errorDetails);
		/* 103 */ response.setResult(res);
		/* 104 */ res = serializer.exclude(new String[] { "*.class" }).deepSerialize(response);
		/* 105 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Password reset API", response = String.class)
	@RequestMapping(value = { "/sme/{id}/reset/password" }, method = { RequestMethod.POST })
	public ResponseEntity<String> resetPassword(@RequestBody LoginBean login) {
		/* 111 */ logger.info("Sme id " + login.getUserName() + " request for password reset");
		/* 112 */ JSONSerializer serializer = new JSONSerializer();
		/* 113 */ ServerResponse response = this.smeService.resetPassword(login);
		/* 114 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 115 */ logger.info("Sme id " + login.getUserName() + " request for password reset and got response " + res);
		/* 116 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Base file uploaded history", response = String.class)
	@RequestMapping(value = { "/sme/{id}/base/upload/histroy" }, method = { RequestMethod.POST })
	public ResponseEntity<String> getBaseUploadedHistory(@PathVariable long id) {
		/* 122 */ JSONSerializer serializer = new JSONSerializer();
		/* 123 */ ServerResponse response = this.smeService.getBaseUploadHistory(id);
		/* 124 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 125 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "For uploading base list ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/upload/base/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> uploadToTempExcel(
			@RequestParam(value = "countryCode", required = true) String countryCode,
			@RequestParam(value = "agentId", required = true) String agentId, @PathVariable long id,
			@RequestBody List<UploadedBaseDesc> baseList) {
		/* 137 */ JSONSerializer serializer = new JSONSerializer();

		/* 139 */ String res = serializer.exclude(new String[] { "*.class" })
				.serialize(this.smeService.saveBaseList(baseList, countryCode, agentId, id));
		/* 140 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "For uploading files only ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/upload/base" }, method = { RequestMethod.POST }, consumes = {
			"multipart/form-data" })
	public ResponseEntity<String> uploadToTempExcel(
			@RequestParam(value = "countryCode", required = true) String countryCode,
			@RequestParam(value = "agentId", required = true) String agentId, @PathVariable long id,
			@RequestPart("file") @NotNull @NotEmpty MultipartFile file)
			throws JsonParseException, JsonMappingException, IOException {
		/* 152 */ logger.info("File upload request with file name " + file.getOriginalFilename());

		/* 154 */ InputStream inputStream = null;
		/* 155 */ OutputStream outputStream = null;
		/* 156 */ JSONSerializer serializer = new JSONSerializer();
		/* 157 */ inputStream = file.getInputStream();
		/* 158 */ String dir = Constants.UPLOAD_BASE_PATH;
		/* 159 */ File newFile = new File(dir + System.currentTimeMillis() + "_" + file.getOriginalFilename());
		/* 160 */ if (!newFile.exists()) {
			/* 161 */ newFile.createNewFile();
		}
		/* 163 */ outputStream = new FileOutputStream(newFile);
		/* 164 */ int read = 0;
		/* 165 */ byte[] bytes = new byte[1024];
		/* 166 */ while ((read = inputStream.read(bytes)) != -1) {
			/* 167 */ outputStream.write(bytes, 0, read);
		}
		/* 169 */ FileInputStream fis = new FileInputStream(newFile);
		/* 170 */ XSSFWorkbook workbook = new XSSFWorkbook(fis);
		/* 171 */ XSSFSheet sheet = workbook.getSheetAt(0);

		/* 173 */ int count = sheet.getPhysicalNumberOfRows();
		/* 174 */ if (count > 10000) {
			/* 175 */ ServerResponse response = new ServerResponse();
			/* 176 */ ErrorDetails error = new ErrorDetails();
			/* 177 */ error.setErrorCode(ErrorConstants.ERROR_MAX_BASE.getErrorCode());
			/* 178 */ error.setErrorString("Base upload ::" + ErrorConstants.ERROR_MAX_BASE.getErrorMessage());
			/* 179 */ error.setDisplayMessage(ErrorConstants.ERROR_MAX_BASE.getDisplayMessage());
			/* 180 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(error));
			/* 181 */ return new ResponseEntity(serializer.exclude(new String[] { "*.class" }).serialize(response),
					HttpStatus.OK);
		}

		/* 184 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(
				this.smeService.saveBaseFileData((Sheet) sheet, countryCode, agentId, id, file.getOriginalFilename()));
		/* 185 */ logger
				.info("File upload request with file name " + file.getOriginalFilename() + " got  response  " + res);
		/* 186 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "For uploading files only ", response = String.class)
	@RequestMapping(value = { "/tempupload" }, method = { RequestMethod.POST }, consumes = { "multipart/form-data" })
	public ResponseEntity<String> uploadToTempDir(@RequestPart("file") @NotNull @NotEmpty MultipartFile file)
			throws JsonParseException, JsonMappingException, IOException {
		/* 194 */ logger.info("File upload request with file name " + file.getOriginalFilename());
		/* 195 */ ServerResponse response = new ServerResponse();
		/* 196 */ InputStream inputStream = null;
		/* 197 */ OutputStream outputStream = null;
		/* 198 */ JSONSerializer serializer = new JSONSerializer();
		/* 199 */ FileUpload fileUpload = new FileUpload();
		/* 200 */ inputStream = file.getInputStream();
		/* 201 */ String dir = Constants.UPLOAD_BASE_PATH;
		/* 202 */ File newFile = new File(dir + file.getOriginalFilename());
		/* 203 */ if (!newFile.exists()) {
			/* 204 */ newFile.createNewFile();
		}
		outputStream = new FileOutputStream(newFile);
		int read = 0;
		byte[] bytes = new byte[1024];
		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		fileUpload.setFileName(file.getOriginalFilename());
		fileUpload.setFilePath(newFile.getAbsolutePath());
		response.setSuccess(true);
		response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(fileUpload));
		String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		logger.info("File upload request with file name " + file.getOriginalFilename() + " got  response  " + res);
		return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "For uploading SME Prompt", response = String.class)
	@RequestMapping(value = { "/sme/{id}/upload/prompt" }, method = { RequestMethod.POST })
	public ResponseEntity<String> smeUploadPrompts(@PathVariable Long id, @RequestParam Long promptId,
			@RequestParam String promptName, @RequestParam String promptDescription,
			@RequestParam String promptCategory, @RequestPart("file") @NotNull @NotEmpty MultipartFile file)
			throws Exception {
		logger.info("sme id:" + id + "file path" + file.getOriginalFilename() + " value of promptId param is ::"
				+ promptId + "promptName:" + promptName);

		String promptPath = "";
		String pathSeparator = FileSystems.getDefault().getSeparator();
		try (InputStream inputStream = file.getInputStream()) {
			String dir = Constants.MEDIA_BASE_PATH + pathSeparator + id + pathSeparator;
			File newFile = new File(dir + file.getOriginalFilename());
			
			if (!newFile.exists()) {
				File parentPath = new File(newFile.getParent());
				if(!parentPath.exists()) {
					parentPath.mkdirs();
				}
				newFile.createNewFile();
			}
			promptPath = newFile.getAbsolutePath();
			try (OutputStream outputStream = new FileOutputStream(newFile)) {
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
			}
		}
		PromptDetailsBean promptDetails = new PromptDetailsBean();
		promptDetails.setPromptCategory(promptCategory);
		promptDetails.setPromptDescription(promptDescription);
		promptDetails.setPromptId(promptId);
		promptDetails.setPromptName(promptName);
		promptDetails.setSmeId(id);
		promptDetails.setPromptPath(promptPath);
		promptDetails.setPromptUrl(Constants.MEDIA_BASE_URL+"/"+ id +"/"+file.getOriginalFilename());
		ServerResponse response = smeService.savePrompt(promptDetails);
		JSONSerializer serializer = new JSONSerializer();
		String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		logger.info("Sme id " + id + "request for saving prompt got resoponse " + res);
		return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "For fetching SME Prompt", response = String.class)
	@RequestMapping(value = { "/sme/{id}/prompt" }, method = { RequestMethod.GET })

	public ResponseEntity<String> smeUploadPrompts(@PathVariable Long id, @RequestParam Long promptId,
			@RequestParam(required = false, defaultValue = "1") int pageNum,
			@RequestParam(required = false, defaultValue = "20") int pageSize) throws Exception {
		logger.info("sme id:" + id + " value of promptId param is ::" + promptId);
		ServerResponse response = smeService.getSmePrompt(id, promptId, pageNum, pageSize);
		JSONSerializer serializer = new JSONSerializer();
		String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		logger.info("Sme id " + id + "request for fetching prompt got resoponse " + res);
		return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "System Detail graph for SME portal ", response = SmeDashboardSystemDetail.class)
	@RequestMapping(value = { "/sme/{id}/dashboard/systemdetail" }, method = { RequestMethod.POST })
	public ResponseEntity<String> smeDashboardSystem(@PathVariable Long id,
			@RequestBody StartEndDateRequest dashboardDate, UriComponentsBuilder ucBuilder) throws Exception {
		/* 225 */ logger.info("Sme id " + id + "request for system dashboard ");
		/* 226 */ ServerResponse response = this.smeService.smeDashboardDeatial(dashboardDate, id.longValue());
		/* 227 */ JSONSerializer serializer = new JSONSerializer();
		/* 228 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 229 */ logger.info("Sme id " + id + "request for system dashboard got resoponse " + res);
		/* 230 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Agent Detail graph for SME portal ", response = SmeAgentDetailBean.class)
	@RequestMapping(value = { "/sme/{id}/dashboard/agentdetail" }, method = { RequestMethod.POST })
	public ResponseEntity<String> smeDashboardSystemForAgent(@PathVariable Long id) throws Exception {
		/* 236 */ logger.info("Sme id " + id + "request for agent report on dashboard ");
		/* 237 */ ServerResponse response = this.smeService.smeDashboardDeatialForAgent(id.longValue());
		/* 238 */ JSONSerializer serializer = new JSONSerializer();
		/* 239 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 240 */ logger.info("Sme id " + id + "request for agent dashboard got resoponse " + res);
		/* 241 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Agent status summary for SME portal on the basis of status", response = AgentDetailSummaryDashboard.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{id}/dashboard/agentdetail/{status}/summary" }, method = { RequestMethod.POST })
	public ResponseEntity<String> smeDashboardAgentSummaryViaStatus(@PathVariable Long id, @PathVariable Byte status)
			throws Exception {
		/* 248 */ logger.info("Sme id " + id + "request for agent report summary  on dashboard ");
		/* 249 */ ServerResponse response = this.smeService.agentDashboardSummary(id, status);
		/* 250 */ JSONSerializer serializer = new JSONSerializer();
		/* 251 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 252 */ logger.info("Sme id " + id + "request for agent summary dashboard got resoponse " + res);
		/* 253 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Agent status summary for SME portal all type of status", response = AgentDetailSummaryDashboard.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{id}/dashboard/agentdetail/summary" }, method = { RequestMethod.POST })
	public ResponseEntity<String> smeDashboardAgentSummary(@PathVariable Long id) throws Exception {
		/* 260 */ logger.info("Sme id " + id + "request for agent report summary  on dashboard ");
		/* 261 */ ServerResponse response = this.smeService.agentDashboardSummary(id);
		/* 262 */ JSONSerializer serializer = new JSONSerializer();
		/* 263 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 264 */ logger.info("Sme id " + id + "request for agent summary dashboard got resoponse " + res);
		/* 265 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Outgoing call cdr list ", response = CdrPaginationFilerationList.class)
	@RequestMapping(value = { "/sme/{id}/cdr/outgoing/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> ivrManger(@PathVariable Long id, @RequestBody ManageClientPageRequest request,
			UriComponentsBuilder ucBuilder) throws Exception {
		/* 272 */ logger.info("Sme id " + id + "request for outgoing list");
		/* 273 */ ServerResponse response = this.smeService.getCDRList(request, id.longValue(), Byte.valueOf((byte) 2));
		/* 274 */ JSONSerializer serializer = new JSONSerializer();
		/* 275 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 276 */ logger.info("Sme id " + id + "request for outgoing list got response");
		/* 277 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Incoming call cdr list ", response = CdrPaginationFilerationList.class)
	@RequestMapping(value = { "/sme/{id}/cdr/incoming/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> incomingCDRList(@PathVariable Long id, @RequestBody ManageClientPageRequest request,
			UriComponentsBuilder ucBuilder) throws Exception {
		/* 284 */ logger.info("Sme id " + id + "request for incoming list");
		/* 285 */ ServerResponse response = this.smeService.getCDRList(request, id.longValue(), Byte.valueOf((byte) 1));
		/* 286 */ JSONSerializer serializer = new JSONSerializer();
		/* 287 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 288 */ logger.info("Sme id " + id + "request for incoming list got response");
		/* 289 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Voicemail call cdr list ", response = CdrPaginationFilerationList.class)
	@RequestMapping(value = { "/sme/{id}/cdr/voicemail/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> voicemailCDRList(@PathVariable Long id, @RequestBody ManageClientPageRequest request,
			UriComponentsBuilder ucBuilder) throws Exception {
		/* 296 */ logger.info("Sme id " + id + "request for voicemail list");
		/* 297 */ ServerResponse response = this.smeService.getCDRList(request, id.longValue(), Byte.valueOf((byte) 3));
		/* 298 */ JSONSerializer serializer = new JSONSerializer();
		/* 299 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 300 */ logger.info("Sme id " + id + "request for recording list got respose");
		/* 301 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "To fetch CDR recording list ", response = CallCdrResponseBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{id}/cdr/recording/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> recordingCDRList(@PathVariable Long id, @RequestBody ManageClientPageRequest request,
			UriComponentsBuilder ucBuilder) throws Exception {
		/* 308 */ logger.info("Sme id " + id + "request for cdr recording list");
		/* 309 */ ServerResponse response = this.smeService.getCDRList(request, id.longValue(), Byte.valueOf((byte) 4));
		/* 310 */ JSONSerializer serializer = new JSONSerializer();
		/* 311 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 312 */ logger.info("Sme id " + id + "request for cdr recording list got response");
		/* 313 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "To fetch agent group detail list ", response = AgentGroupDetailBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{smeId}/groupdetail" }, method = { RequestMethod.POST })
	public ResponseEntity<String> groupdetail(@PathVariable Long smeId, UriComponentsBuilder ucBuilder)
			throws Exception {
		/* 320 */ logger.info("Sme id " + smeId + "request for groupdetail");
		/* 321 */ JSONSerializer serializer = new JSONSerializer();
		/* 322 */ ServerResponse response = this.smeService.getAgentGroupDetail(smeId);
		/* 323 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 324 */ logger.info("Sme id " + smeId + "request for groupdetail got response");
		/* 325 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "To create IVR flow ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/createivrflow" }, method = { RequestMethod.POST })
	public ResponseEntity<String> createIvr(@PathVariable String id, @RequestBody List<IvrCallFlowBean> ivrCallFlowBean,
			UriComponentsBuilder ucBuilder) throws Exception {
		/* 332 */ logger.info("Sme id " + id + "request to create ivr flow with data " + ivrCallFlowBean.toString());
		/* 333 */ JSONSerializer serializer = new JSONSerializer();
		/* 334 */ ServerResponse response = this.smeService.addIvrmanager(ivrCallFlowBean, id);
		/* 335 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 336 */ logger.info("Sme id " + id + "request to create ivr flow with data " + ivrCallFlowBean.toString()
				+ " got response");

		/* 338 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Getting ivr flow for SME poertal ", response = IvrCallFlowBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{id}/getivrflow" }, method = { RequestMethod.POST })
	public ResponseEntity<String> getIvrFlow(@PathVariable String id, UriComponentsBuilder ucBuilder) throws Exception {
		/* 344 */ logger.info("Sme id " + id + "request for ivr flow");
		/* 345 */ ServerResponse response = this.smeService.getIvrmanager(id);
		/* 346 */ JSONSerializer serializer = new JSONSerializer();
		/* 347 */ response.setSuccess(true);
		/* 348 */ if (!this.smeService.isIvrManagerExist(id)) {
			/* 349 */ response.setResult("Not Exist");
			/* 350 */ String str = serializer.exclude(new String[] { "*.class" }).serialize(response);
			/* 351 */ return new ResponseEntity(str, HttpStatus.OK);
		}
		/* 353 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 354 */ logger.info("Sme id " + id + "request for ivr flow got response");
		/* 355 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Updating existing ivr flow for SME poertal ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/updateivrflow" }, method = { RequestMethod.POST })
	public ResponseEntity<String> updateIvrFlow(@PathVariable String id, List<IvrCallFlowBean> ivrCallFlowBean,
			UriComponentsBuilder ucBuilder) throws IOException {
		/* 362 */ logger.info("Sme id " + id + "request to update ivr flow with data " + ivrCallFlowBean.toString());
		/* 363 */ JSONSerializer serializer = new JSONSerializer();
		/* 364 */ ServerResponse response = new ServerResponse();
		/* 365 */ response.setSuccess(true);
		/* 366 */ if (!this.smeService.isIvrManagerExist(id)) {
			/* 367 */ response.setResult("Not Exist");
			/* 368 */ String str = serializer.exclude(new String[] { "*.class" }).serialize(response);
			/* 369 */ return new ResponseEntity(str, HttpStatus.OK);
		}
		/* 371 */ this.smeService.addIvrmanager(ivrCallFlowBean, id);
		/* 372 */ response.setResult("Updated");
		/* 373 */ response.setSuccess(true);
		/* 374 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 375 */ logger.info("Sme id " + id + "request to update ivr flow with data " + ivrCallFlowBean.toString()
				+ " got response");

		/* 377 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Adding complaint on  SME poertal ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/complaint/add" }, method = { RequestMethod.POST })
	public ResponseEntity<String> complaintAdd(@PathVariable Long id, @RequestBody SmeComplaintBean smeComplaintBean) {
		/* 383 */ ServerResponse re = this.smeService.addSmeComplaint(smeComplaintBean, id);
		/* 384 */ JSONSerializer serializer = new JSONSerializer();
		/* 385 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 386 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Updating complaint on  SME poertal ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/complaint/update" }, method = { RequestMethod.POST })
	public ResponseEntity<String> complaintUpdate(@PathVariable Long id,
			@RequestBody SmeComplaintBean smeComplaintBean) {
		/* 393 */ logger.info("Sme id " + id + "request to update complaint with data " + smeComplaintBean.toString());
		/* 394 */ ServerResponse re = this.smeService.updateSmeComplaint(smeComplaintBean, id);
		/* 395 */ JSONSerializer serializer = new JSONSerializer();
		/* 396 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 397 */ logger.info("Sme id " + id + "request to update complaint with data " + smeComplaintBean.toString()
				+ "gor response " + res);

		/* 399 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching complaing list ", response = SmeComplaintBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{id}/complaint/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> complaintFilter(@PathVariable Long id) {
		/* 405 */ logger.info("Sme id " + id + "request to fetch complaint list in method complaintFilter");
		/* 406 */ ServerResponse re = this.smeService.getSmeComplaints(id);
		/* 407 */ JSONSerializer serializer = new JSONSerializer();
		/* 408 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 409 */ logger
				.info("Sme id " + id + "request to fetch complaint list in method complaintFilter got response");
		/* 410 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Deleting a particular complaint ", response = String.class)
	@RequestMapping(value = { "/sme/{id}/complaint/delete/{complaintId}" }, method = { RequestMethod.POST })
	public ResponseEntity<String> complaintDelete(@PathVariable Long id, @PathVariable Long complaintId) {
		/* 416 */ logger.info("Sme id " + id + "request to delete the complain with id " + complaintId);
		/* 417 */ ServerResponse re = this.smeService.deleteSmeComplaint(id, complaintId);
		/* 418 */ JSONSerializer serializer = new JSONSerializer();
		/* 419 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 420 */ logger
				.info("Sme id " + id + "request to delete the complain with id " + complaintId + " got response");
		/* 421 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching sme balance ", response = String.class)
	@RequestMapping(value = { "/sme/{smeId}/fetch/balance" }, method = { RequestMethod.POST })
	public ResponseEntity<String> fetchSmeBalance(@PathVariable Long smeId) throws JSONException {
		/* 427 */ logger.info("Sme id " + smeId + "request for sme balance ");
		/* 428 */ JSONSerializer serializer = new JSONSerializer();
		/* 429 */ ServerResponse response = this.smeService.fetchSmeBalance(smeId);
		/* 430 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 431 */ logger.info("Sme id " + smeId + "request for balance got response " + res);
		/* 432 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching Sme info for setting page ", response = SmeSettingBean.class)
	@RequestMapping(value = { "/sme/{smeId}/fetchSetting" }, method = { RequestMethod.POST })
	public ResponseEntity<String> fetchSetting(@PathVariable Long smeId) throws JSONException {
		/* 438 */ logger.info("Sme id " + smeId + "request fetch update setting");
		/* 439 */ JSONSerializer serializer = new JSONSerializer();
		/* 440 */ ServerResponse response = this.smeService.fetchSetting(smeId);
		/* 441 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 442 */ logger.info("Sme id " + smeId + "request fetch update setting got response");
		/* 443 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Updating Sme info for setting page ", response = String.class)
	@RequestMapping(value = { "/sme/{smeId}/updateSetting" }, method = { RequestMethod.POST })
	public ResponseEntity<String> updateSetting(@PathVariable Long smeId, @RequestBody SettingWapperBean settings) {
		/* 449 */ logger.info("Sme id " + smeId + "request for update setting with data " + settings.toString());
		/* 450 */ JSONSerializer serializer = new JSONSerializer();
		/* 451 */ ServerResponse response = this.smeService.updateSetting(smeId, settings);
		/* 452 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 453 */ logger.info("Sme id " + smeId + "request for update setting got response");
		/* 454 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Resfesh user token for CRM", response = String.class)
	@RequestMapping(value = { "/sme/{smeId}/refreshToken" }, method = { RequestMethod.POST })
	public ResponseEntity<String> refreshToekn(@NotNull @PathVariable Long smeId) {
		/* 460 */ logger.info("Sme id " + smeId + " request for refreshing user token");
		/* 461 */ JSONSerializer serializer = new JSONSerializer();
		/* 462 */ ServerResponse response = this.smeService.refreshUserToken(smeId);
		/* 463 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 464 */ logger.info("Sme id " + smeId + "request for token refresh got response");
		/* 465 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching Agent repoer on Sme portal ", response = AgentReportFilterPagination.class)
	@RequestMapping(value = { "/sme/{smeId}/agent/reportdetail" }, method = { RequestMethod.POST })
	public ResponseEntity<String> fetchAgentReport(@PathVariable Long smeId,
			@RequestBody ManageClientPageRequest request) {
		/* 472 */ logger.info("Sme id " + smeId + "request for agent report detail");
		/* 473 */ JSONSerializer serializer = new JSONSerializer();
		/* 474 */ ServerResponse response = this.smeService.fetchAgentReportDetail(smeId, request);
		/* 475 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 476 */ logger.info("Sme id " + smeId + "request for agent report detail got correct response");
		/* 477 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Drop down list on cdr pages for filteration", response = CdrDropDownFilter.class)
	@RequestMapping(value = { "/sme/{smeId}/cdr/dropdown" }, method = { RequestMethod.POST })
	public ResponseEntity<String> fetchCdrDropDown(@PathVariable String smeId)
			throws FileNotFoundException, IOException {
		/* 484 */ logger.info("Sme id " + smeId + "request for dropdown list");
		/* 485 */ JSONSerializer serializer = new JSONSerializer();
		/* 486 */ ServerResponse response = this.smeService.fetchCdrDropDown();
		/* 487 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(response);
		/* 488 */ logger.info("Sme id " + smeId + "request for drop down list got correct response");
		/* 489 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Charging history list - both success  and fail ", response = ChargingResponseBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{smeId}/charging/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> chargingList(@PathVariable Long smeId, @RequestBody StartEndDateRequest request) {
		/* 495 */ logger.info("Sme " + smeId + " request for sme chatging list");
		/* 496 */ ServerResponse re = this.componentService.chargingDetails(request, smeId);
		/* 497 */ JSONSerializer serializer = new JSONSerializer();
		/* 498 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 499 */ logger.info("Sme " + smeId + " response for sme charging list");
		/* 500 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "New notification list on SME portal ", response = AlertNotificationBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{userId}/notification/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> notificationList(@PathVariable String userId) {
		/* 506 */ logger.info("User " + userId + " request for sme notification list");
		/* 507 */ ServerResponse re = this.smeService.getAlertNotications(userId, 1);
		/* 508 */ JSONSerializer serializer = new JSONSerializer();
		/* 509 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 510 */ logger.info("User " + userId + " response for notification list");
		/* 511 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Unseen notification list on SME portal ", response = AlertNotificationBean.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{userId}/notification/unseen" }, method = { RequestMethod.POST })
	public ResponseEntity<String> notificationUnseen(@PathVariable String userId) {
		/* 517 */ logger.info("User " + userId + " request for sme notification list");
		/* 518 */ ServerResponse re = this.smeService.getAlertNotications(userId, -1);
		/* 519 */ JSONSerializer serializer = new JSONSerializer();
		/* 520 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 521 */ logger.info("User " + userId + " response for notification list");
		/* 522 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Setting  notification status to seen ", response = String.class)
	@RequestMapping(value = { "/sme/{userId}/notification/{id}/seen" }, method = { RequestMethod.POST })
	public ResponseEntity<String> notificationSeen(@PathVariable String userId, @PathVariable int id) {
		/* 528 */ logger.info("User " + userId + " seen the notification with id " + id);
		/* 529 */ ServerResponse re = this.smeService.updateAlertNotification(id);
		/* 530 */ JSONSerializer serializer = new JSONSerializer();
		/* 531 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 532 */ logger.info("User " + userId + " got response " + res);
		/* 533 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Deleting notification  ", response = String.class)
	@RequestMapping(value = { "/sme/{userId}/notification/{id}/delete" }, method = { RequestMethod.POST })
	public ResponseEntity<String> notificationList(@PathVariable String userId, @PathVariable int id) {
		/* 539 */ logger.info("User " + userId + " delete the notification with id " + id);
		/* 540 */ ServerResponse re = this.smeService.deleteAlertNotification(id);
		/* 541 */ JSONSerializer serializer = new JSONSerializer();
		/* 542 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 543 */ logger.info("User " + userId + " got response " + res);
		/* 544 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Recording file list on SME portal ", response = String.class, responseContainer = "List")
	@RequestMapping(value = { "/sme/{smeId}/recording/{id}/list" }, method = { RequestMethod.POST })
	public ResponseEntity<String> recordingList(@PathVariable String smeId, @PathVariable String id) {
		/* 550 */ logger.info("Sme " + smeId + " request files for callid " + id);
		/* 551 */ ServerResponse re = this.smeService.getRecordingList(id);
		/* 552 */ JSONSerializer serializer = new JSONSerializer();
		/* 553 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(re);
		/* 554 */ logger.info("Sme " + smeId + " got response " + res);
		/* 555 */ return new ResponseEntity(res, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching out leg if exist for incoming CDR ", response = CallCdrResponseBean.class)
	@RequestMapping(value = { "/sme/{smeId}/incoming/{sessionId}/leg" }, method = { RequestMethod.POST })
	public ResponseEntity<String> outLegIncoming(@PathVariable Long smeId, @PathVariable String sessionId)
			throws FileNotFoundException, IOException {
		/* 562 */ logger.info("Sme " + smeId + " request files for out leg for session id " + sessionId);
		/* 563 */ ServerResponse res = this.smeService.getOutLeg(smeId, sessionId, (byte) 1);
		/* 564 */ JSONSerializer serializer = new JSONSerializer();
		/* 565 */ String result = serializer.exclude(new String[] { "*.class" }).serialize(res);
		/* 566 */ logger.info("Sme " + smeId + " got response " + res);
		/* 567 */ return new ResponseEntity(result, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetching out leg if exist for outgoing CDR ", response = CallCdrResponseBean.class)
	@RequestMapping(value = { "/sme/{smeId}/outgoing/{sessionId}/leg" }, method = { RequestMethod.POST })
	public ResponseEntity<String> outLegOutgoing(@PathVariable Long smeId, @PathVariable String sessionId)
			throws FileNotFoundException, IOException {
		/* 574 */ logger.info("Sme " + smeId + " request files for out leg for session id " + sessionId);
		/* 575 */ ServerResponse res = this.smeService.getOutLeg(smeId, sessionId, (byte) 2);
		/* 576 */ JSONSerializer serializer = new JSONSerializer();
		/* 577 */ String result = serializer.exclude(new String[] { "*.class" }).serialize(res);
		/* 578 */ logger.info("Sme " + smeId + " got response " + res);
		/* 579 */ return new ResponseEntity(result, HttpStatus.OK);
	}
}
