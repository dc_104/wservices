package in.wringg.config;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CORSFilter
  implements Filter
{
  public void doFilter(ServletRequest request, ServletResponse res, FilterChain chain) throws IOException, ServletException {
HttpServletRequest oRequest = (HttpServletRequest)request;
 HttpServletResponse response = (HttpServletResponse)res;
response.setHeader("Access-Control-Allow-Origin", "*");
response.setHeader("Access-Control-Allow-Credentials", "true");
response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
response.setHeader("Access-Control-Max-Age", "3600");
response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization, Origin, Accept, Access-Control-Request-Method, Access-Control-Request-Headers,AUTH-TOKEN");

    
/* 27 */     if (oRequest.getMethod().equals("OPTIONS")) {
/* 28 */       response.flushBuffer();
    } else {
/* 30 */       chain.doFilter(request, (ServletResponse)response);
    } 
  }
  
  public void init(FilterConfig filterConfig) {}
  
  public void destroy() {}
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\config\CORSFilter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */