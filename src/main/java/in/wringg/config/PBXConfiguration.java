package in.wringg.config;

import in.wringg.utility.Constants;
import java.util.Properties;
import java.util.TimeZone;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
@EnableWebMvc
@ComponentScans({ @ComponentScan(basePackages = { "in.wringg" }), @ComponentScan({ "in.wringg.config" }) })
@EnableTransactionManagement
@PropertySource({ "classpath:hibernate.properties" })
public class PBXConfiguration extends WebMvcConfigurerAdapter {
	@Autowired
	private Environment environment;

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		/* 46 */ registry.addResourceHandler(new String[] { "swagger-ui.html" })
				.addResourceLocations(new String[] { "classpath:/META-INF/resources/" });
		/* 47 */ registry.addResourceHandler(new String[] { "/webjars/**" })
				.addResourceLocations(new String[] { "classpath:/META-INF/resources/webjars/" });
	}

	@Bean
	public Docket apiManual() {
		/* 52 */ return (new Docket(DocumentationType.SWAGGER_2)).apiInfo(getApiInfo()).select()
				/* 53 */ .apis(RequestHandlerSelectors.basePackage("in.wringg.controller")).paths(PathSelectors.any())
				.build();
	}

	private ApiInfo getApiInfo() {
		/* 57 */ Contact contact = new Contact("Tarsem Singh", "www.google.com", "");
		/* 58 */ return (new ApiInfoBuilder()).title("Wringg").description("Wringg Rest Api").version("1.0.0")
				/* 59 */ .license("Apache 8.0.33").licenseUrl("http://www.apache.org/licenses/LICENSE-8.0")
				.contact(contact)/* 60 */ .build();
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		/* 65 */ LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		/* 66 */ sessionFactory.setDataSource(dataSource());
		/* 67 */ sessionFactory.setPackagesToScan(new String[] { "in.wringg.entity" });
		/* 68 */ sessionFactory.setHibernateProperties(hibernateProperties());
		/* 69 */ return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		/* 74 */ Constants.UPLOAD_BASE_PATH = this.environment.getProperty("path.upload");
		/* 75 */ Constants.APP_URL = this.environment.getProperty("app.path");
		Constants.MEDIA_BASE_PATH = this.environment.getProperty("media.path");
		Constants.MEDIA_BASE_URL = this.environment.getProperty("media.url");
		/* 76 */ DriverManagerDataSource dataSource = new DriverManagerDataSource();
		/* 77 */ dataSource.setDriverClassName(this.environment.getRequiredProperty("jdbc.driverClassName"));
		/* 78 */ dataSource.setUrl(this.environment.getRequiredProperty("jdbc.url"));
		/* 79 */ dataSource.setUsername(this.environment.getRequiredProperty("jdbc.username"));
		/* 80 */ dataSource.setPassword(this.environment.getRequiredProperty("jdbc.password"));
		/* 81 */ return (DataSource) dataSource;
	}

	private Properties hibernateProperties() {
		/* 85 */ Constants.SYSTEM_TIME_ZONE = TimeZone.getDefault();
		/* 86 */ TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		/* 87 */ Properties properties = new Properties();
		/* 88 */ properties.put("hibernate.dialect", this.environment.getRequiredProperty("hibernate.dialect"));
		/* 89 */ properties.put("hibernate.show_sql", this.environment.getRequiredProperty("hibernate.show_sql"));
		/* 90 */ properties.put("hibernate.format_sql", this.environment.getRequiredProperty("hibernate.format_sql"));

		/* 102 */ return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory session) {
		/* 108 */ HibernateTransactionManager txManager = new HibernateTransactionManager();
		/* 109 */ txManager.setSessionFactory(session);
		/* 110 */ return txManager;
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\config\PBXConfiguration.
 * class Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */