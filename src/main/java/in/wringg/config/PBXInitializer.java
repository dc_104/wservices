package in.wringg.config;

import in.wringg.utility.Constants;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class PBXInitializer
  extends AbstractAnnotationConfigDispatcherServletInitializer
{
  protected Class<?>[] getRootConfigClasses() {
/* 15 */     return null;
  }

  
  protected Class<?>[] getServletConfigClasses() {
/* 20 */     return new Class[] { PBXConfiguration.class };
  }

  
  protected String[] getServletMappings() {
/* 25 */     return new String[] { "/" };
  }

  
  protected Filter[] getServletFilters() {
/* 30 */     Filter[] singleton = { (Filter)new CORSFilter() };
/* 31 */     return singleton;
  }

  
  protected void customizeRegistration(ServletRegistration.Dynamic registration) {
/* 36 */     registration.setMultipartConfig(new MultipartConfigElement(Constants.UPLOAD_BASE_PATH, 10485760L, 10485760L, 10485760));
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\config\PBXInitializer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */