package in.wringg.pojo;


public class SmeAgentDetailBean
{
  private Integer totalAgents;
  private Integer busyAgents;
  
  public Integer getRegisteredAgents() {
/* 10 */     return this.registeredAgents;
  } private Integer freeAgents; private Integer registeredAgents; private Integer unregisteredAgents;
  public void setRegisteredAgents(Integer registeredAgents) {
/* 13 */     this.registeredAgents = registeredAgents;
  }
  public Integer getUnregisteredAgents() {
/* 16 */     return this.unregisteredAgents;
  }
  public void setUnregisteredAgents(Integer unregisteredAgents) {
/* 19 */     this.unregisteredAgents = unregisteredAgents;
  }
  public Integer getTotalAgents() {
/* 22 */     return this.totalAgents;
  }
  public void setTotalAgents(Integer totalAgents) {
/* 25 */     this.totalAgents = totalAgents;
  }
  
  public Integer getFreeAgents() {
/* 29 */     return this.freeAgents;
  }
  public void setFreeAgents(Integer freeAgents) {
/* 32 */     this.freeAgents = freeAgents;
  }
  public Integer getBusyAgents() {
/* 35 */     return this.busyAgents;
  }
  public void setBusyAgents(Integer busyAgents) {
/* 38 */     this.busyAgents = busyAgents;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmeAgentDetailBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */