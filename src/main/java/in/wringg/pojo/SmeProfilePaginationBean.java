package in.wringg.pojo;
import in.wringg.pojo.AdminSmeProfileBean;
import java.util.List;

public class SmeProfilePaginationBean {
  private Long totalRecords;
  
  public Long getTotalRecords() {
/*  9 */     return this.totalRecords;
  } private List<AdminSmeProfileBean> records;
  public void setTotalRecords(Long totalRecords) {
/* 12 */     this.totalRecords = totalRecords;
  }
  public List<AdminSmeProfileBean> getRecords() {
/* 15 */     return this.records;
  }
  public void setRecords(List<AdminSmeProfileBean> records) {
/* 18 */     this.records = records;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmeProfilePaginationBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */