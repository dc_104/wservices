package in.wringg.pojo;



public class AgentInsightPojo
{
  private Long agentId;
  private Long totalCalls;
  private Long totalInCalls;
  private Long totalOutCalls;
  private Long noAnswer;
  private Long inSuccessCalls;
  private Long inFailedCalls;
  private Long outSuccessCalls;
  private Long outFailedCalls;
  private Long avgCallDuration;
  private Long totalCallDuration;
  private Long officeHours;
  private Long lunchHours;
  private double inSuccess;
  private double outSuccess;
  
  public AgentInsightPojo(Long agentId, Long totalCalls, Long totalInCalls, Long totalOutCalls, Long noAnswer, Long inSuccessCalls, Long inFailedCalls, Long outSuccessCalls, Long outFailedCalls, Long avgCallDuration, Long totalCallDuration, Long officeHours, Long lunchHours) {
/*  24 */     this.agentId = agentId;
/*  25 */     this.totalCalls = totalCalls;
/*  26 */     this.totalInCalls = totalInCalls;
/*  27 */     this.totalOutCalls = totalOutCalls;
/*  28 */     this.noAnswer = noAnswer;
/*  29 */     this.inSuccessCalls = inSuccessCalls;
/*  30 */     this.inFailedCalls = inFailedCalls;
/*  31 */     this.outSuccessCalls = outSuccessCalls;
/*  32 */     this.outFailedCalls = outFailedCalls;
/*  33 */     this.avgCallDuration = avgCallDuration;
/*  34 */     this.totalCallDuration = totalCallDuration;
/*  35 */     this.officeHours = officeHours;
/*  36 */     this.lunchHours = lunchHours;
  }
  
  public Long getAgentId() {
/*  40 */     return this.agentId;
  }
  
  public void setAgentId(Long agentId) {
/*  44 */     this.agentId = agentId;
  }
  
  public Long getTotalCalls() {
/*  48 */     return this.totalCalls;
  }
  
  public void setTotalCalls(Long totalCalls) {
/*  52 */     this.totalCalls = totalCalls;
  }
  
  public Long getNoAnswer() {
/*  56 */     return this.noAnswer;
  }
  
  public void setNoAnswer(Long noAnswer) {
/*  60 */     this.noAnswer = noAnswer;
  }
  
  public Long getInSuccessCalls() {
/*  64 */     return this.inSuccessCalls;
  }
  
  public void setInSuccessCalls(Long inSuccessCalls) {
/*  68 */     this.inSuccessCalls = inSuccessCalls;
  }
  
  public Long getInFailedCalls() {
/*  72 */     return this.inFailedCalls;
  }
  
  public void setInFailedCalls(Long inFailedCalls) {
/*  76 */     this.inFailedCalls = inFailedCalls;
  }
  
  public Long getOutSuccessCalls() {
/*  80 */     return this.outSuccessCalls;
  }
  
  public void setOutSuccessCalls(Long outSuccessCalls) {
/*  84 */     this.outSuccessCalls = outSuccessCalls;
  }
  
  public Long getOutFailedCalls() {
/*  88 */     return this.outFailedCalls;
  }
  
  public void setOutFailedCalls(Long outFailedCalls) {
/*  92 */     this.outFailedCalls = outFailedCalls;
  }
  
  public Long getAvgCallDuration() {
/*  96 */     return this.avgCallDuration;
  }
  
  public void setAvgCallDuration(Long avgCallDuration) {
/* 100 */     this.avgCallDuration = avgCallDuration;
  }
  
  public Long getTotalCallDuration() {
/* 104 */     return this.totalCallDuration;
  }
  
  public void setTotalCallDuration(Long totalCallDuration) {
/* 108 */     this.totalCallDuration = totalCallDuration;
  }
  
  public Long getOfficeHours() {
/* 112 */     return this.officeHours;
  }
  
  public void setOfficeHours(Long officeHours) {
/* 116 */     this.officeHours = officeHours;
  }
  
  public Long getLunchHours() {
/* 120 */     return this.lunchHours;
  }
  
  public void setLunchHours(Long lunchHours) {
/* 124 */     this.lunchHours = lunchHours;
  }
  
  public Long getTotalOutCalls() {
/* 128 */     return this.totalOutCalls;
  }
  
  public void setTotalOutCalls(Long totalOutCalls) {
/* 132 */     this.totalOutCalls = totalOutCalls;
  }
  
  public Long getTotalInCalls() {
/* 136 */     return this.totalInCalls;
  }
  
  public void setTotalInCalls(Long totalInCalls) {
/* 140 */     this.totalInCalls = totalInCalls;
  }
  
  public double getInSuccess() {
/* 144 */     return this.inSuccess;
  }
  
  public void setInSuccess(double inSuccess) {
/* 148 */     this.inSuccess = inSuccess;
  }
  
  public double getOutSuccess() {
/* 152 */     return this.outSuccess;
  }
  
  public void setOutSuccess(double outSuccess) {
/* 156 */     this.outSuccess = outSuccess;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AgentInsightPojo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */