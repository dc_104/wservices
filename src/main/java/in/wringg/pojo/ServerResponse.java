package in.wringg.pojo;

public class ServerResponse {
  private boolean success;
  private String result;
  
  public String getResult() {
/*  8 */     return this.result;
  }
  public void setResult(String result) {
/* 11 */     this.result = result;
  }
  public boolean isSuccess() {
/* 14 */     return this.success;
  }
  public void setSuccess(boolean success) {
/* 17 */     this.success = success;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\ServerResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */