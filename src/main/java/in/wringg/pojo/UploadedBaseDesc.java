package in.wringg.pojo;



public class UploadedBaseDesc
{
  private String mobile;
  private String desc;
  private long agentId;
  
  public UploadedBaseDesc() {}
  
  public UploadedBaseDesc(String mobile, String desc, long agentId) {
/* 14 */     this.mobile = mobile;
/* 15 */     this.desc = desc;
/* 16 */     this.agentId = agentId;
  }
  
  public String getMobile() {
/* 20 */     return this.mobile;
  }
  
  public void setMobile(String mobile) {
/* 24 */     this.mobile = mobile;
  }
  
  public String getDesc() {
/* 28 */     return this.desc;
  }
  
  public void setDesc(String desc) {
/* 32 */     this.desc = desc;
  }
  
  public long getAgentId() {
/* 36 */     return this.agentId;
  }
  
  public void setAgentId(long agentId) {
/* 40 */     this.agentId = agentId;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\UploadedBaseDesc.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */