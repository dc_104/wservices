package in.wringg.pojo;

public class EdvaantageDemoBean
{
  private String name;
  private String emailId;
  private String mobileNumber;
  private String message;
  private String packId;
  
  public String getMobileNumber() {
/* 12 */     return this.mobileNumber;
  }
  public void setMobileNumber(String mobileNumber) {
/* 15 */     this.mobileNumber = mobileNumber;
  }
  public String getMessage() {
/* 18 */     return this.message;
  }
  public void setMessage(String message) {
/* 21 */     this.message = message;
  }
  public String getEmailId() {
/* 24 */     return this.emailId;
  }
  public void setEmailId(String emailId) {
/* 27 */     this.emailId = emailId;
  }
  public String getName() {
/* 30 */     return this.name;
  }
  public void setName(String name) {
/* 33 */     this.name = name;
  }
  public String getPackId() {
/* 36 */     return this.packId;
  }
  public void setPackId(String packId) {
/* 39 */     this.packId = packId;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\EdvaantageDemoBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */