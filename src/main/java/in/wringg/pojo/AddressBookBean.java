package in.wringg.pojo;

import java.util.Date;
import java.util.List;

public class AddressBookBean {
  private Long addressBookId;
  private Long smeId;
  private String customerName;
  private String customerNumberPrimary;
  private List<String> customerNumberSecondary;
  private Byte status;
  private Date insertDateTime;
  private Date updatedDateTime;
  private Byte mode;
  private String companyName;
  private String emailId;
  private Long createdBy;
  private Byte visibilityFlag;
  
  public Long getSmeId() {
/* 22 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/* 25 */     this.smeId = smeId;
  }
  public String getCustomerName() {
/* 28 */     return this.customerName;
  }
  public void setCustomerName(String customerName) {
/* 31 */     this.customerName = customerName;
  }
  public String getCustomerNumberPrimary() {
/* 34 */     return this.customerNumberPrimary;
  }
  public void setCustomerNumberPrimary(String customerNumberPrimary) {
/* 37 */     this.customerNumberPrimary = customerNumberPrimary;
  }
  public Byte getStatus() {
/* 40 */     return this.status;
  }
  public void setStatus(Byte status) {
/* 43 */     this.status = status;
  }
  public Date getInsertDateTime() {
/* 46 */     return this.insertDateTime;
  }
  public void setInsertDateTime(Date insertDateTime) {
/* 49 */     this.insertDateTime = insertDateTime;
  }
  public Date getUpdatedDateTime() {
/* 52 */     return this.updatedDateTime;
  }
  public void setUpdatedDateTime(Date updatedDateTime) {
/* 55 */     this.updatedDateTime = updatedDateTime;
  }
  public Byte getMode() {
/* 58 */     return this.mode;
  }
  public void setMode(Byte mode) {
/* 61 */     this.mode = mode;
  }
  public String getCompanyName() {
/* 64 */     return this.companyName;
  }
  public void setCompanyName(String companyName) {
/* 67 */     this.companyName = companyName;
  }
  public String getEmailId() {
/* 70 */     return this.emailId;
  }
  public void setEmailId(String emailId) {
/* 73 */     this.emailId = emailId;
  }
  public Long getCreatedBy() {
/* 76 */     return this.createdBy;
  }
  public void setCreatedBy(Long createdBy) {
/* 79 */     this.createdBy = createdBy;
  }
  public Byte getVisibilityFlag() {
/* 82 */     return this.visibilityFlag;
  }
  public void setVisibilityFlag(Byte visibilityFlag) {
/* 85 */     this.visibilityFlag = visibilityFlag;
  }
  public List<String> getCustomerNumberSecondary() {
/* 88 */     return this.customerNumberSecondary;
  }
  public void setCustomerNumberSecondary(List<String> customerNumberSecondary) {
/* 91 */     this.customerNumberSecondary = customerNumberSecondary;
  }
  public Long getAddressBookId() {
/* 94 */     return this.addressBookId;
  }
  public void setAddressBookId(Long addressBookId) {
/* 97 */     this.addressBookId = addressBookId;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AddressBookBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */