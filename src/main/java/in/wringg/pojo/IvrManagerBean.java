package in.wringg.pojo;

public class IvrManagerBean
{
/*  5 */   private String callId = "CDL1234";
/*  6 */   private String startDate = "2017-05-06 12:12:45";
/*  7 */   private String endDate = "2017-07-06 12:01:45";
/*  8 */   private String huntingnumber = "+324232235";
/*  9 */   private String mobileNumber = "24322+";
/* 10 */   private String confrence = "yes";
/* 11 */   private String missCall = "no";
/* 12 */   private String inbound = "yes";
  public String getCallId() {
/* 14 */     return this.callId;
  }
  public void setCallId(String callId) {
/* 17 */     this.callId = callId;
  }
  public String getStartDate() {
/* 20 */     return this.startDate;
  }
  public void setStartDate(String startDate) {
/* 23 */     this.startDate = startDate;
  }
  public String getEndDate() {
/* 26 */     return this.endDate;
  }
  public void setEndDate(String endDate) {
/* 29 */     this.endDate = endDate;
  }
  public String getHuntingnumber() {
/* 32 */     return this.huntingnumber;
  }
  public void setHuntingnumber(String huntingnumber) {
/* 35 */     this.huntingnumber = huntingnumber;
  }
  public String getMobileNumber() {
/* 38 */     return this.mobileNumber;
  }
  public void setMobileNumber(String mobileNumber) {
/* 41 */     this.mobileNumber = mobileNumber;
  }
  public String getConfrence() {
/* 44 */     return this.confrence;
  }
  public void setConfrence(String confrence) {
/* 47 */     this.confrence = confrence;
  }
  public String getMissCall() {
/* 50 */     return this.missCall;
  }
  public void setMissCall(String missCall) {
/* 53 */     this.missCall = missCall;
  }
  public String getInbound() {
/* 56 */     return this.inbound;
  }
  public void setInbound(String inbound) {
/* 59 */     this.inbound = inbound;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\IvrManagerBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */