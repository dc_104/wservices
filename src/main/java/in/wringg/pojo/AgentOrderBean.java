package in.wringg.pojo;

import javax.validation.constraints.NotNull;

public class AgentOrderBean
{
  @NotNull
  private Long id;
  @NotNull
  private Integer order;
  
  public Long getId() {
/* 13 */     return this.id;
  }
  public void setId(Long id) {
/* 16 */     this.id = id;
  }
  public Integer getOrder() {
/* 19 */     return this.order;
  }
  public void setOrder(Integer order) {
/* 22 */     this.order = order;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AgentOrderBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */