package in.wringg.pojo;

public class PromptDetailsBean {

	String promptName;
	String promptCategory;
	Long promptId;
	String promptDescription;
	Long smeId;
	String promptPath;
	String promptUrl;
	
	
	public Long getSmeId() {
		return smeId;
	}
	public void setSmeId(Long smeId) {
		this.smeId = smeId;
	}
	public String getPromptPath() {
		return promptPath;
	}
	public void setPromptPath(String promptPath) {
		this.promptPath = promptPath;
	}
	public String getPromptUrl() {
		return promptUrl;
	}
	public void setPromptUrl(String promptUrl) {
		this.promptUrl = promptUrl;
	}
	public String getPromptName() {
		return promptName;
	}
	public void setPromptName(String promptName) {
		this.promptName = promptName;
	}
	public String getPromptCategory() {
		return promptCategory;
	}
	public void setPromptCategory(String promptCategory) {
		this.promptCategory = promptCategory;
	}
	public Long getPromptId() {
		return promptId;
	}
	public void setPromptId(Long promptId) {
		this.promptId = promptId;
	}
	public String getPromptDescription() {
		return promptDescription;
	}
	public void setPromptDescription(String promptDescription) {
		this.promptDescription = promptDescription;
	}
	
}
