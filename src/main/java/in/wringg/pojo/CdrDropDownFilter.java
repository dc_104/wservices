package in.wringg.pojo;
import in.wringg.pojo.CdrModeBean;
import in.wringg.pojo.KeyValueGenricBean;
import java.util.List;

public class CdrDropDownFilter {
  List<KeyValueGenricBean> callRecordingStatus;
  List<KeyValueGenricBean> voiceMailRecordingStatus;
  
  public List<KeyValueGenricBean> getCallRecordingStatus() {
/* 11 */     return this.callRecordingStatus;
  } List<KeyValueGenricBean> callDirectionStatus; CdrModeBean cdrMode;
  public void setCallRecordingStatus(List<KeyValueGenricBean> callRecordingStatus) {
/* 14 */     this.callRecordingStatus = callRecordingStatus;
  }
  public List<KeyValueGenricBean> getVoiceMailRecordingStatus() {
/* 17 */     return this.voiceMailRecordingStatus;
  }
  public void setVoiceMailRecordingStatus(List<KeyValueGenricBean> voiceMailRecordingStatus) {
/* 20 */     this.voiceMailRecordingStatus = voiceMailRecordingStatus;
  }
  public List<KeyValueGenricBean> getCallDirectionStatus() {
/* 23 */     return this.callDirectionStatus;
  }
  public void setCallDirectionStatus(List<KeyValueGenricBean> callDirectionStatus) {
/* 26 */     this.callDirectionStatus = callDirectionStatus;
  }
  public CdrModeBean getCdrMode() {
/* 29 */     return this.cdrMode;
  }
  public void setCdrMode(CdrModeBean cdrMode) {
/* 32 */     this.cdrMode = cdrMode;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CdrDropDownFilter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */