package in.wringg.pojo;

public class IdValueBean
{
  int id;
  String value;
  
  public int getId() {
/*  9 */     return this.id;
  }
  public void setId(int id) {
/* 12 */     this.id = id;
  }
  public String getValue() {
/* 15 */     return this.value;
  }
  public void setValue(String value) {
/* 18 */     this.value = value;
  }
  
  public IdValueBean(int id, String value) {
/* 22 */     this.id = id;
/* 23 */     this.value = value;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\IdValueBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */