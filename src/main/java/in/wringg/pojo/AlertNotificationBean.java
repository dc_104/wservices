package in.wringg.pojo;

public class AlertNotificationBean {
  private int id;
  private String description;
  private String insertedDate;
  private byte status;
  private String title;
  
  public int getId() {
/* 11 */     return this.id;
  }
  
  public void setId(int id) {
/* 15 */     this.id = id;
  }
  
  public String getDescription() {
/* 19 */     return this.description;
  }
  
  public void setDescription(String description) {
/* 23 */     this.description = description;
  }
  
  public String getInsertedDate() {
/* 27 */     return this.insertedDate;
  }
  
  public void setInsertedDate(String insertedDate) {
/* 31 */     this.insertedDate = insertedDate;
  }
  
  public byte getStatus() {
/* 35 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 39 */     this.status = status;
  }
  
  public String getTitle() {
/* 43 */     return this.title;
  }
  
  public void setTitle(String title) {
/* 47 */     this.title = title;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AlertNotificationBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */