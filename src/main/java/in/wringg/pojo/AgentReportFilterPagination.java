package in.wringg.pojo;

import in.wringg.pojo.AgentDetailReportBean;
import java.util.List;

public class AgentReportFilterPagination {
  private Long totalRecords;
  
  public Long getTotalRecords() {
/* 10 */     return this.totalRecords;
  } private List<AgentDetailReportBean> records;
  public void setTotalRecords(Long totalRecords) {
/* 13 */     this.totalRecords = totalRecords;
  }
  public List<AgentDetailReportBean> getRecords() {
/* 16 */     return this.records;
  }
  public void setRecords(List<AgentDetailReportBean> records) {
/* 19 */     this.records = records;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AgentReportFilterPagination.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */