package in.wringg.pojo;

import java.util.Date;

public class MissCallMarketingBean {
  private String missCallid;
  private Long longCode;
  private String callpartyNumber;
  private Date dateTime;
  private Integer asignedAgent;
  private String agentName;
  private String status;
  
  public String getMissCallid() {
/* 15 */     return this.missCallid;
  }
  public void setMissCallid(String missCallid) {
/* 18 */     this.missCallid = missCallid;
  }
  public Long getLongCode() {
/* 21 */     return this.longCode;
  }
  public void setLongCode(Long longCode) {
/* 24 */     this.longCode = longCode;
  }
  
  public String getCallpartyNumber() {
/* 28 */     return this.callpartyNumber;
  }
  public void setCallpartyNumber(String callpartyNumber) {
/* 31 */     this.callpartyNumber = callpartyNumber;
  }
  public Date getDateTime() {
/* 34 */     return this.dateTime;
  }
  public void setDateTime(Date dateTime) {
/* 37 */     this.dateTime = dateTime;
  }
  public Integer getAsignedAgent() {
/* 40 */     return this.asignedAgent;
  }
  public void setAsignedAgent(Integer asignedAgent) {
/* 43 */     this.asignedAgent = asignedAgent;
  }
  
  public String getAgentName() {
/* 47 */     return this.agentName;
  }
  public void setAgentName(String agentName) {
/* 50 */     this.agentName = agentName;
  }
  public String getStatus() {
/* 53 */     return this.status;
  }
  public void setStatus(String status) {
/* 56 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\MissCallMarketingBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */