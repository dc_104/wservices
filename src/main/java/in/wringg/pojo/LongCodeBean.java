package in.wringg.pojo;

import java.util.Date;
import javax.validation.constraints.NotNull;


public class LongCodeBean
{
  private Long id;
  private Date insertTime;
  @NotNull
  private String hlr;
  @NotNull
  private String smeIdentifier;
  
  public Long getId() {
/* 17 */     return this.id; } @NotNull
  private Long longCode; @NotNull
  private byte status; @NotNull
  private Long inShortCode; @NotNull
/* 21 */   private Long outShortCode; public void setId(Long id) { this.id = id; }

  
  public Date getInsertTime() {
/* 25 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/* 29 */     this.insertTime = insertTime;
  }

  
  public byte getStatus() {
/* 34 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 38 */     this.status = status;
  }
  
  public Long getInShortCode() {
/* 42 */     return this.inShortCode;
  }
  
  public void setInShortCode(Long inShortCode) {
/* 46 */     this.inShortCode = inShortCode;
  }
  
  public Long getOutShortCode() {
/* 50 */     return this.outShortCode;
  }
  
  public void setOutShortCode(Long outShortCode) {
/* 54 */     this.outShortCode = outShortCode;
  }
  
  public String getHlr() {
/* 58 */     return this.hlr;
  }
  
  public void setHlr(String hlr) {
/* 62 */     this.hlr = hlr;
  }
  
  public String getSmeIdentifier() {
/* 66 */     return this.smeIdentifier;
  }
  
  public void setSmeIdentifier(String smeIdentifier) {
/* 70 */     this.smeIdentifier = smeIdentifier;
  }


  
  public Long getLongCode() {
/* 76 */     return this.longCode;
  }
  
  public void setLongCode(Long longCode) {
/* 80 */     this.longCode = longCode;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\LongCodeBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */