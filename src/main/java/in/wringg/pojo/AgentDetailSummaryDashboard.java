package in.wringg.pojo;

public class AgentDetailSummaryDashboard
{
  private Long agentId;
  private String agentMobile;
  private String agentName;
  private String agentGroup;
  private Integer agentScore;
  
  public Integer getAgentScore() {
/* 12 */     return this.agentScore;
  }
  public void setAgentScore(Integer agentScore) {
/* 15 */     this.agentScore = agentScore;
  }
  public Long getAgentId() {
/* 18 */     return this.agentId;
  }
  public void setAgentId(Long agentId) {
/* 21 */     this.agentId = agentId;
  }
  public String getAgentMobile() {
/* 24 */     return this.agentMobile;
  }
  public void setAgentMobile(String agentMobile) {
/* 27 */     this.agentMobile = agentMobile;
  }
  public String getAgentName() {
/* 30 */     return this.agentName;
  }
  public void setAgentName(String agentName) {
/* 33 */     this.agentName = agentName;
  }
  public String getAgentGroup() {
/* 36 */     return this.agentGroup;
  }
  public void setAgentGroup(String agentGroup) {
/* 39 */     this.agentGroup = agentGroup;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AgentDetailSummaryDashboard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */