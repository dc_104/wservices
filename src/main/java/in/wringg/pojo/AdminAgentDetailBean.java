package in.wringg.pojo;

public class AdminAgentDetailBean
{
  private int totalAgents;
  
  public int getTotalAgents() {
/*  8 */     return this.totalAgents;
  } private int currentAgents; private int freeAgents;
  public void setTotalAgents(int totalAgents) {
/* 11 */     this.totalAgents = totalAgents;
  }
  public int getCurrentAgents() {
/* 14 */     return this.currentAgents;
  }
  public void setCurrentAgents(int currentAgents) {
/* 17 */     this.currentAgents = currentAgents;
  }
  public int getFreeAgents() {
/* 20 */     return this.freeAgents;
  }
  public void setFreeAgents(int freeAgents) {
/* 23 */     this.freeAgents = freeAgents;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AdminAgentDetailBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */