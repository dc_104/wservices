package in.wringg.pojo;

import java.util.Date;


public class SmeCustomerRemarksBean
{
  private Long id;
  private Long addressBookId;
  private Long smeId;
  private Date insertDateTime;
  private Date updatedDateTime;
  private Long agentId;
  private String remarks;
  private Byte status;
  private boolean canUpdateDel;
  private Byte visibilityFlag;
  
  public Long getId() {
/* 20 */     return this.id;
  }
  
  public void setId(Long id) {
/* 24 */     this.id = id;
  }
  
  public Long getAddressBookId() {
/* 28 */     return this.addressBookId;
  }
  
  public void setAddressBookId(Long addressBookId) {
/* 32 */     this.addressBookId = addressBookId;
  }
  
  public Long getSmeId() {
/* 36 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 40 */     this.smeId = smeId;
  }
  
  public Date getInsertDateTime() {
/* 44 */     return this.insertDateTime;
  }
  
  public void setInsertDateTime(Date insertDateTime) {
/* 48 */     this.insertDateTime = insertDateTime;
  }
  
  public Date getUpdatedDateTime() {
/* 52 */     return this.updatedDateTime;
  }
  
  public void setUpdatedDateTime(Date updatedDateTime) {
/* 56 */     this.updatedDateTime = updatedDateTime;
  }
  
  public Long getAgentId() {
/* 60 */     return this.agentId;
  }
  
  public void setAgentId(Long agentId) {
/* 64 */     this.agentId = agentId;
  }
  
  public String getRemarks() {
/* 68 */     return this.remarks;
  }
  
  public void setRemarks(String remarks) {
/* 72 */     this.remarks = remarks;
  }
  
  public Byte getStatus() {
/* 76 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 80 */     this.status = status;
  }
  
  public boolean isCanUpdateDel() {
/* 84 */     return this.canUpdateDel;
  }
  
  public void setCanUpdateDel(boolean canUpdateDel) {
/* 88 */     this.canUpdateDel = canUpdateDel;
  }
  
  public Byte getVisibilityFlag() {
/* 92 */     return this.visibilityFlag;
  }
  
  public void setVisibilityFlag(Byte visibilityFlag) {
/* 96 */     this.visibilityFlag = visibilityFlag;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmeCustomerRemarksBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */