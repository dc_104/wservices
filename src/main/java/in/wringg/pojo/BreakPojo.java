package in.wringg.pojo;

import java.util.Date;

public class BreakPojo
{
  private Date breakIn;
  private Date breakOut;
  private String breakInStr;
  private String breakOutStr;
  
  public Date getBreakIn() {
/* 13 */     return this.breakIn;
  }
  
  public void setBreakIn(Date breakIn) {
/* 17 */     this.breakIn = breakIn;
  }
  
  public Date getBreakOut() {
/* 21 */     return this.breakOut;
  }
  
  public void setBreakOut(Date breakOut) {
/* 25 */     this.breakOut = breakOut;
  }
  
  public String getBreakInStr() {
/* 29 */     return this.breakInStr;
  }
  
  public void setBreakInStr(String breakInStr) {
/* 33 */     this.breakInStr = breakInStr;
  }
  
  public String getBreakOutStr() {
/* 37 */     return this.breakOutStr;
  }
  
  public void setBreakOutStr(String breakOutStr) {
/* 41 */     this.breakOutStr = breakOutStr;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\BreakPojo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */