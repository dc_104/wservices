package in.wringg.pojo;

import java.sql.Date;

public class CallCdrBean
{
  private String callId;
  private Long id;
  private String agentGroup;
  private String callDirection;
  private int callDirectionStatus;
  private String callRecordedFile;
  private int callRecordingStatus;
  private String calledNumber;
  private String callingNumber;
  private Byte cdrMode;
  private int channelNo;
  private int duration;
  private Date endDateTime;
  private String endDateTimeS;
  private String hlr;
  private Date insertDateTime;
  private String insertDateTimeS;
  
  public String getAgentGroup() {
/*  41 */     return this.agentGroup;
  } private Long longcode; private String masterShortcode; private Long patchedAgentId; private String serverIpAddress; private String shortcodeMapping; private Long smeId; private String smeIdentifier; private Date startDateTime; private String startDateTimeS; private String voicemailRecordingFile; private String groupId; private byte answer; private byte callStatus; private String disconnectedBy; private Long addressBookId; private String customerName; private int voicemailRecordingStatus;
  public void setAgentGroup(String agentGroup) {
/*  44 */     this.agentGroup = agentGroup;
  }
  public String getCallDirection() {
/*  47 */     return this.callDirection;
  }
  public void setCallDirection(String callDirection) {
/*  50 */     this.callDirection = callDirection;
  }
  public int getCallDirectionStatus() {
/*  53 */     return this.callDirectionStatus;
  }
  public void setCallDirectionStatus(int callDirectionStatus) {
/*  56 */     this.callDirectionStatus = callDirectionStatus;
  }
  public String getCallRecordedFile() {
/*  59 */     return this.callRecordedFile;
  }
  public void setCallRecordedFile(String callRecordedFile) {
/*  62 */     this.callRecordedFile = callRecordedFile;
  }
  public int getCallRecordingStatus() {
/*  65 */     return this.callRecordingStatus;
  }
  public void setCallRecordingStatus(int callRecordingStatus) {
/*  68 */     this.callRecordingStatus = callRecordingStatus;
  }
  public String getCalledNumber() {
/*  71 */     return this.calledNumber;
  }
  public void setCalledNumber(String calledNumber) {
/*  74 */     this.calledNumber = calledNumber;
  }
  public String getCallingNumber() {
/*  77 */     return this.callingNumber;
  }
  public void setCallingNumber(String callingNumber) {
/*  80 */     this.callingNumber = callingNumber;
  }
  public Byte getCdrMode() {
/*  83 */     return this.cdrMode;
  }
  public void setCdrMode(Byte cdrMode) {
/*  86 */     this.cdrMode = cdrMode;
  }
  public int getChannelNo() {
/*  89 */     return this.channelNo;
  }
  public void setChannelNo(int channelNo) {
/*  92 */     this.channelNo = channelNo;
  }
  public int getDuration() {
/*  95 */     return this.duration;
  }
  public void setDuration(int duration) {
/*  98 */     this.duration = duration;
  }
  public Date getEndDateTime() {
/* 101 */     return this.endDateTime;
  }
  public void setEndDateTime(Date endDateTime) {
/* 104 */     this.endDateTime = endDateTime;
  }
  public String getHlr() {
/* 107 */     return this.hlr;
  }
  public void setHlr(String hlr) {
/* 110 */     this.hlr = hlr;
  }
  public Date getInsertDateTime() {
/* 113 */     return this.insertDateTime;
  }
  public void setInsertDateTime(Date insertDateTime) {
/* 116 */     this.insertDateTime = insertDateTime;
  }
  public String getMasterShortcode() {
/* 119 */     return this.masterShortcode;
  }
  public void setMasterShortcode(String masterShortcode) {
/* 122 */     this.masterShortcode = masterShortcode;
  }
  public Long getPatchedAgentId() {
/* 125 */     return this.patchedAgentId;
  }
  public void setPatchedAgentId(Long patchedAgentId) {
/* 128 */     this.patchedAgentId = patchedAgentId;
  }
  public String getServerIpAddress() {
/* 131 */     return this.serverIpAddress;
  }
  public void setServerIpAddress(String serverIpAddress) {
/* 134 */     this.serverIpAddress = serverIpAddress;
  }
  public String getShortcodeMapping() {
/* 137 */     return this.shortcodeMapping;
  }
  public void setShortcodeMapping(String shortcodeMapping) {
/* 140 */     this.shortcodeMapping = shortcodeMapping;
  }
  public Long getSmeId() {
/* 143 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/* 146 */     this.smeId = smeId;
  }
  public String getSmeIdentifier() {
/* 149 */     return this.smeIdentifier;
  }
  public void setSmeIdentifier(String smeIdentifier) {
/* 152 */     this.smeIdentifier = smeIdentifier;
  }
  public Date getStartDateTime() {
/* 155 */     return this.startDateTime;
  }
  public void setStartDateTime(Date startDateTime) {
/* 158 */     this.startDateTime = startDateTime;
  }
  public String getVoicemailRecordingFile() {
/* 161 */     return this.voicemailRecordingFile;
  }
  public void setVoicemailRecordingFile(String voicemailRecordingFile) {
/* 164 */     this.voicemailRecordingFile = voicemailRecordingFile;
  }
  public int getVoicemailRecordingStatus() {
/* 167 */     return this.voicemailRecordingStatus;
  }
  public void setVoicemailRecordingStatus(int voicemailRecordingStatus) {
/* 170 */     this.voicemailRecordingStatus = voicemailRecordingStatus;
  }
  public Long getLongcode() {
/* 173 */     return this.longcode;
  }
  public void setLongcode(Long longcode) {
/* 176 */     this.longcode = longcode;
  }
  public Long getId() {
/* 179 */     return this.id;
  }
  public void setId(Long id) {
/* 182 */     this.id = id;
  }
  public String getStartDateTimeS() {
/* 185 */     return this.startDateTimeS;
  }
  public void setStartDateTimeS(String startDateTimeS) {
/* 188 */     this.startDateTimeS = startDateTimeS;
  }
  public String getInsertDateTimeS() {
/* 191 */     return this.insertDateTimeS;
  }
  public void setInsertDateTimeS(String insertDateTimeS) {
/* 194 */     this.insertDateTimeS = insertDateTimeS;
  }
  public String getEndDateTimeS() {
/* 197 */     return this.endDateTimeS;
  }
  public void setEndDateTimeS(String endDateTimeS) {
/* 200 */     this.endDateTimeS = endDateTimeS;
  }
  public String getGroupId() {
/* 203 */     return this.groupId;
  }
  public void setGroupId(String groupId) {
/* 206 */     this.groupId = groupId;
  }

  
  public String toString() {
/* 211 */     return "CallCdrBean [answer=" + this.answer + ", agentGroup=" + this.agentGroup + ", callStatus=" + this.callStatus + ", callId=" + this.callId + ", callDirection=" + this.callDirection + ", callDirectionStatus=" + this.callDirectionStatus + ", callRecordedFile=" + this.callRecordedFile + ", callRecordingStatus=" + this.callRecordingStatus + ", calledNumber=" + this.calledNumber + ", callingNumber=" + this.callingNumber + ", cdrMode=" + this.cdrMode + ", channelNo=" + this.channelNo + ", disconnectedBy=" + this.disconnectedBy + ", duration=" + this.duration + ", endDateTime=" + this.endDateTime + ", endDateTimeS=" + this.endDateTimeS + ", groupId=" + this.groupId + ", hlr=" + this.hlr + ", id=" + this.id + ", insertDateTime=" + this.insertDateTime + ", insertDateTimeS=" + this.insertDateTimeS + ", longcode=" + this.longcode + ", masterShortcode=" + this.masterShortcode + ", patchedAgentId=" + this.patchedAgentId + ", serverIpAddress=" + this.serverIpAddress + ", shortcodeMapping=" + this.shortcodeMapping + ", smeId=" + this.smeId + ", smeIdentifier=" + this.smeIdentifier + ", startDateTime=" + this.startDateTime + ", startDateTimeS=" + this.startDateTimeS + ", voicemailRecordingFile=" + this.voicemailRecordingFile + ", voicemailRecordingStatus=" + this.voicemailRecordingStatus + "]";
  }

































  
  public String getCallId() {
/* 248 */     return this.callId;
  }
  public void setCallId(String callId) {
/* 251 */     this.callId = callId;
  }
  
  public byte getAnswer() {
/* 255 */     return this.answer;
  }
  public void setAnswer(byte answer) {
/* 258 */     this.answer = answer;
  }


  
  public byte getCallStatus() {
/* 264 */     return this.callStatus;
  }
  public void setCallStatus(byte callStatus) {
/* 267 */     this.callStatus = callStatus;
  }
  public String getDisconnectedBy() {
/* 270 */     return this.disconnectedBy;
  }
  public void setDisconnectedBy(String disconnectedBy) {
/* 273 */     this.disconnectedBy = disconnectedBy;
  }
  public Long getAddressBookId() {
/* 276 */     return this.addressBookId;
  }
  public void setAddressBookId(Long addressBookId) {
/* 279 */     this.addressBookId = addressBookId;
  }
  public String getCustomerName() {
/* 282 */     return this.customerName;
  }
  public void setCustomerName(String customerName) {
/* 285 */     this.customerName = customerName;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CallCdrBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */