package in.wringg.pojo;

public class BulkSmsBean {
/*  4 */   private Integer taskId = Integer.valueOf(1223);
/*  5 */   private String status = "Editable";
/*  6 */   private Integer smsCount = Integer.valueOf(123);
/*  7 */   private Integer numberCount = Integer.valueOf(89);
/*  8 */   private String smsType = "Test";
  public Integer getTaskId() {
/* 10 */     return this.taskId;
  }
  public void setTaskId(Integer taskId) {
/* 13 */     this.taskId = taskId;
  }
  public String getStatus() {
/* 16 */     return this.status;
  }
  public void setStatus(String status) {
/* 19 */     this.status = status;
  }
  public Integer getSmsCount() {
/* 22 */     return this.smsCount;
  }
  public void setSmsCount(Integer smsCount) {
/* 25 */     this.smsCount = smsCount;
  }
  public Integer getNumberCount() {
/* 28 */     return this.numberCount;
  }
  public void setNumberCount(Integer numberCount) {
/* 31 */     this.numberCount = numberCount;
  }
  public String getSmsType() {
/* 34 */     return this.smsType;
  }
  public void setSmsType(String smsType) {
/* 37 */     this.smsType = smsType;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\BulkSmsBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */