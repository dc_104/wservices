package in.wringg.pojo;

public class CdrBean
{
/*  5 */   private String callId = "CDL1234";
/*  6 */   private String startDate = "2017-05-06 12:12:45";
/*  7 */   private String endDate = "2017-07-06 12:01:45";
/*  8 */   private String huntingnumber = "+324232235";
/*  9 */   private String mobileNumber = "24322+";
/* 10 */   private String confrence = "yes";
/* 11 */   private String missCall = "no";
/* 12 */   private String inbound = "yes";
  
  public String getCallId() {
/* 15 */     return this.callId;
  }
  public void setCallId(String callId) {
/* 18 */     this.callId = callId;
  }
  public String getStartDate() {
/* 21 */     return this.startDate;
  }
  public void setStartDate(String startDate) {
/* 24 */     this.startDate = startDate;
  }
  public String getEndDate() {
/* 27 */     return this.endDate;
  }
  public void setEndDate(String endDate) {
/* 30 */     this.endDate = endDate;
  }
  public String getHuntingnumber() {
/* 33 */     return this.huntingnumber;
  }
  public void setHuntingnumber(String huntingnumber) {
/* 36 */     this.huntingnumber = huntingnumber;
  }
  public String getMobileNumber() {
/* 39 */     return this.mobileNumber;
  }
  public void setMobileNumber(String mobileNumber) {
/* 42 */     this.mobileNumber = mobileNumber;
  }
  public String getConfrence() {
/* 45 */     return this.confrence;
  }
  public void setConfrence(String confrence) {
/* 48 */     this.confrence = confrence;
  }
  public String getMissCall() {
/* 51 */     return this.missCall;
  }
  public void setMissCall(String missCall) {
/* 54 */     this.missCall = missCall;
  }
  public String getInbound() {
/* 57 */     return this.inbound;
  }
  public void setInbound(String inbound) {
/* 60 */     this.inbound = inbound;
  }

  
  public CdrBean(String callId, String startDate, String endDate, String huntingnumber, String mobileNumber, String confrence, String missCall, String inbound) {
/* 65 */     this.callId = callId;
/* 66 */     this.startDate = startDate;
/* 67 */     this.endDate = endDate;
/* 68 */     this.huntingnumber = huntingnumber;
/* 69 */     this.mobileNumber = mobileNumber;
/* 70 */     this.confrence = confrence;
/* 71 */     this.missCall = missCall;
/* 72 */     this.inbound = inbound;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CdrBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */