package in.wringg.pojo;

import java.util.Date;

public class SmeComplaintBean {
  Long id;
  
  public String toString() {
/*   9 */     return "SmeComplaintBean [id=" + this.id + ", assigneeName=" + this.assigneeName + ", complaintDateTime=" + this.complaintDateTime + ", solutionDateTime=" + this.solutionDateTime + ", complaintDetail=" + this.complaintDetail + ", emailId=" + this.emailId + ", category=" + this.category + ", smeId=" + this.smeId + ", parentId=" + this.parentId + ", status=" + this.status + ", subject=" + this.subject + ", solution=" + this.solution + ", smeName=" + this.smeName + ", smeEmailId=" + this.smeEmailId + "]";
  }

  
  String assigneeName;
  
  Date complaintDateTime;
  
  Date solutionDateTime;
  
  String complaintDetail;
  String emailId;
  Integer category;
  Long smeId;
  Long parentId;
  Byte status;
  String subject;
  String solution;
  String smeName;
  String smeEmailId;
  
  public String getSmeName() {
/*  31 */     return this.smeName;
  }
  public void setSmeName(String smeName) {
/*  34 */     this.smeName = smeName;
  }
  public String getSmeEmailId() {
/*  37 */     return this.smeEmailId;
  }
  public void setSmeEmailId(String smeEmailId) {
/*  40 */     this.smeEmailId = smeEmailId;
  }
  public String getSolution() {
/*  43 */     return this.solution;
  }
  public void setSolution(String solution) {
/*  46 */     this.solution = solution;
  }
  public Long getId() {
/*  49 */     return this.id;
  }
  public void setId(Long id) {
/*  52 */     this.id = id;
  }
  public String getAssigneeName() {
/*  55 */     return this.assigneeName;
  }
  public void setAssigneeName(String assigneeName) {
/*  58 */     this.assigneeName = assigneeName;
  }
  public Date getComplaintDateTime() {
/*  61 */     return this.complaintDateTime;
  }
  public void setComplaintDateTime(Date complaintDateTime) {
/*  64 */     this.complaintDateTime = complaintDateTime;
  }
  public Date getSolutionDateTime() {
/*  67 */     return this.solutionDateTime;
  }
  public void setSolutionDateTime(Date solutionDateTime) {
/*  70 */     this.solutionDateTime = solutionDateTime;
  }
  public String getComplaintDetail() {
/*  73 */     return this.complaintDetail;
  }
  public void setComplaintDetail(String complaintDetail) {
/*  76 */     this.complaintDetail = complaintDetail;
  }
  public String getEmailId() {
/*  79 */     return this.emailId;
  }
  public void setEmailId(String emailId) {
/*  82 */     this.emailId = emailId;
  }
  public Integer getCategory() {
/*  85 */     return this.category;
  }
  public void setCategory(Integer category) {
/*  88 */     this.category = category;
  }
  public Long getSmeId() {
/*  91 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/*  94 */     this.smeId = smeId;
  }
  public Long getParentId() {
/*  97 */     return this.parentId;
  }
  public void setParentId(Long parentId) {
/* 100 */     this.parentId = parentId;
  }
  public Byte getStatus() {
/* 103 */     return this.status;
  }
  public void setStatus(Byte status) {
/* 106 */     this.status = status;
  }
  public String getSubject() {
/* 109 */     return this.subject;
  }
  public void setSubject(String subject) {
/* 112 */     this.subject = subject;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmeComplaintBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */