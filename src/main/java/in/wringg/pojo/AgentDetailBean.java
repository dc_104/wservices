package in.wringg.pojo;

import java.sql.Time;
import java.util.Date;
import java.util.List;

public class AgentDetailBean {
	private Long agentId;
	private String agentMobile;
	private String agentName;
	private Time inTime;
	private Date insertTime;
	private Time outTime;
	private Long smeId;
	private Byte status;
	private String inDate;
	private String outDate;
	private Integer daysFlag;
	private Integer agentExtention;
	private String agentEmail;
	private Byte stickyAgent;
	private Byte agentMasking;
	private Integer agentScore;
	private Integer stickyDays;
	private Byte assignFailedCalls;
	private Byte assignVoicemailCalls;
	private Boolean inPermissionFlag;
	private Boolean outPermissionFlag;

	List<Long> agentGroups;

	public Boolean getInPermissionFlag() {
		return inPermissionFlag;
	}

	public void setInPermissionFlag(Boolean inPermissionFlag) {
		this.inPermissionFlag = inPermissionFlag;
	}

	public Boolean getOutPermissionFlag() {
		return outPermissionFlag;
	}

	public void setOutPermissionFlag(Boolean outPermissionFlag) {
		this.outPermissionFlag = outPermissionFlag;
	}

	public Integer getAgentScore() {
		/* 31 */ return this.agentScore;
	}

	public void setAgentScore(Integer agentScore) {
		/* 35 */ this.agentScore = agentScore;
	}

	public Integer getAgentExtention() {
		/* 39 */ return this.agentExtention;
	}

	public void setAgentExtention(Integer agentExtention) {
		/* 43 */ this.agentExtention = agentExtention;
	}

	public String getAgentEmail() {
		/* 47 */ return this.agentEmail;
	}

	public void setAgentEmail(String agentEmail) {
		/* 51 */ this.agentEmail = agentEmail;
	}

	public Integer getDaysFlag() {
		/* 55 */ return this.daysFlag;
	}

	public void setDaysFlag(Integer daysFlag) {
		/* 59 */ this.daysFlag = daysFlag;
	}

	public String getInDate() {
		/* 63 */ return this.inDate;
	}

	public void setInDate(String inDate) {
		/* 67 */ this.inDate = inDate;
	}

	public String getOutDate() {
		/* 71 */ return this.outDate;
	}

	public void setOutDate(String outDate) {
		/* 75 */ this.outDate = outDate;
	}

	public List<Long> getAgentGroups() {
		/* 81 */ return this.agentGroups;
	}

	public void setAgentGroups(List<Long> agentGroups) {
		/* 85 */ this.agentGroups = agentGroups;
	}

	public Long getAgentId() {
		/* 89 */ return this.agentId;
	}

	public void setAgentId(Long agentId) {
		/* 93 */ this.agentId = agentId;
	}

	public String getAgentMobile() {
		/* 97 */ return this.agentMobile;
	}

	public void setAgentMobile(String agentMobile) {
		/* 101 */ this.agentMobile = agentMobile;
	}

	public String getAgentName() {
		/* 105 */ return this.agentName;
	}

	public void setAgentName(String agentName) {
		/* 109 */ this.agentName = agentName;
	}

	public Time getInTime() {
		/* 113 */ return this.inTime;
	}

	public void setInTime(Time inTime) {
		/* 117 */ this.inTime = inTime;
	}

	public Date getInsertTime() {
		/* 121 */ return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		/* 125 */ this.insertTime = insertTime;
	}

	public Time getOutTime() {
		/* 129 */ return this.outTime;
	}

	public void setOutTime(Time outTime) {
		/* 133 */ this.outTime = outTime;
	}

	public Long getSmeId() {
		/* 137 */ return this.smeId;
	}

	public void setSmeId(Long smeId) {
		/* 141 */ this.smeId = smeId;
	}

	public Byte getStatus() {
		/* 145 */ return this.status;
	}

	public void setStatus(Byte status) {
		/* 149 */ this.status = status;
	}

	public Byte getStickyAgent() {
		/* 153 */ return this.stickyAgent;
	}

	public void setStickyAgent(Byte stickyAgent) {
		/* 157 */ this.stickyAgent = stickyAgent;
	}

	public Byte getAgentMasking() {
		/* 161 */ return this.agentMasking;
	}

	public void setAgentMasking(Byte agentMasking) {
		/* 165 */ this.agentMasking = agentMasking;
	}

	public Integer getStickyDays() {
		/* 169 */ return this.stickyDays;
	}

	public void setStickyDays(Integer stickyDays) {
		/* 173 */ this.stickyDays = stickyDays;
	}

	public Byte getAssignFailedCalls() {
		/* 177 */ return this.assignFailedCalls;
	}

	public void setAssignFailedCalls(Byte assignFailedCalls) {
		/* 181 */ this.assignFailedCalls = assignFailedCalls;
	}

	public Byte getAssignVoicemailCalls() {
		/* 185 */ return this.assignVoicemailCalls;
	}

	public void setAssignVoicemailCalls(Byte assignVoicemailCalls) {
		/* 189 */ this.assignVoicemailCalls = assignVoicemailCalls;
	}
}
