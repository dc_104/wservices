package in.wringg.pojo;


public class OutBoundIvrBean
{
/*  6 */   private String ivrName = "Pizza Hut";
/*  7 */   private String systemId = "OBD 16527";
/*  8 */   private Integer callCount = Integer.valueOf(123);
/*  9 */   private String dateTime = "2017-05-23 13:23:13";
/* 10 */   private String status = "Delivered";
  public String getIvrName() {
/* 12 */     return this.ivrName;
  }
  public void setIvrName(String ivrName) {
/* 15 */     this.ivrName = ivrName;
  }
  public String getSystemId() {
/* 18 */     return this.systemId;
  }
  public void setSystemId(String systemId) {
/* 21 */     this.systemId = systemId;
  }
  public Integer getCallCount() {
/* 24 */     return this.callCount;
  }
  public void setCallCount(Integer callCount) {
/* 27 */     this.callCount = callCount;
  }
  public String getDateTime() {
/* 30 */     return this.dateTime;
  }
  public void setDateTime(String dateTime) {
/* 33 */     this.dateTime = dateTime;
  }
  public String getStatus() {
/* 36 */     return this.status;
  }
  public void setStatus(String status) {
/* 39 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\OutBoundIvrBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */