package in.wringg.pojo;

public class CallCdrResponseBean {
  private String id;
  private String agentGroup;
  private String callDirection;
  private String callDirectionStatus;
  private String callRecordedFile;
  private String callRecordingStatus;
  private String calledNumber;
  private String callingNumber;
  private String cdrMode;
  private String channelNo;
  private String duration;
  private String endDateTime;
  private String endDateTimeS;
  private String hlr;
  private String insertDateTime;
  private String insertDateTimeS;
  private String longcode;
  private String masterShortcode;
  private String patchedAgentId;
  private String serverIpAddress;
  private String shortcodeMapping;
  private String smeIdentifier;
  private String startDateTimeS;
  private String voicemailRecordingFile;
  private String groupId;
  private String callId;
  private String agentName;
  private byte answer;
  private byte callStatus;
  private String disconnectedBy;
  private Byte mergeStatus;
  private Long addressBookId;
  private String customerName;
  private String voicemailRecordingStatus;
  
  public String getId() {
/*  40 */     return this.id;
  }
  public void setId(String id) {
/*  43 */     this.id = id;
  }
  public String getAgentGroup() {
/*  46 */     return this.agentGroup;
  }
  public void setAgentGroup(String agentGroup) {
/*  49 */     this.agentGroup = agentGroup;
  }
  public String getCallDirection() {
/*  52 */     return this.callDirection;
  }
  public void setCallDirection(String callDirection) {
/*  55 */     this.callDirection = callDirection;
  }
  public String getCallDirectionStatus() {
/*  58 */     return this.callDirectionStatus;
  }
  public void setCallDirectionStatus(String callDirectionStatus) {
/*  61 */     this.callDirectionStatus = callDirectionStatus;
  }
  public String getCallRecordedFile() {
/*  64 */     return this.callRecordedFile;
  }
  public void setCallRecordedFile(String callRecordedFile) {
/*  67 */     this.callRecordedFile = callRecordedFile;
  }
  public String getCallRecordingStatus() {
/*  70 */     return this.callRecordingStatus;
  }
  public void setCallRecordingStatus(String callRecordingStatus) {
/*  73 */     this.callRecordingStatus = callRecordingStatus;
  }
  public String getCalledNumber() {
/*  76 */     return this.calledNumber;
  }
  public void setCalledNumber(String calledNumber) {
/*  79 */     this.calledNumber = calledNumber;
  }
  public String getCallingNumber() {
/*  82 */     return this.callingNumber;
  }
  public void setCallingNumber(String callingNumber) {
/*  85 */     this.callingNumber = callingNumber;
  }
  public String getCdrMode() {
/*  88 */     return this.cdrMode;
  }
  public void setCdrMode(String cdrMode) {
/*  91 */     this.cdrMode = cdrMode;
  }
  public String getChannelNo() {
/*  94 */     return this.channelNo;
  }
  public void setChannelNo(String channelNo) {
/*  97 */     this.channelNo = channelNo;
  }
  public String getDuration() {
/* 100 */     return this.duration;
  }
  public void setDuration(String duration) {
/* 103 */     this.duration = duration;
  }
  public String getEndDateTime() {
/* 106 */     return this.endDateTime;
  }
  public void setEndDateTime(String endDateTime) {
/* 109 */     this.endDateTime = endDateTime;
  }
  public String getEndDateTimeS() {
/* 112 */     return this.endDateTimeS;
  }
  public void setEndDateTimeS(String endDateTimeS) {
/* 115 */     this.endDateTimeS = endDateTimeS;
  }
  public String getHlr() {
/* 118 */     return this.hlr;
  }
  public void setHlr(String hlr) {
/* 121 */     this.hlr = hlr;
  }
  public String getInsertDateTime() {
/* 124 */     return this.insertDateTime;
  }
  public void setInsertDateTime(String insertDateTime) {
/* 127 */     this.insertDateTime = insertDateTime;
  }
  public String getInsertDateTimeS() {
/* 130 */     return this.insertDateTimeS;
  }
  public void setInsertDateTimeS(String insertDateTimeS) {
/* 133 */     this.insertDateTimeS = insertDateTimeS;
  }
  public String getLongcode() {
/* 136 */     return this.longcode;
  }
  public void setLongcode(String longcode) {
/* 139 */     this.longcode = longcode;
  }
  public String getMasterShortcode() {
/* 142 */     return this.masterShortcode;
  }
  public void setMasterShortcode(String masterShortcode) {
/* 145 */     this.masterShortcode = masterShortcode;
  }
  public String getPatchedAgentId() {
/* 148 */     return this.patchedAgentId;
  }
  public void setPatchedAgentId(String patchedAgentId) {
/* 151 */     this.patchedAgentId = patchedAgentId;
  }
  public String getServerIpAddress() {
/* 154 */     return this.serverIpAddress;
  }
  public void setServerIpAddress(String serverIpAddress) {
/* 157 */     this.serverIpAddress = serverIpAddress;
  }
  public String getShortcodeMapping() {
/* 160 */     return this.shortcodeMapping;
  }
  public void setShortcodeMapping(String shortcodeMapping) {
/* 163 */     this.shortcodeMapping = shortcodeMapping;
  }
  
  public String getSmeIdentifier() {
/* 167 */     return this.smeIdentifier;
  }
  public void setSmeIdentifier(String smeIdentifier) {
/* 170 */     this.smeIdentifier = smeIdentifier;
  }
  public String getStartDateTimeS() {
/* 173 */     return this.startDateTimeS;
  }
  public void setStartDateTimeS(String startDateTimeS) {
/* 176 */     this.startDateTimeS = startDateTimeS;
  }
  public String getVoicemailRecordingFile() {
/* 179 */     return this.voicemailRecordingFile;
  }
  public void setVoicemailRecordingFile(String voicemailRecordingFile) {
/* 182 */     this.voicemailRecordingFile = voicemailRecordingFile;
  }
  public String getGroupId() {
/* 185 */     return this.groupId;
  }
  public void setGroupId(String groupId) {
/* 188 */     this.groupId = groupId;
  }
  public String getVoicemailRecordingStatus() {
/* 191 */     return this.voicemailRecordingStatus;
  }
  public void setVoicemailRecordingStatus(String voicemailRecordingStatus) {
/* 194 */     this.voicemailRecordingStatus = voicemailRecordingStatus;
  }
  public String getCallId() {
/* 197 */     return this.callId;
  }
  public void setCallId(String callId) {
/* 200 */     this.callId = callId;
  }
  public String getAgentName() {
/* 203 */     return this.agentName;
  }
  public void setAgentName(String agentName) {
/* 206 */     this.agentName = agentName;
  }
  public Byte getMergeStatus() {
/* 209 */     return this.mergeStatus;
  }
  public void setMergeStatus(Byte mergeStatus) {
/* 212 */     this.mergeStatus = mergeStatus;
  }
  public byte getAnswer() {
/* 215 */     return this.answer;
  }
  public void setAnswer(byte answer) {
/* 218 */     this.answer = answer;
  }
  
  public byte getCallStatus() {
/* 222 */     return this.callStatus;
  }
  public void setCallStatus(byte callStatus) {
/* 225 */     this.callStatus = callStatus;
  }
  public String getDisconnectedBy() {
/* 228 */     return this.disconnectedBy;
  }
  public void setDisconnectedBy(String disconnectedBy) {
/* 231 */     this.disconnectedBy = disconnectedBy;
  }
  public Long getAddressBookId() {
/* 234 */     return this.addressBookId;
  }
  public void setAddressBookId(Long addressBookId) {
/* 237 */     this.addressBookId = addressBookId;
  }
  public String getCustomerName() {
/* 240 */     return this.customerName;
  }
  public void setCustomerName(String customerName) {
/* 243 */     this.customerName = customerName;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CallCdrResponseBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */