package in.wringg.pojo;

import in.wringg.utility.Utility;
import java.util.Date;

public class AdminDashboardRequest
{
  private Date startDate;
  private Date endDate;
  
  public Date getStartDate() {
/* 12 */     return Utility.getFormattedFromDateTime(this.startDate);
  }
  public void setStartDate(Date startDate) {
/* 15 */     this.startDate = startDate;
  }
  public Date getEndDate() {
/* 18 */     return Utility.getFormattedToDateTime(this.endDate);
  }
  public void setEndDate(Date endDate) {
/* 21 */     this.endDate = endDate;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AdminDashboardRequest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */