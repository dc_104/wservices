package in.wringg.pojo;

import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

public class AddOutBoundIvrBean {
  private BigInteger id;
  private String ivrSystemId;
  private Integer numberOfCalls;
  private Date date;
  private Time startTime;
  private Time endTime;
  private String baseFileName;
  private String ivrName;
  private Long smeId;
  private Byte status;
  private String ivrType;
  private String cli;
  private String mediaFile;
  private Integer simontanouslyCalls;
  
  public Long getSmeId() {
/*  24 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/*  28 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/*  32 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/*  36 */     this.status = status;
  }
  
  public String getIvrType() {
/*  40 */     return this.ivrType;
  }
  
  public void setIvrType(String ivrType) {
/*  44 */     this.ivrType = ivrType;
  }
  
  public String getCli() {
/*  48 */     return this.cli;
  }
  
  public void setCli(String cli) {
/*  52 */     this.cli = cli;
  }
  
  public String getBaseFileName() {
/*  56 */     return this.baseFileName;
  }
  
  public void setBaseFileName(String baseFileName) {
/*  60 */     this.baseFileName = baseFileName;
  }
  
  public String getIvrName() {
/*  64 */     return this.ivrName;
  }
  
  public void setIvrName(String ivrName) {
/*  68 */     this.ivrName = ivrName;
  }
  
  public String getIvrSystemId() {
/*  72 */     return this.ivrSystemId;
  }
  
  public void setIvrSystemId(String ivrSystemId) {
/*  76 */     this.ivrSystemId = ivrSystemId;
  }
  
  public Integer getNumberOfCalls() {
/*  80 */     return this.numberOfCalls;
  }
  
  public void setNumberOfCalls(Integer numberOfCalls) {
/*  84 */     this.numberOfCalls = numberOfCalls;
  }
  
  public Date getDate() {
/*  88 */     return this.date;
  }
  
  public void setDate(Date date) {
/*  92 */     this.date = date;
  }
  
  public Time getStartTime() {
/*  96 */     return this.startTime;
  }
  
  public void setStartTime(Time startTime) {
/* 100 */     this.startTime = startTime;
  }
  
  public Time getEndTime() {
/* 104 */     return this.endTime;
  }
  
  public void setEndTime(Time endTime) {
/* 108 */     this.endTime = endTime;
  }
  
  public String getMediaFile() {
/* 112 */     return this.mediaFile;
  }
  
  public void setMediaFile(String mediaFile) {
/* 116 */     this.mediaFile = mediaFile;
  }
  
  public Integer getSimontanouslyCalls() {
/* 120 */     return this.simontanouslyCalls;
  }
  
  public void setSimontanouslyCalls(Integer simontanouslyCalls) {
/* 124 */     this.simontanouslyCalls = simontanouslyCalls;
  }
  
  public BigInteger getId() {
/* 128 */     return this.id;
  }
  
  public void setId(BigInteger id) {
/* 132 */     this.id = id;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AddOutBoundIvrBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */