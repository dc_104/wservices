package in.wringg.pojo;

public class UserRoleDetailBean {
	private String userName;
	private String roles;
	Boolean inPermissionFlag;
	Boolean outPermissionFlag;
	private AdminSmeProfileBean profileDetail;

	
	public Boolean getInPermissionFlag() {
		return inPermissionFlag;
	}
	public void setInPermissionFlag(Boolean inPermissionFlag) {
		this.inPermissionFlag = inPermissionFlag;
	}
	public Boolean getOutPermissionFlag() {
		return outPermissionFlag;
	}
	public void setOutPermissionFlag(Boolean outPermissionFlag) {
		this.outPermissionFlag = outPermissionFlag;
	}
	public String getUserName() {
		return this.userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoles() {
		return this.roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public AdminSmeProfileBean getProfileDetail() {
		if (this.profileDetail == null)
			this.profileDetail = new AdminSmeProfileBean();
		return this.profileDetail;
	}

	public void setProfileDetail(AdminSmeProfileBean profileDetail) {
		this.profileDetail = profileDetail;
	}
}
