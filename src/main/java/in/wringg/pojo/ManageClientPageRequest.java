package in.wringg.pojo;

import in.wringg.pojo.FilterBean;
import in.wringg.utility.Utility;
import java.util.Date;
import java.util.List;


public class ManageClientPageRequest
{
  private Long id;
  private Date startDate;
  private Date endDate;
  
  public Long getId() {
/* 16 */     return this.id;
  } private int initialRecord; private int batchSize; private List<FilterBean> filterList;
  public void setId(Long id) {
/* 19 */     this.id = id;
  }
  
  public int getInitialRecord() {
/* 23 */     return this.initialRecord;
  }
  public void setInitialRecord(int initialRecord) {
/* 26 */     this.initialRecord = initialRecord;
  }
  public int getBatchSize() {
/* 29 */     return this.batchSize;
  }
  public void setBatchSize(int batchSize) {
/* 32 */     this.batchSize = batchSize;
  }
  public List<FilterBean> getFilterList() {
/* 35 */     return this.filterList;
  }
  public void setFilterList(List<FilterBean> filterList) {
/* 38 */     this.filterList = filterList;
  }
  public Date getStartDate() {
/* 41 */     return Utility.getFormattedFromDateTime(this.startDate);
  }
  public void setStartDate(Date startDate) {
/* 44 */     this.startDate = startDate;
  }
  public Date getEndDate() {
/* 47 */     return Utility.getFormattedToDateTime(this.endDate);
  }
  public void setEndDate(Date endDate) {
/* 50 */     this.endDate = endDate;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\ManageClientPageRequest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */