package in.wringg.pojo;

import in.wringg.utility.Utility;
import java.util.Date;


public class StartEndDateRequest
{
  private Long id;
  private Long smeId;
  private Long agentId;
  private Date startDate;
  private Date endDate;
  private String message;
  
  public Date getStartDate() {
/* 17 */     return Utility.getFormattedFromDateTime(this.startDate);
  }
  public void setStartDate(Date startDate) {
/* 20 */     this.startDate = startDate;
  }
  public Date getEndDate() {
/* 23 */     return Utility.getFormattedToDateTime(this.endDate);
  }
  public void setEndDate(Date endDate) {
/* 26 */     this.endDate = endDate;
  }
  public Long getId() {
/* 29 */     return this.id;
  }
  public void setId(Long id) {
/* 32 */     this.id = id;
  }
  public Long getAgentId() {
/* 35 */     return this.agentId;
  }
  public void setAgentId(Long agentId) {
/* 38 */     this.agentId = agentId;
  }
  public Date getActualStartDate() {
/* 41 */     return this.startDate;
  }
  public Date getActualEndDate() {
/* 44 */     return this.endDate;
  }
  public Long getSmeId() {
/* 47 */     return this.smeId;
  }
  public void setSmeId(Long smeId) {
/* 50 */     this.smeId = smeId;
  }
  public String getMessage() {
/* 53 */     return this.message;
  }
  public void setMessage(String message) {
/* 56 */     this.message = message;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\StartEndDateRequest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */