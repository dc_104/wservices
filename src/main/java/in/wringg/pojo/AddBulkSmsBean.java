package in.wringg.pojo;

import java.sql.Time;
import java.util.Date;

public class AddBulkSmsBean
{
  public String toString() {
/*   9 */     return "AddBulkSmsBean [smsContent=" + this.smsContent + ", smsContentType=" + this.smsContentType + ", cli=" + this.cli + ", date=" + this.date + ", startTime=" + this.startTime + ", campaignId=" + this.campaignId + ", filePath=" + this.filePath + ", baseCount=" + this.baseCount + ", status=" + this.status + ", endTime=" + this.endTime + "]";
  }
  
  private String smsContent;
  private byte smsContentType;
  private String cli;
  private Date date;
  private Time startTime;
  private Long campaignId;
  private String filePath;
  private Integer baseCount;
  private byte status;
  private Time endTime;
  
  public Long getCampaignId() {
/*  24 */     return this.campaignId;
  }
  
  public void setCampaignId(Long campaignId) {
/*  28 */     this.campaignId = campaignId;
  }


  
  public Date getDate() {
/*  34 */     return this.date;
  }
  
  public void setDate(Date date) {
/*  38 */     this.date = date;
  }
  
  public Time getStartTime() {
/*  42 */     return this.startTime;
  }
  
  public void setStartTime(Time startTime) {
/*  46 */     this.startTime = startTime;
  }
  
  public Time getEndTime() {
/*  50 */     return this.endTime;
  }
  
  public void setEndTime(Time endTime) {
/*  54 */     this.endTime = endTime;
  }
  
  public String getSmsContent() {
/*  58 */     return this.smsContent;
  }
  
  public void setSmsContent(String smsContent) {
/*  62 */     this.smsContent = smsContent;
  }
  
  public byte getSmsContentType() {
/*  66 */     return this.smsContentType;
  }
  
  public void setSmsContentType(byte smsContentType) {
/*  70 */     this.smsContentType = smsContentType;
  }
  
  public String getCli() {
/*  74 */     return this.cli;
  }
  
  public void setCli(String cli) {
/*  78 */     this.cli = cli;
  }
  
  public String getFilePath() {
/*  82 */     return this.filePath;
  }
  
  public void setFilePath(String filePath) {
/*  86 */     this.filePath = filePath;
  }
  
  public Integer getBaseCount() {
/*  90 */     return this.baseCount;
  }
  
  public void setBaseCount(Integer baseCount) {
/*  94 */     this.baseCount = baseCount;
  }
  
  public byte getStatus() {
/*  98 */     return this.status;
  }
  
  public void setStatus(byte status) {
/* 102 */     this.status = status;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AddBulkSmsBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */