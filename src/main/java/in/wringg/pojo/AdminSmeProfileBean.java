package in.wringg.pojo;

import java.util.List;
import java.util.Set;

public class AdminSmeProfileBean extends AgentDetailBean {
	private long id;
	private String smeName;
	private String emailId;
	private String smeMobile;
	private String alternateNumber;
	private int allowedAgents;
	private long longCodeId;
//	private List<Long> longCodes;
//	private String strLongcodes;
	private long zoneId;
	private int serviceFlag;
	private Double balance;
	private Boolean recording;
	private Boolean conference;
	private Boolean masking;
	private Boolean voicemail;
	private Boolean inPermissionFlag;
	private Boolean outPermissionFlag;
	private Boolean eodReportFlag;
	private int routingType;
	private int inChannels;
	private int inQueueChannels;
	private int outChannels;
	private int guiTimer;
	private int agentRelaxTime;

//	public List<Long> getLongCodes() {
//		return longCodes;
//	}
//
//	public void setLongCodes(List<Long> longCodes) {
//		this.longCodes = longCodes;
//	}

	public Boolean getInPermissionFlag() {
		return inPermissionFlag;
	}

	public void setInPermissionFlag(Boolean inPermissionFlag) {
		this.inPermissionFlag = inPermissionFlag;
	}

	public Boolean getOutPermissionFlag() {
		return outPermissionFlag;
	}

	public void setOutPermissionFlag(Boolean outPermissionFlag) {
		this.outPermissionFlag = outPermissionFlag;
	}

	public String getSelectionAlgo() {
		/* 35 */ return this.selectionAlgo;
	}

	private String selectionAlgo;
	private String bizAddress;
	private Long longCode;
	private Long inShortCode;
	private Long outShortCode;
	private int recValidity;
	private Long virtualLongCode;
	private Byte language;
	private char clientType;
	private String accountSid;
	private String userToken;
	private Integer queueLimit;
	private String callBackUrl;
	private Byte stickyAlgo;

	public void setSelectionAlgo(String selectionAlgo) {
		/* 39 */ this.selectionAlgo = selectionAlgo;
	}

	public Boolean getMasking() {
		/* 43 */ return this.masking;
	}

	public void setMasking(Boolean masking) {
		/* 47 */ this.masking = masking;
	}

	public Boolean getRecording() {
		/* 51 */ return this.recording;
	}

	public void setRecording(Boolean recording) {
		/* 55 */ this.recording = recording;
	}

	public long getId() {
		/* 59 */ return this.id;
	}

	public void setId(long id) {
		/* 63 */ this.id = id;
	}

	public long getLongCodeId() {
		/* 67 */ return this.longCodeId;
	}

	public void setLongCodeId(long longCodeId) {
		/* 71 */ this.longCodeId = longCodeId;
	}

	public long getZoneId() {
		/* 75 */ return this.zoneId;
	}

	public void setZoneId(long zoneId) {
		/* 79 */ this.zoneId = zoneId;
	}

	public String getSmeName() {
		/* 88 */ return this.smeName;
	}

	public void setSmeName(String smeName) {
		/* 92 */ this.smeName = smeName;
	}

	public String getEmailId() {
		/* 96 */ return this.emailId;
	}

	public void setEmailId(String emailId) {
		/* 100 */ this.emailId = emailId;
	}

	public String getSmeMobile() {
		/* 104 */ return this.smeMobile;
	}

	public void setSmeMobile(String smeMobile) {
		/* 108 */ this.smeMobile = smeMobile;
	}

	public String getAlternateNumber() {
		/* 112 */ return this.alternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		/* 116 */ this.alternateNumber = alternateNumber;
	}

	public int getAllowedAgents() {
		/* 120 */ return this.allowedAgents;
	}

	public void setAllowedAgents(int allowedAgents) {
		/* 124 */ this.allowedAgents = allowedAgents;
	}

	public int getServiceFlag() {
		/* 128 */ return this.serviceFlag;
	}

	public void setServiceFlag(int serviceFlag) {
		/* 132 */ this.serviceFlag = serviceFlag;
	}

	public String toString() {
		/* 137 */ return "AdminSmeProfileBean [id=" + this.id + ", smeName=" + this.smeName + ", emailId="
				+ this.emailId + ", smeMobile=" + this.smeMobile + ", alternateNumber=" + this.alternateNumber
				+ ", allowedAgents=" + this.allowedAgents + ", longCodeId=" + this.longCodeId + ", zoneId="
				+ this.zoneId + ", serviceFlag=" + this.serviceFlag + ",  balance=" + this.balance + ", recording="
				+ this.recording + ", conference=" + this.conference + ", masking=" + this.masking + ", voicemail="
				+ this.voicemail + ", selectionAlgo=" + this.selectionAlgo + ", bizAddress=" + this.bizAddress
				+ ", longCode=" + this.longCode + ", inShortCode=" + this.inShortCode + ", outShortCode="
				+ this.outShortCode + ", recValidity=" + this.recValidity + ", virtualLongCode=" + this.virtualLongCode
				+ ", language=" + this.language + ", clientType=" + this.clientType + "]";
	}

	public Double getBalance() {
		/* 142 */ return this.balance;
	}

//	@Override
//	public String toString() {
//		return "AdminSmeProfileBean [id=" + id + ", smeName=" + smeName + ", emailId=" + emailId + ", smeMobile="
//				+ smeMobile + ", alternateNumber=" + alternateNumber + ", allowedAgents=" + allowedAgents
//				+ ", longCodeId=" + longCodeId + ", longCodes=" + longCodes + ", zoneId=" + zoneId + ", serviceFlag="
//				+ serviceFlag + ", balance=" + balance + ", recording=" + recording + ", conference=" + conference
//				+ ", masking=" + masking + ", voicemail=" + voicemail + ", inPermissionFlag=" + inPermissionFlag
//				+ ", outPermissionFlag=" + outPermissionFlag + ", eodReportFlag=" + eodReportFlag + ", routingType="
//				+ routingType + ", inChannels=" + inChannels + ", inQueueChannels=" + inQueueChannels + ", outChannels="
//				+ outChannels + ", guiTimer=" + guiTimer + ", agentRelaxTime=" + agentRelaxTime + ", selectionAlgo="
//				+ selectionAlgo + ", bizAddress=" + bizAddress + ", longCode=" + longCode + ", inShortCode="
//				+ inShortCode + ", outShortCode=" + outShortCode + ", recValidity=" + recValidity + ", virtualLongCode="
//				+ virtualLongCode + ", language=" + language + ", clientType=" + clientType + ", accountSid="
//				+ accountSid + ", userToken=" + userToken + ", queueLimit=" + queueLimit + ", callBackUrl="
//				+ callBackUrl + ", stickyAlgo=" + stickyAlgo + "]";
//	}

	public void setBalance(Double balance) {
		/* 146 */ this.balance = balance;
	}

	public String getBizAddress() {
		/* 150 */ return this.bizAddress;
	}

	public void setBizAddress(String bizAddress) {
		/* 154 */ this.bizAddress = bizAddress;
	}

	public Boolean getVoicemail() {
		/* 158 */ return this.voicemail;
	}

	public void setVoicemail(Boolean voicemail) {
		/* 162 */ this.voicemail = voicemail;
	}

	public Long getLongCode() {
		/* 166 */ return this.longCode;
	}

	public void setLongCode(Long longCode) {
		/* 170 */ this.longCode = longCode;
	}

	public Long getInShortCode() {
		/* 174 */ return this.inShortCode;
	}

	public void setInShortCode(Long inShortCode) {
		/* 178 */ this.inShortCode = inShortCode;
	}

	public Long getOutShortCode() {
		/* 182 */ return this.outShortCode;
	}

	public void setOutShortCode(Long outShortCode) {
		/* 186 */ this.outShortCode = outShortCode;
	}

	public int getRecValidity() {
		/* 190 */ return this.recValidity;
	}

	public void setRecValidity(int recValidity) {
		/* 194 */ this.recValidity = recValidity;
	}

	public Byte getLanguage() {
		/* 198 */ return this.language;
	}

	public void setLanguage(Byte language) {
		/* 202 */ this.language = language;
	}

	public char getClientType() {
		/* 206 */ return this.clientType;
	}

	public void setClientType(char clientType) {
		/* 210 */ this.clientType = clientType;
	}

	public Boolean getConference() {
		/* 214 */ return this.conference;
	}

	public void setConference(Boolean conference) {
		/* 218 */ this.conference = conference;
	}

	public Long getVirtualLongCode() {
		/* 222 */ return this.virtualLongCode;
	}

	public void setVirtualLongCode(Long virtualLongCode) {
		/* 226 */ this.virtualLongCode = virtualLongCode;
	}

	public String getAccountSid() {
		/* 230 */ return this.accountSid;
	}

	public void setAccountSid(String accountSid) {
		/* 234 */ this.accountSid = accountSid;
	}

	public String getUserToken() {
		/* 238 */ return this.userToken;
	}

	public void setUserToken(String userToken) {
		/* 242 */ this.userToken = userToken;
	}

	public Integer getQueueLimit() {
		/* 246 */ return this.queueLimit;
	}

	public void setQueueLimit(Integer queueLimit) {
		/* 250 */ this.queueLimit = queueLimit;
	}

	public String getCallBackUrl() {
		/* 254 */ return this.callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		/* 258 */ this.callBackUrl = callBackUrl;
	}

	public Byte getStickyAlgo() {
		/* 262 */ return this.stickyAlgo;
	}

	public void setStickyAlgo(Byte stickyAlgo) {
		/* 266 */ this.stickyAlgo = stickyAlgo;
	}

	public Boolean getEodReportFlag() {
		return eodReportFlag;
	}

	public void setEodReportFlag(Boolean eodReportFlag) {
		this.eodReportFlag = eodReportFlag;
	}

	public int getRoutingType() {
		return routingType;
	}

	public void setRoutingType(int routingType) {
		this.routingType = routingType;
	}

	public int getInChannels() {
		return inChannels;
	}

	public void setInChannels(int inChannels) {
		this.inChannels = inChannels;
	}

	public int getInQueueChannels() {
		return inQueueChannels;
	}

	public void setInQueueChannels(int inQueueChannels) {
		this.inQueueChannels = inQueueChannels;
	}

	public int getOutChannels() {
		return outChannels;
	}

	public void setOutChannels(int outChannels) {
		this.outChannels = outChannels;
	}

	public int getGuiTimer() {
		return guiTimer;
	}

	public void setGuiTimer(int guiTimer) {
		this.guiTimer = guiTimer;
	}

	public int getAgentRelaxTime() {
		return agentRelaxTime;
	}

	public void setAgentRelaxTime(int agentRelaxTime) {
		this.agentRelaxTime = agentRelaxTime;
	}

//	public String getStrLongcodes() {
//		return strLongcodes;
//	}
//
//	public void setStrLongcodes(String strLongcodes) {
//		this.strLongcodes = strLongcodes;
//	}

}
