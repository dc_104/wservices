package in.wringg.pojo;

import in.wringg.pojo.SmeSettingBean;
import java.util.ArrayList;
import java.util.List;

public class SettingWapperBean {
  private String selectionAlgo;
  private String balance;
  private Integer queueLimit;
  private String callBackUrl;
  private Byte stickyAlgo;
  List<SmeSettingBean> flagList;
  int language;
  
  public String toString() {
/* 17 */     return "SettingWapperBean [flagList=" + this.flagList + ", language=" + this.language + "]";
  }








  
  public SettingWapperBean() {
/* 29 */     this.flagList = new ArrayList<>(); } public SettingWapperBean(List<SmeSettingBean> flagList, int language) { this.flagList = new ArrayList<>();
    this.flagList = flagList;
    this.language = language; }
   public List<SmeSettingBean> getFlagList() {
/* 33 */     return this.flagList;
  }
  
  public void setFlagList(List<SmeSettingBean> flagList) {
/* 37 */     this.flagList = flagList;
  }
  
  public int getLanguage() {
/* 41 */     return this.language;
  }
  
  public void setLanguage(int language) {
/* 45 */     this.language = language;
  }
  
  public String getBalance() {
/* 49 */     return this.balance;
  }
  
  public void setBalance(String balance) {
/* 53 */     this.balance = balance;
  }
  
  public String getSelectionAlgo() {
/* 57 */     return this.selectionAlgo;
  }
  
  public void setSelectionAlgo(String selectionAlgo) {
/* 61 */     this.selectionAlgo = selectionAlgo;
  }
  
  public Integer getQueueLimit() {
/* 65 */     return this.queueLimit;
  }
  
  public void setQueueLimit(Integer queueLimit) {
/* 69 */     this.queueLimit = queueLimit;
  }
  
  public String getCallBackUrl() {
/* 73 */     return this.callBackUrl;
  }
  
  public void setCallBackUrl(String callBackUrl) {
/* 77 */     this.callBackUrl = callBackUrl;
  }
  
  public Byte getStickyAlgo() {
/* 81 */     return this.stickyAlgo;
  }
  
  public void setStickyAlgo(Byte stickyAlgo) {
/* 85 */     this.stickyAlgo = stickyAlgo;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SettingWapperBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */