package in.wringg.pojo;

import java.util.Date;


public class CallBaseBean
{
  private Long id;
  private Long baseId;
  private String moblie;
  private String desc;
  private Date insertTime;
  private Integer callCounter;
  private Byte status;
  private Date scheduleDateTime;
  private Integer callType;
  private String scheduleDateTimeStr;
  
  public CallBaseBean() {}
  
  public Long getId() {
/*  22 */     return this.id;
  }
  
  public void setId(Long id) {
/*  26 */     this.id = id;
  }
  
  public String getMoblie() {
/*  30 */     return this.moblie;
  }
  
  public void setMoblie(String moblie) {
/*  34 */     this.moblie = moblie;
  }
  
  public String getDesc() {
/*  38 */     return this.desc;
  }
  
  public void setDesc(String desc) {
/*  42 */     this.desc = desc;
  }
  
  public Date getInsertTime() {
/*  46 */     return this.insertTime;
  }
  
  public void setInsertTime(Date insertTime) {
/*  50 */     this.insertTime = insertTime;
  }


  
  public CallBaseBean(long id, long baseId, String moblie, String desc, Date insertTime, Integer callCounter, Byte status, Date scheduleDateTime, Integer callType, String scheduleDateTimeStr) {
/*  56 */     this.id = Long.valueOf(id);
/*  57 */     this.baseId = Long.valueOf(baseId);
/*  58 */     this.moblie = moblie;
/*  59 */     this.desc = desc;
/*  60 */     this.insertTime = insertTime;
/*  61 */     this.callCounter = callCounter;
/*  62 */     this.status = status;
/*  63 */     this.scheduleDateTime = scheduleDateTime;
/*  64 */     this.callType = callType;
/*  65 */     this.scheduleDateTimeStr = scheduleDateTimeStr;
  }
  
  public Integer getCallCounter() {
/*  69 */     return this.callCounter;
  }
  
  public void setCallCounter(Integer callCounter) {
/*  73 */     this.callCounter = callCounter;
  }
  
  public Byte getStatus() {
/*  77 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/*  81 */     this.status = status;
  }
  
  public Long getBaseId() {
/*  85 */     return this.baseId;
  }
  
  public void setBaseId(Long baseId) {
/*  89 */     this.baseId = baseId;
  }
  
  public Date getScheduleDateTime() {
/*  93 */     return this.scheduleDateTime;
  }
  
  public void setScheduleDateTime(Date scheduleDateTime) {
/*  97 */     this.scheduleDateTime = scheduleDateTime;
  }
  
  public Integer getCallType() {
/* 101 */     return this.callType;
  }
  
  public void setCallType(Integer callType) {
/* 105 */     this.callType = callType;
  }
  
  public String getScheduleDateTimeStr() {
/* 109 */     return this.scheduleDateTimeStr;
  }
  
  public void setScheduleDateTimeStr(String scheduleDateTimeStr) {
/* 113 */     this.scheduleDateTimeStr = scheduleDateTimeStr;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CallBaseBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */