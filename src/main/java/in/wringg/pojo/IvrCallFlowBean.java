package in.wringg.pojo;public class IvrCallFlowBean {
  private String id;
  private String catDescription;
  private String catId;
  
  public String toString() {
/*  7 */     return "IvrCallFlowBean [id=" + this.id + ", catDescription=" + this.catDescription + ", catId=" + this.catId + ", catTitle=" + this.catTitle + ", child=" + this.child + ", dtmf=" + this.dtmf + ", eventType=" + this.eventType + ", mediaFilePath=" + this.mediaFilePath + ", mediaFileStatus=" + this.mediaFileStatus + ", parentId=" + this.parentId + ", servicrType=" + this.servicrType + ", type=" + this.type + "]";
  }
  private String catTitle; private byte child; private byte dtmf; private String eventType; private String mediaFilePath; private String mediaFileStatus; private String parentId;
  private String servicrType;
  private String type;
  
  public String getId() {
/* 14 */     return this.id;
  }
  public void setId(String id) {
/* 17 */     this.id = id;
  }








  
  public String getCatDescription() {
/* 29 */     return this.catDescription;
  }
  public void setCatDescription(String catDescription) {
/* 32 */     this.catDescription = catDescription;
  }
  public String getCatId() {
/* 35 */     return this.catId;
  }
  public void setCatId(String catId) {
/* 38 */     this.catId = catId;
  }
  public String getCatTitle() {
/* 41 */     return this.catTitle;
  }
  public void setCatTitle(String catTitle) {
/* 44 */     this.catTitle = catTitle;
  }
  public byte getChild() {
/* 47 */     return this.child;
  }
  public void setChild(byte child) {
/* 50 */     this.child = child;
  }
  public byte getDtmf() {
/* 53 */     return this.dtmf;
  }
  public void setDtmf(byte dtmf) {
/* 56 */     this.dtmf = dtmf;
  }
  public String getEventType() {
/* 59 */     return this.eventType;
  }
  public void setEventType(String eventType) {
/* 62 */     this.eventType = eventType;
  }
  
  public String getMediaFilePath() {
/* 66 */     return this.mediaFilePath;
  }
  public void setMediaFilePath(String mediaFilePath) {
/* 69 */     this.mediaFilePath = mediaFilePath;
  }
  public String getMediaFileStatus() {
/* 72 */     return this.mediaFileStatus;
  }
  public void setMediaFileStatus(String mediaFileStatus) {
/* 75 */     this.mediaFileStatus = mediaFileStatus;
  }
  public String getParentId() {
/* 78 */     return this.parentId;
  }
  public void setParentId(String parentId) {
/* 81 */     this.parentId = parentId;
  }
  
  public String getServicrType() {
/* 85 */     return this.servicrType;
  }
  public void setServicrType(String servicrType) {
/* 88 */     this.servicrType = servicrType;
  }
  public String getType() {
/* 91 */     return this.type;
  }
  public void setType(String type) {
/* 94 */     this.type = type;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\IvrCallFlowBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */