package in.wringg.pojo;


public class SmsStats
{
/*  6 */   private Long total = Long.valueOf(0L);
/*  7 */   private Long pending = Long.valueOf(0L);
/*  8 */   private Long canceled = Long.valueOf(0L);
/*  9 */   private Long successful = Long.valueOf(0L);
/* 10 */   private Long failed = Long.valueOf(0L);
/* 11 */   private Long processed = Long.valueOf(0L);
  public Long getTotal() {
/* 13 */     return this.total;
  }
  public void setTotal(Long total) {
/* 16 */     this.total = total;
  }
  public Long getPending() {
/* 19 */     return this.pending;
  }
  public void setPending(Long pending) {
/* 22 */     this.pending = pending;
  }
  public Long getCanceled() {
/* 25 */     return this.canceled;
  }
  public void setCanceled(Long canceled) {
/* 28 */     this.canceled = canceled;
  }
  public Long getSuccessful() {
/* 31 */     return this.successful;
  }
  public void setSuccessful(Long successful) {
/* 34 */     this.successful = successful;
  }
  public Long getFailed() {
/* 37 */     return this.failed;
  }
  public void setFailed(Long failed) {
/* 40 */     this.failed = failed;
  }
  public Long getProcessed() {
/* 43 */     return this.processed;
  }
  public void setProcessed(Long processed) {
/* 46 */     this.processed = processed;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmsStats.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */