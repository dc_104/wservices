package in.wringg.pojo;

public class AdminSmeDetailBean {
  private int totalSME;
  private int currentSME;
  private int freeSME;
  
  public int getTotalSME() {
/*  9 */     return this.totalSME;
  }
  public void setTotalSME(int totalSME) {
/* 12 */     this.totalSME = totalSME;
  }
  public int getCurrentSME() {
/* 15 */     return this.currentSME;
  }
  public void setCurrentSME(int currentSME) {
/* 18 */     this.currentSME = currentSME;
  }
  public int getFreeSME() {
/* 21 */     return this.freeSME;
  }
  public void setFreeSME(int freeSME) {
/* 24 */     this.freeSME = freeSME;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AdminSmeDetailBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */