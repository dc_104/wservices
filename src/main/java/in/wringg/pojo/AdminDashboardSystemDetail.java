package in.wringg.pojo;

import in.wringg.pojo.AdminAgentDetailBean;
import in.wringg.pojo.AdminSmeDetailBean;
import in.wringg.pojo.DashboardCallDeatialBean;
import java.util.List;

public class AdminDashboardSystemDetail
{
  private AdminSmeDetailBean smeDetail;
  
  public AdminSmeDetailBean getSmeDetail() {
/* 13 */     return this.smeDetail;
  } private AdminAgentDetailBean agentDetail; private List<DashboardCallDeatialBean> callDetail;
  public void setSmeDetail(AdminSmeDetailBean smeDetail) {
/* 16 */     this.smeDetail = smeDetail;
  }
  public AdminAgentDetailBean getAgentDetail() {
/* 19 */     return this.agentDetail;
  }
  public void setAgentDetail(AdminAgentDetailBean agentDetail) {
/* 22 */     this.agentDetail = agentDetail;
  }
  public List<DashboardCallDeatialBean> getCallDetail() {
/* 25 */     return this.callDetail;
  }
  public void setCallDetail(List<DashboardCallDeatialBean> callDetail) {
/* 28 */     this.callDetail = callDetail;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AdminDashboardSystemDetail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */