package in.wringg.pojo;

public class LongCodeIdOnly
{
  private Long id;
  private Long longCode;
  
  public Long getId() {
/*  9 */     return this.id;
  }
  public void setId(Long id) {
/* 12 */     this.id = id;
  }
  public Long getLongCode() {
/* 15 */     return this.longCode;
  }
  public void setLongCode(Long longCode) {
/* 18 */     this.longCode = longCode;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\LongCodeIdOnly.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */