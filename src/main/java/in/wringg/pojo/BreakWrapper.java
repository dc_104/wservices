package in.wringg.pojo;

import in.wringg.pojo.BreakPojo;
import java.util.Date;
import java.util.List;

public class BreakWrapper {
  private Byte status;
  private List<BreakPojo> breaks;
  private String signInStr;
  private String signOutStr;
  private Date signIn;
  private Date signOut;
  private long totalActiveTime;
  private String totalActiveTimeStr;
  private long totalBreakTime;
  private String totalBreakTimeStr;
  private Long schAssinedCount;
  private Long schFollowUpCount;
  
  public String getSignInStr() {
/* 22 */     return this.signInStr;
  }
  public void setSignInStr(String signInStr) {
/* 25 */     this.signInStr = signInStr;
  }
  public String getSignOutStr() {
/* 28 */     return this.signOutStr;
  }
  public void setSignOutStr(String signOutStr) {
/* 31 */     this.signOutStr = signOutStr;
  }
  public Date getSignIn() {
/* 34 */     return this.signIn;
  }
  public void setSignIn(Date signIn) {
/* 37 */     this.signIn = signIn;
  }
  public Date getSignOut() {
/* 40 */     return this.signOut;
  }
  public void setSignOut(Date signOut) {
/* 43 */     this.signOut = signOut;
  }
  public List<BreakPojo> getBreaks() {
/* 46 */     return this.breaks;
  }
  public void setBreaks(List<BreakPojo> breaks) {
/* 49 */     this.breaks = breaks;
  }
  public long getTotalActiveTime() {
/* 52 */     return this.totalActiveTime;
  }
  public void setTotalActiveTime(long totalActiveTime) {
/* 55 */     this.totalActiveTime = totalActiveTime;
  }
  public String getTotalActiveTimeStr() {
/* 58 */     return this.totalActiveTimeStr;
  }
  public void setTotalActiveTimeStr(String totalActiveTimeStr) {
/* 61 */     this.totalActiveTimeStr = totalActiveTimeStr;
  }
  public long getTotalBreakTime() {
/* 64 */     return this.totalBreakTime;
  }
  public void setTotalBreakTime(long totalBreakTime) {
/* 67 */     this.totalBreakTime = totalBreakTime;
  }
  public String getTotalBreakTimeStr() {
/* 70 */     return this.totalBreakTimeStr;
  }
  public void setTotalBreakTimeStr(String totalBreakTimeStr) {
/* 73 */     this.totalBreakTimeStr = totalBreakTimeStr;
  }
  public Byte getStatus() {
/* 76 */     return this.status;
  }
  public void setStatus(Byte status) {
/* 79 */     this.status = status;
  }
  public Long getSchAssinedCount() {
/* 82 */     return this.schAssinedCount;
  }
  public void setSchAssinedCount(Long schAssinedCount) {
/* 85 */     this.schAssinedCount = schAssinedCount;
  }
  public Long getSchFollowUpCount() {
/* 88 */     return this.schFollowUpCount;
  }
  public void setSchFollowUpCount(Long schFollowUpCount) {
/* 91 */     this.schFollowUpCount = schFollowUpCount;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\BreakWrapper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */