package in.wringg.pojo;

import java.util.Date;


public class ChargingResponseBean
{
  private Integer agentAllocate;
  private String ani;
  private Date billingDate;
  private Float charegedAmt;
  private String chargingResp;
  private String inFlag;
  private String inMode;
  private Long packId;
  private Byte prePost;
  private Date renewDate;
  private Long smeId;
  private Byte status;
  private Date subDate;
  private Integer totalAgentAllocate;
  private Float userBalance;
  private String userType;
  
  public Integer getAgentAllocate() {
/*  26 */     return this.agentAllocate;
  }
  
  public void setAgentAllocate(Integer agentAllocate) {
/*  30 */     this.agentAllocate = agentAllocate;
  }
  
  public String getAni() {
/*  34 */     return this.ani;
  }
  
  public void setAni(String ani) {
/*  38 */     this.ani = ani;
  }
  
  public Date getBillingDate() {
/*  42 */     return this.billingDate;
  }
  
  public void setBillingDate(Date billingDate) {
/*  46 */     this.billingDate = billingDate;
  }
  
  public Float getCharegedAmt() {
/*  50 */     return this.charegedAmt;
  }
  
  public void setCharegedAmt(Float charegedAmt) {
/*  54 */     this.charegedAmt = charegedAmt;
  }
  
  public String getChargingResp() {
/*  58 */     return this.chargingResp;
  }
  
  public void setChargingResp(String chargingResp) {
/*  62 */     this.chargingResp = chargingResp;
  }

  
  public String getInFlag() {
/*  67 */     return this.inFlag;
  }
  
  public void setInFlag(String inFlag) {
/*  71 */     this.inFlag = inFlag;
  }
  
  public String getInMode() {
/*  75 */     return this.inMode;
  }
  
  public void setInMode(String inMode) {
/*  79 */     this.inMode = inMode;
  }
  
  public Long getPackId() {
/*  83 */     return this.packId;
  }
  
  public void setPackId(Long packId) {
/*  87 */     this.packId = packId;
  }
  
  public Byte getPrePost() {
/*  91 */     return this.prePost;
  }
  
  public void setPrePost(Byte prePost) {
/*  95 */     this.prePost = prePost;
  }
  
  public Date getRenewDate() {
/*  99 */     return this.renewDate;
  }
  
  public void setRenewDate(Date renewDate) {
/* 103 */     this.renewDate = renewDate;
  }
  
  public Long getSmeId() {
/* 107 */     return this.smeId;
  }
  
  public void setSmeId(Long smeId) {
/* 111 */     this.smeId = smeId;
  }
  
  public Byte getStatus() {
/* 115 */     return this.status;
  }
  
  public void setStatus(Byte status) {
/* 119 */     this.status = status;
  }
  
  public Date getSubDate() {
/* 123 */     return this.subDate;
  }
  
  public void setSubDate(Date subDate) {
/* 127 */     this.subDate = subDate;
  }
  
  public Integer getTotalAgentAllocate() {
/* 131 */     return this.totalAgentAllocate;
  }
  
  public void setTotalAgentAllocate(Integer totalAgentAllocate) {
/* 135 */     this.totalAgentAllocate = totalAgentAllocate;
  }
  
  public Float getUserBalance() {
/* 139 */     return this.userBalance;
  }
  
  public void setUserBalance(Float userBalance) {
/* 143 */     this.userBalance = userBalance;
  }
  
  public String getUserType() {
/* 147 */     return this.userType;
  }
  
  public void setUserType(String userType) {
/* 151 */     if (userType != null)
/* 152 */       userType = userType.toLowerCase(); 
/* 153 */     this.userType = userType;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\ChargingResponseBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */