package in.wringg.pojo;

import java.util.List;

public class CdrModeBean {
	List<KeyValueGenricBean> incoming;

	public List<KeyValueGenricBean> getIncoming() {
		/* 8 */ return this.incoming;
	}

	List<KeyValueGenricBean> outgoing;
	List<KeyValueGenricBean> voicemail;

	public void setIncoming(List<KeyValueGenricBean> incoming) {
		/* 11 */ this.incoming = incoming;
	}

	public List<KeyValueGenricBean> getOutgoing() {
		/* 14 */ return this.outgoing;
	}

	public void setOutgoing(List<KeyValueGenricBean> outgoing) {
		/* 17 */ this.outgoing = outgoing;
	}

	public List<KeyValueGenricBean> getVoicemail() {
		/* 20 */ return this.voicemail;
	}

	public void setVoicemail(List<KeyValueGenricBean> voicemail) {
		/* 23 */ this.voicemail = voicemail;
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\CdrModeBean.class Java
 * compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */