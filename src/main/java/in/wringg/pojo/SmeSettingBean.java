package in.wringg.pojo;

public class SmeSettingBean {
  Integer id;
  String displayName;
  Boolean value;
  
  public Integer getId() {
/*  9 */     return this.id;
  }
  public void setId(Integer id) {
/* 12 */     this.id = id;
  }
  public String getDisplayName() {
/* 15 */     return this.displayName;
  }
  public void setDisplayName(String displayName) {
/* 18 */     this.displayName = displayName;
  }
  public Boolean getValue() {
/* 21 */     return this.value;
  }
  public void setValue(Boolean value) {
/* 24 */     this.value = value;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\SmeSettingBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */