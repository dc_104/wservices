package in.wringg.pojo;

public class AgentDetailReportBean
{
  private Integer id;
  private String agentGroup;
  private Long agentId;
  private String customerAni;
  private int duration;
  private String endDate;
  private String insertDate;
  private String responseCode;
  private String responseMessage;
  private Long smeId;
  private String startDate;
  private int status;
  private String agentNumber;
  private String agentName;
  private String sessionId;
  private String callMode;
  private String callInfo;
  
  public int getId() {
/*  24 */     return this.id.intValue();
  }
  
  public void setId(int id) {
/*  28 */     this.id = Integer.valueOf(id);
  }
  
  public String getAgentGroup() {
/*  32 */     return this.agentGroup;
  }
  
  public void setAgentGroup(String agentGroup) {
/*  36 */     this.agentGroup = agentGroup;
  }
  
  public Long getAgentId() {
/*  40 */     return this.agentId;
  }
  
  public void setAgentId(Long agentId) {
/*  44 */     this.agentId = agentId;
  }
  
  public String getCustomerAni() {
/*  48 */     return this.customerAni;
  }
  
  public void setCustomerAni(String customerAni) {
/*  52 */     this.customerAni = customerAni;
  }
  
  public int getDuration() {
/*  56 */     return this.duration;
  }
  
  public void setDuration(int duration) {
/*  60 */     this.duration = duration;
  }



  
  public String getResponseCode() {
/*  67 */     return this.responseCode;
  }
  
  public void setResponseCode(String responseCode) {
/*  71 */     this.responseCode = responseCode;
  }
  
  public Long getSmeId() {
/*  75 */     return this.smeId;
  }
  
  public String getEndDate() {
/*  79 */     return this.endDate;
  }
  
  public void setEndDate(String endDate) {
/*  83 */     this.endDate = endDate;
  }
  
  public String getInsertDate() {
/*  87 */     return this.insertDate;
  }
  
  public void setInsertDate(String insertDate) {
/*  91 */     this.insertDate = insertDate;
  }
  
  public String getStartDate() {
/*  95 */     return this.startDate;
  }
  
  public void setStartDate(String startDate) {
/*  99 */     this.startDate = startDate;
  }
  
  public void setSmeId(Long smeId) {
/* 103 */     this.smeId = smeId;
  }
  
  public int getStatus() {
/* 107 */     return this.status;
  }
  
  public void setStatus(int status) {
/* 111 */     this.status = status;
  }
  
  public String getAgentNumber() {
/* 115 */     return this.agentNumber;
  }
  
  public void setAgentNumber(String agentNumber) {
/* 119 */     this.agentNumber = agentNumber;
  }
  
  public String getAgentName() {
/* 123 */     return this.agentName;
  }
  
  public void setAgentName(String agentName) {
/* 127 */     this.agentName = agentName;
  }
  
  public String getSessionId() {
/* 131 */     return this.sessionId;
  }
  
  public void setSessionId(String sessionId) {
/* 135 */     this.sessionId = sessionId;
  }
  public String getResponseMessage() {
/* 138 */     return this.responseMessage;
  }
  
  public void setResponseMessage(String responseMessage) {
/* 142 */     this.responseMessage = responseMessage;
  }
  
  public String getCallMode() {
/* 146 */     return this.callMode;
  }
  
  public void setCallMode(String callMode) {
/* 150 */     this.callMode = callMode;
  }
  
  public String getCallInfo() {
/* 154 */     return this.callInfo;
  }
  
  public void setCallInfo(String callInfo) {
/* 158 */     this.callInfo = callInfo;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\AgentDetailReportBean.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */