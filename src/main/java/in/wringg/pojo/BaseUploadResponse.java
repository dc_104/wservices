package in.wringg.pojo;

import java.util.Date;
import java.util.Set;



public class BaseUploadResponse
{
  private int totalCount;
  private int uploadedCound;
  private int duplicateCount;
  private int invalidCount;
  private Set<String> duplicateSet;
  private Date date;
  private String fileName;
  private Set<String> invalidSet;
  
  public BaseUploadResponse() {}
  
  public BaseUploadResponse(int totalCount, int uploadedCound, int duplicateCount, int invalidCount, Set<String> duplicateSet, Set<String> invalidSet, Date date, String fileName) {
/* 22 */     this.totalCount = totalCount;
/* 23 */     this.uploadedCound = uploadedCound;
/* 24 */     this.duplicateCount = duplicateCount;
/* 25 */     this.invalidCount = invalidCount;
/* 26 */     this.duplicateSet = duplicateSet;
/* 27 */     this.invalidSet = invalidSet;
/* 28 */     this.date = date;
/* 29 */     this.fileName = fileName;
  }
  public int getTotalCount() {
/* 32 */     return this.totalCount;
  }
  public void setTotalCount(int totalCount) {
/* 35 */     this.totalCount = totalCount;
  }
  public int getUploadedCound() {
/* 38 */     return this.uploadedCound;
  }
  public void setUploadedCound(int uploadedCound) {
/* 41 */     this.uploadedCound = uploadedCound;
  }
  public int getDuplicateCount() {
/* 44 */     return this.duplicateCount;
  }
  public void setDuplicateCount(int duplicateCount) {
/* 47 */     this.duplicateCount = duplicateCount;
  }
  public int getInvalidCount() {
/* 50 */     return this.invalidCount;
  }
  public void setInvalidCount(int invalidCount) {
/* 53 */     this.invalidCount = invalidCount;
  }
  public Set<String> getDuplicateSet() {
/* 56 */     return this.duplicateSet;
  }
  public void setDuplicateSet(Set<String> duplicateSet) {
/* 59 */     this.duplicateSet = duplicateSet;
  }
  public Set<String> getInvalidSet() {
/* 62 */     return this.invalidSet;
  }
  public void setInvalidSet(Set<String> invalidSet) {
/* 65 */     this.invalidSet = invalidSet;
  }
  
  public Date getDate() {
/* 69 */     return this.date;
  }
  public void setDate(Date date) {
/* 72 */     this.date = date;
  }
  public String getFileName() {
/* 75 */     return this.fileName;
  }
  public void setFileName(String fileName) {
/* 78 */     this.fileName = fileName;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\pojo\BaseUploadResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */