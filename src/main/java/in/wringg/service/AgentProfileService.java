package in.wringg.service;

import in.wringg.pojo.AddressBookBean;
import in.wringg.pojo.AgentDetailBean;
import in.wringg.pojo.AgentOrderBean;
import in.wringg.pojo.CallBaseBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SmeCustomerRemarksBean;
import in.wringg.pojo.StartEndDateRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface AgentProfileService {
  ServerResponse addAgentProfile(AgentDetailBean paramAgentDetailBean) throws IOException;
  
  ServerResponse updateAgentProfile(AgentDetailBean paramAgentDetailBean, Long paramLong) throws IOException;
  
  ServerResponse deleteAgentProfile(Long paramLong);
  
  ServerResponse listAgentProfile(Long paramLong);
  
  ServerResponse canCreateAgent(Long paramLong);
  
  ServerResponse maintainOrder(Long paramLong, List<AgentOrderBean> paramList);
  
  ServerResponse getBaseForAgent(Long paramLong);
  
  ServerResponse getAgentInsighnt(StartEndDateRequest paramStartEndDateRequest);
  
  ServerResponse lunchIn(Long paramLong1, Long paramLong2, Date paramDate, String paramString);
  
  ServerResponse lunchOut(Long paramLong, Date paramDate);
  
  ServerResponse breakList(Long paramLong);
  
  ServerResponse addAddressBook(AddressBookBean paramAddressBookBean);
  
  ServerResponse updateAddressBook(AddressBookBean paramAddressBookBean);
  
  ServerResponse deleteAddressBook(Long paramLong);
  
  ServerResponse getAddressBookForAgent(Long paramLong);
  
  ServerResponse getAddressBookForSme(Long paramLong);
  
  ServerResponse addRemaks(SmeCustomerRemarksBean paramSmeCustomerRemarksBean);
  
  ServerResponse updateRemaks(SmeCustomerRemarksBean paramSmeCustomerRemarksBean);
  
  ServerResponse getRemaksByAddressBook(Long paramLong1, Long paramLong2);
  
  ServerResponse deleteRemaks(Long paramLong);
  
  ServerResponse deleteRemaksByAddressBook(Long paramLong);
  
  ServerResponse notPermitted();
  
  boolean canAgentUpdateOrDelete(Long paramLong1, Long paramLong2, Long paramLong3);
  
  ServerResponse callSchedule(CallBaseBean paramCallBaseBean, Long paramLong);
  
  ServerResponse callScheduleUpdate(CallBaseBean paramCallBaseBean, Long paramLong);
  
  ServerResponse getAddressBookById(Long paramLong);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\AgentProfileService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */