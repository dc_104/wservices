package in.wringg.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import flexjson.JSONSerializer;
import in.wringg.dao.AdminDao;
import in.wringg.dao.SmeProfileDao;
import in.wringg.email.MailSender;
import in.wringg.entity.EdvaantageDemoRequest;
import in.wringg.entity.Longcode;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.Users;
import in.wringg.pojo.AdminDashboardRequest;
import in.wringg.pojo.AdminDashboardRevenue;
import in.wringg.pojo.AdminDashboardSystemDetail;
import in.wringg.pojo.AdminMaxAgents;
import in.wringg.pojo.AdminSmeProfileBean;
import in.wringg.pojo.EdvaantageDemoBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.LongCodeBean;
import in.wringg.pojo.LongCodeIdOnly;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.ServiceListPojo;
import in.wringg.pojo.SmeProfilePaginationBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UserRoleDetailBean;
import in.wringg.pojo.ZoneBean;
import in.wringg.service.AdminService;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import in.wringg.utility.Utility;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("smeProfile")
@Transactional
public class AdminServiceImp implements AdminService {
	@Autowired
	private SmeProfileDao smeDao;
	@Autowired
	private AdminDao repoDao;

	public ServerResponse smeProcessor(String action, AdminSmeProfileBean profile) throws Exception {
		/* 57 */ ServerResponse res = new ServerResponse();
		/* 58 */ switch (action) {
		case "add":
			/* 60 */ res = this.smeDao.addSmeProfile(profile);
			break;
		case "update":
			/* 63 */ res = this.smeDao.updateSmeProfile(profile);
			break;
		case "delete":
			/* 66 */ res = this.smeDao.deleteSmeProfile(Long.valueOf(profile.getId()));
			break;
		}

		/* 71 */ return res;
	}

	public ServerResponse adminDashboardRevenue(AdminDashboardRequest revenueDateRange) {
		/* 76 */ List<AdminDashboardRevenue> list = this.repoDao.dashBoardRevenue(revenueDateRange);
		/* 77 */ ServerResponse response = new ServerResponse();
		/* 78 */ response.setSuccess(true);
		/* 79 */ if (list.isEmpty()) {
			/* 80 */ response.setResult("");
		} else {
			/* 82 */ JSONSerializer serializer = new JSONSerializer();
			/* 83 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(list);
			/* 84 */ response.setResult(res);
		}
		/* 86 */ return response;
	}

	public ServerResponse adminSystemSnapshot(AdminDashboardRequest revenueDateRange) throws ParseException {
		/* 91 */ AdminDashboardSystemDetail object = this.repoDao.dashSystemSnapshot(revenueDateRange);
		/* 92 */ ServerResponse response = new ServerResponse();
		/* 93 */ response.setSuccess(true);
		/* 94 */ if (object == null) {
			/* 95 */ response.setResult("");
		} else {
			/* 97 */ JSONSerializer serializer = new JSONSerializer();
			/* 98 */ String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(object);
			/* 99 */ response.setResult(res);
		}
		/* 101 */ return response;
	}

	public ServerResponse getZone() {
		/* 106 */ ServerResponse response = new ServerResponse();
		/* 107 */ List<ZoneBean> data = this.repoDao.getZone();
		/* 108 */ response.setSuccess(true);
		/* 109 */ if (data.isEmpty()) {

			/* 111 */ response.setResult("");
		} else {

			/* 114 */ response.setSuccess(true);
			/* 115 */ JSONSerializer serializer = new JSONSerializer();
			/* 116 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(data);
			/* 117 */ response.setResult(res);
		}
		/* 119 */ System.out.println("response data " + response.getResult());
		/* 120 */ return response;
	}

	public ServerResponse getLongCode(Byte flag) {
		/* 126 */ JSONSerializer serializer = new JSONSerializer();
		/* 127 */ ServerResponse response = new ServerResponse();
		/* 128 */ List<Longcode> longcodeList = this.repoDao.getLongCode(flag);

		/* 130 */ List<LongCodeBean> listBean = new ArrayList<>();
		/* 131 */ List<LongCodeIdOnly> listIdBean = new ArrayList<>();
		/* 132 */ if (flag != Constants.LONGCODE_ACTIVE) {

			/* 134 */ for (Longcode longCode : longcodeList) {
				/* 135 */ LongCodeBean bean = new LongCodeBean();
				/* 136 */ bean.setId(longCode.getId());
				/* 137 */ bean.setInsertTime(longCode.getInsertTime());
				/* 138 */ bean.setInShortCode(longCode.getInShortCode());
				/* 139 */ bean.setOutShortCode(longCode.getOutShortCode());
				/* 140 */ bean.setHlr(longCode.getHlr());
				/* 141 */ bean.setSmeIdentifier(longCode.getSmeIdentifier());
				/* 142 */ bean.setLongCode(longCode.getLongcode());
				/* 143 */ bean.setStatus(longCode.getStatus());
				/* 144 */ listBean.add(bean);
			}

			/* 150 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(listBean);
			/* 151 */ response.setResult(res);
		} else {

			/* 155 */ for (Longcode longCode : longcodeList) {
				/* 156 */ LongCodeIdOnly bean = new LongCodeIdOnly();
				/* 157 */ bean.setId(longCode.getId());
				/* 158 */ bean.setLongCode(longCode.getLongcode());
				/* 159 */ listIdBean.add(bean);
			}

			/* 165 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(listIdBean);
			/* 166 */ response.setResult(res);
		}
		/* 168 */ response.setSuccess(true);

		/* 170 */ return response;
	}

	public ServerResponse updateLongCode(Long id, Byte status) {
		/* 175 */ ServerResponse response = new ServerResponse();
		/* 176 */ Longcode longCode = this.repoDao.fetchSingeLongCode(id);
		/* 177 */ JSONSerializer serializer = new JSONSerializer();
		/* 178 */ if (longCode == null || longCode.getStatus() == -9) {
			/* 179 */ ErrorDetails errorDetails = new ErrorDetails();
			/* 180 */ errorDetails.setErrorCode(ErrorConstants.ERROR_LONGCODE_NOT_EXIST.getErrorCode());
			/* 181 */ errorDetails
					.setErrorString("Longcode ::" + ErrorConstants.ERROR_LONGCODE_NOT_EXIST.getErrorMessage());
			/* 182 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_LONGCODE_NOT_EXIST.getDisplayMessage());
			/* 183 */ String str = serializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
			/* 184 */ response.setResult(str);
			/* 185 */ return response;
		}

		/* 188 */ if (longCode.getStatus() == Constants.LONG_CODE_ASSIGNED.byteValue()) {
			/* 189 */ ErrorDetails errorDetails = new ErrorDetails();
			/* 190 */ errorDetails.setErrorCode(ErrorConstants.ERROR_LONGCODE_ASSIGNED.getErrorCode());
			/* 191 */ errorDetails
					.setErrorString("Longcode ::" + ErrorConstants.ERROR_LONGCODE_ASSIGNED.getErrorMessage());
			/* 192 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_LONGCODE_ASSIGNED.getDisplayMessage());
			/* 193 */ String str = serializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
			/* 194 */ response.setResult(str);
			/* 195 */ return response;
		}

		/* 198 */ if (status.byteValue() == -9) {
			/* 199 */ longCode.setStatus(status.byteValue());
			/* 200 */ response.setSuccess(true);
			/* 201 */ response.setResult("Successfully Deleted ");
			/* 202 */ return response;
		}

		/* 205 */ if (status == Constants.LONGCODE_ACTIVE || status == Constants.LONGCODE_IN_ACTIVE) {
			/* 206 */ longCode.setStatus(status.byteValue());
			/* 207 */ response.setSuccess(true);
			/* 208 */ response.setResult("Successfully Updated ");
			/* 209 */ return response;
		}
		/* 211 */ ErrorDetails error = new ErrorDetails();
		/* 212 */ error.setErrorCode(ErrorConstants.ERROR_INVALID_LONGCODE_STATUS.getErrorCode());
		/* 213 */ error.setErrorString("Longcode ::" + ErrorConstants.ERROR_INVALID_LONGCODE_STATUS.getErrorMessage());
		/* 214 */ error.setDisplayMessage(ErrorConstants.ERROR_INVALID_LONGCODE_STATUS.getDisplayMessage());
		/* 215 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 216 */ response.setResult(res);
		/* 217 */ return response;
	}

	public ServerResponse fetchSme(StartEndDateRequest filter) {
		/* 224 */ JSONSerializer serializer = new JSONSerializer();
		/* 225 */ ServerResponse response = new ServerResponse();
		/* 226 */ response.setSuccess(true);
		/* 227 */ List<SmeProfile> list = this.smeDao.findAllSMEs(filter);
		/* 228 */ List<AdminSmeProfileBean> beanList = new ArrayList<>();
		/* 229 */ for (SmeProfile smeProfile : list) {
			/* 230 */ AdminSmeProfileBean smeProfileBean = new AdminSmeProfileBean();
			/* 231 */ smeProfileBean.setAllowedAgents(smeProfile.getAllowedAgents());
			/* 232 */ smeProfileBean.setEmailId(smeProfile.getEmailId());
			/* 233 */ smeProfileBean.setAlternateNumber(smeProfile.getAlternateNumber());
			/* 234 */ smeProfileBean.setId(smeProfile.getId().longValue());
			/* 235 */ smeProfileBean.setSmeName(smeProfile.getName());
			/* 236 */ smeProfileBean.setZoneId(smeProfile.getZoneId().getId().longValue());
			/* 237 */ smeProfileBean.setLongCodeId(smeProfile.getLongcodeId().getId().longValue());
			/* 238 */ smeProfileBean.setStatus(smeProfile.getStatus());
			/* 239 */ smeProfileBean.setServiceFlag(smeProfile.getServiceFlag());
			/* 240 */ smeProfileBean.setSmeMobile(smeProfile.getSmeMobile());
			/* 241 */ smeProfileBean.setStatus(smeProfile.getStatus());
			/* 242 */ smeProfileBean.setBalance(smeProfile.getBalance());
			/* 243 */ smeProfileBean.setRecording(smeProfile.getRecording());
			/* 244 */ smeProfileBean.setMasking(smeProfile.getMasking());
			/* 245 */ smeProfileBean.setSelectionAlgo(smeProfile.getSelectionAlgo());
			/* 246 */ smeProfileBean.setBizAddress(smeProfile.getBizAddress());
			/* 247 */ smeProfileBean.setLongCode(smeProfile.getLongcodeId().getLongcode());
			/* 248 */ smeProfileBean.setInShortCode(smeProfile.getLongcodeId().getInShortCode());
			/* 249 */ smeProfileBean.setOutShortCode(smeProfile.getLongcodeId().getOutShortCode());
			/* 250 */ smeProfileBean.setLanguage(smeProfile.getLanguage());
			/* 251 */ smeProfileBean.setVoicemail(smeProfile.getVoicemail());
			/* 252 */ smeProfileBean.setRecValidity(smeProfile.getRecValidity());
			/* 253 */ smeProfileBean.setAccountSid(smeProfile.getAccountSid());
			/* 254 */ smeProfileBean.setQueueLimit(smeProfile.getQueueLimit());
			/* 255 */ smeProfileBean.setCallBackUrl(smeProfile.getCallBackUrl());
			/* 256 */ smeProfileBean.setSelectionAlgo(smeProfile.getSelectionAlgo());
			/* 257 */ smeProfileBean.setSelectionAlgo(smeProfile.getSelectionAlgo());
			/* 258 */ smeProfileBean.setBizAddress(smeProfile.getBizAddress());
			/* 259 */ smeProfileBean/* 260 */ .setLongCode(
					(smeProfile.getLongcodeId() == null) ? null : smeProfile.getLongcodeId().getLongcode());
			/* 261 */ smeProfileBean.setInShortCode(smeProfile.getLongcodeId().getInShortCode());
			/* 262 */ smeProfileBean.setOutShortCode(smeProfile.getLongcodeId().getOutShortCode());
			/* 263 */ smeProfileBean.setLanguage(smeProfile.getLanguage());
			/* 264 */ smeProfileBean.setRecValidity(smeProfile.getRecValidity());
			/* 265 */ smeProfileBean.setQueueLimit(smeProfile.getQueueLimit());
			/* 266 */ smeProfileBean.setAccountSid(smeProfile.getAccountSid());
			/* 267 */ smeProfileBean.setCallBackUrl(smeProfile.getCallBackUrl());
			/* 268 */ smeProfileBean.setStickyAlgo(smeProfile.getStickyAlgo());
			smeProfileBean.setInPermissionFlag(smeProfile.getInPermissionFlag());
			smeProfileBean.setOutPermissionFlag(smeProfile.getOutPermissionFlag());
			smeProfileBean.setEodReportFlag(smeProfile.getEodReportFlag());
			smeProfileBean.setRoutingType(smeProfile.getRoutingType());
			smeProfileBean.setInChannels(smeProfile.getInChannels());
			smeProfileBean.setInQueueChannels(smeProfile.getInQueueChannels());
			smeProfileBean.setOutChannels(smeProfile.getOutChannels());
			smeProfileBean.setGuiTimer(smeProfile.getGuiTimer());
			smeProfileBean.setAgentRelaxTime(smeProfile.getAgentRelaxTime());
//			smeProfileBean.setLongCodes(fetchLongCodes(smeProfile.getLongcodes()));
//			smeProfileBean.setStrLongcodes(""+smeProfileBean.getLongCodes());
			System.out.println("pro Ban ::::::::::::"+smeProfileBean.toString());
			beanList.add(smeProfileBean);
		}
		response.setSuccess(true);
		String res = serializer.exclude(new String[] { "*.class" }).serialize(beanList);
		response.setResult(res);
		return response;
	}

	private List<Long> fetchLongCodes(Set<Longcode> longcodes) {
		System.out.println(longcodes+"size"+longcodes.size());
		List<Long> longCodeList = new ArrayList<Long>();
		for (Longcode longcode : longcodes) {
			longCodeList.add(longcode.getId());
		}
		System.out.println(longCodeList+" size::"+longCodeList.size());
		
		return longCodeList;
	}

	public ServerResponse getServiceList() throws JsonProcessingException {
		/* 281 */ List<ServiceListPojo> list = new ArrayList<>();
		/* 282 */ for (Constants.Services serv : Arrays.<Constants.Services>asList(Constants.Services.values())) {
			/* 283 */ ServiceListPojo serviceListPojo = new ServiceListPojo();
			/* 284 */ serviceListPojo.setName(serv.getDisplayName());
			/* 285 */ serviceListPojo.setValue(Integer.valueOf(serv.getServiceId()));
			/* 286 */ list.add(serviceListPojo);
		}
		/* 288 */ JSONSerializer serializer = new JSONSerializer();
		/* 289 */ ServerResponse response = new ServerResponse();
		/* 290 */ response.setSuccess(true);
		/* 291 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(list));
		/* 292 */ return response;
	}

	public ServerResponse getMaxAgent() {
		/* 297 */ AdminMaxAgents max = this.repoDao.getMaxAgent();

		/* 299 */ ServerResponse response = new ServerResponse();
		/* 300 */ response.setSuccess(true);
		/* 301 */ JSONSerializer serializer = new JSONSerializer();
		/* 302 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(max));
		/* 303 */ return response;
	}

	public ServerResponse getUserRoleDetail(String id) {
		/* 308 */ UserRoleDetailBean userDetailBean = this.repoDao.roleDeatail(id);
		/* 309 */ ServerResponse response = new ServerResponse();
		/* 310 */ response.setSuccess(true);
		/* 311 */ JSONSerializer serializer = new JSONSerializer();
		/* 312 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(userDetailBean));
		/* 313 */ return response;
	}

	public ServerResponse login(LoginBean bean) {
		/* 318 */ ServerResponse response = new ServerResponse();

		/* 320 */ if (this.repoDao.login(bean)) {
			/* 321 */ response.setSuccess(true);
			/* 322 */ response.setResult("Succcess");
		} else {
			/* 324 */ response.setSuccess(false);
			/* 325 */ response.setResult("Invalid user/Password");
		}
		/* 327 */ return response;
	}

	public ServerResponse fetchSmePage(ManageClientPageRequest filter) {
		/* 333 */ JSONSerializer serializer = new JSONSerializer();
		/* 334 */ ServerResponse response = new ServerResponse();
		/* 335 */ response.setSuccess(true);
		/* 336 */ if (filter.getId() != null && filter.getId().longValue() != 0L) {
			/* 337 */ SmeProfile smeProfile = this.smeDao.findByID(filter.getId());

			/* 339 */ if (smeProfile == null) {
				/* 340 */ response.setResult("");
			} else {
				/* 342 */ AdminSmeProfileBean smeProfileBean = new AdminSmeProfileBean();
				/* 343 */ smeProfileBean.setAllowedAgents(smeProfile.getAllowedAgents());
				/* 344 */ smeProfileBean.setEmailId(smeProfile.getEmailId());
				/* 345 */ smeProfileBean.setAlternateNumber(smeProfile.getAlternateNumber());
				/* 346 */ smeProfileBean.setId(smeProfile.getId().longValue());
				/* 347 */ smeProfileBean.setSmeName(smeProfile.getName());
				/* 348 */ smeProfileBean.setZoneId(smeProfile.getZoneId().getId().longValue());
				/* 349 */ smeProfileBean.setLongCodeId(smeProfile.getLongcodeId().getId().longValue());
				/* 350 */ smeProfileBean.setStatus(smeProfile.getStatus());
				/* 351 */ smeProfileBean.setServiceFlag(smeProfile.getServiceFlag());
				/* 352 */ smeProfileBean.setSmeMobile(smeProfile.getSmeMobile());
				/* 353 */ smeProfileBean.setStatus(smeProfile.getStatus());
				/* 354 */ smeProfileBean.setBalance(smeProfile.getBalance());
				/* 355 */ smeProfileBean.setRecording(smeProfile.getRecording());
				/* 356 */ smeProfileBean.setMasking(smeProfile.getMasking());
				/* 357 */ smeProfileBean.setSelectionAlgo(smeProfile.getSelectionAlgo());
				/* 358 */ smeProfileBean.setBizAddress(smeProfile.getBizAddress());
				/* 359 */ smeProfileBean.setLongCode(/* 360 */ (smeProfile.getLongcodeId() == null) ? null
						: smeProfile.getLongcodeId().getLongcode());
				/* 361 */ smeProfileBean.setInShortCode(smeProfile.getLongcodeId().getInShortCode());
				/* 362 */ smeProfileBean.setOutShortCode(smeProfile.getLongcodeId().getOutShortCode());
				/* 363 */ smeProfileBean.setLanguage(smeProfile.getLanguage());
				/* 364 */ smeProfileBean.setRecValidity(smeProfile.getRecValidity());
				/* 365 */ smeProfileBean.setQueueLimit(smeProfile.getQueueLimit());
				/* 366 */ smeProfileBean.setAccountSid(smeProfile.getAccountSid());
				/* 367 */ smeProfileBean.setCallBackUrl(smeProfile.getCallBackUrl());
				/* 368 */ smeProfileBean.setStickyAlgo(smeProfile.getStickyAlgo());
				/* 369 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(smeProfileBean);
				/* 370 */ response.setResult(res);
			}
		} else {
			/* 373 */ String hql = "from SmeProfile sp left join fetch sp.longcodeId left join fetch sp.zoneId ";
			/* 374 */ String condition = this.smeDao.createQueryForSME(filter);
			/* 375 */ List<SmeProfile> list = this.smeDao.findAllSMEs(hql + condition,
					Integer.valueOf(filter.getInitialRecord()), /* 376 */ Integer.valueOf(filter.getBatchSize()));
			/* 377 */ hql = "from SmeProfile sp ";
			/* 378 */ Long rowCount = this.smeDao.getRowCount(hql + condition);
			/* 379 */ List<AdminSmeProfileBean> beanList = new ArrayList<>();
			/* 380 */ for (SmeProfile smeProfile : list) {
				/* 381 */ AdminSmeProfileBean smeProfileBean = new AdminSmeProfileBean();
				/* 382 */ smeProfileBean.setAllowedAgents(smeProfile.getAllowedAgents());
				/* 383 */ smeProfileBean.setEmailId(smeProfile.getEmailId());
				/* 384 */ smeProfileBean.setAlternateNumber(smeProfile.getAlternateNumber());
				/* 385 */ smeProfileBean.setId(smeProfile.getId().longValue());
				/* 386 */ smeProfileBean.setSmeName(smeProfile.getName());
				/* 387 */ smeProfileBean.setZoneId(smeProfile.getZoneId().getId().longValue());
				/* 388 */ smeProfileBean.setLongCodeId(smeProfile.getLongcodeId().getId().longValue());
				/* 389 */ smeProfileBean.setStatus(smeProfile.getStatus());
				/* 390 */ smeProfileBean.setServiceFlag(smeProfile.getServiceFlag());
				/* 391 */ smeProfileBean.setSmeMobile(smeProfile.getSmeMobile());
				/* 392 */ smeProfileBean.setStatus(smeProfile.getStatus());
				/* 393 */ smeProfileBean.setBalance(smeProfile.getBalance());
				/* 394 */ smeProfileBean.setRecording(smeProfile.getRecording());
				/* 395 */ smeProfileBean.setMasking(smeProfile.getMasking());
				/* 396 */ smeProfileBean.setSelectionAlgo(smeProfile.getSelectionAlgo());
				/* 397 */ smeProfileBean.setBizAddress(smeProfile.getBizAddress());
				/* 398 */ smeProfileBean.setLongCode(smeProfile.getLongcodeId().getLongcode());
				/* 399 */ smeProfileBean.setInShortCode(smeProfile.getLongcodeId().getInShortCode());
				/* 400 */ smeProfileBean.setOutShortCode(smeProfile.getLongcodeId().getOutShortCode());
				/* 401 */ smeProfileBean.setLanguage(smeProfile.getLanguage());
				/* 402 */ smeProfileBean.setRecValidity(smeProfile.getRecValidity());
				/* 403 */ smeProfileBean.setQueueLimit(smeProfile.getQueueLimit());
				/* 404 */ smeProfileBean.setAccountSid(smeProfile.getAccountSid());
				/* 405 */ smeProfileBean.setCallBackUrl(smeProfile.getCallBackUrl());
				/* 406 */ smeProfileBean.setStickyAlgo(smeProfile.getStickyAlgo());
				/* 407 */ beanList.add(smeProfileBean);
			}
			/* 409 */ SmeProfilePaginationBean pageBean = new SmeProfilePaginationBean();
			/* 410 */ pageBean.setTotalRecords(rowCount);
			/* 411 */ pageBean.setRecords(beanList);
			/* 412 */ response.setSuccess(true);
			/* 413 */ String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(pageBean);
			/* 414 */ response.setResult(res);
		}
		/* 416 */ return response;
	}

	public ServerResponse saveLongCode(LongCodeBean bean) {
		/* 421 */ Longcode longCode = new Longcode();
		/* 422 */ longCode.setHlr(bean.getHlr());
		/* 423 */ longCode.setInsertTime(new Date());
		/* 424 */ longCode.setOutShortCode(bean.getOutShortCode());
		/* 425 */ longCode.setInShortCode(bean.getInShortCode());
		/* 426 */ longCode.setLongcode(bean.getLongCode());
		/* 427 */ longCode.setSmeIdentifier(bean.getSmeIdentifier());
		/* 428 */ longCode.setStatus(bean.getStatus());
		/* 429 */ this.repoDao.saveLongCode(longCode);
		/* 430 */ ServerResponse res = new ServerResponse();
		/* 431 */ res.setSuccess(true);
		/* 432 */ res.setResult("Successfully Added");
		/* 433 */ return res;
	}

	public Users getUserInfo(String userName) {
		/* 439 */ Users user = this.repoDao.getUserInfoForLogin(userName);
		/* 440 */ if (user != null && user.getUserToken() == null) {
			/* 441 */ user.setUserToken(Utility.genrateToken(64));
		}
		/* 443 */ return user;
	}

	private String demoTemplate(EdvaantageDemoBean bean) {
		/* 447 */ StringBuilder builder = new StringBuilder();
		/* 448 */ builder.append("Name : ");
		/* 449 */ builder.append(bean.getName());
		/* 450 */ builder.append("\nEmail : ");
		/* 451 */ builder.append(bean.getEmailId());
		/* 452 */ builder.append("\nMobile : ");
		/* 453 */ builder.append(bean.getMobileNumber());
		/* 454 */ builder.append("\nPack ID : ");
		/* 455 */ builder.append(bean.getPackId());
		/* 456 */ builder.append("\nMessage : ");
		/* 457 */ builder.append(bean.getMessage());
		/* 458 */ return builder.toString();
	}

	public ServerResponse addDemoRequest(EdvaantageDemoBean bean) throws Exception {
		/* 464 */ EdvaantageDemoRequest demoRequest = new EdvaantageDemoRequest();
		/* 465 */ demoRequest.setEmailId(bean.getEmailId());
		/* 466 */ demoRequest.setMessage(bean.getMessage());
		/* 467 */ demoRequest.setMobileNumber(bean.getMobileNumber());
		/* 468 */ demoRequest.setName(bean.getName());
		/* 469 */ demoRequest.setStatus(Constants.ACTIVE_STATUS);
		/* 470 */ demoRequest.setPackId(bean.getPackId());
		/* 471 */ this.repoDao.saveDemoRequest(demoRequest);
		/* 472 */ ServerResponse res = new ServerResponse();
		/* 473 */ new MailSender("Hi Edvaantage , \n \n \t This is to inform you that someone request for a demo.\n",
				"\n\n Thanks and Regards, \n Team Wringg", demoTemplate(bean));
		/* 474 */ res.setSuccess(true);
		/* 475 */ res.setSuccess(true);
		/* 476 */ res.setResult("Successfully Done");
		/* 477 */ return res;
	}
}

/*
 * Location:
 * D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\AdminServiceImp.
 * class Java compiler version: 8 (52.0) JD-Core Version: 1.1.3
 */