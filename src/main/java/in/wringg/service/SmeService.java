package in.wringg.service;

import in.wringg.pojo.AddBulkSmsBean;
import in.wringg.pojo.AddOutBoundIvrBean;
import in.wringg.pojo.IvrCallFlowBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.PassswordTokenBean;
import in.wringg.pojo.PromptDetailsBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SettingWapperBean;
import in.wringg.pojo.SmeComplaintBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UploadedBaseDesc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONException;

public interface SmeService {
	ServerResponse resetPassword(LoginBean paramLoginBean);

	ServerResponse genPaswordToken(String paramString) throws ParseException, IOException;

	ServerResponse validateToken(String paramString1, String paramString2) throws ParseException;

	ServerResponse newPasword(String paramString, PassswordTokenBean paramPassswordTokenBean) throws ParseException;

	ServerResponse getAgentGroupDetail(Long paramLong);

	ServerResponse smeDashboardDeatial(StartEndDateRequest paramStartEndDateRequest, long paramLong);

	ServerResponse smeDashboardDeatialForAgent(long paramLong);

	ServerResponse addSmsCampaign(AddBulkSmsBean paramAddBulkSmsBean, long paramLong) throws IOException;

	void deleteSmsList(String paramString, Long paramLong);

	ServerResponse getSmeSmsList(String paramString);

	ServerResponse getSmeSmsById(String paramString, Long paramLong);

	ServerResponse updateSmsCampaign(AddBulkSmsBean paramAddBulkSmsBean, long paramLong1, long paramLong2);

	ServerResponse getIvrmanager(String paramString);

	boolean isIvrManagerExist(String paramString);

	ServerResponse addIvrmanager(List<IvrCallFlowBean> paramList, String paramString) throws IOException;

	ServerResponse getMissCallMarketingList(StartEndDateRequest paramStartEndDateRequest, long paramLong);

	ServerResponse recordingList(StartEndDateRequest paramStartEndDateRequest, long paramLong);

	ServerResponse addSmeComplaint(SmeComplaintBean paramSmeComplaintBean, Long paramLong);

	ServerResponse updateSmeComplaint(SmeComplaintBean paramSmeComplaintBean, Long paramLong);

	ServerResponse getSmeComplaints(Long paramLong);

	ServerResponse deleteSmeComplaint(Long paramLong1, Long paramLong2);

	ServerResponse addOutboundIvr(AddOutBoundIvrBean paramAddOutBoundIvrBean);

	ServerResponse updateOutbounfIvr(AddOutBoundIvrBean paramAddOutBoundIvrBean, BigInteger paramBigInteger);

	ServerResponse deleteOutbounfIvr(BigInteger paramBigInteger);

	ServerResponse getOutbounfIvr();

	ServerResponse getCDRList(ManageClientPageRequest paramManageClientPageRequest, long paramLong, Byte paramByte)
			throws FileNotFoundException, IOException;

	ServerResponse fetchSetting(Long paramLong) throws JSONException;

	ServerResponse updateSetting(Long paramLong, SettingWapperBean paramSettingWapperBean);

	ServerResponse refreshUserToken(Long paramLong);

	ServerResponse fetchAgentReportDetail(Long paramLong, ManageClientPageRequest paramManageClientPageRequest);

	ServerResponse fetchCdrDropDown() throws FileNotFoundException, IOException;

	ServerResponse fetchSmeBalance(Long paramLong) throws JSONException;

	ServerResponse agentDashboardSummary(Long paramLong, Byte paramByte);

	ServerResponse agentDashboardSummary(Long paramLong);

	ServerResponse getAlertNotications(String paramString, int paramInt);

	ServerResponse updateAlertNotification(int paramInt);

	ServerResponse deleteAlertNotification(int paramInt);

	ServerResponse getRecordingList(String paramString);

	ServerResponse getOutLeg(Long paramLong, String paramString, byte paramByte)
			throws FileNotFoundException, IOException;

	ServerResponse saveBaseFileData(Sheet paramSheet, String paramString1, String paramString2, long paramLong,
			String paramString3);

	ServerResponse saveBaseList(List<UploadedBaseDesc> paramList, String paramString1, String paramString2,
			long paramLong);

	ServerResponse getBaseUploadHistory(long paramLong);

	ServerResponse getCDRForAgent(long paramLong, byte paramByte) throws FileNotFoundException, IOException;

	ServerResponse getMissCallCDRForAgent(long paramLong) throws FileNotFoundException, IOException;

	ServerResponse savePrompt(PromptDetailsBean promptDetails);

	ServerResponse getSmePrompt(Long id, Long promptId,int pageNum,int pageSize);
}
