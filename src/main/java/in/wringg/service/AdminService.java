package in.wringg.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import in.wringg.entity.Users;
import in.wringg.pojo.AdminDashboardRequest;
import in.wringg.pojo.AdminSmeProfileBean;
import in.wringg.pojo.EdvaantageDemoBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.LongCodeBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import java.text.ParseException;

public interface AdminService {
  ServerResponse smeProcessor(String paramString, AdminSmeProfileBean paramAdminSmeProfileBean) throws Exception;
  
  ServerResponse adminDashboardRevenue(AdminDashboardRequest paramAdminDashboardRequest);
  
  ServerResponse adminSystemSnapshot(AdminDashboardRequest paramAdminDashboardRequest) throws ParseException;
  
  ServerResponse getZone();
  
  ServerResponse getLongCode(Byte paramByte);
  
  ServerResponse updateLongCode(Long paramLong, Byte paramByte);
  
  ServerResponse saveLongCode(LongCodeBean paramLongCodeBean);
  
  ServerResponse fetchSme(StartEndDateRequest paramStartEndDateRequest);
  
  ServerResponse getServiceList() throws JsonProcessingException;
  
  ServerResponse getMaxAgent();
  
  ServerResponse getUserRoleDetail(String paramString);
  
  ServerResponse login(LoginBean paramLoginBean);
  
  ServerResponse fetchSmePage(ManageClientPageRequest paramManageClientPageRequest);
  
  Users getUserInfo(String paramString);
  
  ServerResponse addDemoRequest(EdvaantageDemoBean paramEdvaantageDemoBean) throws Exception;
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\AdminService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */