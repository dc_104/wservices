package in.wringg.service;

import in.wringg.dao.AdminDao;
import in.wringg.entity.UserRole;
import in.wringg.entity.Users;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component("userDetailService")
public class UserDetailServiceImp
  implements UserDetailsService
{
  @Autowired
  private AdminDao userDao;
  
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
/* 28 */     Users user = this.userDao.getUserInfoForLogin(userName);
    
/* 30 */     if (user == null) {
/* 31 */       throw new UsernameNotFoundException("Invalid username or password.");
    }
/* 33 */     List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
/* 34 */     return (UserDetails)new User(user.getUsername(), user
/* 35 */         .getPassword(), authorities);
  }

  
  private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {
/* 40 */     Set<GrantedAuthority> setAuths = new HashSet<>();

    
/* 43 */     for (UserRole userRole : userRoles) {
/* 44 */       setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
    }
    
/* 47 */     List<GrantedAuthority> Result = new ArrayList<>(setAuths);

    
/* 50 */     return Result;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\UserDetailServiceImp.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */