package in.wringg.service;

import in.wringg.pojo.CallCdrBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;

public interface ComponentService {
  ServerResponse addCallCdr(CallCdrBean paramCallCdrBean);
  
  ServerResponse changeSchduleStatus(Long paramLong);
  
  ServerResponse chargingDetails(StartEndDateRequest paramStartEndDateRequest, Long paramLong);
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\ComponentService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */