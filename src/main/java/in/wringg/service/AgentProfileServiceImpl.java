package in.wringg.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import flexjson.JSONSerializer;
import in.wringg.dao.AdminDao;
import in.wringg.dao.AgentProfileDao;
import in.wringg.entity.AddressBook;
import in.wringg.entity.AgentCallingDetail;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentDetailTime;
import in.wringg.entity.AgentGroupDetail;
import in.wringg.entity.AgentLunchDetail;
import in.wringg.entity.CallSchedulerBase;
import in.wringg.entity.SmeCustomerRemarks;
import in.wringg.entity.Users;
import in.wringg.pojo.AddressBookBean;
import in.wringg.pojo.AgentDetailBean;
import in.wringg.pojo.AgentInsightPojo;
import in.wringg.pojo.AgentOrderBean;
import in.wringg.pojo.BreakPojo;
import in.wringg.pojo.BreakWrapper;
import in.wringg.pojo.CallBaseBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SmeCustomerRemarksBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import in.wringg.utility.Utility;

@Transactional
@Service("agentDetail")
public class AgentProfileServiceImpl implements AgentProfileService {
	@Autowired
	AgentProfileDao agentDao;
	@Autowired
	private AdminDao adminDao;

	public ServerResponse addAgentProfile(AgentDetailBean bean) throws IOException {
		/* 60 */ ServerResponse response = new ServerResponse();
		/* 61 */ response = this.agentDao.createAgent(bean);
		/* 62 */ return response;
	}

	public ServerResponse updateAgentProfile(AgentDetailBean bean, Long id) throws IOException {
		/* 67 */ ServerResponse response = this.agentDao.updateAgentDeatil(bean, id);
		/* 68 */ return response;
	}

	public ServerResponse deleteAgentProfile(Long id) {
		/* 73 */ ServerResponse res = new ServerResponse();
		/* 74 */ AgentDetail detail = this.agentDao.fetchSingleAgent(id);
		/* 75 */ if (detail != null && detail.getStatus().byteValue() != -9) {
			/* 76 */ this.agentDao.deleteSingleAgent(id);
			/* 77 */ res.setSuccess(true);
			/* 78 */ Users users = this.adminDao.getUserInfo(detail.getAgentEmail());
			/* 79 */ if (users != null) {
				/* 80 */ users.setEnabled((byte) -9);
			}
			/* 82 */ res.setResult("Successfully Deleted");
		} else {
			/* 84 */ res.setSuccess(false);
			/* 85 */ ErrorDetails error = new ErrorDetails();
			/* 86 */ error.setErrorCode(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorCode());
			/* 87 */ error.setErrorString(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getErrorMessage());
			/* 88 */ error.setDisplayMessage(ErrorConstants.ERROR_PROFILE_NOT_FOUND.getDisplayMessage());
			/* 89 */ JSONSerializer serializer = new JSONSerializer();
			/* 90 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 91 */ res.setResult(response);
		}
		/* 93 */ return res;
	}

	public ServerResponse listAgentProfile(Long id) {
		/* 98 */ JSONSerializer serializer = new JSONSerializer();
		/* 99 */ ServerResponse response = new ServerResponse();
		/* 100 */ List<AgentDetailBean> detailBeans = new ArrayList<>();
		/* 101 */ List<AgentDetail> agentDetails = this.agentDao.fetchAgentDetailList(id);
		/* 102 */ for (AgentDetail obj : agentDetails) {
			/* 103 */ AgentDetailBean bean = new AgentDetailBean();
			/* 104 */ bean.setStickyDays(obj.getStickyDays());
			/* 105 */ bean.setAgentScore(obj.getAgentScore());
			/* 106 */ bean.setAgentMasking(obj.getAgentMasking());
			/* 107 */ bean.setStickyAgent(obj.getStickyAgent());
			/* 108 */ bean.setAgentEmail(obj.getAgentEmail());
			/* 109 */ bean.setAgentExtention(obj.getAgentExtention());
			/* 110 */ bean.setAgentId(obj.getAgentId());
			/* 111 */ bean.setAgentMobile(obj.getAgentMobile());
			/* 112 */ bean.setAgentName(obj.getAgentName());
			/* 113 */ bean.setInsertTime(obj.getInsertTime());
			/* 114 */ bean.setInTime(obj.getInTime());
			/* 115 */ bean.setOutTime(obj.getOutTime());
			/* 116 */ bean.setSmeId(obj.getSmeId());
			/* 117 */ bean.setStatus(obj.getStatus());
			/* 118 */ bean.setDaysFlag(obj.getDaysFlag());
			/* 119 */ bean.setInDate(
					Utility.getFormattedDateTime(Utility.timeToDate(obj.getInTime()), "yyyy-MM-dd HH:mm:ss"));
			/* 120 */ bean.setOutDate(
					Utility.getFormattedDateTime(Utility.timeToDate(obj.getOutTime()), "yyyy-MM-dd HH:mm:ss"));
			/* 121 */ bean.setAssignFailedCalls(obj.getAssignFailedCalls());
			/* 122 */ bean.setAssignVoicemailCalls(obj.getAssignVoicemailCalls());
			bean.setInPermissionFlag(obj.getInPermissionFlag());
			bean.setOutPermissionFlag(obj.getOutPermissionFlag());
			/* 123 */ List<Long> groups = new ArrayList<>();
			for (AgentGroupDetail group : obj.getGroups()) {
				groups.add(group.getGroupId());
			}
			/* 127 */ bean.setAgentGroups(groups);
			/* 128 */ detailBeans.add(bean);
		}
		/* 130 */ response.setSuccess(true);
		/* 131 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(detailBeans));
		return response;
	}

	public ServerResponse canCreateAgent(Long smeId) {
		/* 137 */ ServerResponse response = new ServerResponse();
		/* 138 */ boolean permission = this.agentDao.canCreateAgent(smeId);
		if (permission) {
			/* 140 */ response.setSuccess(true);
			/* 141 */ response.setResult("Can create more agents");
		} else {
			response.setResult("Capacity full");
		}

		/* 146 */ return response;
	}

	public ServerResponse maintainOrder(Long id, List<AgentOrderBean> orderList) {
		for (AgentOrderBean order : orderList) {
			/* 152 */ AgentDetail agent = this.agentDao.fetchSingleAgent(order.getId());
			/* 153 */ agent.setAgentPosition(order.getOrder());
		}
		/* 155 */ ServerResponse response = new ServerResponse();
		/* 156 */ response.setSuccess(true);
		/* 157 */ response.setResult("Order Updated");
		/* 158 */ return response;
	}

	/* 161 */ private static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("MM-dd-yyyy HH:mm");

	public String castFormattedDate(Date result) {
		Date d1;
		/* 165 */ if (result == null) {
			/* 166 */ return null;
		}
		/* 168 */ String dateString = DATE_FORMATER.format(result);

		try {
			/* 171 */ d1 = DATE_FORMATER.parse(dateString);
			/* 172 */ } catch (ParseException e) {
			/* 173 */ e.printStackTrace();
			/* 174 */ return null;
		}
		/* 176 */ DATE_FORMATER.applyPattern("dd MMM, yyyy | hh:mm a");
		/* 177 */ return DATE_FORMATER.format(d1).toString();
	}

	private static final String PATTERN = "dd MMM, yyyy | hh:mm a";

	public ServerResponse getBaseForAgent(Long agentDetail) {
		/* 182 */ JSONSerializer serializer = new JSONSerializer();
		/* 183 */ ServerResponse response = new ServerResponse();
		/* 184 */ response.setSuccess(true);
		/* 185 */ List<CallSchedulerBase> daoList = this.agentDao.getBaseForAgent(agentDetail);

		/* 190 */ List<CallBaseBean> baseList = (List<CallBaseBean>) daoList.parallelStream()
				.map(dao -> new CallBaseBean(dao.getBaseId().longValue(), dao.getBaseId().longValue(), dao.getMobile(),
						dao.getDescription(), dao.getInsertTime(), dao.getCallCounter(), dao.getStatus(),
						dao.getScheduleDateTime(), dao.getCallType(), castFormattedDate(dao.getScheduleDateTime())))
				.collect(Collectors.toList());
		/* 191 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(baseList));
		/* 192 */ return response;
	}

	public ServerResponse getAgentInsighnt(StartEndDateRequest request) {
		/* 197 */ long totalCalls = 0L;
		/* 198 */ long totalInCalls = 0L;
		/* 199 */ long totalOutCalls = 0L;
		/* 200 */ long noAnswer = 0L;
		/* 201 */ long inSuccessCalls = 0L;
		/* 202 */ long inFailedCalls = 0L;
		/* 203 */ long outSuccessCalls = 0L;
		/* 204 */ long outFailedCalls = 0L;
		/* 205 */ long avgCallDuration = 0L;
		/* 206 */ long totalCallDuration = 0L;
		/* 207 */ long officeHours = 0L;
		/* 208 */ long lunchHours = 0L;
		/* 209 */ JSONSerializer serializer = new JSONSerializer();
		/* 210 */ ServerResponse response = new ServerResponse();
		/* 211 */ response.setSuccess(true);
		/* 212 */ List<AgentCallingDetail> daoList = this.agentDao.getAgentCallingDetail(request);
		/* 213 */ for (AgentCallingDetail acd : daoList) {
			/* 214 */ totalCalls += (acd.getTotalCalls() == null) ? 0L : acd.getTotalCalls().longValue();
			/* 215 */ totalInCalls += (acd.getTotalInCalls() == null) ? 0L : acd.getTotalInCalls().longValue();
			/* 216 */ totalOutCalls += (acd.getTotalOutCalls() == null) ? 0L : acd.getTotalOutCalls().longValue();
			/* 217 */ noAnswer += (acd.getNoAnswer() == null) ? 0L : acd.getNoAnswer().longValue();
			/* 218 */ inSuccessCalls += (acd.getInSuccessCalls() == null) ? 0L : acd.getInSuccessCalls().longValue();
			/* 219 */ inFailedCalls += (acd.getInFailedCalls() == null) ? 0L : acd.getInFailedCalls().longValue();
			/* 220 */ outSuccessCalls += (acd.getOutFailedCalls() == null) ? 0L : acd.getOutSuccessCalls().longValue();
			/* 221 */ outFailedCalls += (acd.getOutFailedCalls() == null) ? 0L : acd.getOutFailedCalls().longValue();
			/* 222 */ avgCallDuration += (acd.getAvgCallDuration() == null) ? 0L : acd.getAvgCallDuration().longValue();
			/* 223 */ totalCallDuration += (acd.getTotalCallDuration() == null) ? 0L
					: acd.getTotalCallDuration().longValue();
			/* 224 */ officeHours += (acd.getOfficeHours() == null) ? 0L : acd.getOfficeHours().longValue();
			/* 225 */ lunchHours += (acd.getLunchHours() == null) ? 0L : acd.getLunchHours().longValue();
		}
		/* 227 */ avgCallDuration = (daoList.size() == 0) ? 0L : (avgCallDuration / daoList.size());

		/* 230 */ AgentInsightPojo insightPojo = new AgentInsightPojo(request.getId(), Long.valueOf(totalCalls),
				Long.valueOf(totalInCalls), Long.valueOf(totalOutCalls), Long.valueOf(noAnswer),
				Long.valueOf(inSuccessCalls), Long.valueOf(inFailedCalls), Long.valueOf(outSuccessCalls),
				Long.valueOf(outFailedCalls), Long.valueOf(avgCallDuration), Long.valueOf(totalCallDuration),
				Long.valueOf(officeHours), Long.valueOf(lunchHours));
		/* 231 */ insightPojo
				.setInSuccess((totalInCalls == 0L) ? 0.0D : (100L * (totalInCalls - inFailedCalls) / totalInCalls));
		/* 232 */ insightPojo.setOutSuccess(
				(totalOutCalls == 0L) ? 0.0D : (100L * (totalOutCalls - outFailedCalls) / totalOutCalls));
		/* 233 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(insightPojo));
		/* 234 */ return response;
	}

	private boolean isSameDay(Date date, Date current) {
		/* 238 */ LocalDate localDate1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		/* 239 */ LocalDate localDate2 = current.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		/* 240 */ return localDate1.isEqual(localDate2);
	}

	public ServerResponse lunchIn(Long smeId, Long agentId, Date date, String message) {
		/* 245 */ ServerResponse res = new ServerResponse();
		/* 246 */ AgentDetail agentDetail = this.agentDao.fetchSingleAgent(agentId);
		/* 247 */ if (agentDetail.getStatus() == Constants.ACTIVE_STATUS
				|| agentDetail.getStatus() == Constants.LUNCH) {
			/* 248 */ AgentLunchDetail agentLunchDetail = this.agentDao.getLastInEntry(agentId);
			/* 249 */ if (agentLunchDetail != null && isSameDay(agentLunchDetail.getInTime(), date)
					&& agentLunchDetail/* 250 */ .getOutTime() == null) {
				/* 251 */ res.setSuccess(false);
				/* 252 */ ErrorDetails errorDetails = new ErrorDetails();
				/* 253 */ errorDetails.setErrorCode(ErrorConstants.ERROR_ALREADY_LUNCH_IN.getErrorCode());
				/* 254 */ errorDetails.setErrorString(ErrorConstants.ERROR_ALREADY_LUNCH_IN.getErrorMessage());
				/* 255 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_ALREADY_LUNCH_IN.getDisplayMessage());
				/* 256 */ JSONSerializer jSONSerializer = new JSONSerializer();
				/* 257 */ String str = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
				/* 258 */ res.setResult(str);
				/* 259 */ return res;
			}
			/* 261 */ this.agentDao.lunchInEntry(smeId, agentId, date, message);
			/* 262 */ agentDetail.setStatus(Constants.LUNCH);
			/* 263 */ res.setSuccess(true);
			/* 264 */ res.setResult("Successfully Done");
			/* 265 */ return res;
		}
		/* 267 */ ErrorDetails error = new ErrorDetails();
		/* 268 */ error.setErrorCode(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getErrorCode());
		/* 269 */ error.setErrorString(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getErrorMessage());
		/* 270 */ error.setDisplayMessage(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getDisplayMessage());
		/* 271 */ JSONSerializer serializer = new JSONSerializer();
		/* 272 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 273 */ res.setResult(response);
		/* 274 */ return res;
	}

	public ServerResponse lunchOut(Long agentId, Date date) {
		/* 279 */ ServerResponse res = new ServerResponse();
		/* 280 */ AgentDetail agentDetail = this.agentDao.fetchSingleAgent(agentId);
		/* 281 */ if (agentDetail.getStatus() == Constants.ACTIVE_STATUS
				|| agentDetail.getStatus() == Constants.LUNCH) {
			/* 282 */ AgentLunchDetail agentLunchDetail = this.agentDao.getLastInEntry(agentId);
			/* 283 */ if (agentLunchDetail != null && isSameDay(agentLunchDetail.getInsertDate(), date)
					&& agentLunchDetail/* 284 */ .getOutTime() == null) {
				/* 285 */ agentLunchDetail.setOutTime(date);
				/* 286 */ agentDetail.setStatus(Constants.ACTIVE_STATUS);
				/* 287 */ res.setSuccess(true);
				/* 288 */ res.setResult("Successfully Done");
				/* 289 */ return res;
			}
			/* 291 */ ErrorDetails errorDetails = new ErrorDetails();
			/* 292 */ errorDetails.setErrorCode(ErrorConstants.ERROR_NOT_LUNCH_IN.getErrorCode());
			/* 293 */ errorDetails.setErrorString(ErrorConstants.ERROR_NOT_LUNCH_IN.getErrorMessage());
			/* 294 */ errorDetails.setDisplayMessage(ErrorConstants.ERROR_NOT_LUNCH_IN.getDisplayMessage());
			/* 295 */ JSONSerializer jSONSerializer = new JSONSerializer();
			/* 296 */ String str = jSONSerializer.exclude(new String[] { "*.class" }).serialize(errorDetails);
			/* 297 */ res.setResult(str);
			/* 298 */ return res;
		}
		/* 300 */ ErrorDetails error = new ErrorDetails();
		/* 301 */ error.setErrorCode(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getErrorCode());
		/* 302 */ error.setErrorString(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getErrorMessage());
		/* 303 */ error.setDisplayMessage(ErrorConstants.ERROR_AGENT_NOT_ELIGIBLE.getDisplayMessage());
		/* 304 */ JSONSerializer serializer = new JSONSerializer();
		/* 305 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 306 */ res.setResult(response);
		/* 307 */ return res;
	}

	private String secondsToStr(long seconds) {
		/* 311 */ StringBuilder time = new StringBuilder();
		/* 312 */ time.append(String.format("%02d", new Object[] { Long.valueOf(seconds / 3600L) }) + ":");
		/* 313 */ time.append(String.format("%02d", new Object[] { Long.valueOf(seconds / 60L % 60L) }) + ":");
		/* 314 */ time.append(String.format("%02d", new Object[] { Long.valueOf(seconds % 60L) }));
		/* 315 */ return time.toString();
	}

	public ServerResponse breakList(Long agentId) {
		/* 320 */ ServerResponse res = new ServerResponse();
		/* 321 */ BreakWrapper breakWrapper = new BreakWrapper();
		/* 322 */ List<AgentLunchDetail> agentLunchDetails = this.agentDao.getSameDayBrkEntry(agentId);
		/* 323 */ List<BreakPojo> breakPojos = new ArrayList<>();
		/* 324 */ AgentDetailTime agentDetailTime = this.agentDao.getTimeForToday(agentId);
		/* 325 */ if (agentDetailTime != null) {
			/* 326 */ breakWrapper.setSignIn(agentDetailTime.getInTime());
			/* 327 */ breakWrapper.setSignOut(agentDetailTime.getOutTime());
			/* 328 */ breakWrapper.setSignInStr(agentDetailTime.getInTime().toString());
			/* 329 */ breakWrapper.setSignOutStr(agentDetailTime.getOutTime().toString());
			/* 330 */ LocalDateTime currentDateTime = LocalDateTime.now().plusHours(5L).plusMinutes(30L);
			/* 331 */ LocalDate date = LocalDate.now();
			/* 332 */ LocalDateTime singOutDateTime = date.atTime(agentDetailTime.getOutTime().getHours(),
					agentDetailTime/* 333 */ .getOutTime().getMinutes(), agentDetailTime.getOutTime().getSeconds());

			/* 335 */ if (currentDateTime.isAfter(singOutDateTime)) {
				/* 336 */ currentDateTime = singOutDateTime;
			}

			/* 339 */ LocalDateTime localDateAtDayStart = date.atTime(agentDetailTime.getInTime().getHours(),
					agentDetailTime/* 340 */ .getInTime().getMinutes(), agentDetailTime.getInTime().getSeconds());
			/* 341 */ Duration duration = Duration.between(localDateAtDayStart, currentDateTime);
			/* 342 */ breakWrapper.setTotalActiveTime(duration.getSeconds());
			/* 343 */ breakWrapper.setTotalActiveTimeStr(secondsToStr(duration.getSeconds()));
		}
		/* 345 */ long breakSeconds = 0L;
		/* 346 */ for (AgentLunchDetail lunchDetail : agentLunchDetails) {
			/* 347 */ BreakPojo breakPojo = new BreakPojo();
			/* 348 */ breakPojo.setBreakIn(lunchDetail.getInTime());
			/* 349 */ breakPojo.setBreakOut(lunchDetail.getOutTime());
			/* 350 */ breakPojo.setBreakInStr((lunchDetail.getInTime() == null) ? null
					: lunchDetail/* 351 */ .getInTime().toString().substring(0,
							lunchDetail.getInTime().toString().length() - 2));
			/* 352 */ breakPojo.setBreakOutStr((lunchDetail.getOutTime() == null) ? null
					: lunchDetail/* 353 */ .getOutTime().toString().substring(0,
							lunchDetail/* 354 */ .getOutTime().toString().length() - 2));
			/* 355 */ long diffInSecond = Math
					.abs(lunchDetail.getOutTime().getTime() - lunchDetail.getInTime().getTime()) / 1000L;

			/* 357 */ breakSeconds += diffInSecond;
			/* 358 */ breakPojos.add(breakPojo);
		}
		/* 360 */ JSONSerializer serializer = new JSONSerializer();
		/* 361 */ breakWrapper.setBreaks(breakPojos);
		/* 362 */ breakWrapper.setTotalBreakTime(breakSeconds);
		/* 363 */ breakWrapper.setTotalBreakTimeStr(secondsToStr(breakSeconds));
		/* 364 */ breakWrapper.setStatus(this.agentDao.fetchSingleAgent(agentId).getStatus());
		/* 365 */ breakWrapper
				.setSchAssinedCount(this.agentDao.callSchBaseCounterOnCallType(agentId, Integer.valueOf(0)));
		/* 366 */ breakWrapper
				.setSchFollowUpCount(this.agentDao.callSchBaseCounterOnCallType(agentId, Integer.valueOf(1)));
		/* 367 */ String response = serializer.exclude(new String[] { "*.class" }).deepSerialize(breakWrapper);
		/* 368 */ res.setResult(response);
		/* 369 */ res.setSuccess(true);
		/* 370 */ return res;
	}

	public ServerResponse addAddressBook(AddressBookBean bean) {
		/* 375 */ ServerResponse res = new ServerResponse();
		/* 376 */ Long smeId = this.agentDao.fetchSingleAgent(bean.getCreatedBy()).getSmeId();
		/* 377 */ AddressBook addressBook = this.agentDao.getAddressBookBySmeIdAndMobile(smeId,
				bean/* 378 */ .getCustomerNumberPrimary().trim());

		/* 380 */ if (addressBook != null && addressBook.getStatus().byteValue() != -9) {
			/* 381 */ ErrorDetails error = new ErrorDetails();
			/* 382 */ error.setErrorCode(ErrorConstants.DUPLICATE_ADDRESS_BOOK.getErrorCode());
			/* 383 */ error.setErrorString(ErrorConstants.DUPLICATE_ADDRESS_BOOK.getErrorMessage());
			/* 384 */ error.setDisplayMessage(ErrorConstants.DUPLICATE_ADDRESS_BOOK.getDisplayMessage());
			/* 385 */ JSONSerializer serializer = new JSONSerializer();
			/* 386 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 387 */ res.setResult(response);
			/* 388 */ return res;
		}
		/* 390 */ addressBook = (addressBook != null) ? addressBook : new AddressBook();
		/* 391 */ addressBook.setCompanyName(bean.getCompanyName());
		/* 392 */ addressBook.setCreatedBy(bean.getCreatedBy());
		/* 393 */ addressBook.setCustomerName(bean.getCustomerName());
		/* 394 */ addressBook.setCustomerNumberPrimary(bean.getCustomerNumberPrimary());
		/* 395 */ if (!CollectionUtils.isEmpty(bean.getCustomerNumberSecondary())) {
			/* 396 */ addressBook.setCustomerNumberSecondary(String.join(",", bean.getCustomerNumberSecondary()));
		}
		/* 398 */ addressBook.setEmailId(bean.getEmailId());
		/* 399 */ addressBook.setSmeId(smeId);
		/* 400 */ addressBook.setInsertDateTime(new Date());
		/* 401 */ addressBook.setMode(bean.getMode());
		/* 402 */ addressBook.setStatus(Constants.ACTIVE_STATUS);
		/* 403 */ addressBook.setVisibilityFlag(bean.getVisibilityFlag());
		/* 404 */ addressBook.setIsUpdated(Constants.ACTIVE_STATUS);
		/* 405 */ this.agentDao.saveAddressBook(addressBook);
		/* 406 */ res.setSuccess(true);
		/* 407 */ res.setResult("Successfully Done");
		/* 408 */ return res;
	}

	public ServerResponse updateAddressBook(AddressBookBean bean) {
		/* 413 */ ServerResponse res = new ServerResponse();
		/* 414 */ AddressBook addressBook = this.agentDao.getAddressBookById(bean.getAddressBookId());
		/* 415 */ if (addressBook == null || addressBook.getStatus().byteValue() == -9) {
			/* 416 */ ErrorDetails error = new ErrorDetails();
			/* 417 */ error.setErrorCode(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorCode());
			/* 418 */ error.setErrorString(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorMessage());
			/* 419 */ error.setDisplayMessage(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getDisplayMessage());
			/* 420 */ JSONSerializer serializer = new JSONSerializer();
			/* 421 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 422 */ res.setResult(response);
			/* 423 */ return res;
		}
		/* 425 */ addressBook = (addressBook != null) ? addressBook : new AddressBook();
		/* 426 */ addressBook.setCompanyName(bean.getCompanyName());
		/* 427 */ addressBook.setCreatedBy(bean.getCreatedBy());
		/* 428 */ addressBook.setCustomerName(bean.getCustomerName());
		/* 429 */ addressBook.setCustomerNumberPrimary(bean.getCustomerNumberPrimary());
		/* 430 */ if (!CollectionUtils.isEmpty(bean.getCustomerNumberSecondary())) {
			/* 431 */ addressBook.setCustomerNumberSecondary(String.join(",", bean.getCustomerNumberSecondary()));
		}
		/* 433 */ addressBook.setEmailId(bean.getEmailId());
		/* 434 */ addressBook.setMode(bean.getMode());
		/* 435 */ if (bean.getStatus() != null && bean.getStatus().byteValue() == -7) {
			/* 436 */ addressBook.setStatus(Byte.valueOf((byte) -7));
		}
		/* 438 */ addressBook.setUpdatedDateTime(new Date());
		/* 439 */ addressBook.setVisibilityFlag(bean.getVisibilityFlag());
		/* 440 */ addressBook.setIsUpdated(Constants.ACTIVE_STATUS);
		/* 441 */ res.setSuccess(true);
		/* 442 */ res.setResult("Successfully Done");
		/* 443 */ return res;
	}

	public ServerResponse deleteAddressBook(Long id) {
		/* 448 */ ServerResponse res = new ServerResponse();
		/* 449 */ AddressBook addressBook = this.agentDao.getAddressBookById(id);
		/* 450 */ if (addressBook == null || addressBook.getStatus().byteValue() == -9) {
			/* 451 */ ErrorDetails error = new ErrorDetails();
			/* 452 */ error.setErrorCode(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorCode());
			/* 453 */ error.setErrorString(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorMessage());
			/* 454 */ error.setDisplayMessage(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getDisplayMessage());
			/* 455 */ JSONSerializer serializer = new JSONSerializer();
			/* 456 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 457 */ res.setResult(response);
			/* 458 */ return res;
		}

		/* 461 */ addressBook.setStatus(Byte.valueOf((byte) -9));
		/* 462 */ res.setSuccess(true);
		/* 463 */ res.setResult("Successfully Done");
		/* 464 */ return res;
	}

	private AddressBookBean entityToBean(AddressBook addressBook) {
		/* 468 */ AddressBookBean bean = new AddressBookBean();
		/* 469 */ bean.setCompanyName(addressBook.getCompanyName());
		/* 470 */ bean.setCreatedBy(addressBook.getCreatedBy());
		/* 471 */ bean.setCustomerName(addressBook.getCustomerName());
		/* 472 */ bean.setCustomerNumberPrimary(addressBook.getCustomerNumberPrimary());
		/* 473 */ if (addressBook.getCustomerNumberSecondary() != null) {

			/* 475 */ List<String> numberSecondary = (List<String>) Stream
					.<String>of(addressBook.getCustomerNumberSecondary().split(",", -1)).collect(Collectors.toList());
			/* 476 */ bean.setCustomerNumberSecondary(numberSecondary);
		}
		/* 478 */ bean.setEmailId(addressBook.getEmailId());
		/* 479 */ bean.setAddressBookId(addressBook.getId());
		/* 480 */ bean.setInsertDateTime(addressBook.getInsertDateTime());
		/* 481 */ bean.setMode(addressBook.getMode());
		/* 482 */ bean.setSmeId(bean.getSmeId());
		/* 483 */ bean.setStatus(addressBook.getStatus());
		/* 484 */ bean.setUpdatedDateTime(addressBook.getUpdatedDateTime());
		/* 485 */ bean.setVisibilityFlag(addressBook.getVisibilityFlag());

		/* 487 */ return bean;
	}

	public ServerResponse getAddressBookForAgent(Long agentId) {
		/* 492 */ ServerResponse res = new ServerResponse();
		/* 493 */ JSONSerializer serializer = new JSONSerializer();
		/* 494 */ List<AddressBookBean> addressBookBeans = new ArrayList<>();
		/* 495 */ List<AddressBook> addressBooks = this.agentDao.getAddressBookByAgentId(agentId);
		/* 496 */ for (AddressBook addressBook : addressBooks) {
			/* 497 */ addressBookBeans.add(entityToBean(addressBook));
		}
		/* 499 */ String response = serializer.exclude(new String[] { "*.class" }).deepSerialize(addressBookBeans);
		/* 500 */ res.setResult(response);
		/* 501 */ res.setSuccess(true);
		/* 502 */ return res;
	}

	public ServerResponse getAddressBookForSme(Long agentId) {
		/* 507 */ Long smeId = this.agentDao.fetchSingleAgent(agentId).getSmeId();
		/* 508 */ ServerResponse res = new ServerResponse();
		/* 509 */ JSONSerializer serializer = new JSONSerializer();
		/* 510 */ List<AddressBookBean> addressBookBeans = new ArrayList<>();
		/* 511 */ List<AddressBook> addressBooks = this.agentDao.getAddressBookBySmeId(smeId);
		/* 512 */ for (AddressBook addressBook : addressBooks) {
			/* 513 */ addressBookBeans.add(entityToBean(addressBook));
		}
		/* 515 */ String response = serializer.exclude(new String[] { "*.class" }).deepSerialize(addressBookBeans);
		/* 516 */ res.setResult(response);
		/* 517 */ res.setSuccess(true);
		/* 518 */ return res;
	}

	public ServerResponse addRemaks(SmeCustomerRemarksBean bean) {
		/* 523 */ Long smeId = this.agentDao.fetchSingleAgent(bean.getAgentId()).getSmeId();
		/* 524 */ SmeCustomerRemarks customerRemarks = new SmeCustomerRemarks();
		/* 525 */ customerRemarks.setAddressBookId(bean.getAddressBookId());
		/* 526 */ customerRemarks.setAgentId(bean.getAgentId());
		/* 527 */ customerRemarks.setInsertDateTime(new Date());
		/* 528 */ customerRemarks.setRemarks(bean.getRemarks());
		/* 529 */ customerRemarks.setSmeId(smeId);
		/* 530 */ customerRemarks.setStatus(Constants.ACTIVE_STATUS);
		/* 531 */ this.agentDao.saveSmeCustomerRemarks(customerRemarks);
		/* 532 */ ServerResponse res = new ServerResponse();
		/* 533 */ res.setSuccess(true);
		/* 534 */ res.setResult("Successfully Done");
		/* 535 */ return res;
	}

	public ServerResponse updateRemaks(SmeCustomerRemarksBean bean) {
		/* 540 */ ServerResponse res = new ServerResponse();
		/* 541 */ SmeCustomerRemarks customerRemarks = this.agentDao.getSmeCustomerRemarks(bean.getId());
		/* 542 */ if (customerRemarks == null) {
			/* 543 */ ErrorDetails error = new ErrorDetails();
			/* 544 */ error.setErrorCode(ErrorConstants.REMARK_NOT_EXIST.getErrorCode());
			/* 545 */ error.setErrorString(ErrorConstants.REMARK_NOT_EXIST.getErrorMessage());
			/* 546 */ error.setDisplayMessage(ErrorConstants.REMARK_NOT_EXIST.getDisplayMessage());
			/* 547 */ JSONSerializer serializer = new JSONSerializer();
			/* 548 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 549 */ res.setResult(response);
			/* 550 */ return res;
		}
		/* 552 */ customerRemarks.setUpdatedDateTime(new Date());
		/* 553 */ customerRemarks.setRemarks(bean.getRemarks());
		/* 554 */ res.setSuccess(true);
		/* 555 */ res.setResult("Successfully Done");
		/* 556 */ return res;
	}

	public ServerResponse getRemaksByAddressBook(Long addressBookId, Long agentId) {
		/* 561 */ ServerResponse res = new ServerResponse();
		/* 562 */ List<SmeCustomerRemarks> customerRemarks = this.agentDao.getRemaksByAddressBookId(addressBookId);
		/* 563 */ List<SmeCustomerRemarksBean> beanList = new ArrayList<>();
		/* 564 */ AddressBook addressBook = this.agentDao.getAddressBookById(addressBookId);
		/* 565 */ for (SmeCustomerRemarks customerRemark : customerRemarks) {
			/* 566 */ SmeCustomerRemarksBean bean = new SmeCustomerRemarksBean();
			/* 567 */ bean.setCanUpdateDel(canAgentUpdateOrDelete(agentId, null, addressBookId));
			/* 568 */ bean.setInsertDateTime(customerRemark.getInsertDateTime());
			/* 569 */ bean.setAddressBookId(addressBookId);
			/* 570 */ bean.setAgentId(customerRemark.getAgentId());
			/* 571 */ bean.setId(customerRemark.getId());
			/* 572 */ bean.setRemarks(customerRemark.getRemarks());
			/* 573 */ bean.setSmeId(customerRemark.getSmeId());
			/* 574 */ bean.setUpdatedDateTime(customerRemark.getUpdatedDateTime());
			/* 575 */ bean.setVisibilityFlag(addressBook.getVisibilityFlag());
			/* 576 */ beanList.add(bean);
		}
		/* 578 */ JSONSerializer serializer = new JSONSerializer();
		/* 579 */ String response = serializer.exclude(new String[] { "*.class" }).deepSerialize(beanList);
		/* 580 */ res.setSuccess(true);
		/* 581 */ res.setResult(response);
		/* 582 */ return res;
	}

	public ServerResponse deleteRemaks(Long id) {
		/* 587 */ ServerResponse res = new ServerResponse();
		/* 588 */ SmeCustomerRemarks customerRemarks = this.agentDao.getSmeCustomerRemarks(id);
		/* 589 */ if (customerRemarks == null) {
			/* 590 */ ErrorDetails error = new ErrorDetails();
			/* 591 */ error.setErrorCode(ErrorConstants.REMARK_NOT_EXIST.getErrorCode());
			/* 592 */ error.setErrorString(ErrorConstants.REMARK_NOT_EXIST.getErrorMessage());
			/* 593 */ error.setDisplayMessage(ErrorConstants.REMARK_NOT_EXIST.getDisplayMessage());
			/* 594 */ JSONSerializer serializer = new JSONSerializer();
			/* 595 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 596 */ res.setResult(response);
			/* 597 */ return res;
		}
		/* 599 */ customerRemarks.setStatus(Byte.valueOf((byte) -9));
		/* 600 */ res.setSuccess(true);
		/* 601 */ res.setResult("Successfully Done");
		/* 602 */ return res;
	}

	public ServerResponse deleteRemaksByAddressBook(Long addressBookId) {
		/* 607 */ ServerResponse res = new ServerResponse();
		/* 608 */ List<SmeCustomerRemarks> customerRemarks = this.agentDao.getRemaksByAddressBookId(addressBookId);
		/* 609 */ if (CollectionUtils.isEmpty(customerRemarks)) {
			/* 610 */ ErrorDetails error = new ErrorDetails();
			/* 611 */ error.setErrorCode(ErrorConstants.REMARK_NOT_EXIST.getErrorCode());
			/* 612 */ error.setErrorString(ErrorConstants.REMARK_NOT_EXIST.getErrorMessage());
			/* 613 */ error.setDisplayMessage(ErrorConstants.REMARK_NOT_EXIST.getDisplayMessage());
			/* 614 */ JSONSerializer serializer = new JSONSerializer();
			/* 615 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 616 */ res.setResult(response);
			/* 617 */ return res;
		}
		/* 619 */ for (SmeCustomerRemarks customerRemark : customerRemarks) {
			/* 620 */ customerRemark.setStatus(Byte.valueOf((byte) -9));
		}
		/* 622 */ res.setSuccess(true);
		/* 623 */ res.setResult("Successfully Done");
		/* 624 */ return res;
	}

	public ServerResponse notPermitted() {
		/* 629 */ ServerResponse res = new ServerResponse();
		/* 630 */ ErrorDetails error = new ErrorDetails();
		/* 631 */ error.setErrorCode(ErrorConstants.NOT_PERMITTED.getErrorCode());
		/* 632 */ error.setErrorString(ErrorConstants.NOT_PERMITTED.getErrorMessage());
		/* 633 */ error.setDisplayMessage(ErrorConstants.NOT_PERMITTED.getDisplayMessage());
		/* 634 */ JSONSerializer serializer = new JSONSerializer();
		/* 635 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 636 */ res.setResult(response);
		/* 637 */ return res;
	}

	public boolean canAgentUpdateOrDelete(Long agentId, Long singleRemark, Long addressBookId) {
		/* 642 */ AddressBook addressBook = null;
		/* 643 */ if (singleRemark != null) {
			/* 644 */ SmeCustomerRemarks customerRemarks = this.agentDao.getSmeCustomerRemarks(singleRemark);
			/* 645 */ addressBookId = customerRemarks.getAddressBookId();
		}
		/* 647 */ addressBook = this.agentDao.getAddressBookById(addressBookId);
		/* 648 */ if (Constants.BYTE_1 == addressBook.getVisibilityFlag()) {
			/* 649 */ return (agentId == addressBook.getCreatedBy());
		}
		/* 651 */ return true;
	}

	public ServerResponse callSchedule(CallBaseBean callBaseBean, Long agentId) {
		/* 656 */ this.agentDao.saveCallSheduleBase(callBaseBean, agentId);
		/* 657 */ ServerResponse res = new ServerResponse();
		/* 658 */ res.setSuccess(true);
		/* 659 */ res.setResult("Successfully Done");
		/* 660 */ return res;
	}

	public ServerResponse callScheduleUpdate(CallBaseBean callBaseBean, Long agentId) {
		/* 665 */ if (callBaseBean.getBaseId() == null) {
			/* 666 */ callBaseBean.setBaseId(callBaseBean.getId());
		}
		/* 668 */ CallSchedulerBase base = this.agentDao.findById(callBaseBean.getBaseId());
		/* 669 */ ServerResponse res = new ServerResponse();
		/* 670 */ if (base == null) {
			/* 671 */ ErrorDetails error = new ErrorDetails();
			/* 672 */ error.setErrorCode(ErrorConstants.SCHEDULE_CALL_NOT_EXIST.getErrorCode());
			/* 673 */ error.setErrorString(ErrorConstants.SCHEDULE_CALL_NOT_EXIST.getErrorMessage());
			/* 674 */ error.setDisplayMessage(ErrorConstants.SCHEDULE_CALL_NOT_EXIST.getDisplayMessage());
			/* 675 */ JSONSerializer serializer = new JSONSerializer();
			/* 676 */ String response = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 677 */ res.setResult(response);
			/* 678 */ return res;
		}
		/* 680 */ base.setScheduleDateTime(callBaseBean.getScheduleDateTime());
		/* 681 */ res.setSuccess(true);
		/* 682 */ res.setResult("Successfully Done");
		/* 683 */ return res;
	}

	public ServerResponse getAddressBookById(Long id) {
		/* 688 */ ServerResponse res = new ServerResponse();
		/* 689 */ JSONSerializer serializer = new JSONSerializer();
		/* 690 */ AddressBook addressBook = this.agentDao.getAddressBookById(id);
		/* 691 */ if (addressBook == null) {
			/* 692 */ ErrorDetails error = new ErrorDetails();
			/* 693 */ error.setErrorCode(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorCode());
			/* 694 */ error.setErrorString(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getErrorMessage());
			/* 695 */ error.setDisplayMessage(ErrorConstants.ADDRESS_BOOK_NOT_EXIST.getDisplayMessage());
			/* 696 */ String str = serializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 697 */ res.setResult(str);
			/* 698 */ return res;
		}
		/* 700 */ AddressBookBean bean = entityToBean(addressBook);
		/* 701 */ String response = serializer.exclude(new String[] { "*.class" }).deepSerialize(bean);
		/* 702 */ res.setResult(response);
		/* 703 */ res.setSuccess(true);
		/* 704 */ return res;
	}
}

/*
 * Location: D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\
 * AgentProfileServiceImpl.class Java compiler version: 8 (52.0) JD-Core
 * Version: 1.1.3
 */