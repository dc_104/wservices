package in.wringg.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import flexjson.JSONSerializer;
import in.wringg.dao.SmeDao;
import in.wringg.dao.SmeProfileDao;
import in.wringg.entity.AgentDetail;
import in.wringg.entity.AgentGroupDetail;
import in.wringg.entity.AgentReportDetail;
import in.wringg.entity.AlertNotification;
import in.wringg.entity.CallBaseHistory;
import in.wringg.entity.IncomingIvrCallCdr;
import in.wringg.entity.MpbxCallRecording;
import in.wringg.entity.SmeComplaint;
import in.wringg.entity.SmeProfile;
import in.wringg.entity.SmePrompts;
import in.wringg.entity.Users;
import in.wringg.pojo.AddBulkSmsBean;
import in.wringg.pojo.AddOutBoundIvrBean;
import in.wringg.pojo.AgentDetailReportBean;
import in.wringg.pojo.AgentDetailSummaryDashboard;
import in.wringg.pojo.AgentGroupDetailBean;
import in.wringg.pojo.AgentReportFilterPagination;
import in.wringg.pojo.AlertNotificationBean;
import in.wringg.pojo.BaseUploadResponse;
import in.wringg.pojo.CallCdrResponseBean;
import in.wringg.pojo.CdrBean;
import in.wringg.pojo.CdrDropDownFilter;
import in.wringg.pojo.CdrModeBean;
import in.wringg.pojo.CdrPaginationFilerationList;
import in.wringg.pojo.IvrCallFlowBean;
import in.wringg.pojo.KeyValueGenricBean;
import in.wringg.pojo.LoginBean;
import in.wringg.pojo.ManageClientPageRequest;
import in.wringg.pojo.MissCallMarketingBean;
import in.wringg.pojo.PassswordTokenBean;
import in.wringg.pojo.PromptDetailsBean;
import in.wringg.pojo.RecordingBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.SettingWapperBean;
import in.wringg.pojo.SmeAgentDetailBean;
import in.wringg.pojo.SmeComplaintBean;
import in.wringg.pojo.SmeDashboardSystemDetail;
import in.wringg.pojo.SmeSettingBean;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.pojo.UploadedBaseDesc;
import in.wringg.service.SmeService;
import in.wringg.utility.Constants;
import in.wringg.utility.ErrorConstants;
import in.wringg.utility.ErrorDetails;
import in.wringg.utility.Utility;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("smeService")
@Transactional
public class SmeServiceImp implements SmeService {
	@Autowired
	SmeDao smeDao;
	@Autowired
	private SmeProfileDao smeProfileDao;
	/* 85 */ static final Logger logger = Logger.getLogger(in.wringg.service.SmeServiceImp.class);

	public ServerResponse smeDashboardDeatial(StartEndDateRequest req, long id) {
		SmeDashboardSystemDetail detail = this.smeDao.smeDashboardDetail(req, Long.valueOf(id));
		ServerResponse response = new ServerResponse();
		response.setSuccess(true);
		if (detail == null) {
			response.setResult("");
		} else {

			/* 96 */ response.setSuccess(true);
			/* 97 */ JSONSerializer serializer = new JSONSerializer();
			/* 98 */ String res = serializer.exclude(new String[] { "*.class" }).deepSerialize(detail);
			/* 99 */ response.setResult(res);
		}
		/* 101 */ return response;
	}

	public ServerResponse addSmsCampaign(AddBulkSmsBean bean, long id) throws IOException {
		/* 106 */ this.smeDao.addSmsCampaign(bean, Long.valueOf(id));
		/* 107 */ return null;
	}

	public ServerResponse addIvrmanager(List<IvrCallFlowBean> bean, String id) throws IOException {
		/* 112 */ this.smeDao.addIvrManager(bean, id);
		/* 113 */ ServerResponse response = new ServerResponse();
		/* 114 */ response.setSuccess(true);
		/* 115 */ response.setResult("Saved");
		/* 116 */ return response;
	}

	public ServerResponse getIvrmanager(String id) {
		/* 121 */ List<IvrCallFlowBean> ivrCallFlowBeans = this.smeDao.getIvrManger(id);
		/* 122 */ ServerResponse response = new ServerResponse();
		/* 123 */ JSONSerializer serializer = new JSONSerializer();
		/* 124 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(ivrCallFlowBeans);
		/* 125 */ response.setResult(result);
		/* 126 */ return response;
	}

	public boolean isIvrManagerExist(String id) {
		/* 132 */ return this.smeDao.isIvrManger(id);
	}

	public ServerResponse getSmeSmsList(String id) {
		/* 137 */ List<AddBulkSmsBean> listBulkSmsBeans = this.smeDao.getBulkSmslist(id);

		/* 139 */ ServerResponse serverResponse = new ServerResponse();
		/* 140 */ serverResponse.setSuccess(true);
		/* 141 */ JSONSerializer serializer = new JSONSerializer();
		/* 142 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(listBulkSmsBeans);
		/* 143 */ serverResponse.setResult(result);
		/* 144 */ return serverResponse;
	}

	public void deleteSmsList(String id, Long capmaignId) {
		/* 149 */ this.smeDao.deleteBulkSmsList(id, capmaignId);
	}

	public ServerResponse getSmeSmsById(String id, Long campaignId) {
		/* 156 */ AddBulkSmsBean smsDetailBean = this.smeDao.fetchBulkSmsById(id, campaignId);
		/* 157 */ ServerResponse serverResponse = new ServerResponse();
		/* 158 */ serverResponse.setSuccess(true);
		/* 159 */ JSONSerializer serializer = new JSONSerializer();
		/* 160 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(smsDetailBean);
		/* 161 */ serverResponse.setResult(result);
		/* 162 */ return serverResponse;
	}

	public ServerResponse updateSmsCampaign(AddBulkSmsBean bean, long id, long campaignId) {
		/* 167 */ this.smeDao.updateSmsCampaign(bean, Long.valueOf(id), Long.valueOf(campaignId));
		/* 168 */ ServerResponse response = new ServerResponse();
		/* 169 */ response.setSuccess(true);
		/* 170 */ response.setResult("Data uploaded");
		/* 171 */ return response;
	}

	public ServerResponse getMissCallMarketingList(StartEndDateRequest request, long id) {
		/* 176 */ List<MissCallMarketingBean> callMarketingBeans = this.smeDao.getMissCallList(request, id);
		/* 177 */ ServerResponse response = new ServerResponse();
		/* 178 */ response.setSuccess(true);
		/* 179 */ JSONSerializer serializer = new JSONSerializer();
		/* 180 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(callMarketingBeans));
		/* 181 */ return response;
	}

	public ServerResponse getAgentGroupDetail(Long smeId) {
		/* 186 */ List<AgentGroupDetailBean> groupDetailBeans = this.smeDao.getAgentGroupDetail(smeId);
		/* 187 */ ServerResponse response = new ServerResponse();
		/* 188 */ response.setSuccess(true);
		/* 189 */ JSONSerializer serializer = new JSONSerializer();
		/* 190 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(groupDetailBeans));
		/* 191 */ return response;
	}

	public ServerResponse recordingList(StartEndDateRequest request, long id) {
		/* 196 */ List<RecordingBean> list = new ArrayList<>();
		/* 197 */ RecordingBean bean1 = new RecordingBean(Integer.valueOf(1001), "recording/1/recording1.wav");
		/* 198 */ RecordingBean bean2 = new RecordingBean(Integer.valueOf(1002), "recording\\1\\recording2.wav");
		/* 199 */ list.add(bean1);
		/* 200 */ list.add(bean2);
		/* 201 */ ServerResponse response = new ServerResponse();
		/* 202 */ response.setSuccess(true);
		/* 203 */ JSONSerializer serializer = new JSONSerializer();
		/* 204 */ response.setResult(serializer.exclude(new String[] { "*.class" }).serialize(list));
		/* 205 */ return response;
	}

	public ServerResponse addSmeComplaint(SmeComplaintBean bean, Long id) {
		/* 210 */ ServerResponse response = new ServerResponse();
		/* 211 */ this.smeDao.addSmeComplaint(bean, id);
		/* 212 */ response.setSuccess(true);
		/* 213 */ response.setResult("Complaint Saved");
		/* 214 */ return response;
	}

	public ServerResponse updateSmeComplaint(SmeComplaintBean bean, Long id) {
		/* 219 */ ServerResponse response = new ServerResponse();
		/* 220 */ response = this.smeDao.updateSmeComplaint(bean, id);
		/* 221 */ return response;
	}

	public ServerResponse getSmeComplaints(Long smeId) {
		/* 226 */ ServerResponse response = new ServerResponse();
		/* 227 */ List<SmeComplaintBean> complaintList = new ArrayList<>();
		/* 228 */ List<SmeComplaint> complaints = this.smeDao.getSmeComplaints(smeId);
		/* 229 */ for (SmeComplaint complaint : complaints) {
			/* 230 */ complaintList.add(transformSmeComplaint(complaint));
		}
		/* 232 */ JSONSerializer serializer = new JSONSerializer();
		/* 233 */ String result = serializer.exclude(new String[] { "*.class" }).serialize(complaintList);
		/* 234 */ response.setSuccess(true);
		/* 235 */ response.setResult(result);
		/* 236 */ return response;
	}

	public ServerResponse deleteSmeComplaint(Long smeId, Long complaintId) {
		/* 241 */ ServerResponse response = new ServerResponse();
		/* 242 */ response = this.smeDao.deleteSmeComplaint(smeId, complaintId);
		/* 243 */ return response;
	}

	private SmeComplaintBean transformSmeComplaint(SmeComplaint complaintEntity) {
		/* 247 */ SmeComplaintBean complaintBean = new SmeComplaintBean();
		/* 248 */ complaintBean.setAssigneeName(complaintEntity.getAssigneeName());
		/* 249 */ complaintBean.setCategory(Integer.valueOf(complaintEntity.getCategory()));
		/* 250 */ complaintBean.setComplaintDateTime(complaintEntity.getComplaintDateTime());
		/* 251 */ complaintBean.setComplaintDetail(complaintEntity.getComplaintDetail());
		/* 252 */ complaintBean.setEmailId(complaintEntity.getEmailId());
		/* 253 */ complaintBean.setId(complaintEntity.getId());
		/* 254 */ complaintBean.setParentId(complaintEntity.getParentId());
		/* 255 */ complaintBean.setSmeId(complaintEntity.getSmeId().getId());
		/* 256 */ complaintBean.setSolution(complaintEntity.getSolution());
		/* 257 */ complaintBean.setSolutionDateTime(complaintEntity.getSolutionDateTime());
		/* 258 */ complaintBean.setStatus(complaintEntity.getStatus());
		/* 259 */ complaintBean.setSubject(complaintEntity.getSubject());
		/* 260 */ complaintBean.setSmeEmailId(complaintEntity.getSmeId().getEmailId());
		/* 261 */ complaintBean.setSmeName(complaintEntity.getSmeId().getName());

		/* 263 */ return complaintBean;
	}

	public ServerResponse addOutboundIvr(AddOutBoundIvrBean bean) {
		/* 268 */ ServerResponse response = new ServerResponse();
		/* 269 */ this.smeDao.addOutboundIvrCampaign(bean);
		/* 270 */ response.setSuccess(true);
		/* 271 */ response.setResult("Deleted");
		/* 272 */ return response;
	}

	public ServerResponse updateOutbounfIvr(AddOutBoundIvrBean bean, BigInteger id) {
		/* 278 */ return null;
	}

	public ServerResponse deleteOutbounfIvr(BigInteger id) {
		/* 284 */ return null;
	}

	public ServerResponse getOutbounfIvr() {
		/* 290 */ return null;
	}

	public ServerResponse getCDRList(ManageClientPageRequest request, long id, Byte catagory)
			throws FileNotFoundException, IOException {
		/* 296 */ ServerResponse response = new ServerResponse();
		/* 297 */ List<CallCdrResponseBean> cdrList = this.smeDao.getCDRListForService(request, Long.valueOf(id),
				catagory);
		/* 298 */ Long total = this.smeDao.getCdrListCount(request, Long.valueOf(id), catagory);
		/* 299 */ CdrPaginationFilerationList cdrPaginationList = new CdrPaginationFilerationList();
		/* 300 */ cdrPaginationList.setTotalRecords(total);
		/* 301 */ cdrPaginationList.setRecords(cdrList);
		/* 302 */ JSONSerializer serializer = new JSONSerializer();
		/* 303 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(cdrPaginationList);
		/* 304 */ response.setSuccess(true);
		/* 305 */ response.setResult(result);

		/* 307 */ return response;
	}

	private CdrBean transformCDR(IncomingIvrCallCdr callCDR) {
		/* 312 */ CdrBean cdrBean = new CdrBean(callCDR.getId().toString(), callCDR.getStartDateTime().toString(),
				callCDR.getEndDateTime().toString(), callCDR.getLongcode().toString(), callCDR.getCallingNumber(), "NA",
				"yes", "");

		/* 317 */ return cdrBean;
	}

	public ServerResponse fetchSetting(Long smeId) throws JSONException {
		/* 322 */ ServerResponse response = new ServerResponse();
		/* 323 */ SettingWapperBean settings = new SettingWapperBean();
		/* 324 */ List<SmeSettingBean> list = new ArrayList<>();

		/* 326 */ SmeProfile profile = this.smeProfileDao.findByID(smeId);
		/* 327 */ for (Constants.Services serv : Constants.settingServices) {
			/* 328 */ if ((profile.getServiceFlag() & serv.getServiceId()) > 0) {
				/* 329 */ SmeSettingBean setting = new SmeSettingBean();
				/* 330 */ setting.setDisplayName(serv.getDisplayName());
				/* 331 */ setting.setId(Integer.valueOf(serv.getServiceId()));

				/* 334 */ if (serv.getServiceId() == Constants.Services.RECORDING_SERVICE.getServiceId()) {
					/* 335 */ setting.setValue(profile.getRecording());
					/* 336 */ } else if (serv.getServiceId() == Constants.Services.MASKING_SERVICE.getServiceId()) {
					/* 337 */ setting.setValue(profile.getMasking());
					/* 338 */ } else if (serv.getServiceId() == Constants.Services.VOICEMAIL_SERVICE.getServiceId()) {
					/* 339 */ setting.setValue(profile.getVoicemail());
				}

				/* 346 */ list.add(setting);
			}
		}
		/* 349 */ settings.setSelectionAlgo(profile.getSelectionAlgo());
		/* 350 */ int language = profile.getLanguage().intValue();
		/* 351 */ settings.setFlagList(list);
		/* 352 */ settings.setLanguage(language);
		/* 353 */ settings.setBalance(findSmeBalance(profile.getLongcodeId().getLongcode().toString()));
		/* 354 */ settings.setQueueLimit(profile.getQueueLimit());
		/* 355 */ settings.setCallBackUrl(profile.getCallBackUrl());
		/* 356 */ settings.setStickyAlgo(profile.getStickyAlgo());
		/* 357 */ JSONSerializer serializer = new JSONSerializer();

		/* 359 */ response.setSuccess(true);
		/* 360 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(settings));
		/* 361 */ return response;
	}

	private String findSmeBalance(String longcode) throws JSONException {
		/* 390 */ return "0.0";
	}

	public ServerResponse updateSetting(Long smeId, SettingWapperBean settings) {
		/* 395 */ ServerResponse response = new ServerResponse();

		/* 397 */ List<SmeSettingBean> list = settings.getFlagList();
		/* 398 */ SmeProfile profile = this.smeProfileDao.findByID(smeId);
		/* 399 */ for (SmeSettingBean serv : list) {
			/* 400 */ if ((profile.getServiceFlag() & serv.getId().intValue()) > 0) {
				/* 401 */ if (serv.getId().intValue() == Constants.Services.RECORDING_SERVICE.getServiceId()) {
					/* 402 */ profile.setRecording(serv.getValue());
					continue;
					/* 403 */ }
				if (serv.getId().intValue() == Constants.Services.MASKING_SERVICE.getServiceId()) {
					/* 404 */ profile.setMasking(serv.getValue());
					continue;
					/* 405 */ }
				if (serv.getId().intValue() == Constants.Services.VOICEMAIL_SERVICE.getServiceId()) {
					/* 406 */ profile.setVoicemail(serv.getValue());
				}
			}
		}

		/* 414 */ Integer langId = Integer.valueOf(settings.getLanguage());
		/* 415 */ profile.setLanguage(Byte.valueOf(langId.byteValue()));
		/* 416 */ profile.setSelectionAlgo(settings.getSelectionAlgo());
		/* 417 */ profile.setQueueLimit(settings.getQueueLimit());
		/* 418 */ profile.setCallBackUrl(settings.getCallBackUrl());
		/* 419 */ profile.setSelectionAlgo(settings.getSelectionAlgo());
		/* 420 */ profile.setStickyAlgo(settings.getStickyAlgo());
		/* 421 */ JSONSerializer serializer = new JSONSerializer();
		/* 422 */ response.setSuccess(true);
		/* 423 */ response.setResult(serializer.exclude(new String[] { "*.class" }).deepSerialize(settings));
		/* 424 */ return response;
	}

	public ServerResponse resetPassword(LoginBean bean) {
		/* 429 */ ServerResponse response = this.smeDao.resetPassword(bean);
		/* 430 */ return response;
	}

	public ServerResponse genPaswordToken(String smeId) throws ParseException, IOException {
		/* 435 */ ServerResponse response = this.smeDao.genForgotToken(smeId);
		/* 436 */ return response;
	}

	public ServerResponse validateToken(String smeId, String token) throws ParseException {
		/* 441 */ ServerResponse response = this.smeDao.forgetPassword(smeId, token);
		/* 442 */ return response;
	}

	public ServerResponse newPasword(String smeId, PassswordTokenBean bean) throws ParseException {
		/* 447 */ ServerResponse response = this.smeDao.newPassword(smeId, bean);
		/* 448 */ return response;
	}

	public ServerResponse fetchAgentReportDetail(Long smeId, ManageClientPageRequest clientPageRequest) {
		/* 453 */ List<AgentReportDetail> agentReport = this.smeDao.fetchAgentReportDetail(smeId, clientPageRequest);
		/* 454 */ ServerResponse response = new ServerResponse();
		/* 455 */ List<AgentDetailReportBean> beanList = new ArrayList<>();
		/* 456 */ Long total = Long.valueOf(0L);
		/* 457 */ for (AgentReportDetail detail : agentReport) {
			/* 458 */ AgentDetailReportBean bean = new AgentDetailReportBean();
			/* 459 */ bean.setSmeId(detail.getSmeId());
			/* 460 */ bean.setAgentName(detail.getAgentId().getAgentName());
			/* 461 */ bean.setAgentNumber(detail.getAgentId().getAgentMobile());
			/* 462 */ bean.setAgentGroup(detail.getAgentGroup());
			/* 463 */ bean.setAgentId(detail.getAgentId().getAgentId());
			/* 464 */ bean.setCustomerAni(detail.getCustomerAni());
			/* 465 */ bean.setDuration(detail.getDuration());
			/* 466 */ bean.setEndDate(Utility.getFormattedDateTime(detail.getEndDate(), "yyyy-MM-dd HH:mm:ss"));
			/* 467 */ bean.setId(detail.getId().intValue());
			/* 468 */ bean.setInsertDate(Utility.getFormattedDateTime(detail.getInsertDate(), "yyyy-MM-dd"));
			/* 469 */ bean.setResponseCode(detail.getResponseCode());
			/* 470 */ bean.setResponseMessage(detail.getResponseMessage());
			/* 471 */ bean.setSmeId(detail.getSmeId());
			/* 472 */ bean.setStartDate(Utility.getFormattedDateTime(detail.getStartDate(), "yyyy-MM-dd HH:mm:ss"));
			/* 473 */ bean.setStatus(detail.getStatus());
			/* 474 */ bean.setSessionId(detail.getSessionId());
			/* 475 */ bean.setCallMode(detail.getCallMode());
			/* 476 */ bean.setCallInfo(detail.getCallInfo());
			/* 477 */ beanList.add(bean);
		}
		/* 479 */ AgentReportFilterPagination pagination = new AgentReportFilterPagination();
		/* 480 */ pagination.setRecords(beanList);
		/* 481 */ pagination.setTotalRecords(this.smeDao.getAgetntReportCount(smeId, clientPageRequest));
		/* 482 */ JSONSerializer serializer = new JSONSerializer();
		/* 483 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(pagination);
		/* 484 */ response.setSuccess(true);
		/* 485 */ response.setResult(result);
		/* 486 */ return response;
	}

	public ServerResponse fetchCdrDropDown() throws FileNotFoundException, IOException {
		/* 491 */ CdrDropDownFilter dropDown = new CdrDropDownFilter();
		/* 492 */ CdrModeBean cdrMode = new CdrModeBean();
		/* 493 */ List<KeyValueGenricBean> callRecordingStatus = new ArrayList<>();
		/* 494 */ List<KeyValueGenricBean> voiceMailRecordingStatus = new ArrayList<>();
		/* 495 */ List<KeyValueGenricBean> callDirectionStatus = new ArrayList<>();

		/* 497 */ List<KeyValueGenricBean> incoming = new ArrayList<>();
		/* 498 */ List<KeyValueGenricBean> outgoing = new ArrayList<>();
		/* 499 */ List<KeyValueGenricBean> voicemail = new ArrayList<>();
		/* 500 */ Set<Object> keys = Utility.getProjectPropertyKeySet();

		/* 502 */ for (Object obj : keys) {

			/* 504 */ if (obj.toString().startsWith("callRecordingStatus")) {
				/* 505 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 506 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 507 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 508 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 509 */ callRecordingStatus.add(keyValue);
				continue;
			}
			/* 511 */ if (obj.toString().startsWith("voiceMailRecordingStatus")) {
				/* 512 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 513 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 514 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 515 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 516 */ voiceMailRecordingStatus.add(keyValue);
				continue;
			}
			/* 518 */ if (obj.toString().startsWith("callDirectionStatus")) {
				/* 519 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 520 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 521 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 522 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 523 */ callDirectionStatus.add(keyValue);
				continue;
			}
			/* 525 */ if (obj.toString().startsWith("cdrMode.1")) {
				/* 526 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 527 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 528 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 529 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 530 */ incoming.add(keyValue);

				continue;
			}
			/* 534 */ if (obj.toString().startsWith("cdrMode.2")) {
				/* 535 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 536 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 537 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 538 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 539 */ outgoing.add(keyValue);
				continue;
			}
			/* 541 */ if (obj.toString().startsWith("cdrMode.3")) {
				/* 542 */ StringBuffer keyBuffer = new StringBuffer(obj.toString());
				/* 543 */ KeyValueGenricBean keyValue = new KeyValueGenricBean();
				/* 544 */ keyValue.setKey(keyBuffer.substring(keyBuffer.indexOf(".") + 1));
				/* 545 */ keyValue.setValue(Utility.getProjectProperty(obj.toString()));
				/* 546 */ voicemail.add(keyValue);
			}
		}

		/* 550 */ cdrMode.setIncoming(incoming);
		/* 551 */ cdrMode.setOutgoing(outgoing);
		/* 552 */ cdrMode.setVoicemail(voicemail);
		/* 553 */ dropDown.setCallDirectionStatus(callDirectionStatus);
		/* 554 */ dropDown.setCallRecordingStatus(callRecordingStatus);
		/* 555 */ dropDown.setVoiceMailRecordingStatus(voiceMailRecordingStatus);
		/* 556 */ dropDown.setCdrMode(cdrMode);
		/* 557 */ ServerResponse response = new ServerResponse();
		/* 558 */ JSONSerializer serializer = new JSONSerializer();
		/* 559 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(dropDown);
		/* 560 */ response.setSuccess(true);
		/* 561 */ response.setResult(result);
		/* 562 */ return response;
	}

	public ServerResponse fetchSmeBalance(Long smeId) throws JSONException {
		/* 567 */ ServerResponse response = new ServerResponse();
		/* 568 */ SmeProfile profile = this.smeProfileDao.findByID(smeId);
		/* 569 */ String balance = findSmeBalance(
				(profile != null) ? profile.getLongcodeId().getLongcode().toString() : "0000");
		/* 570 */ response.setSuccess(true);
		/* 571 */ response.setResult("{ \"balance\":\"" + balance + "\" }");
		/* 572 */ return response;
	}

	public ServerResponse smeDashboardDeatialForAgent(long id) {
		/* 577 */ ServerResponse response = new ServerResponse();
		/* 578 */ SmeAgentDetailBean dashboardAgent = this.smeDao.smeDashboadForAgent(Long.valueOf(id));
		/* 579 */ JSONSerializer serializer = new JSONSerializer();
		/* 580 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(dashboardAgent);
		/* 581 */ response.setSuccess(true);
		/* 582 */ response.setResult(result);
		/* 583 */ return response;
	}

	public ServerResponse agentDashboardSummary(Long smeId, Byte status) {
		/* 588 */ ServerResponse response = new ServerResponse();
		/* 589 */ List<AgentDetail> detailSummary = this.smeDao.agentDashboardSummary(smeId, status);
		/* 590 */ List<AgentDetailSummaryDashboard> dashboardSummary = new ArrayList<>();
		/* 591 */ for (AgentDetail detail : detailSummary) {
			/* 592 */ AgentDetailSummaryDashboard bean = new AgentDetailSummaryDashboard();

			/* 594 */ bean.setAgentId(detail.getAgentId());
			/* 595 */ bean.setAgentMobile(detail.getAgentMobile());
			/* 596 */ bean.setAgentName(detail.getAgentName());
			/* 597 */ bean.setAgentScore(detail.getAgentScore());
			/* 598 */ List<Long> groups = new ArrayList<>();
			/* 599 */ String groupName = "";
			/* 600 */ for (AgentGroupDetail group : detail.getGroups()) {
				/* 601 */ groups.add(group.getGroupId());
				/* 602 */ groupName = group.getGroupName();
			}
			/* 604 */ bean.setAgentGroup(groupName);
			/* 605 */ dashboardSummary.add(bean);
		}

		/* 608 */ JSONSerializer serializer = new JSONSerializer();
		/* 609 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(dashboardSummary);
		/* 610 */ response.setSuccess(true);
		/* 611 */ response.setResult(result);
		/* 612 */ return response;
	}

	/* 615 */ private List<Byte> AGENT_STATUS = Arrays.asList(new Byte[] { Byte.valueOf((byte) 1), Constants.IN_ACTIVE,
			/* 616 */ Byte.valueOf((byte) 2), Constants.LUNCH, Constants.OFF_HOUR });

	public ServerResponse agentDashboardSummary(Long smeId) {
		/* 620 */ ServerResponse response = new ServerResponse();
		/* 621 */ List<AgentDetail> detailSummary = this.smeDao.agentDashboardSummary(smeId, this.AGENT_STATUS);
		/* 622 */ Map<String, List<AgentDetailSummaryDashboard>> dashboardSummaryMap = new HashMap<>();
		/* 623 */ for (Byte status : this.AGENT_STATUS) {
			/* 624 */ dashboardSummaryMap.put(status.toString(), new ArrayList<>());
		}

		/* 627 */ for (AgentDetail detail : detailSummary) {
			/* 628 */ List<AgentDetailSummaryDashboard> dashboardSummary = dashboardSummaryMap
					.get(detail.getStatus().toString());
			/* 629 */ AgentDetailSummaryDashboard bean = new AgentDetailSummaryDashboard();

			/* 631 */ bean.setAgentId(detail.getAgentId());
			/* 632 */ bean.setAgentMobile(detail.getAgentMobile());
			/* 633 */ bean.setAgentName(detail.getAgentName());
			/* 634 */ bean.setAgentScore(detail.getAgentScore());
			/* 635 */ List<Long> groups = new ArrayList<>();
			/* 636 */ String groupName = "";
			/* 637 */ for (AgentGroupDetail group : detail.getGroups()) {
				/* 638 */ groups.add(group.getGroupId());
				/* 639 */ groupName = group.getGroupName();
			}
			/* 641 */ bean.setAgentGroup(groupName);
			/* 642 */ dashboardSummary.add(bean);
			/* 643 */ dashboardSummaryMap.put(detail.getStatus().toString(), dashboardSummary);
		}

		/* 646 */ JSONSerializer serializer = new JSONSerializer();
		/* 647 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(dashboardSummaryMap);
		/* 648 */ response.setSuccess(true);
		/* 649 */ response.setResult(result);
		/* 650 */ return response;
	}

	public ServerResponse getAlertNotications(String userId, int seen) {
		/* 655 */ ServerResponse response = new ServerResponse();
		/* 656 */ List<AlertNotification> notifications = this.smeDao.alertNotificationList(userId, seen);
		/* 657 */ List<AlertNotificationBean> list = new ArrayList<>();
		/* 658 */ for (AlertNotification alert : notifications) {
			/* 659 */ AlertNotificationBean bean = new AlertNotificationBean();
			/* 660 */ bean.setId(alert.getId());
			/* 661 */ bean.setDescription(alert.getDescription());
			/* 662 */ bean.setInsertedDate(alert.getInsertedDate().toString());
			/* 663 */ bean.setStatus(alert.getStatus());
			/* 664 */ bean.setTitle(alert.getTitle());
			/* 665 */ list.add(bean);
		}

		/* 668 */ JSONSerializer serializer = new JSONSerializer();
		/* 669 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(list);
		/* 670 */ response.setSuccess(true);
		/* 671 */ response.setResult(result);
		/* 672 */ return response;
	}

	public ServerResponse updateAlertNotification(int id) {
		/* 677 */ ServerResponse response = new ServerResponse();
		/* 678 */ AlertNotification notification = this.smeDao.getAlertNotification(id);
		/* 679 */ if (notification != null) {
			/* 680 */ this.smeDao.updateAlertNotification(notification);
			/* 681 */ response.setSuccess(true);
			/* 682 */ response.setResult("Seen");
			/* 683 */ return response;
		}
		/* 685 */ ErrorDetails error = new ErrorDetails();
		/* 686 */ error.setErrorCode(ErrorConstants.ERROR_NOTIFICATION.getErrorCode());
		/* 687 */ error.setErrorString("Notification ::" + ErrorConstants.ERROR_NOTIFICATION.getErrorMessage());
		/* 688 */ error.setDisplayMessage(ErrorConstants.ERROR_NOTIFICATION.getDisplayMessage());
		/* 689 */ JSONSerializer serializer = new JSONSerializer();
		/* 690 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 691 */ response.setResult(res);
		/* 692 */ return response;
	}

	public ServerResponse deleteAlertNotification(int id) {
		/* 698 */ ServerResponse response = new ServerResponse();
		/* 699 */ AlertNotification notification = this.smeDao.getAlertNotification(id);
		/* 700 */ if (notification != null) {
			/* 701 */ this.smeDao.deleteAlertNotification(notification);
			/* 702 */ response.setSuccess(true);
			/* 703 */ response.setResult("Deleted");
			/* 704 */ return response;
		}
		/* 706 */ ErrorDetails error = new ErrorDetails();
		/* 707 */ error.setErrorCode(ErrorConstants.ERROR_NOTIFICATION.getErrorCode());
		/* 708 */ error.setErrorString("Notification ::" + ErrorConstants.ERROR_NOTIFICATION.getErrorMessage());
		/* 709 */ error.setDisplayMessage(ErrorConstants.ERROR_NOTIFICATION.getDisplayMessage());
		/* 710 */ JSONSerializer serializer = new JSONSerializer();
		/* 711 */ String res = serializer.exclude(new String[] { "*.class" }).serialize(error);
		/* 712 */ response.setResult(res);
		/* 713 */ return response;
	}

	public ServerResponse getRecordingList(String id) {
		/* 719 */ ServerResponse response = new ServerResponse();
		/* 720 */ List<String> fileList = new ArrayList<>();
		/* 721 */ MpbxCallRecording recording = this.smeDao.recordingFileList(id);
		/* 722 */ if (recording != null && !recording.getMergedFile().equals("0")) {
			/* 723 */ String[] token = recording.getMergedFile().split("#");
			/* 724 */ for (String path : token) {
				/* 725 */ fileList.add(path);
			}
		} else {
			/* 728 */ fileList.add("");
		}
		/* 730 */ JSONSerializer serializer = new JSONSerializer();
		/* 731 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(fileList);
		/* 732 */ response.setSuccess(true);
		/* 733 */ response.setResult(result);
		/* 734 */ return response;
	}

	public ServerResponse getOutLeg(Long smeId, String sessionId, byte flag) throws FileNotFoundException, IOException {
		/* 740 */ ServerResponse response = new ServerResponse();
		/* 741 */ CallCdrResponseBean bean = this.smeDao.fetchOutLeg(smeId, sessionId, flag);
		/* 742 */ JSONSerializer serializer = new JSONSerializer();
		/* 743 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(bean);
		/* 744 */ response.setSuccess(true);
		/* 745 */ response.setResult(result);
		/* 746 */ return response;
	}

	public ServerResponse refreshUserToken(Long smeId) {
		/* 751 */ ServerResponse response = new ServerResponse();
		/* 752 */ Users users = this.smeDao.fetchUser(smeId.toString());
		/* 753 */ if (users == null || users.getEnabled() == -9) {

			/* 755 */ ErrorDetails error = new ErrorDetails();
			/* 756 */ error.setErrorCode(ErrorConstants.ERROR_USER_NOT_EXIST.getErrorCode());
			/* 757 */ error.setErrorString("Notification ::" + ErrorConstants.ERROR_USER_NOT_EXIST.getErrorMessage());
			/* 758 */ error.setDisplayMessage(ErrorConstants.ERROR_USER_NOT_EXIST.getDisplayMessage());
			/* 759 */ JSONSerializer jSONSerializer = new JSONSerializer();
			/* 760 */ String res = jSONSerializer.exclude(new String[] { "*.class" }).serialize(error);
			/* 761 */ response.setResult(res);
			/* 762 */ return response;
		}
		/* 764 */ String token = Utility.genrateToken(64);
		/* 765 */ users.setUserToken(token);
		/* 766 */ Map<String, String> bean = new HashMap<>();
		/* 767 */ bean.put("userToken", token);
		/* 768 */ JSONSerializer serializer = new JSONSerializer();
		/* 769 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(bean);
		/* 770 */ response.setSuccess(true);
		/* 771 */ response.setResult(result);
		/* 772 */ return response;
	}

	public ServerResponse saveBaseList(List<UploadedBaseDesc> baseListReq, String countryCode, String agentId,
			long smeId) {
		/* 778 */ long[] array = Arrays.<String>asList(agentId.split(",")).stream().mapToLong(Long::parseLong)
				.toArray();
		/* 779 */ int lenth = array.length;

		/* 781 */ List<UploadedBaseDesc> baseList = new ArrayList<>();
		/* 782 */ int i = 0;
		/* 783 */ for (UploadedBaseDesc base : baseListReq) {
			/* 784 */ baseList.add(new UploadedBaseDesc(countryCode + base.getMobile(), base.getDesc(), array[i++]));
			/* 785 */ i = (i == lenth) ? 0 : i;
		}
		/* 787 */ this.smeDao.saveBase(baseList, smeId);
		/* 788 */ ServerResponse response = new ServerResponse();
		/* 789 */ response.setSuccess(true);
		/* 790 */ response.setResult("Successfully Done");

		/* 792 */ return response;
	}

	public ServerResponse saveBaseFileData(Sheet sheet, String countryCode, String agentId, long smeId,
			String fileName) {
		long[] array = Arrays.<String>asList(agentId.split(",")).stream().mapToLong(Long::parseLong).toArray();
		int lenth = array.length;
		Set<String> set = new HashSet<>();
		Set<String> duplicate = new HashSet<>();
		Set<String> invalid = new HashSet<>();
		List<UploadedBaseDesc> baseList = new ArrayList<>();
		Iterator<Row> rowIter = sheet.rowIterator();
		int i = 0;
		if (rowIter.hasNext())
			rowIter.next();
		while (rowIter.hasNext()) {

			Row nextRow = rowIter.next();

			String mobile = (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC)
					? (new BigDecimal(nextRow.getCell(0).getNumericCellValue()) + "")
					: nextRow.getCell(0).getStringCellValue();
			String desc = (nextRow.getCell(1) != null) ? nextRow.getCell(1).getStringCellValue() : null;
			String mobileFilter = (mobile != null && mobile.length() > 10) ? mobile.substring(mobile.length() - 10)
					: mobile;

			if (isValidIndianMobileNumber(mobileFilter)) {
				if (set.add(countryCode + mobileFilter)) {
					baseList.add(new UploadedBaseDesc(countryCode + mobileFilter, desc, array[i++]));
					i = (i == lenth) ? 0 : i;
					continue;
				}
				duplicate.add(mobile);

				continue;
			}
			invalid.add(mobile);
		}

		JSONSerializer serializer = new JSONSerializer();
		this.smeDao.saveBase(baseList, smeId);
		Date date = new Date();

		BaseUploadResponse baseUploadResponse = new BaseUploadResponse(sheet.getPhysicalNumberOfRows() - 1,
				baseList.size(), duplicate.size(), invalid.size(), duplicate, invalid, date, fileName);
		CallBaseHistory callBaseHistory = new CallBaseHistory();
		callBaseHistory.setSmeId(Long.valueOf(smeId));
		callBaseHistory.setData(serializer.exclude(new String[] { "*.class" }).deepSerialize(baseUploadResponse));
		this.smeDao.saveCallBaseHistory(callBaseHistory);
		ServerResponse response = new ServerResponse();
		String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(baseUploadResponse);
		response.setSuccess(true);
		response.setResult(result);
		return response;
	}

	public boolean isValidIndianMobileNumber(String s) {
		/* 846 */ Pattern p = Pattern.compile("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$");
		/* 847 */ Matcher m = p.matcher(s);
		/* 848 */ return (m.find() && m.group().equals(s));
	}

	public ServerResponse getBaseUploadHistory(long smeId) {
		/* 853 */ JSONSerializer serializer = new JSONSerializer();
		/* 854 */ ObjectMapper objectMapper = new ObjectMapper();
		/* 855 */ List<BaseUploadResponse> baseUploadList = new ArrayList<>();
		/* 856 */ ServerResponse response = new ServerResponse();
		/* 857 */ List<CallBaseHistory> entityList = this.smeDao.getBaseHistory(smeId);
		/* 858 */ for (CallBaseHistory entity : entityList) {
			try {
				/* 860 */ BaseUploadResponse base = (BaseUploadResponse) objectMapper.readValue(entity.getData(),
						BaseUploadResponse.class);
				/* 861 */ baseUploadList.add(base);
				/* 862 */ } catch (IOException e) {
				/* 863 */ e.printStackTrace();
			}
		}

		/* 867 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(baseUploadList);
		/* 868 */ response.setSuccess(true);
		/* 869 */ response.setResult(result);
		/* 870 */ return response;
	}

	public ServerResponse getCDRForAgent(long agentId, byte catagory) throws FileNotFoundException, IOException {
		/* 875 */ ServerResponse response = new ServerResponse();
		/* 876 */ JSONSerializer serializer = new JSONSerializer();
		/* 877 */ String result = serializer.exclude(new String[] { "*.class" })
				.deepSerialize(this.smeDao.getCDRLForAgent(agentId, catagory));
		/* 878 */ response.setSuccess(true);
		/* 879 */ response.setResult(result);
		/* 880 */ return response;
	}

	private Set<String> filterSessionId(List<AgentReportDetail> agentReportDetails) {
		/* 884 */ Set<String> set = new HashSet<>();
		/* 885 */ for (AgentReportDetail bean : agentReportDetails) {
			/* 886 */ set.add(bean.getSessionId());
		}
		/* 888 */ for (AgentReportDetail bean : agentReportDetails) {
			/* 889 */ if (bean.getStatus() == 0) {
				/* 890 */ set.remove(bean.getSessionId());
			}
		}
		/* 893 */ return set;
	}

	public ServerResponse getMissCallCDRForAgent(long agentId) throws FileNotFoundException, IOException {
		/* 898 */ ServerResponse response = new ServerResponse();
		/* 899 */ JSONSerializer serializer = new JSONSerializer();
		/* 900 */ List<AgentReportDetail> reportList = this.smeDao.fetchAgentReportDetail(agentId);
		/* 901 */ Set<String> set = filterSessionId(reportList);
		/* 902 */ List<CallCdrResponseBean> missCallCdr = null;
		/* 903 */ missCallCdr = (set.size() == 0) ? new ArrayList<>() : this.smeDao.getNotAnsweredMissCall(set);
		/* 904 */ String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(missCallCdr);
		/* 905 */ response.setSuccess(true);
		/* 906 */ response.setResult(result);
		/* 907 */ return response;
	}

	@Override
	public ServerResponse savePrompt(PromptDetailsBean promptDetails) {
		ServerResponse resp = new ServerResponse();
		smeDao.saveSmePrompt(promptDetails);
		resp.setSuccess(true);
		resp.setResult("Success");
		return resp;
	}

	@Override
	public ServerResponse getSmePrompt(Long id, Long promptId, int pageNum, int pageSize) {
		ServerResponse resp = new ServerResponse();
		JSONSerializer serializer = new JSONSerializer();
		List<SmePrompts> promptsList = smeDao.getSmePrompt(id, promptId, pageNum, pageSize);
		resp.setSuccess(true);
		String result = serializer.exclude(new String[] { "*.class" }).deepSerialize(promptsList);
		resp.setResult(result);
		return resp;

	}
}
