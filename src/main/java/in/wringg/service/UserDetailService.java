package in.wringg.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDetailService {
  UserDetails loadUserByUsername(String paramString) throws UsernameNotFoundException;
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\UserDetailService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */