package in.wringg.service;

import in.wringg.dao.ComponentDao;
import in.wringg.pojo.CallCdrBean;
import in.wringg.pojo.ServerResponse;
import in.wringg.pojo.StartEndDateRequest;
import in.wringg.service.ComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("componentService")
@Transactional
public class ComponentServiceImpl
  implements ComponentService
{
  @Autowired
  ComponentDao componentDao;
  
  public ServerResponse addCallCdr(CallCdrBean bean) {
/* 21 */     ServerResponse response = new ServerResponse();
/* 22 */     this.componentDao.addCdrDetails(bean);
/* 23 */     response.setSuccess(true);
/* 24 */     response.setResult("Added");
/* 25 */     return response;
  }

  
  public ServerResponse changeSchduleStatus(Long id) {
/* 30 */     ServerResponse response = new ServerResponse();
/* 31 */     this.componentDao.changeSchuduleStatus(id);
/* 32 */     response.setSuccess(true);
/* 33 */     response.setResult("Updated");
/* 34 */     return response;
  }

  
  public ServerResponse chargingDetails(StartEndDateRequest request, Long id) {
/* 39 */     ServerResponse response = this.componentDao.chargingDetails(request, id);
/* 40 */     return response;
  }
}


/* Location:              D:\Work\wservices.war!\WEB-INF\classes\in\wringg\service\ComponentServiceImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */